##Dependências:

* [composer require riverskies/laravel-mobile-detect](https://github.com/riverskies/laravel-mobile-detect)
* [composer require barryvdh/laravel-dompdf](https://github.com/barryvdh/laravel-dompdf)
* [composer require simplesoftwareio/simple-qrcode"](https://www.simplesoftware.io/simple-qrcode/)
* [composer require maatwebsite/excel](https://github.com/Maatwebsite/Laravel-Excel)

##Publicação:

[Virtual host](https://websiteforstudents.com/install-laravel-on-ubuntu-17-04-17-10-with-apache2-mariadb-and-php-support/):

      DocumentRoot /var/www/html/public
      <Directory /var/www/html/public>
              Options +FollowSymlinks
              AllowOverride All
              Require all granted
      </Directory>


No .htacess 
    
    RewriteEngine on
    RewriteCond %{REQUEST_URI} !^public
    RewriteRule ^(.*)$ public/$1 [L]
     
## Criar link para documentos e imagens

    php artisan storage:link 
