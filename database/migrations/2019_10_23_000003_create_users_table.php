<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'users';

    /**
     * Run the migrations.
     * @table users
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('cadastro');
            $table->string('nome');
            $table->string('cpf');
            $table->integer('cr')->nullable();
            $table->string('telefone', 14);
            $table->string('telefone2', 14)->nullable();
            $table->string('telefone3', 14)->nullable();
            $table->string('email', 200)->nullable();
            $table->string('endereco')->nullable();
            $table->string('bairro', 100)->nullable();
            $table->string('cidade', 100);
            $table->string('cep', 10)->nullable();
            $table->dateTime('data_filiacao')->nullable();
            $table->dateTime('data_desligamento')->nullable();
            $table->string('tipo_desligamento', 45)->nullable()->comment('Por pedido ou de ofício. Obrigatório se data_desligamento for preenchido. ');
            $table->dateTime('data_primeira_habiltualidade')->nullable();
            $table->unsignedBigInteger('indicado_por_id')->nullable()->comment('Associado que indocou para afiliar-se ao clube. ');
            $table->tinyInteger('janta')->nullable()->default('0');
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->string('tipo', 100)->nullable()->default('associado')->comment('associado | presidente | tesoureiro | secretario\\n');

            $table->index(["indicado_por_id"], 'fk_users_info_1_idx');

            $table->unique(["cpf"], 'users_cpf_uindex');
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
