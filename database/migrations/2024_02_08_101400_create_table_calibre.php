<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCalibre extends Migration
{
    public $tableName = 'calibre';

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('nome')->nullable();
            $table->boolean('restrito')->default(false); 

            $table->nullableTimestamps();
        });
    }

     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
