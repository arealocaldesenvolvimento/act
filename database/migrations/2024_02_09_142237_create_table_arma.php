<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableArma extends Migration
{
    public $tableName = 'arma';

    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');

            $table->string('tipo_arma');
            $table->string('calibre');
            $table->boolean('restrito')->default(false);

            $table->string('marca');
            $table->string('modelo');
            $table->string('numero_serie');
            $table->string('numero_sigma');
            $table->string('numero_gte')->nullable();
            $table->dateTime('validade')->nullable();

            $table->nullableTimestamps();
        });
    }

     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
