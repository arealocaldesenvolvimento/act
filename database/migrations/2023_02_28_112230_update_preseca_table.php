<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePresecaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('presenca', function ($table) {
            $table->string('user_agent')->nullable();
            $table->string('plataforma')->nullable();
            $table->string('ip')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('presenca', function ($table) {
            $table->dropColumn('user_agent');
            $table->dropColumn('plataforma');
            $table->dropColumn('ip');
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
        });
    }
}
