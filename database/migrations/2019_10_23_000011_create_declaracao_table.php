<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeclaracaoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'declaracao';

    /**
     * Run the migrations.
     * @table declaracao
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->integer('numero_sequencial');
            $table->unsignedBigInteger('users_id');
            $table->string('tipo')->nullable();
            $table->dateTime('data_emissao')->nullable();
            $table->dateTime('data_validade')->nullable();
            $table->string('situacao')->nullable();
            $table->dateTime('data_liberacao')->nullable();

            $table->index(["users_id"], 'fk_declaracoes_1_idx');
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
