<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMandatoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'mandato';

    /**
     * Run the migrations.
     * @table mandato
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->dateTime('data_inicio');
            $table->dateTime('data_fim');
            $table->unsignedBigInteger('users_id');
            $table->unsignedBigInteger('documento_id')->nullable()->default(null);

            $table->index(["documento_id"], 'mandato_documento_id_fk');

            $table->index(["users_id"], 'fk_mandato_1_idx');
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
