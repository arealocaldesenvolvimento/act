<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresencaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'presenca';

    /**
     * Run the migrations.
     * @table presenca
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->integer('cr');
            $table->unsignedBigInteger('users_id');
            $table->dateTime('data');
            $table->string('arma')->nullable()->default(null);
            $table->integer('quantidade_disparos')->nullable()->default(null);
            $table->unsignedBigInteger('evento_id')->comment('presença/treino/prova\\n');
            $table->string('local');
            $table->string('pagina', 10);

            $table->index(["users_id"], 'fk_precensa_1_idx');

            $table->index(["evento_id"], 'fk_precensa_2_idx');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('users_id', 'fk_precensa_1_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('evento_id', 'fk_precensa_2_idx')
                ->references('id')->on('evento')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
