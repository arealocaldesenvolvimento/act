<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTablePresenca extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('presenca', function (Blueprint $table) {
            $table->string('numero_serie')->nullable()->after('arma');
            $table->string('numero_sigma')->nullable()->after('arma');
            $table->string('numero_gte')->nullable()->after('arma');
            $table->string('validade')->nullable()->after('arma');

            $table->renameColumn('arma', 'calibre');

            $table->boolean('calibre_restrito')->nullable()->after('arma');
            $table->string('modelo_arma')->nullable()->after('arma');
            $table->string('marca_arma')->nullable()->after('arma');
            $table->string('tipo_arma')->nullable()->after('arma');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('presenca', function (Blueprint $table) {
        $table->dropColumn('tipo_arma');
        $table->dropColumn('marca');
        $table->dropColumn('modelo');
        $table->dropColumn('calibre_restrito');
        $table->renameColumn('calibre', 'arma');
        $table->dropColumn('validade');
        $table->dropColumn('numero_gte');
        $table->dropColumn('numero_sigma');
        $table->dropColumn('numero_serie');
    });
    }
}
