<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableUser extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('estado')->nullable()->after('cidade');
          
            $table->string('nacionalidade')->default('Brasileiro')->after('cep')->nullable();
            $table->string('estado_nasceu')->nullable()->after('cep');
            $table->string('cidade_nasceu')->nullable()->after('cep');
        
            $table->dateTime('data_nascimento')->nullable()->after('email');
            
            $table->string('profissao')->nullable()->after('data_primeira_habitualidade');
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('estado');
            $table->dropColumn('nacionalidade');
            $table->dropColumn('cidade_nasceu');
            $table->dropColumn('estado_nasceu');
            $table->dropColumn('data_nascimento');
            $table->dropColumn('profissao');
        });
    }
}

