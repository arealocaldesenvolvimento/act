<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameRestritoToCalibreRestritoOnArmaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arma', function (Blueprint $table) {
            // Verifica se a coluna 'calibre_restrito' não existe E 'restrito' existe
            if (!Schema::hasColumn('arma', 'calibre_restrito') && Schema::hasColumn('arma', 'restrito')) {
                $table->renameColumn('restrito', 'calibre_restrito');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arma', function (Blueprint $table) {
            // Reverte a renomeação se necessário
            if (Schema::hasColumn('arma', 'calibre_restrito')) {
                $table->renameColumn('calibre_restrito', 'restrito');
            }
        });
    }
}
