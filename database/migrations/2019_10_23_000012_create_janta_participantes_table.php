<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJantaParticipantesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'janta_participantes';

    /**
     * Run the migrations.
     * @table desconto
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->unsignedBigInteger('janta_id');
            $table->unsignedBigInteger('users_id');
            $table->boolean('pago')->default(true);

            $table->index(["janta_id"], 'fk_jantaparticipantes_1_idx');
            $table->index(["users_id"], 'fk_jantaparticipantes_2_idx');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('janta_id', 'fk_jantaparticipantes_1_idx')
                ->references('id')->on('janta')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('users_id', 'fk_jantaparticipantes_2_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
