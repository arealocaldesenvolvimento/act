<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDescontoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'desconto';

    /**
     * Run the migrations.
     * @table desconto
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->double('valor');
            $table->dateTime('data_fim');
            $table->unsignedBigInteger('clube_id');

            $table->index(["clube_id"], 'fk_mensalidade_1_idx');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('clube_id', 'fk_mensalidade_1_idx')
                ->references('id')->on('clube')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
