<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\QrCodeController;
use App\Presenca;

Route::resource('/', 'BlogController');
Route::get('/contato', 'LandingController@contato')->name('contato');
Route::get('/sobre', 'LandingController@sobre')->name('sobre');
Route::get('links', 'LandingController@links')->name('links');

Route::get('/painel', 'PainelController@index')->middleware('auth')->name('painel-index');

Route::get('/atualizar-dados', 'UserController@atualizarDados')->name('atualizar-dados');



// LISTAR ASSOCIADOS
//--------------------------------------------------------------------------------------------------------//

    Route::get('painel/associados/listar', 'ListaAssociadoController@index')->name('associados');

// USUÁRIOS
//--------------------------------------------------------------------------------------------------------//

    // PÁGINAS
    Route::get('/painel/usuarios/listagem', 'UserController@listagem')->name('painel-usuarios-listagem');
    Route::get('/painel/usuarios/cadastrar', 'UserController@cadastro')->name('painel-usuarios-cadastro');
    Route::get('/painel/usuarios/lixeira', 'UserController@lixeira')->name('painel-usuarios-lixeira');
    Route::get('/painel/usuarios/editar/{id}', 'UserController@editar')->name('painel-usuarios-editar');

    // REQUESTS
    Route::post('/painel/usuarios/cadastrar', 'UserController@createUser')->name('painel-usuarios-criar');
    Route::post('/painel/usuarios/update', 'UserController@updateUser')->name('painel-usuarios-update');
    Route::post('/painel/usuarios/newpassword/', 'UserController@newPassword')->name('painel-usuarios-newpassword');
    Route::post('/painel/usuarios/usuario/block', 'UserController@block')->name('painel-usuarios-block');
    Route::post('/painel/usuarios/usuario/unblock', 'UserController@unblock')->name('painel-usuarios-unblock');
    Route::post('/painel/usuarios/usuario/destroy', 'UserController@destroy')->name('painel-usuarios-destroy');
    Route::get('/painel/usuarios/usuario/exportacao-completa', 'UserController@exportacaoCompleta')->name('painel-usuarios-exportacao-completa');


// USUÁRIO (SELF EDIT)
//--------------------------------------------------------------------------------------------------------//

    // PÁGINAS
    Route::get('/painel/meus-dados', 'UserController@meusDados')->name('painel-usuario-meus-dados');

    // REQUESTS
    Route::post('/painel/meus-dados', 'UserController@selfUpdate')->name('painel-usuario-meus-dados');


//CERTIFICADO
    Route::get('/painel/certificado', 'CertificadoController@index')->name('painel-certificado');
    Route::post('/painel/certificado', 'CertificadoController@store')->name('painel-certificado-store');




// CLUBE
//--------------------------------------------------------------------------------------------------------//

    // PÁGINAS
    Route::get('/painel/sobre-clube', 'ClubeController@index')->name('painel-sobre-clube');

    // REQUESTS
    Route::post('/painel/sobre-clube', 'ClubeController@store')->name('painel-sobre-clube');
    Route::post('/painel/sobre-clube/presidente', 'ClubeController@storePresidente')->name('painel-sobre-clube-presidente');
    Route::post('/painel/sobre-clube/secretario', 'ClubeController@storeSecretario')->name('painel-sobre-clube-secretario');
    Route::post('/painel/sobre-clube/tesoureiro', 'ClubeController@storeTesoureiro')->name('painel-sobre-clube-tesoureiro');

// DESCONTO
//--------------------------------------------------------------------------------------------------------//

    // PÁGINAS
    Route::get('/painel/mensalidade/listagem', 'DescontoController@listagem')->name('painel-desconto-listagem');

    // REQUESTS
    Route::post('/painel/mensalidade/listagem', 'DescontoController@store')->name('painel-desconto-listagem');
    Route::post('/painel/mensalidade/destroy', 'DescontoController@destroy')->name('painel-desconto-destroy');

// ANUIDADE
//--------------------------------------------------------------------------------------------------------//

    // PÁGINAS
    Route::get('/painel/anuidade/listagem', 'AnuidadeController@listagem')->name('painel-anuidade-listagem');

    // REQUESTS
    Route::post('/painel/anuidade/listagem', 'AnuidadeController@store')->name('painel-anuidade-listagem');
    Route::post('/painel/anuidade/destroy', 'AnuidadeController@destroy')->name('painel-anuidade-destroy');


// EVENTO
//--------------------------------------------------------------------------------------------------------//

    // PÁGINAS
    Route::get('/painel/evento/listagem', 'EventoController@listagem')->name('painel-evento-listagem');
    Route::get('/painel/evento/editar/{id}', 'EventoController@editar')->name('painel-evento-editar');

    // REQUESTS
    Route::post('/painel/evento/listagem', 'EventoController@store')->name('painel-evento-listagem');
    Route::post('/painel/evento/destroy', 'EventoController@destroy')->name('painel-evento-destroy');
    Route::post('/painel/evento/update', 'EventoController@update')->name('painel-evento-update');

// HABITUALIDADE
//--------------------------------------------------------------------------------------------------------//

    // PÁGINAS
    Route::get('/painel/habitualidade/listagem', 'PresencaController@listagem')->name('painel-habitualidade-listagem');
    Route::get('/painel/habitualidade/minhas', 'PresencaController@listagemAssociado')->name('painel-habitualidade-listagem-associado');
    Route::get('/painel/habitualidade/cadastrar', 'PresencaController@cadastro')->name('painel-habitualidade-cadastro');
    Route::get('/painel/habitualidade/editar/{id}', 'PresencaController@editar')->name('painel-habitualidade-editar');
    Route::get('/painel/presenca/dados/{id}', 'PresencaController@getDetalhes')->name('get-presenca-detalhes');

    Route::get('/painel/habitualidade/qr_code/{qrcode}', 'PresencaController@cadastroQRCode')->name('painel-habitualidade-qr-code');
    Route::get('/painel/habitualidade/imprimir_qr_code', 'PresencaController@imprimirQRCode')->name('painel-habitualidade-imprimir-qr-code');
    Route::get('/painel/habitualidade/gerar_qr_code', 'PresencaController@gerarQRCode')->name('painel-habitualidade-gerar-qr-code');


    // REQUESTS
    Route::post('/painel/habitualidade/cadastrar', 'PresencaController@store')->name('painel-habitualidade-cadastro');
    Route::post('/painel/habitualidade/destroy', 'PresencaController@destroy')->name('painel-habitualidade-destroy');
    Route::post('/painel/habitualidade/editar/{id}', 'PresencaController@update')->name('painel-habitualidade-update');
    Route::post('/painel/habitualidade/cadastrar_qr_code', 'PresencaController@storeQRCode')->name('painel-habitualidade-cadastro_qr_code');


    // QR CODE
    Route::get('/qrcode', [QrCodeController::class, 'index']);



// ARMAS
//--------------------------------------------------------------------------------------------------------//

    Route::get('/painel/armas', 'ArmaController@index')->name('painel-armas');


    //Armas
    Route::get('/painel/arma/listagem', 'ArmaController@listagemArma')->name('painel-arma-listagem');
    Route::get('/painel/arma/cadastro', 'ArmaController@cadastroArmaAdmin')->name('painel-arma-cadastro');
    Route::get('/painel/arma/editar/{id}', 'ArmaController@editarArma')->name('painel-arma-editar');

    Route::post('/painel/arma/cadastro/admin',    'ArmaController@storeArmaAdmin')->name('painel-arma-store-admin');
    Route::post('/painel/arma/editar/{id}', 'ArmaController@updateArma')->name('painel-arma-update');
    Route::post('/painel/arma/destroy',     'ArmaController@destroyArma')->name('painel-arma-destroy');

    Route::get('/painel/usuario/{userId}/armas', 'ArmaController@armaByUser')->name('painel-arma-by-user');

    // CALIBRES
    Route::get('/painel/calibre/listagem', 'ArmaController@listagemCalibre')->name('painel-calibre-listagem');

    Route::post('/painel/calibre/cadastro', 'ArmaController@storeCalibre')->name('painel-calibre-cadastro');
    Route::post('/painel/calibre/destroy', 'ArmaController@destroyCalibre')->name('painel-calibre-destroy');


    // TIPOS DE ARMAS
    Route::get('/painel/tipo-arma/listagem', 'ArmaController@listagemTipoArma')->name('painel-tipo-arma-listagem');

    Route::post('/painel/tipo-arma/cadastro', 'ArmaController@storeTipoArma')->name('painel-tipo-arma-cadastro');
    Route::post('/painel/tipo-arma/destroy', 'ArmaController@destroyTipoArma')->name('painel-tipo-arma-destroy');


    //Armas associado:
    Route::get('/painel/arma/minhas-armas', 'ArmaController@minhasArmas')->name('painel-minhas-armas');
    Route::get('/painel/arma/cadastrar-minha-arma', 'ArmaController@cadastroArmaAssoc')->name('painel-minha-arma-cadastro');
    Route::post('/painel/arma/cadastro/assoc',    'ArmaController@storeArmaAssoc')->name('painel-arma-store-assoc');




// CONTA
//--------------------------------------------------------------------------------------------------------//

    // PÁGINAS
    Route::get('/painel/conta/listagem', 'ContaController@listagem')->name('painel-conta-listagem');
    Route::get('/painel/conta/historico/{id}', 'ContaController@historico')->name('painel-conta-historico');
    Route::get('/painel/conta/debitar-creditar/', 'ContaController@debitarCreditar')->name('painel-conta-debitar-creditar');
    Route::get('/painel/conta/anuidade/', 'ContaController@anuidade')->name('painel-conta-anuidade');
    Route::get('/painel/conta/editar/{id}', 'ContaController@editar')->name('painel-conta-editar');

    // REQUESTS
    Route::post('/painel/conta/debitar-creditar/', 'ContaController@debitarCreditarStore')->name('painel-conta-debitar-creditar');
    Route::get('/painel/conta/anuidade/gerar', 'ContaController@anuidadeGerar')->name('painel-conta-anuidade-gerar');
    Route::post('/painel/conta/editar/{id}', 'ContaController@update')->name('painel-conta-update');
    Route::post('/painel/conta/editar/', 'ContaController@destroy')->name('painel-conta-destroy');
    Route::get('/painel/conta/exportacao-completa', 'ContaController@exportacaoCompleta')->name('painel-conta-exportacao-completa');


// JANTA
//--------------------------------------------------------------------------------------------------------//

    // PÁGINAS
    Route::get('/painel/janta/listagem', 'JantaController@listagem')->name('painel-janta-listagem');
    Route::get('/painel/janta/novo-ano', 'JantaController@novoAno')->name('painel-janta-novoano');
    Route::get('/painel/janta', 'JantaController@listagemAssociado')->name('painel-janta-associado');

    //REQUESTS
    Route::post('/painel/janta/listagem', 'JantaController@store')->name('painel-janta-listagem');
    Route::post('/painel/janta/destroy', 'JantaController@destroy')->name('painel-janta-destroy');
    Route::get('/painel/janta/gera-novo-ano/{data}', 'JantaController@gerarNovoAno')->name('painel-janta-gerar-ano');
    Route::get('/painel/janta/download-lista-completa', 'JantaController@downloadLista')->name('painel-janta-associado-download');
    Route::get('/painel/janta/exportacao-completa', 'JantaController@exportacaoCompleta')->name('painel-janta-exportacao-completa');

// JANTA PARTICIPANTES
//--------------------------------------------------------------------------------------------------------//

    //PÁGINAS
    Route::get('/painel/janta/{id}/participantes', 'JantaParticipantesController@participantes')->name('painel-janta-participantes');

    //REQUESTS
    Route::post('/painel/janta/{id}/participantes', 'JantaParticipantesController@store')->name('painel-janta-participantes');
    Route::post('/painel/janta/{id}/participantes/destroy', 'JantaParticipantesController@destroy')->name('painel-janta-participante-destroy');
    Route::post('/painel/janta/{id}/participantes/status-pagou', 'JantaParticipantesController@alteraPagou')->name('painel-janta-participante-pagou');

// JANTA PARTICIPANTES
//--------------------------------------------------------------------------------------------------------//

    //PÁGINAS
    Route::get('/painel/downloads/', 'DownloadController@listagem')->name('painel-download-listagem');
    Route::get('/painel/downloads/arquivos', 'DownloadController@listagemCli')->name('painel-download-listagem-cli');

    //REQUESTS
    Route::post('/painel/downloads/', 'DownloadController@store')->name('painel-download-listagem');
    Route::post('/painel/downloads/destroy', 'DownloadController@destroy')->name('painel-download-destroy');
    Route::get('/painel/downloads/{id}', 'DownloadController@download')->name('painel-download-download');

// DECLARACOES
//--------------------------------------------------------------------------------------------------------//

    //PÁGINAS
    Route::get('/painel/declaracoes', 'DeclaracoesController@listagemCli')->name('painel-declaracoes-listagem-cli');
    Route::get('/validacao-declaracao', 'DeclaracoesController@validacao')->name('painel-declaracoes-validacao');
    Route::get('/validacao-declaracao/{codigo}', 'DeclaracoesController@validacao')->name('painel-declaracoes-validacao-codigo');
    Route::get('/validacaodeclaracao', 'DeclaracoesController@validacao')->name('painel-declaracoes-validacao-old');
    Route::get('/validacaodeclaracao/{codigo}', 'DeclaracoesController@validacao')->name('painel-declaracoes-validacao-old-codigo');
    Route::get('/validacao-declaracao/visualizar-documento/{codigo}', 'DeclaracoesController@viewDocumento')->name('painel-ver-documento');


    //REQUESTS
    Route::get('/painel/declaracoes/filiacao', 'DeclaracoesController@declaracaoFiliacao')->name('painel-declaracoes-filiacao');
    Route::get('/painel/declaracoes/guiatrafego', 'DeclaracoesController@declaracaoGuiaTrafego')->name('painel-declaracoes-guiatrafego');
    Route::get('/painel/declaracoes/ranking', 'DeclaracoesController@declaracaoRanking')->name('painel-declaracoes-ranking');
    Route::get('/painel/declaracoes/habitualidade', 'DeclaracoesController@declaracaoHabitualidade')->name('painel-declaracoes-habitualidade');
    Route::get('/painel/declaracoes/habitualidade-portaria150', 'DeclaracoesController@declaracaoHabitualidadePortaria150')->name('painel-declaracoes-habitualidade-portaria150');
    Route::post('/painel/declaracoes/seguranca-acervo', 'DeclaracoesController@declaracaoSegurancaAcervo');
    Route::get('/painel/declaracoes/compromisso', 'DeclaracoesController@declaracaoCompromisso')->name('painel-compromisso');

    Route::get('/painel/declaracoes/habitualidade-portaria166', 'DeclaracoesController@declaracaoHabitualidadePortaria166')->name('painel-declaracoes-habitualidade-portaria166');
    Route::get('/painel/gerar/habitualidade166', 'DeclaracoesController@gerarDeclaracao166');

    //post





    Route::post('/painel/declaracoes/modalidadeProva', 'DeclaracoesController@declaracaoModalidadeProva')->name('painel-declaracoes-modalidade-prova');

// CARTEIRINHA
//--------------------------------------------------------------------------------------------------------//

    //PÁGINAS
    Route::get('/painel/carteirinha', 'CarteirinhaController@index')->name('painel-declaracoes-carteirinha');

    //REQUESTS
    Route::post('/painel/carteirinha', 'CarteirinhaController@geraCarteirinha')->name('painel-declaracoes-carteirinha');

// BLOG
//--------------------------------------------------------------------------------------------------------//

    Route::resource('noticias', 'NoticiasController');
    Route::resource('categorias','CategoriasController');
    Route::resource('blog', 'BlogController');
    Route::resource('blog.category', 'BlogCategoryController');


// LINKS
//--------------------------------------------------------------------------------------------------------//

    Route::resource('link', 'LinkController');

// API
//--------------------------------------------------------------------------------------------------------//

    // ENVIA EMAIL DE CONTATO
    Route::post('/api/send-email/contato', 'ApiController@emailContato')->name('email-contato');
    // RETORNA O USUÁRIO DO CR INFORMADO
    Route::get('/api/usuario/crtouser', 'ApiController@crToUser')->name('cr-to-user');
    // RETORNA O CR DO USUÁRIO INFORMADO
    Route::get('/api/usuario/usertocr', 'ApiController@userToCr')->name('user-to-user');
    // GERA DATAS DA JANTA
    Route::get('/api/janta/getdatas/{data}', 'ApiController@getDatas')->name('janta-get-datas');
    // ATUALIZA PRIMEIRO LOGIN
    Route::get('/api/firstlogin', 'ApiController@firstLogin')->name('api-firstlogin');
    // HABITUALIDADES POR ANO
    Route::get('/api/getPresencasAno', 'ApiController@presencasAnoAtual')->name('api-presenca-ano');
    // DECLARAÇÕES POR ANO
    Route::get('/api/getDeclaracoesAno', 'ApiController@declaracoesAnoAtual')->name('api-declaracoes-ano');
    // ENVIA EMAIL VENCIMENTO CR
    Route::get('/api/vencimentoCR', 'ApiController@emailVencimento')->name('api-vencimento-cr');

    // DEVELOPER ONLY
//--------------------------------------------------------------------------------------------------------//

    if(env('DEVELOPER', false)){
        // TROCA TIPO DA CONTA DO USUÁRIO DO DESENVOLVEDOR COM ID FIXO 1.
        Route::get('/trocatipoconta', function (){
            $user = \App\User::find(1);
            $user->tipo = $user->tipo == "associado" ? "presidente" : "associado";
            return json_encode($user->save());
        });

        //IMPORTAÇÃO DE CONTAS
        Route::get('/importacao/contas', function (){
            return view('outros.importadorContas');
        })->middleware('auth');
        Route::post('/importacao/contas', 'ApiController@importacaoContas')->middleware('auth');

        // IMPORTAçÂO DE HABITUALIDADES
        Route::get('/importacao/habitualidades', function (){
            return view('outros.importadorHabitualidades');
        })->middleware('auth');
        Route::post('/importacao/habitualidades', 'ApiController@importacaoHabitualidades')->middleware('auth');

        // IMPORTAÇÃO DE JANTAS
        Route::get('/importacao/janta', function (){
            return view('outros.importadorJantas');
        });
        Route::post('/importacao/janta', 'ApiController@importacaoJantas')->middleware('auth');
    }

Route::post('/api/log-error', 'PresencaController@storeLog')->name('store-log');

Auth::routes();
