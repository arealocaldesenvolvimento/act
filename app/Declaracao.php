<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Declaracao extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'declaracao';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * Atributos da classe
     *
     * @var array
     */
    protected $fillable = ['id', 'users_id', 'tipo', 'data_emissao', 'data_validade', 'situacao', 'nome_arquivo', 'data_liberacao'];
}
