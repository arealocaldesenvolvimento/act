<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'data_nascimento',
        'password',
        'cadastro',
        'nome',
        'cpf',
        'cr',
        'telefone',
        'telefone2',
        'telefone3',
        'endereco',
        'bairro',
        'cidade',
        'estado',
        'cep',
        'cidade_nasceu',
        'estado_nasceu',
        'nacionalidade',
        'data_filiacao',
        'data_desligamento',
        'tipo_desligamento',
        'data_primeira_habitualidade',
        'profissao',
        'estado_civil',
        'indicado_por_id',
        'janta',
        'first_login',
        'validade_cr'
    ];


    /*
      NOVOS CAMPOS PORTARIA:

        apenas exibir pra att  = 1. nome completo, sem abreviaturas, conforme certidão de nascimento/casamento
        novo (onde nasceu)     = 2. nacionalidade
        novo (onde nasceu)     = 3. cidade/UF
        novo                   = 4. dia/mês/ano
        novo                   = 5. profissão
        exibir pra att         = 6. endereço com cidade e UF
        novo                   = 7. colecionador, atirador desportivo e/ou caçador excepcional


        nasceu = cidade/UF
        mora   = cidade/UF


        pra atualizar os dados, vai atualizar os dois,
         com campos vazios e dividido ao meio:

         apenas cidade e UF

        NASCEU   |   Reside atualmente

        */


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
