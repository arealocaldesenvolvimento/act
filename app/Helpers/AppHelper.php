<?php

namespace App\Helpers;

use App\Presenca;
use DateTime;
use Illuminate\Http\RedirectResponse as RedirectResponseAlias;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use function dump;
use function redirect;
use function strlen;

/**
 * Class AppHelper
 * @package App\Helpers
 */
class AppHelper
{

    private const LOCAIS =
    array(
        ['ACTRS'                    => 'Associação Clube de Tiro Rio do Sul'],
        ['ACPTCAVI'                 => 'ACPTCAVI'],
        ['AMOCTECP'                 => 'AMOCTECP'],
        ['CCTTT'                    => 'CCTTT'],
        ['CCTFD'                    => 'CCTFD'],
        ['CJCT'                     => 'CJCT'],
        ['CCTDV'                    => 'CCTDV'],
        ['CBCTE'                    => 'CBCTE'],
        ['CCPAS'                    => 'CCPAS'],
        ['CCTP'                     => 'CCTP'],
        ['ATPC'                     => 'ATPC'],
        ['CEPCT'                    => 'CEPCT'],
        ['CMTASM'                   => 'CMTASM']
    );

    /**
     * @return bool
     */
    public static function isAdmin(){
        return Auth::user()->tipo == 'secretario' || Auth::user()->tipo == 'presidente';
    }

    /**
     * Retorna a quantidade de provas participadas nos ultimos 12 meses.
     * @param $id
     * @return int
     */
    public static function habitualidadeProvasDozeMeses($id){
       return DB::table('presenca')
            ->join('evento', 'evento.id', '=', 'presenca.evento_id')
            ->whereRaw("presenca.users_id = ? and presenca.data > date_sub(now(), interval 12 month) and presenca.deleted_at is null", $id)
            ->count();
    }

    /**
     * @param $id
     * @return \Illuminate\Support\Collection
     */
    public static function listaHabitualidadeComDisparosDozeMeses($id){
        return DB::table('presenca')
            ->select('presenca.data')
            ->whereRaw("presenca.users_id = ? and presenca.data > date_sub(now(), interval 12 month) and presenca.quantidade_disparos > 0", $id)
            ->groupBy(['presenca.data'])
            ->get();
    }

    /**
     * @param $id
     * @return \Illuminate\Support\Collection
     */
    public static function listaHabitualidadeComDisparosDozeMesesLocal($id){
        return DB::table('presenca')
            ->selectRaw("DISTINCT(presenca.data), presenca.local, evento.titulo")
            ->leftJoin('evento', 'evento.id', '=', 'presenca.evento_id')
            ->whereRaw("presenca.users_id = ? and presenca.data > date_sub(now(), interval 12 month) and presenca.quantidade_disparos > 0", $id)
            ->groupBy('presenca.data')
            ->get();
    }

    public static function replaceNameLocal($presencas)
    {
        foreach ($presencas as &$presenca) {
            foreach (self::LOCAIS[0] as $key => $value) {
                if ($presenca->local == $key) {
                    $presenca->local = $value;
                }
            }
        }
        unset($presenca);
        return $presencas;
    }

    /**
     * Mascara o CPF
     * @param $val
     * @param $mask
     * @return string
     */
    public static function mask($val, $mask){
        $maskared = '';
        $k = 0;
        for($i = 0; $i<=strlen($mask)-1; $i++)
        {
            if($mask[$i] == '#')
            {
                if(isset($val[$k]))
                    $maskared .= $val[$k++];
            }
            else
            {
                if(isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|object|null
     */
    public static function getPresidente(){
        return DB::table('users')
            ->select('users.nome', 'documento.diretorio as assinatura')
            ->leftJoin('mandato', 'mandato.users_id', '=', 'users.id')
            ->leftJoin('documento', 'mandato.documento_id', '=', 'documento_id')
            ->where([
                ['users.tipo', '=', 'presidente'],
                ['users.id', '=', '273'],
                ['documento.categoria', '=', 'assinatura digital'],
            ])
            ->whereNull('users.deleted_at')
            ->orderByDesc('documento.id')
            ->first();
    }

    /**
     * @param $user_id
     * @return int|mixed
     */
    public static function getSaldo($user_id){
        return DB::table('conta')
            ->selectRaw('sum(credito) - sum(debito) as saldo')
            ->whereNull('deleted_at')
            ->where('users_id', '=', $user_id)
            ->first()->saldo ?? 0;
    }

}
