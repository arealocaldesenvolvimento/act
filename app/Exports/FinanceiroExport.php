<?php
namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use App\Conta;

class FinanceiroExport implements FromQuery, WithColumnFormatting
{
    use Exportable;
    public function query(){
        return Conta::select(
                'users.nome as Nome',
                DB::raw("date_format(conta.data, '%d/%m/%Y') as Data"),
                'conta.debito as Débito',
                'conta.credito as Crédito',
                'conta.descricao as Descrição',
                'parent.nome as Gerador'
            )
            ->leftJoin('users', 'users.id', '=', 'conta.users_id')
            ->leftJoin('users as parent', 'parent.id', '=', 'conta.gerador_id')
            ->whereNull('conta.deleted_at')
            ->limit(100)
            ->orderBy('users.nome');
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'C' => NumberFormat::FORMAT_NUMBER_00,
            'D' => NumberFormat::FORMAT_NUMBER_00,
            'E' => NumberFormat::FORMAT_TEXT,
            'F' => NumberFormat::FORMAT_TEXT,
        ];
    }
}

