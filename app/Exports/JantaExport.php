<?php
namespace App\Exports;

use App\Invoice;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class JantaExport implements FromCollection, WithColumnFormatting{
    public function collection(){
        return collect(DB::select("select
            users.id,
            users.nome as Nome,
            date_format(janta.data, '%d/%m/%Y') as Filiação,
            concat_ws(', ', users.telefone, users.telefone2, users.telefone3) as Fone,
            if(now() >= janta.data, 'X', '') as Feita
            from janta_participantes
            join janta on (janta.id = janta_participantes.janta_id)
            join users on (users.id = janta_participantes.users_id)
            where users.deleted_at is null and janta.deleted_at is null and janta_participantes.deleted_at is null
            and year(janta.data) = year(now())
            order by janta.data"));
    }
    

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_NUMBER,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }
}
