<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Janta extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'janta';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * Atributos da classe
     *
     * @var array
     */
    protected $fillable = ['id', 'data', 'observacao'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function participantes(){
        return $this->hasMany(JantaParticipantes::class, 'janta_id', 'id');
    }
}
