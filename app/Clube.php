<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clube extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clube';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * Atributos da classe
     *
     * @var array
     */
    protected $fillable =
    [
        'id',
        'nome',
        'cnpj',
        'cr',
        'validade_cr',
        'endereco',
        'bairro',
        'cidade',
        'cep',
        'banco',
        'agencia',
        'conta_corrente',
        'pix1',
        'pix2',
        'pix3',
        'qr_code'
    ];
}
