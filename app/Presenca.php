<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Presenca extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'presenca';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * Atributos da classe
     *
     * @var array
     */
    protected $fillable =
    [
        'id',
        'cr',
        'users_id',
        'data',

        'calibre',
        'tipo_arma',
        'marca_arma',
        'modelo_arma',
        'calibre_restrito', /*bool */
        'validade', /* datetime */
        'numero_gte',
        'numero_sigma',
        'numero_serie',

        'quantidade_disparos',
        'evento_id',
        'local',
        'pagina',
        'user_agent',
        'plataforma',
        'ip',
        'latitude',
        'longitude'
    ];
}
