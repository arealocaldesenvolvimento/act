<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arma extends Model
{

    protected $table = 'arma';

    protected $primaryKey = 'id';

    protected $fillable =
    [
        'id',
        'users_id', // user id do associado ou 'clube'
        'tipo_arma',
        'calibre',
        'calibre_restrito',
        'marca',
        'modelo',
        'numero_serie',
        'numero_sigma',
        'numero_gte',
        'validade',
    ];
}
