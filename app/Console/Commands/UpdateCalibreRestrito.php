<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Arma;
use App\Calibre;

class UpdateCalibreRestrito extends Command
{
    protected $signature = 'update:calibreRestrito';
    protected $description = 'Atualiza o campo calibre_restrito nas armas com base nos calibres';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // Busca todas as armas
        $armas = Arma::all();

        foreach ($armas as $arma) {
            // Encontra o calibre correspondente pelo nome
            $calibre = Calibre::where('nome', $arma->calibre)->first();

            if ($calibre) {
                // Atualiza o campo calibre_restrito com o valor do campo restrito do calibre
                $arma->calibre_restrito = $calibre->restrito;
                $arma->save();

                $this->info("Arma ID {$arma->id} atualizada.");
            }
        }

        $this->info('Atualização completa.');
    }
}
