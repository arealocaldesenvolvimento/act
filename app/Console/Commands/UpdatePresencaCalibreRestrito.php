<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Presenca;
use App\Arma;

class UpdatePresencaCalibreRestrito extends Command
{
    protected $signature = 'update:presencaCalibreRestrito';
    protected $description = 'Atualiza o campo calibre_restrito na tabela presenca com base na tabela arma';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // Busca todas as presenças com modelo_arma não nulo
        $presencas = Presenca::whereNotNull('modelo_arma')->get();

        foreach ($presencas as $presenca) {
            // Encontra a arma correspondente pelo modelo
            $arma = Arma::where('modelo', $presenca->modelo_arma)->first();

            if ($arma) {
                // Atualiza o campo calibre_restrito com o valor do campo calibre_restrito da arma
                $presenca->calibre_restrito = $arma->calibre_restrito;
                $presenca->save();

                $this->info("Presença ID {$presenca->id} atualizada.");
            }
        }

        $this->info('Atualização completa.');
    }
}
