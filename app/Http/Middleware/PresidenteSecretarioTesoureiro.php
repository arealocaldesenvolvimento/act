<?php

namespace App\Http\Middleware;

use Closure;
use function redirect;

class PresidenteSecretarioTesoureiro
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->tipo == 'presidente' or $request->user()->tipo == 'secretario' or $request->user()->tipo == 'tesoureiro')
            return $next($request);

        return redirect()->route('painel-index')->with(['type' => 'alert-danger', 'message' => 'Você não tem permissão para acessar essa página!']);
    }
}
