<?php

namespace App\Http\Controllers;

use App\Exports\JantaExport;
use App\Janta;
use App\JantaParticipantes;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;
use stdClass;
use function array_push;
use function date;
use function dump;
use function end;
use function json_encode;
use function redirect;
use function view;
use PDF;

class JantaController extends Controller
{
    /**
     * JantaController constructor.
     */
    public function __construct()
    {

        $this->middleware('auth');
//        $this->middleware('presidente-secretario')->except(['listagemAssociado');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listagem(Request $request){
        return view('painel.janta.listagem', ['anoSelecionado' => $request->anoSelecionado ?? null]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listagemAssociado(){
        if(Auth::user()->janta){
            $data = DB::table('janta_participantes')
                ->select('janta.data')
                ->join('janta', 'janta.id', '=', 'janta_participantes.janta_id')
                ->whereRaw('janta.id = (select janta_id from janta_participantes where users_id = ? and janta_participantes.deleted_at is null) and janta_participantes.deleted_at is null and janta.deleted_at is null', Auth::id())
                ->first();

            $participantes = DB::table('janta_participantes')
                ->select('users.nome', 'users.telefone', 'users.telefone2', 'users.telefone3')
                ->join('janta', 'janta.id', '=', 'janta_participantes.janta_id')
                ->join('users', 'users.id', '=', 'janta_participantes.users_id')
                ->whereRaw('janta.id = (select janta_id from janta_participantes where users_id = ? and janta_participantes.deleted_at is null) and janta_participantes.deleted_at is null and janta.deleted_at is null and users.deleted_at is null', Auth::id())
                ->get();
            return view('painel.janta.jantaassociado', ['participantes' => $participantes, 'data' => is_null($data) ? '' : $data->data]);
        }

        return view('painel.janta.jantaassociado');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
        $request->validate([
            'data' => 'required|date',
            'observacao' => 'nullable|max:255'
        ]);

        $janta = new Janta();
        $janta->data = $request->data;
        $janta->observacao = $request->observacao;

        if($janta->save())
            return redirect()->route('painel-janta-listagem')->with(['type' => 'alert-success', 'message' => 'Desconto criado com sucesso!']);

        return redirect()->route('painel-janta-listagem')->with(['type' => 'alert-danger', 'message' => 'Falha ao criar desconto! tente novamente mais tarde.']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request){
        $request->validate([
            'id' => 'required'
        ]);

        $janta = Janta::find($request->id);
        if($janta->delete())
            return redirect()->route('painel-janta-listagem')->with(['type' => 'alert-success', 'message' => 'Desconto removido com sucesso!']);

        return redirect()->route('painel-janta-listagem')->with(['type' => 'alert-danger', 'message' => 'Falha ao remover desconto! tente novamente mais tarde.']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function novoAno(){
        return view('painel.janta.gerarnovoano');
    }


    public function gerarNovoAno($data){
        $datas = array(new DateTime($data));
        $jantasAntigas = Janta::whereRaw('year(data) = ?', date('Y'))->orderBy('data')->get();
        $novasJantas = array();

        for($i = 1; $i <= count($jantasAntigas); $i++){
            if($i != 1){
                $lastdata = end($datas);
                $lastdata = new DateTime($lastdata->format('Y-m-d'));
                array_push($datas, $lastdata->modify('+7 day'));
            }
        }

        foreach($jantasAntigas as $key => $jantaAntiga){
            $janta = new Janta();
            $janta->data = $datas[$key];
            $janta->observacao = $jantaAntiga->observacao;

            $novaJanta = new StdClass();
            $novaJanta->data = $janta->data;
            $novaJanta->status = $janta->save();
            $novaJanta->quant_part = 0;

            $partipantesAntigos = JantaParticipantes::all()->where('janta_id', '=', $jantaAntiga->id);
            foreach ($partipantesAntigos as $participanteAntigo){
                $novoParticipante = new JantaParticipantes();
                $novoParticipante->janta_id = $janta->id;
                $novoParticipante->users_id = $participanteAntigo->users_id;
                $novoParticipante->save();
                $novaJanta->quant_part ++;
            }

            array_push($novasJantas, $novaJanta);
        }

        foreach ($jantasAntigas as $janta){
            foreach ($janta->participantes as $participante){
                $participante->delete();
            }
            $janta->delete();
        }

        return json_encode($novasJantas);
    }

    /**
     * @return mixed
     */
    public function downloadLista(){
        $dados = array();
        $dados['lista'] = DB::table('janta_participantes')
            ->select('users.nome', 'janta.data', 'users.telefone', 'users.telefone2', 'users.telefone3')
            ->join('janta', 'janta.id', '=', 'janta_participantes.janta_id')
            ->join('users', 'users.id', '=', 'janta_participantes.users_id')
            ->whereNull(['users.deleted_at', 'janta.deleted_at', 'janta_participantes.deleted_at'])
            ->get();
        $pdf = PDF::loadView('painel.janta.pdfs.janta', $dados);
        return $pdf->download('JANTA.pdf');
    }


    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
     public function exportacaoCompleta(){
         return Excel::download(new JantaExport, 'janta.xlsx');
     }
}
