<?php

namespace App\Http\Controllers;

use App\Anuidade;
use App\Clube;
use App\Conta;
use App\Exports\FinanceiroExport;
use App\Exports\JantaExport;
use App\Helpers\AppHelper;
use App\User;
use Box\Spout\Writer\Style\StyleBuilder;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;
use stdClass;
use function array_push;
use function date;
use function dump;
use function json_encode;
use function redirect;
use function view;

class ContaController extends Controller
{
    /**
     * ContaController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('presidente-secretario-tesoureiro')->except('historico');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listagem(){

        $contas = DB::table('users')
            ->selectRaw('
                if(users.deleted_at is not null, false, true) as status,
                users.id,
                users.nome,
                users.cpf,
                users.cr,
                (select sum(c.credito) - sum(c.debito) from conta c where c.users_id = users.id and c.deleted_at is null) as saldo,
                (select data from conta c where c.users_id = users.id and c.deleted_at is null order by created_at desc limit 1) as ultima_data')
//            ->whereNull('deleted_at')
            ->groupBy('status','users.id', 'users.nome', 'users.cpf', 'users.cr')
            ->orderBy('users.nome')
            ->get();

        return view('painel.conta.listagem', ['contas' => $contas]);
    }

    /**
     * @param $id
     * @param null $ano
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function historico($id, Request $request){
        if($request->ano){
            $query = DB::table('conta')
                ->selectRaw("id as idAtual, credito, debito, data as dataAtual, gerador_id, descricao,
                        (select (sum(credito) - sum(debito))
                            from conta b
                            where b.users_id = ".$id."
                            and (b.data <= dataAtual)
                            and b.deleted_at is null
                            order by b.data
                        ) as saldo")
                ->where('users_id', '=', $id)
                ->WhereNull('deleted_at')
                ->whereRaw('year(data) = '. $request->ano)
                ->orderByRaw('data')
                ->get();
        }else{
            $query = DB::table('conta')
                ->selectRaw("id as idAtual, credito, debito, data as dataAtual, gerador_id, descricao,
                        (select (sum(credito) - sum(debito))
                            from conta b
                            where b.users_id = ".$id."
                            and (b.data <= dataAtual)
                            and b.deleted_at is null
                            order by b.data
                        ) as saldo")
                ->where('users_id', '=', $id)
                ->WhereNull('deleted_at')
                ->orderByRaw('data')
                ->get();
        }

        return view('painel.conta.historico', [
                'historico' => $query,
                'usuario' => User::withTrashed()->where('id', '=', $id)->first(),
                'saldo' => AppHelper::getSaldo($id),
                'banco' => Clube::all()->first(),
                'ano' => $request->ano
            ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function debitarCreditar(){
        $contas = DB::table('users')
            ->selectRaw('users.id, users.nome, users.cadastro, (select sum(c.credito) - sum(c.debito) from conta c where c.users_id = users.id) as saldo, users.deleted_at')
            // ->whereNull('deleted_at')
            ->groupBy('users.id', 'users.nome', 'users.cadastro')
            ->orderBy('users.nome')
            ->get();
        foreach($contas as $conta){
            $conta->saldo = AppHelper::getSaldo($conta->id);
        }
        $last = Conta::all()->sortByDesc('created_at')->first();
        return view('painel.conta.debitarcreditar', ['usuarios' => $contas, 'last' => $last]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function debitarCreditarStore(Request $request){
        $request->validate([
            'tipo' => 'required',
            'valor' => 'required|min:0',
            'associado_id' => 'required',
            'descricao' => 'nullable|max:255'
        ]);

        $conta = new Conta();
        if($request->tipo == 'debito'){
            $conta->debito = $request->valor;
        }
        if($request->tipo == 'credito'){
            $conta->credito = $request->valor;
        }
        $conta->users_id = $request->associado_id;
        $conta->gerador_id = Auth::id();
        $conta->descricao = $request->descricao;
        $conta->data = $request->data;

        if($conta->save())
            return redirect()->route('painel-conta-debitar-creditar')->with(['type' => 'alert-success', 'message' => 'Operação efetuada com sucesso!']);

        return redirect()->route('painel-conta-debitar-creditar')->with(['type' => 'alert-danger', 'message' => 'Falha ao efetuar operação! tente novamente mais tarde.']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function anuidade(){
        $anuidade = Anuidade::all()->where('ano', '=', date('Y'))->first();
        return view('painel.conta.geraranuidade', ['anuidade' => $anuidade]);
    }

    /**
     * @return false|string
     */
    public function anuidadeGerar(){
        $todosStatus = array();
        $anuidade = Anuidade::all()->where('ano', '=', date('Y'))->first();

        foreach (User::all() as $user){
            $conta = new Conta();
            $conta->debito = $anuidade->valor;
            $conta->users_id = $user->id;
            $conta->gerador_id = Auth::id();
            $conta->descricao = "ANUIDADE " . $anuidade->ano ;
            $conta->data = now();

            $status = new stdClass();
            $status->nome = User::find($conta->users_id)->nome ;
            $status->status = $conta->save();
            $status->saldo = AppHelper::getSaldo($conta->users_id);
            array_push($todosStatus, $status);
        }

        return json_encode($todosStatus);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editar($id){
        return view('painel.conta.editar', ['conta' => Conta::find($id)]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id){
        $request->validate([
            'tipo' => 'required',
            'valor' => 'required|min:0',
            'descricao' => 'nullable|max:255'
        ]);

        $conta = Conta::find($id);
        if($request->tipo == 'debito'){
            $conta->debito = $request->valor;
            $conta->credito = 0;
        }
        if($request->tipo == 'credito'){
            $conta->credito = $request->valor;
            $conta->debito = 0;
        }
        $conta->gerador_id = Auth::id();
        $conta->descricao = $request->descricao;
        $conta->data = $request->data;

        if($conta->save())
            return redirect()->route('painel-conta-historico' , $conta->users_id)->with(['type' => 'alert-success', 'message' => 'Atualização efetuada com sucesso!']);

        return redirect()->route('painel-conta-historico' , $conta->users_id)->with(['type' => 'alert-danger', 'message' => 'Falha ao efetuar atualização! tente novamente mais tarde.']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request){
        $request->validate([
            'id' => 'required'
        ]);

        $conta = Conta::find($request->id);
        if($conta->delete())
            return redirect()->route('painel-conta-historico' , $conta->users_id)->with(['type' => 'alert-success', 'message' => 'Lançamento removido com sucesso!']);

        return redirect()->route('painel-conta-historico' , $conta->users_id)->with(['type' => 'alert-danger', 'message' => 'Falha ao efetuar remoção! tente novamente mais tarde.']);

    }

    /*
 * Exportador
 */
    public function exportacaoCompleta(){
        $contas = Conta::select(
            'users.nome as Nome',
            DB::raw("date_format(conta.data, '%d/%m/%Y') as Data"),
            'conta.debito as Débito',
            'conta.credito as Crédito',
            'conta.descricao as Descrição',
            'parent.nome as Gerador'
        )
        ->leftJoin('users', 'users.id', '=', 'conta.users_id')
        ->leftJoin('users as parent', 'parent.id', '=', 'conta.gerador_id')
        ->whereNull('conta.deleted_at')
        ->whereNotNull('users.nome')
        ->orderBy('users.nome')->get();
        try {
            return (new FastExcel($contas))->download('Dados Financeiros.xlsx');
        } catch (Exception $e) {
            return false;
        }
    }

}
