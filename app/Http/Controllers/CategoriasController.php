<?php

namespace App\Http\Controllers;

use App\PostCategory;
use Illuminate\Http\Request;

class CategoriasController extends Controller
{
    public function index()
    {
        $categorys = PostCategory::all();
        return view('painel.noticias.categorys.listagem', ['categorys' => $categorys]);
    }

    public function create()
    {
        return view('painel.noticias.categorys.cadastro');
    }

    public function store(Request $request)
    {
        $category = new PostCategory;
        $category->name = $request->name;

        if ( $category->save() ){
            return redirect()->route('categorias.index')->with(['type' => 'alert-success', 'message' => 'Categoria cadastrada com sucesso!']);
        }
    }

    public function edit($id)
    {
        $post_category = PostCategory::find($id);
        return view('painel.noticias.categorys.editar', ['category' => $post_category]);
    }

    public function destroy($id)
    {
        $category = PostCategory::find($id);
        if ( $category->delete() ){
            return response()->json('deleted', 200);
        } else {
            return response()->json('error', 422);
        }
    }

    public function update(Request $request, $id)
    {
        $category = PostCategory::find($id);
        $category->name =  $request->name;

        if( $category->save() ) {
            return redirect()->route('categorias.index')->with(['type' => 'alert-success', 'message' => 'Categoria atualizada com sucesso!']);
        }
    }
}
