<?php

namespace App\Http\Controllers;

use App\JantaParticipantes;
use App\User;
use Box\Spout\Common\Exception\InvalidArgumentException;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Common\Exception\UnsupportedTypeException;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;
use Rap2hpoutre\FastExcel\FastExcel;
use function is_null;
use function redirect;
use function strtoupper;
use function view;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('presidente-secretario')->except(['meusDados', 'newPassword', 'selfUpdate', 'atualizarDados']);
    }

    /**
     * @return Factory|View
     */
    public function listagem(){
        return view('painel.user.listagem');
    }

    public function atualizarDados()
    {
        return view('painel.user.atualizar-dados');
    }

    /**
     * @return Factory|View
     */
    public function cadastro(){
        return view('painel.user.cadastro');
    }

    /**
     * @return Factory|View
     */
    public function lixeira(){
        return view('painel.user.lixeira');
    }

    /**
     * @return Factory|View
     */
    public function editar($id){
        $user = User::find($id);
        return view('painel.user.editar', ['usuario' => $user]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function block(Request $request){
        $request->validate([
            'id' => 'required',
            'motivo' => 'required'
        ]);

       $user = User::find($request->id);
       if(is_null($user))
           return redirect()->route('painel-usuarios-listagem')->with(['type' => 'alert-danger', 'message' => 'Usuário não localizado na base. Favor contatar um administrador.']);
       $user->janta = false;
       $user->tipo_desligamento = $request->motivo;
       $user->save();

       $jantaParticipante = JantaParticipantes::all()->where('users_id', '=', $user->id)->first();
       if($jantaParticipante)
            $jantaParticipante->delete();

       if($user->delete())
           return redirect()->route('painel-usuarios-listagem')->with(['type' => 'alert-success', 'message' => 'O usuario '. strtoupper($user->nome) .' foi removido com sucesso!']);

       return redirect()->route('painel-usuarios-listagem')->with(['type' => 'alert-danger', 'message' => 'O usuario '. strtoupper($user->nome) .' não pode ser removido, tente novamente.']);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function unblock(Request $request){
        $request->validate([
            'id' => 'required',
        ]);

        $user = User::withTrashed()->where('id', '=', $request->id)->first();
        $user->tipo_desligamento = 'restaurado';
        $user->save();
        if($user->restore())
            return redirect()->route('painel-usuarios-lixeira')->with(['type' => 'alert-success', 'message' => 'O usuario '. strtoupper($user->nome) .' foi restaurado com sucesso!']);

        return redirect()->route('painel-usuarios-lixeira')->with(['type' => 'alert-danger', 'message' => 'O usuario '. strtoupper($user->nome) .' não pode ser restaurado, tente novamente.']);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function destroy(Request $request){
        $request->validate([
            'id' => 'required',
        ]);

        $user = User::withTrashed()->where('id', '=', $request->id)->first();
        if($user->forceDelete())
            return redirect()->route('painel-usuarios-lixeira')->with(['type' => 'alert-success', 'message' => 'O usuario '. strtoupper($user->nome) .' foi removido permanentemente com sucesso!']);

        return redirect()->route('painel-usuarios-lixeira')->with(['type' => 'alert-danger', 'message' => 'O usuario '. strtoupper($user->nome) .' não pode ser removido, tente novamente.']);
    }

    public function createUser(Request $request){
        $messages = [
            'cpf.unique'    => 'Este CPF já se encontra cadastrado.',
            'nome.unique'   => 'Este nome já se encontra cadastrado.',
        ];
        $data = $request->validate([
            'cadastro'          => 'required',
            'cr'                => 'nullable',
            'validade_cr'   => 'nullable',
            'nome'              => 'required|unique:users,nome',
            'cpf'               => 'nullable|unique:users,cpf',
            'email'             => 'required',
            'telefone'          => 'required',
            'cidade'            => 'required',
            'estado'            => 'required',
            'endereco'          => 'required',
            'bairro'            => 'required',
            'estado_civil'      => 'nullable',
            'cep'               => 'nullable',
            'estado_nasceu'     => 'required',
            'cidade_nasceu'     => 'required',
            'profissao'         => 'required',
            'data_nascimento'   => 'required',
            'data_filiacao'   => 'required',
            'senha'             => 'required|min:6',
            'senhaconfirm'      => 'required_with:senha|same:senha|min:6',
        ], $messages);

        $user = new User();
        $user->fill($data);
        $user->tipo             = "associado";
        $user->janta            = $request->janta ? true : false;
        $user->nome             = strtoupper($request->nome);
        $user->indicado_por_id  = $request->indicado == "nenhuma" ? null : $request->indicado;
        $user->password         = Hash::make($request->senha);
        // $user->data_nascimento  = Carbon::createFromFormat('d/m/Y', $request->data_nascimento)->format('Y-m-d');
        // $user->data_filiacao    = Carbon::createFromFormat('d/m/Y', $request->datafiliacao)->format('Y-m-d');
        // $user->validade_cr      = Carbon::createFromFormat('d/m/Y', $request->validade_cr)->format('Y-m-d');


        if($user->save())
            return redirect()->route('painel-usuarios-cadastro')->with(['type' => 'alert-success', 'message' => 'Usuário '. strtoupper($user->nome) .' criado com sucesso!']);

        return redirect()->route('painel-usuarios-cadastro')->with(['type' => 'alert-danger', 'message' => 'Falha ao criar usuário!']);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function updateUser(Request $request){
        $request->validate([
            'cadastro' => 'required',
            'nome' => 'required',
            'cpf' => 'required',
            'telefone' => 'required',
            'cidade' => 'required',
            'id' => 'required',
            'data_filiacao' => 'required'
        ]);

        $user = User::find($request->id);
        $user->fill($request->only($user->getFillable()));
        $user->janta = $request->janta ? true : false;
        $user->nome = strtoupper($request->nome);
        $user->indicado_por_id = $request->indicado ?: null;

        if($user->update())
            return redirect()->route('painel-usuarios-editar',  $user->id)->with(['type' => 'alert-success', 'message' => 'Usuário '. strtoupper($user->nome) .' atualizado com sucesso!']);

        return redirect()->route('painel-usuarios-editar', $user->id)->with(['type' => 'alert-danger', 'message' => 'Falha ao atualizar usuário!']);
    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function newPassword(Request $request){
        $request->validate([
            'novasenha' => 'required'
        ]);

        $user = User::find($request->id);
        $user->password = Hash::make($request->novasenha);

        if($user->update())
            return json_encode(true);

        return json_encode(false);
    }

    public function meusDados(){
        return view('painel.user.meusdados');
    }

    public function selfUpdate(Request $request){

        $data = $request->validate([
            'nome'              => 'required|string',
            'email'             => 'required|string',
            'telefone'          => 'required|string',
            'telefone2'         => 'nullable|string',
            'telefone3'         => 'nullable|string',
            'profissao'         => 'required|string',
            'estado_civil'      => 'nullable|string',
            'data_nascimento'   => 'required',
            'estado'            => 'required|string',
            'cidade'            => 'required|string',
            'cep'               => 'nullable|string',
            'bairro'            => 'nullable|string',
            'endereco'          => 'nullable|string',
            'nacionalidade'     => 'required|string',
            'cidade_nasceu'     => 'required|string',
            'estado_nasceu'     => 'required|string',
        ]);

        // $data['data_nascimento'] = Carbon::createFromFormat('d/m/Y', $request->data_nascimento)->format('Y-m-d');
        $data['nome'] = strtoupper($data['nome']);

        $user = User::find(Auth::id());

        $user->update($data);

        return redirect()->route('painel-usuario-meus-dados',  $user->id)->with(['type' => 'alert-success', 'message' => 'Seus dados foram atualizado com sucesso!']);

        return redirect()->route('painel-usuario-meus-dados', $user->id)->with(['type' => 'alert-danger', 'message' => 'Falha ao atualizar dados! Tente novamente mais tarde.']);
    }


    /*
     * Exportador
     */
    public function exportacaoCompleta(){
        $users = DB::select("select
            u.nome as Nome,
            u.cr as 'CR nº',
            date_format(u.validade_cr, '%d/%m/%Y') as 'Validade CR',
            u.cpf as CPF,
            u.cadastro as 'Cadastro',
            date_format(u.data_filiacao, '%d/%m/%Y') as Filiação,
            concat_ws(', ', u.telefone, u.telefone2, u.telefone3) as Telefone,
            IF(ISNULL(u.data_desligamento), date_format(u.deleted_at, '%d/%m/%Y'), date_format(u.data_desligamento, '%d/%m/%Y')) as Deslig,
            date_format(u.data_primeira_habitualidade, '%d/%m/%Y') as '1ª Hab',
            parent.nome as 'Indicado por',
            if(u.janta, 'X', null) as JANTA,
            u.cidade as Cidade
        from users u
        left join users parent on(parent.id = u.indicado_por_id)");

        try {
            return (new FastExcel($users))->download('relatorio-usuarios.xlsx');
        } catch (Exception $e) {
            return false;
        }

    }

}
