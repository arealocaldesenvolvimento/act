<?php

namespace App\Http\Controllers;

use App\Clube;
use App\Documento;
use App\Mandato;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use function is_null;
use function redirect;
use function view;

/**
 * Class ClubeController
 * @package App\Http\Controllers
 */
class ClubeController extends Controller
{
    /**
     * ClubeController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'presidente-secretario']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        $clube = clube::all()->first();
        $usuarios = User::all()->all();

        if (Auth::user()->tipo == "presidente" || Auth::user()->tipo == "secretario") {
            $select = ['users.id', 'mandato.data_inicio', 'mandato.data_fim', 'documento.nome_original'];
            $presidente = DB::table('users')
                ->select($select)
                ->leftJoin('mandato', 'mandato.users_id', '=', 'users.id')
                ->leftJoin('documento', 'mandato.documento_id', '=', 'documento_id')
                ->where([
                    ['users.tipo', '=', 'presidente'],
                    ['documento.categoria', '=', 'assinatura digital'],
                ])
                ->whereNull(['users.deleted_at', 'documento.deleted_at', 'mandato.deleted_at'])
                ->orderByDesc('documento.id')
                ->orderByDesc('mandato.id')
                ->first();

            $secretario = DB::table('users')
                ->select($select)
                ->leftJoin('mandato', 'mandato.users_id', '=', 'users.id')
                ->leftJoin('documento', 'mandato.documento_id', '=', 'documento_id')
                ->where('users.tipo', '=', 'secretario')
                ->whereNull(['users.deleted_at', 'documento.deleted_at', 'mandato.deleted_at'])
                ->orderByDesc('mandato.created_at')
                ->orderByDesc('mandato.id')
                ->first();

            $tesoureiro = DB::table('users')
                ->select($select)
                ->leftJoin('mandato', 'mandato.users_id', '=', 'users.id')
                ->leftJoin('documento', 'mandato.documento_id', '=', 'documento_id')
                ->where('users.tipo', '=', 'tesoureiro')
                ->whereNull(['users.deleted_at', 'documento.deleted_at', 'mandato.deleted_at'])
                ->orderByDesc('mandato.created_at')
                ->orderBy('mandato.id')
                ->first();

            return view('painel.clube.index', ['clube' => $clube, 'usuarios' => $usuarios, 'presidente' => $presidente, 'secretario' => $secretario, 'tesoureiro' => $tesoureiro]);
        }

        return view('painel.clube.index', ['clube' => $clube, 'usuarios' => $usuarios]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'nome'          => 'required',
            'cnpj'          => 'required',
            'cr'            => 'nullable',
            'validade_cr'   => 'nullable',
            'endereco'      => 'required',
            'bairro'        => 'required',
            'cidade'        => 'required',
            'cep'           => 'required',
            'banco'         => 'required',
            'agencia'       => 'required',
            'conta_corrente'=> 'required',
            'pix1'          => 'nullable',
            'pix3'          => 'nullable',
            'pix2'          => 'nullable',
        ]);
        // $data['validade_cr'] = Carbon::createFromFormat('d/m/Y', $request->validade_cr)->format('Y-m-d');

        $clube = Clube::all()->count() != 0 ? Clube::all()->first() : new Clube();;
        $clube->fill($data);

        if ($clube->save())
            return redirect()->route('painel-sobre-clube')->with(['type' => 'alert-success', 'message' => 'Informações do clube atualizadas com sucesso!']);

        return redirect()->route('painel-sobre-clube')->with(['type' => 'alert-danger', 'message' => 'Falha ao atualizar as informações do clube! Tente novamente mais tarde.']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeSecretario(Request $request)
    {
        $oldSecretario = User::select('users.id', 'mandato.data_inicio', 'mandato.data_fim', 'documento.nome_original', 'users.tipo')
            ->leftJoin('mandato', 'mandato.users_id', '=', 'users.id')
            ->leftJoin('documento', 'mandato.documento_id', '=', 'documento_id')
            ->where('users.tipo', '=', 'secretario')
            ->whereNull('users.deleted_at')
            ->first();

        $newSecretario = User::select('users.id', 'mandato.data_inicio', 'mandato.data_fim', 'documento.nome_original', 'users.tipo')
            ->leftJoin('mandato', 'mandato.users_id', '=', 'users.id')
            ->leftJoin('documento', 'mandato.documento_id', '=', 'documento_id')
            ->where('users.id', '=', $request->secretario_id)
            ->whereNull('users.deleted_at')
            ->first();

        if ($oldSecretario != $newSecretario) {
            if (!is_null($oldSecretario)) {
                $oldSecretario->tipo = "associado";
                $oldSecretario->save();
            }

            $updateNewSecretario = User::find($request->secretario_id);
            $updateNewSecretario->tipo = "secretario";
            $updateNewSecretario->save();

        }


        $oldMandato = Mandato::all()->where('users_id', '=', $oldSecretario->id)->first();
        if($oldMandato)
            $oldMandato->delete();

        $mandato = new Mandato();
        $mandato->data_inicio = $request->secretario_data_inicio;
        $mandato->data_fim = $request->secretario_data_fim;
        $mandato->users_id = $request->secretario_id;
        $mandato->save();

        return redirect()->route('painel-sobre-clube')->with(['type' => 'alert-success', 'message' => 'Secretário do clube atualizadas com sucesso!']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storePresidente(Request $request)
    {
        $oldPresidente = User::select('users.id', 'mandato.data_inicio', 'mandato.data_fim', 'documento.nome_original', 'users.tipo')
            ->leftJoin('mandato', 'mandato.users_id', '=', 'users.id')
            ->leftJoin('documento', 'mandato.documento_id', '=', 'documento_id')
            ->where('users.tipo', '=', 'presidente')
            ->where('documento.categoria', '=', 'assinatura digital')
            ->whereNull('users.deleted_at')
            ->first();

        $newPresidente = User::select('users.id', 'mandato.data_inicio', 'mandato.data_fim', 'documento.nome_original', 'users.tipo')
            ->leftJoin('mandato', 'mandato.users_id', '=', 'users.id')
            ->leftJoin('documento', 'mandato.documento_id', '=', 'documento_id')
            ->where('users.id', '=', $request->presidente_id)
            ->whereNull('users.deleted_at')
            ->first();

        if ($oldPresidente != $newPresidente || is_null($oldPresidente)) {
            if (!is_null($oldPresidente)) {
                $oldPresidente->tipo = "associado";
                $oldPresidente->save();

                $oldMandato = Mandato::all()->where('users_id', '=', $oldPresidente->id)->first();
                if ($oldMandato)
                    $oldMandato->delete();
            }

            $updateNewPresidente = User::find($request->presidente_id);
            $updateNewPresidente->tipo = "presidente";
            $updateNewPresidente->save();

            $mandato = new Mandato();
            $mandato->data_inicio = $request->presidente_data_inicio;
            $mandato->data_fim = $request->presidente_data_fim;
            $mandato->users_id = $request->presidente_id;

            if ($request->hasFile('assinatura_digital')) {
                $documento = new Documento();
                $documento->diretorio = $request->file('assinatura_digital')->store('documents');
                $documento->nome_original = $request->file('assinatura_digital')->getClientOriginalName();
                $documento->categoria = "assinatura digital";
                $documento->save();

                $mandato->documento_id = $documento->id;
            }elseif (isset($oldPresidente->documento_id)) {
                $mandato->documento_id = $oldPresidente->documento_id;
            }

            $mandato->save();

        }

        return redirect()->route('painel-sobre-clube')->with(['type' => 'alert-success', 'message' => 'Presidente do clube atualizadas com sucesso!']);
    }

    /**]
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeTesoureiro(Request $request)
    {
        $oldTesoureiro = User::select('users.id', 'mandato.data_inicio', 'mandato.data_fim', 'documento.nome_original', 'users.tipo')
            ->leftJoin('mandato', 'mandato.users_id', '=', 'users.id')
            ->leftJoin('documento', 'mandato.documento_id', '=', 'documento_id')
            ->where('users.tipo', '=', 'tesoureiro')
            ->whereNull('users.deleted_at')
            ->first();

        $newTesoureiro = User::select('users.id', 'mandato.data_inicio', 'mandato.data_fim', 'documento.nome_original', 'users.tipo')
            ->leftJoin('mandato', 'mandato.users_id', '=', 'users.id')
            ->leftJoin('documento', 'mandato.documento_id', '=', 'documento_id')
            ->where('users.id', '=', $request->tesoureiro_id)
            ->whereNull('users.deleted_at')
            ->first();

        if ($oldTesoureiro != $newTesoureiro || is_null($newTesoureiro)) {
            if (!is_null($oldTesoureiro)) {
                $oldTesoureiro->tipo = "associado";
                $oldTesoureiro->save();
            }

            $updateNewTesroureiro = User::find($request->tesoureiro_id);
            $updateNewTesroureiro->tipo = "tesoureiro";
            $updateNewTesroureiro->save();
        }

        $oldMandato = Mandato::all()->where('users_id', '=', $oldTesoureiro->id)->first();
        if($oldMandato)
            $oldMandato->delete();

        $mandato = new Mandato();
        $mandato->data_inicio = $request->tesoureiro_data_inicio;
        $mandato->data_fim = $request->tesoureiro_data_fim;
        $mandato->users_id = $request->tesoureiro_id;
        $mandato->save();

        return redirect()->route('painel-sobre-clube')->with(['type' => 'alert-success', 'message' => 'Tesoureiro do clube atualizadas com sucesso!']);
    }
}
