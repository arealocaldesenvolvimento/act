<?php

namespace App\Http\Controllers;

use App\Anuidade;
use Illuminate\Http\Request;
use function redirect;
use function view;

class AnuidadeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'presidente-secretario']);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listagem(){
        return view('painel.anuidade.listagem');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
        $request->validate([
            'ano' => 'required|integer',
            'valor' => 'required|min:0'
        ]);

        if(Anuidade::all()->where('ano', '=', $request->ano)->first())
            return redirect()->route('painel-anuidade-listagem')->with(['type' => 'alert-warning', 'message' => 'Ano já cadastrado!']);

        $anuidade = new Anuidade();
        $anuidade->ano = $request->ano;
        $anuidade->valor = $request->valor;

        if($anuidade->save())
            return redirect()->route('painel-anuidade-listagem')->with(['type' => 'alert-success', 'message' => 'Anuidade criada com sucesso!']);

        return redirect()->route('painel-anuidade-listagem')->with(['type' => 'alert-danger', 'message' => 'Falha ao criar anuidade! tente novamente mais tarde.']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request){
        $request->validate([
            'id' => 'required'
        ]);

        $anuidade = Anuidade::find($request->id);
        if($anuidade->delete())
            return redirect()->route('painel-anuidade-listagem')->with(['type' => 'alert-success', 'message' => 'Anuidade removido com sucesso!']);

        return redirect()->route('painel-anuidade-listagem')->with(['type' => 'alert-danger', 'message' => 'Falha ao remover anuidade! tente novamente mais tarde.']);
    }
}
