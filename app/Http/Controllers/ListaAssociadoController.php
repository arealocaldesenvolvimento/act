<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ListaAssociadoController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('painel.index-list', ['users' => $users]);
    }
}
