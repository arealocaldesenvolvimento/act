<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/painel';


    protected function authenticated()
    {
        $user = Auth::user();

        if ($user->tipo != 'presidente'){

            //campos obrigatorios para a DECLARAÇÃO DE SEGURANÇA DO ACERVO (DSA)
            $camposParaValidar = [
                'data_nascimento',
                'cidade',
                'estado',
                'cidade_nasceu',
                'estado_nasceu',
                'nacionalidade',
                'profissao',
            ];

            // Verifica se algum dos campos está vazio
            foreach ($camposParaValidar as $campo) {
                if (empty($user->{$campo})) {
                    return redirect()->route('atualizar-dados')->with('error', 'Por favor, preencha todos os campos obrigatórios.');
                }
            }
        }

        return redirect()->intended('/painel');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Change username to login
     *
     * @return string
     */
    public function username()
    {
        return 'cpf';
    }

    protected function redirectTo()
    {
        return redirect()->intended('/painel')->getTargetUrl();
    }

}
