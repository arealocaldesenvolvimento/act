<?php

namespace App\Http\Controllers;

use App\Clube;
use App\Declaracao;
use App\Helpers\AppHelper;
use App\Presenca;
use App\Calibre;
use App\Evento;
use App\Anuidade;
use App\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;
use setasign\Fpdi\Tcpdf\Fpdi;
use Illuminate\Support\Facades\Storage;
use function back;
use function count;
use function is_null;
use function now;
use function view;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class DeclaracoesController extends Controller
{
    /**
     * COMENTAR O TCPDF::Header() - 3471
     */
     public function gerarDeclaracao166(Request $request)
     {
        if(is_null(Auth::user()->nome) || is_null(Auth::user()->cr) || is_null(Auth::user()->cpf)
        || is_null(Auth::user()->cadastro) || is_null(Auth::user()->data_filiacao))
        return response()->json(['tipo' => 'erro', 'message' => 'Erro ao gerar habitualidade']);

        $calibres = $request->calibres ?? ["all"];
        if (!empty($calibres)) {
            if (in_array("all", $calibres)) {
                $calibresA = Calibre::all();
                $calibres = [];
                foreach ($calibresA as $cal) {
                    $calibres[] = $cal->nome;
                }
            }
        }
        $eventos = $request->eventos ?? ["all"];
        if (!empty($eventos)) {
            if (in_array("all", $eventos)) {
                $eventosA = Evento::all();
                $eventos = [];
                foreach ($eventosA as $evento) {
                    $eventos[] = $evento->id;
                }
            }
        }
        $inicio = $request->inicio ?? false;
        $fim = $request->fim ?? false;
        $presencasN = $request->presencas ?? 0;
        $presencas = collect();
        foreach ($calibres as $calibre) {
            foreach ($eventos as $evento) {
                $query = Presenca::select('presenca.*', 'evento.titulo')
                    ->where('users_id', Auth::user()->id)
                    ->where('calibre', $calibre)
                    ->where('evento.id', $evento)
                    ->whereNotNull('numero_sigma')
                    ->join('evento', 'evento.id', '=', 'presenca.evento_id')
                    ->when($inicio, function ($query, $inicio) {
                        return $query->where('presenca.data', '>=', $inicio);
                    })
                    ->when($fim, function ($query, $fim) {
                        return $query->where('presenca.data', '<=', $fim);
                    })
                    ->orderBy('presenca.data', 'desc');
                if ($presencasN > 0) {
                    $query->take($presencasN);
                }
                $presencas = $presencas->merge($query->get());
            }
        }
        $ids = $presencas->pluck('id')->toArray();
        $ids = $presencas->pluck('id')->toArray();
        $idsHabitualidade = implode(', ', $ids);

        $filename =  Auth::user()->id . '_' . date('d-m-Y_H-i-s') . '_Habitualidade.pdf';

        $registro = $this->registro('habitualidade', 'autogerada', $filename);

        $linkQRCode = env('APP_URL') . 'validacao-declaracao?codigo='.$registro;
        $qrCode = QrCode::size(80)->generate($linkQRCode);

        $dados = array();
        $dados['presidente']    = AppHelper::getPresidente();
        $dados['registro']      = $registro;
        $dados['presencas']     = $presencas;

        $pdf = PDF::loadView('painel.declaracoes.pdfs.habitualidadePortaria166',
            [
                'dados' => $dados,
                'qrcode' => $qrCode,
                'clube' => Clube::first(),
                'ids'   => $idsHabitualidade
            ]
        );
        $pdf->setPaper([0, 0, 600, 990]);

        /* Salva o arquivo no storage */
        $content = $pdf->output();
        $path = 'declaracoes/' . $filename;
        Storage::put($path, $content);

        /* Busca o arquivo no storage para assinar */
        $pdfPath = "storage/app/" . $path;
        $pdf = new Fpdi();

        // Define o número inicial da página
        $pageNo = 1;

        // Loop para importar cada página do PDF original
        while ($pageNo <= $pdf->setSourceFile($pdfPath)) {
            // Adiciona uma nova página ao documento
            $pdf->AddPage();

            // Importa a página do PDF original
            $tplId = $pdf->importPage($pageNo);

            // Usa o modelo da página importada
            $pdf->useTemplate($tplId, 0, 0, null, null, true);

            // Incrementa o número da página para o próximo loop
            $pageNo++;
        }

        $info =
        [
            'Name'          => 'JAISON MAURECI BECKER',
            'Location'      => 'Rio do Sul',
            'Reason'        => 'Declaração',
            'ContactInfo'   => 'contferreira@uol.com.br',
        ];
        $certificado = file_get_contents(env('KEY_PATH'));
        $pdf->setSignature($certificado, $certificado, env('KEY_SENHA'), '', 2, $info);

        $content = $pdf->Output($path, 'S');
        Storage::put($path, $content);

        return response()->json(['tipo' => 'sucesso', 'registro'=> $dados['registro'], 'message' => 'Declaração emitida com sucesso!']);
    }

    public function declaracaoHabitualidadePortaria166()
    {
        if(is_null(Auth::user()->nome) || is_null(Auth::user()->cr) || is_null(Auth::user()->cpf)
        || is_null(Auth::user()->cadastro) || is_null(Auth::user()->data_filiacao))
        return back()->with(['type' => 'alert-warning', 'message' => 'Informações pendentes']);

        $filename =  Auth::user()->id . '_' . date('d-m-Y_H-i-s') . '_Habitualidade.pdf';

        $dados = array();
        $dados['nome']          = Auth::user()->nome;
        $dados['cr']            = Auth::user()->cr;
        $dados['cpf']           = Auth::user()->cpf;
        $dados['cadastro']      = Auth::user()->cadastro;
        $dados['data_filiacao'] = Auth::user()->data_filiacao;
        $dados['presidente']    = AppHelper::getPresidente();
        $dados['registro']      = $this->registro('habitualidade', 'autogerada', $filename);
        $dados['presencas']     = AppHelper::listaHabitualidadeComDisparosDozeMesesLocal(Auth::id());
        $dados['presencas']     = AppHelper::replaceNameLocal($dados['presencas']);

        $linkQRCode = env('APP_URL') . 'validacao-declaracao?codigo=' . $dados['registro'];
        $qrCode = QrCode::size(80)->generate($linkQRCode);


        /* Cria o PDF */
        $pdf = PDF::loadView('painel.declaracoes.pdfs.habitualidadePortaria166', ['dados' => $dados, 'qrcode' => $qrCode]);
        $pdf->setPaper([0, 0, 600, 900]);

        /* Salva o arquivo no storage */
        $content = $pdf->output();
        $path = 'declaracoes/' . $filename;
        Storage::put($path, $content);


        /* Busca o arquivo no storage para assinar */
        $pdf = new Fpdi();
        $pdf->AddPage();
        $pdf->setSourceFile("storage/app/" . $path);
        $tplId = $pdf->importPage(1);
        $pdf->useTemplate($tplId, 0, 0, null, null, true);
        $info =
        [
            'Name'          => 'JAISON MAURECI BECKER',
            'Location'      => 'Rio do Sul',
            'Reason'        => 'Declaração',
            'ContactInfo'   => 'contferreira@uol.com.br',
        ];
        $certificado = file_get_contents(env('KEY_PATH'));
        $pdf->setSignature($certificado, $certificado, env('KEY_SENHA'), '', 2, $info);

        $content = $pdf->Output($path, 'S');
        Storage::put($path, $content);

        return response($content, 200)
        ->header('Content-Type', 'application/pdf')
        ->header('Content-Disposition', 'inline; filename="' . $filename . '"');
    }

    /**
     * DeclaracoesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->except('validacao', 'viewDocumento');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listagemCli(){
        $saldo = number_format(AppHelper::getSaldo(Auth::id())) >= 0 ? true : false;
        $provas = AppHelper::habitualidadeProvasDozeMeses(Auth::id()) ?? 0;
        $presencaComDisparos = count(AppHelper::listaHabitualidadeComDisparosDozeMeses(Auth::id()));

        $calibres = Presenca::where('users_id', Auth::user()->id)
        ->whereNotNull('calibre')
        ->whereNotNull('numero_sigma')
        ->select('calibre')
        ->distinct()
        ->get();
        $eventos = Evento::all();
        $all_calibres = Calibre::all();
        return view('painel.declaracoes.listagem-cli',
        [
            'saldo'                 => $saldo,
            'provas'                => $provas,
            'calibres'              => $calibres,
            'all_calibres'          => $all_calibres,
            'eventos'              => $eventos,
            'presencaComDisparos'   => $presencaComDisparos,
        ]);
    }


    public function declaracaoSegurancaAcervo(Request $request)
    {
        if(is_null(Auth::user()->nome) || is_null(Auth::user()->cpf) || is_null(Auth::user()->cadastro) || is_null(Auth::user()->data_filiacao))
        return back()->with(['type' => 'alert-warning', 'message' => 'Informações pendentes']);

        $finalidade     = $request->finalidade;
        $colecionador   = $request->colecionador;
        $atirador       = $request->atirador;
        $cacador        = $request->cacador;

        $acervo = '';

        if ($colecionador) {
            $acervo .= 'colecionador';
        }

        if ($atirador) {
            $acervo .= ($colecionador ? ', ' : '') . 'atirador desportivo';
        }

        if ($cacador) {
            $acervo .= (($colecionador || $atirador) ? ' e/ou ' : '') . 'caçador excepcional';
        }

        if (empty($acervo)) {
            $acervo = 'Sem finalidade especificada';
        }

        $dados['finalidade'] = $finalidade;
        $dados['acervo']     = $acervo;

        $filename =  Auth::user()->id . '_' . date('d-m-Y_H-i-s') . '_Seguranca_do_Acervo.pdf';

        $registro = $this->registro('seguranca', 'autogerada', $filename, 1095);

        $dados = array();
        $dados['nome']          = Auth::user()->nome;
        $dados['cpf']           = Auth::user()->cpf;
        $dados['cadastro']      = Auth::user()->cadastro;
        $dados['data_filiacao'] = Auth::user()->data_filiacao;
        $dados['presidente']    = AppHelper::getPresidente();
        $dados['registro']      = $registro;


        $linkQRCode = env('APP_URL') . 'validacao-declaracao?codigo='.$registro;
        $qrCode = QrCode::size(80)->generate($linkQRCode);


        $pdf = PDF::loadView('painel.declaracoes.pdfs.seguranca-acervo',
        [
            'dados'     => $dados,
            'qrcode'    => $qrCode,
            'finalidade'=> $finalidade,
            'acervo'    => $acervo,
        ]);
        // return response($pdf->output(), 200)
        // ->header('Content-Type', 'application/pdf')
        // ->header('Content-Disposition', 'inline; filename="DECLARACAO DE FILIAÇÃO.pdf"');
        // return $pdf->download('DECLARACAO DE ASSOCIADO - FILIACAO CLUBE.pdf');

        /* Salva o arquivo no storage */
        $content = $pdf->output();
        $path = 'declaracoes/' . $filename;
        Storage::put($path, $content);


        /* Busca o arquivo no storage para assinar */
        $pdf = new Fpdi();
        $pdf->AddPage();
        $pdf->setSourceFile("storage/app/" . $path);
        $tplId = $pdf->importPage(1);
        $pdf->useTemplate($tplId, 0, 0, null, null, true);
        $info =
        [
            'Name'          => 'JAISON MAURECI BECKER',
            'Location'      => 'Rio do Sul',
            'Reason'        => 'Declaração',
            'ContactInfo'   => 'contferreira@uol.com.br',
        ];
        $certificado = file_get_contents(env('KEY_PATH'));
        $pdf->setSignature($certificado, $certificado, env('KEY_SENHA'), '', 2, $info);

        $content = $pdf->Output($path, 'S');
        Storage::put($path, $content);

        return response()->json(['tipo' => 'sucesso', 'registro'=> $dados['registro'], 'message' => 'Declaração emitida com sucesso!']);
    }

    
    public function declaracaoCompromisso()
    {
        if(is_null(Auth::user()->nome) || is_null(Auth::user()->cpf) || is_null(Auth::user()->cadastro) || is_null(Auth::user()->data_filiacao))
        return back()->with(['type' => 'alert-warning', 'message' => 'Informações pendentes']);

        $filename =  Auth::user()->id . '_' . date('d-m-Y_H-i-s') . '_Compromisso.pdf';

        $registro = $this->registro('compromisso', 'autogerada', $filename, 1095);

        $dados = array();
        $dados['nome']          = Auth::user()->nome;
        $dados['cpf']           = Auth::user()->cpf;
        $dados['cadastro']      = Auth::user()->cadastro;
        $dados['data_filiacao'] = Auth::user()->data_filiacao;
        $dados['presidente']    = AppHelper::getPresidente();
        $dados['registro']      = $registro;

        $linkQRCode = env('APP_URL') . 'validacao-declaracao?codigo='.$registro;
        $qrCode = QrCode::size(80)->generate($linkQRCode);

        $pdf = PDF::loadView('painel.declaracoes.pdfs.compromisso',
        [
            'dados'     => $dados,
            'qrcode'    => $qrCode,
            'clube'     => Clube::first()
        ]);

        return response($pdf->output(), 200)
        ->header('Content-Type', 'application/pdf')
        ->header('Content-Disposition', 'inline; filename="DECLARACAO DE COMPROMISSO.pdf"');
        return $pdf->download('DECLARACAO DE COMPROMISSO - FILIACAO CLUBE.pdf');

        /* Salva o arquivo no storage */
        $content = $pdf->output();
        $path = 'declaracoes/' . $filename;
        Storage::put($path, $content);


        /* Busca o arquivo no storage para assinar */
        $pdf = new Fpdi();
        $pdf->AddPage();
        $pdf->setSourceFile("storage/app/" . $path);
        $tplId = $pdf->importPage(1);
        $pdf->useTemplate($tplId, 0, 0, null, null, true);
        $info =
        [
            'Name'          => 'JAISON MAURECI BECKER',
            'Location'      => 'Rio do Sul',
            'Reason'        => 'Declaração',
            'ContactInfo'   => 'contferreira@uol.com.br',
        ];
        $certificado = file_get_contents(env('KEY_PATH'));
        $pdf->setSignature($certificado, $certificado, env('KEY_SENHA'), '', 2, $info);

        $content = $pdf->Output($path, 'S');
        Storage::put($path, $content);

        return redirect()->route('painel-ver-documento', $dados['registro']);
    }

    public function declaracaoFiliacao(){
        if(is_null(Auth::user()->nome) || is_null(Auth::user()->cpf) || is_null(Auth::user()->cadastro) || is_null(Auth::user()->data_filiacao))
            return back()->with(['type' => 'alert-warning', 'message' => 'Informações pendentes']);

        $filename =  Auth::user()->id . '_' . date('d-m-Y_H-i-s') . '_Filiacao.pdf';

        $registro = $this->registro('filiacao', 'autogerada', $filename);

        $dados = array();
        $dados['nome'] = Auth::user()->nome;
        $dados['cpf'] = Auth::user()->cpf;
        $dados['cadastro'] = Auth::user()->cadastro;
        $dados['data_filiacao'] = Auth::user()->data_filiacao;
        $dados['presidente'] = AppHelper::getPresidente();
        $dados['registro'] = $registro;


        $linkQRCode = env('APP_URL') . 'validacao-declaracao?codigo='.$registro;
        $qrCode = QrCode::size(80)->generate($linkQRCode);


        $pdf = PDF::loadView('painel.declaracoes.pdfs.filiacao', ['dados' => $dados, 'qrcode' => $qrCode, 'clube' => Clube::first()]);
        $pdf->setPaper([0, 0, 600, 900]);

        // return response($pdf->output(), 200)
        // ->header('Content-Type', 'application/pdf')
        // ->header('Content-Disposition', 'inline; filename="DECLARACAO DE FILIAÇÃO.pdf"');
        // return $pdf->download('DECLARACAO DE ASSOCIADO - FILIACAO CLUBE.pdf');

        /* Salva o arquivo no storage */
        $content = $pdf->output();
        $path = 'declaracoes/' . $filename;
        Storage::put($path, $content);


        /* Busca o arquivo no storage para assinar */
        $pdf = new Fpdi();
        $pdf->AddPage();
        $pdf->setSourceFile("storage/app/" . $path);
        $tplId = $pdf->importPage(1);
        $pdf->useTemplate($tplId, 0, 0, null, null, true);
        $info =
        [
            'Name'          => 'JAISON MAURECI BECKER',
            'Location'      => 'Rio do Sul',
            'Reason'        => 'Declaração',
            'ContactInfo'   => 'contferreira@uol.com.br',
        ];
        $certificado = file_get_contents(env('KEY_PATH'));
        $pdf->setSignature($certificado, $certificado, env('KEY_SENHA'), '', 2, $info);

        $content = $pdf->Output($path, 'S');
        Storage::put($path, $content);

        return redirect()->route('painel-ver-documento', $dados['registro']);

        return response($pdf->output(), 200)
        ->header('Content-Type', 'application/pdf')
        ->header('Content-Disposition', 'inline; filename="DECLARACAO DE FILIAÇÃO.pdf"');
        return $pdf->download('DECLARACAO DE ASSOCIADO - FILIACAO CLUBE.pdf');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function declaracaoGuiaTrafego(){
        if(is_null(Auth::user()->nome) || is_null(Auth::user()->cr) || is_null(Auth::user()->cpf) || is_null(Auth::user()->cadastro) || is_null(Auth::user()->data_filiacao))
            return back()->with(['type' => 'alert-warning', 'message' => 'Informações pendentes']);

        $dados = array();
        $dados['nome'] = Auth::user()->nome;
        $dados['cr'] = Auth::user()->cr;
        $dados['cpf'] = Auth::user()->cpf;
        $dados['cadastro'] = Auth::user()->cadastro;
        $dados['data_filiacao'] = Auth::user()->data_filiacao;
        $dados['presidente'] = AppHelper::getPresidente();
        $dados['registro'] = $this->registro('guia de trafego', 'autogerada');

        $pdf = PDF::loadView('painel.declaracoes.pdfs.guiatrafego', $dados);
        return $pdf->download('DECLARACAO DE ASSOCIADO - GTE.pdf');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function declaracaoRanking(){
        if(is_null(Auth::user()->nome) || is_null(Auth::user()->cr) || is_null(Auth::user()->cpf) || is_null(Auth::user()->cadastro) || is_null(Auth::user()->data_filiacao))
            return back()->with(['type' => 'alert-warning', 'message' => 'Informações pendentes']);

        $provas = count(AppHelper::listaHabitualidadeComDisparosDozeMeses(Auth::id()));

        $rank = 'I';
        if($provas > 2 && $provas <=4 )
            $rank = 'II';
        elseif($provas >= 4)
            $rank = 'III';

        $dados = array();
        $dados['nome'] = Auth::user()->nome;
        $dados['rank'] = $rank;
        $dados['cr'] = Auth::user()->cr;
        $dados['cpf'] = Auth::user()->cpf;
        $dados['cadastro'] = Auth::user()->cadastro;
        $dados['data_filiacao'] = Auth::user()->data_filiacao;
        $dados['presidente'] = AppHelper::getPresidente();
        $dados['registro'] = $this->registro('ranking', 'autogerada');

        $pdf = PDF::loadView('painel.declaracoes.pdfs.ranking', $dados);
        return $pdf->download('DECLARACAO DE ASSOCIADO - RANKING.pdf');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function declaracaoHabitualidade(){
        if(is_null(Auth::user()->nome) || is_null(Auth::user()->cr) || is_null(Auth::user()->cpf)
            || is_null(Auth::user()->cadastro) || is_null(Auth::user()->data_filiacao))
            return back()->with(['type' => 'alert-warning', 'message' => 'Informações pendentes']);

        if(!is_null(Auth::user()->data_primeira_habitualidade)){
            if(count(AppHelper::listaHabitualidadeComDisparosDozeMeses(Auth::id())) < 8)
                return back()->with(['type' => 'alert-warning', 'message' => 'Habitualidades insuficientes!']);
        }

        if(is_null(Auth::user()->data_primeira_habitualidade)){
            $eu = User::find(Auth::id());
            $eu->data_primeira_habitualidade = now();
            $eu->save();
        }

        $dados = array();
        $dados['nome'] = Auth::user()->nome;
        $dados['cr'] = Auth::user()->cr;
        $dados['cpf'] = Auth::user()->cpf;
        $dados['cadastro'] = Auth::user()->cadastro;
        $dados['data_filiacao'] = Auth::user()->data_filiacao;
        $dados['presidente'] = AppHelper::getPresidente();
        $dados['registro'] = $this->registro('habitualidade', 'autogerada');

        $pdf = PDF::loadView('painel.declaracoes.pdfs.habitualidade', $dados);
        return $pdf->download('DECLARACAO DE ASSOCIADO - HABITUALIDADE.pdf');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function declaracaoHabitualidadePortaria150(){
        $dados = array();
        $dados['nome'] = Auth::user()->nome;
        $dados['cr'] = Auth::user()->cr;
        $dados['cpf'] = Auth::user()->cpf;
        $dados['cadastro'] = Auth::user()->cadastro;
        $dados['data_filiacao'] = Auth::user()->data_filiacao;
        $dados['presidente'] = AppHelper::getPresidente();
        $dados['registro'] = $this->registro('habitualidade', 'autogerada');
        $dados['presencas'] = AppHelper::listaHabitualidadeComDisparosDozeMesesLocal(Auth::id());
        $dados['presencas'] = AppHelper::replaceNameLocal($dados['presencas']);

        $pdf = PDF::loadView('painel.declaracoes.pdfs.habitualidadePortaria150', $dados);
        // return $pdf->download('DECLARACAO DE ASSOCIADO - HABITUALIDADE.pdf');

        $pdf = PDF::loadView('painel.declaracoes.pdfs.habitualidadePortaria166', $dados);
        return $pdf->stream('DECLARACAO DE ASSOCIADO - FILIACAO CLUBE.pdf');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function declaracaoModalidadeProva(Request $request){
        $request->validate([
            'tipo' => 'required',
            'calibre' => 'required',
            'modalidade' => 'required'
        ]);
        $calibres = $request->calibre ?? ["all"];
        if (!empty($calibres)) {
            if (in_array("all", $calibres)) {
                $calibresA = Calibre::all();
                $calibres = [];
                foreach ($calibresA as $cal) {
                    $calibres[] = $cal->nome;
                }
            }
        }
        if(is_null(Auth::user()->nome) || is_null(Auth::user()->cr) || is_null(Auth::user()->cpf)
            || is_null(Auth::user()->cadastro) || is_null(Auth::user()->data_filiacao))
            return back()->with(['type' => 'alert-warning', 'message' => 'Informações pendentes']);

        $dados = array();
        $dados['nome'] = Auth::user()->nome;
        $dados['cr'] = Auth::user()->cr;
        $dados['cpf'] = Auth::user()->cpf;
        $dados['cadastro'] = Auth::user()->cadastro;
        $dados['data_filiacao'] = Auth::user()->data_filiacao;
        $dados['modalidade'] = $request->modalidade;
        $dados['calibres'] = $calibres;
        $dados['tipo'] = $request->tipo;
        $dados['presidente'] = AppHelper::getPresidente();
        $dados['registro'] = $this->registro('modalidade e prova', 'autogerada');;

        $pdf = PDF::loadView('painel.declaracoes.pdfs.modalidadeProva', $dados);
        return $pdf->download('DECLARACAO DE ASSOCIADO - MODALIDADE E PROVA.pdf');
    }

    /**
     * @param $tipo
     * @param $situacao
     * @return \Illuminate\Http\RedirectResponse|mixed
     * @throws \Exception
     */
    private function registro($tipo, $situacao, $filename = NULL, $diasValidade = 90){
        $data = new DateTime(now());

        $registro = new Declaracao();
        $registro->users_id = Auth::id();
        $registro->tipo = $tipo;
        $registro->nome_arquivo = $filename;
        $registro->data_liberacao = $data;
        $registro->data_validade = $data->modify('+'.$diasValidade.'day');
        $registro->situacao = $situacao;
        if($registro->save())
            return $registro->id;

        return back()->with(['type' => 'alert-danger', 'message' => 'Falha ao criar registro da declaração no sistema! Tente novamente mais tarde ou contate o clube!']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function validacao(Request $request){
        $request->validate([
            'codigo' => 'nullable|integer'
        ]);

        return view('landing.validaDeclaracao', ['declaracao' => $request->codigo ? Declaracao::find($request->codigo) : false]);
    }
    
    public function viewDocumento($codigo){
        $declaracao = $codigo ? Declaracao::find($codigo) : false;
        $caminhoArquivo = "declaracoes/". $declaracao->nome_arquivo;
        $caminhoCompleto = storage_path("app/{$caminhoArquivo}");
        $headers =
        [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $declaracao->nome_arquivo . '"',
        ];

        return response()->file($caminhoCompleto, $headers);
    }
}
