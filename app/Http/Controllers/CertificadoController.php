<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use function view;

class CertificadoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'presidente-secretario']);
    }

    public function index()
    {
        return view('painel.certificado.index');
    }

    public function store(Request $request)
    {
        $messages = [
            'arquivo.required'  => 'O arquivo é obrigatório.',
            'arquivo.mimes'     => 'O formato do arquivo deve ser .pfx',
            'senha.required'    => 'A senha do certificado é obrigatória.',
        ];


        $request->validate([
            'arquivo'   => 'required|mimes:pfx',
            'senha'     => 'required',
        ], $messages);

        dd($request->file('arquivo'));


        if ($request->hasFile('arquivo')) {
            $pfxFilePath = $request->file('arquivo')->getPathname();
            $pemFilePath = public_path('certificado.pem');
            $senha = $request->senha;

            exec("openssl pkcs12 -in $pfxFilePath -out $pemFilePath -nodes -passin pass:$senha");

            if (file_exists($pemFilePath)) {
            } else {
               dd(['arquivo' => 'Falha ao converter o arquivo.']);
            }
        } else {
            dd(['arquivo' => 'O arquivo não foi enviado corretamente.']);
        }

        dd('fora');
    }
}
