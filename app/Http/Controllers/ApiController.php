<?php

namespace App\Http\Controllers;

use App\Conta;
use App\Janta;
use App\JantaParticipantes;
use App\Presenca;
use App\User;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Common\Exception\UnsupportedTypeException;
use Box\Spout\Reader\Exception\ReaderNotOpenedException;
use Carbon\Carbon;
use DateTime;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\QueryException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Rap2hpoutre\FastExcel\FastExcel;
use stdClass;
use function App\Helpers\AppHelper;
use function array_filter;
use function array_push;
use function back;
use function count;
use function date;
use function doubleval;
use function end;
use function explode;
use function is_null;
use function json_encode;
use function str_replace;
use function strtoupper;
use function view;

class ApiController extends Controller
{
    /**
     * @param Request $request
     */
    public function emailContato(Request $request){
        Mail::send('layouts.emails.contato-cliente', [
            'nome' => $request->nome,
            'mensagem' => $request->mensagem,
            'equipe' => $request->equipe ? true : false
            ], function ($m) {
            $m->from('vitor@arealocal.com.br', 'Vitor');

            $m->to("vitor@arealocal.com.br", "vitor")->subject('Email Teste!');
        });

        Mail::send('layouts.emails.contato-empresa', [
            'nome' => $request->nome,
            'mensagem' => $request->mensagem,
            'email' => $request->email,
            'telefone' => $request->telefone,
            'estado' => $request->uf,
            'cidade' => $request->cidade,
            'equipe' => $request->equipe ? true : false
            ], function ($m) {
            $m->from('vitor@arealocal.com.br', 'Vitor');

            $m->to("vitor@arealocal.com.br", "vitor")->subject('Email Teste!');
        });
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function crToUser(Request $request){
        $request->validate([
            'usuario' => 'required'
        ]);

        $user = User::find($request->usuario);
        if(!is_null($user))
            return json_encode($user->cr);

        return json_encode(false);
    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function userToCr(Request $request){
        $request->validate([
            'cr' => 'required'
        ]);

        $user = User::all()->where('cr', '=', $request->cr)->first();
        if(!is_null($user))
            return json_encode($user->id);

        return json_encode(false);
    }


    /**
     * @param $data
     * @return false|string
     * @throws \Exception
     */
    public function getDatas($data){
        $datasplit = explode('-', $data);
        if(!checkdate($datasplit[1],$datasplit[0], $datasplit[2]))
            return json_encode(false);

        $jantas = Janta::whereRaw('year(data) = ?', date('Y'))->count();

        $datas = array();
        array_push($datas, new DateTime($data));
        for($i = 1; $i <= $jantas; $i++){
            if($i != 1){
                $lastdata = end($datas);
                $lastdata = new DateTime($lastdata->format('Y-m-d'));
                array_push($datas, $lastdata->modify('+7 day'));
            }
        }
        return json_encode($datas);
    }

    /**
     * @return mixed
     */
    public function firstLogin(){
        $user = User::find(Auth::id());
        $user->first_login = true;
        return $user->save();
    }

    /**
     * @param Request $request
     * @return Factory|RedirectResponse|\Illuminate\View\View
     * @throws IOException
     * @throws UnsupportedTypeException
     * @throws ReaderNotOpenedException
     */
    public function importacaoContas(Request $request){
        $status = array();
        if(!$request->hasFile('arquivos'))
            return back()->with(['type' => 'alert-warning', 'message' => 'Nenhum arquivo enviado']);


        foreach($request->file('arquivos') as $arquivo){
            $nome = str_replace('.xlsx', '', $arquivo->getClientOriginalName());

            $associado = new stdClass();
            $associado->nome = $nome;

            $usuario = DB::table('users')
                ->whereRaw('lower(nome) = lower(?)', $associado->nome)
                ->first();

            if($usuario){
                $associado->status = "Localizado na base";
                $associado->id = $usuario->id;
                $associado->campos = array();
                $associado->sucessos = 0;
                $associado->falhas = 0;
                (new FastExcel)->import($arquivo, function ($line) use ($associado) {
                    if(!empty(array_filter($line))){
                        $saldo = Conta::all()->where('users_id', '=', $associado->id)->sortByDesc('id')->first()->saldo ?? 0;
                        $conta = new Conta();
                        if(isset($line['Valor']) and isset($line['Data']) and isset($line['Descrição']) and $line['Valor'] != "-" and $line['Data'] != "-"){
                            if($line['Valor'] >= 0 ){
                                $conta->credito = doubleval($line['Valor']);
                                $conta->saldo = $saldo + $conta->credito;
                            } else {
                                $conta->debito = doubleval($line['Valor']) * -1;
                                $conta->saldo = $saldo - $conta->debito;
                            }
                            $conta->data = $line['Data']->format('Y-m-d H:i:s');
                            $conta->users_id = $associado->id;
                            $conta->descricao = $line['Descrição'];
                            $conta->gerador_id = Auth::id();
                            $conta->save() ? $associado->sucessos++ : $associado->falhas++;
                            $associado->saldo_final = $conta->saldo;
                        }else{
                            $valor = isset($line['Valor']) ? $line['Valor'] : "false";
                            $data = isset($line['Data']) ? $line['Data']->format('Y-m-d H:i:s') : "false";
                            $descricao = isset($line['Descrição']) ? $line['Descrição'] : "false";
                            array_push($associado->campos, "Valor: {$valor} | Data: {$data} | Descricao: {$descricao}");
                            $associado->falhas++;
                        }
                    }
                });
            }else {
                $associado->status = false;

            }
            array_push($status, $associado);
        }
        return view('outros.importadorContasResultado', ['resultados' => $status]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws IOException
     * @throws UnsupportedTypeException
     * @throws ReaderNotOpenedException
     */
    public function importacaoHabitualidades(Request $request){
        if(!$request->hasFile('arquivo'))
            return back()->with(['type' => 'alert-warning', 'message' => 'Nenhum arquivo enviado']);

        $status = new StdClass();
        $status->sucessos = 0;
        $status->falhas = 0;
        $status->linhas = "";
        (new FastExcel)->import($request->file('arquivo'), function ($line) use($status){
            if(!empty(array_filter($line))){
                $associado = DB::table('users')->whereRaw('lower(nome) = lower(?)',  $line['Nome'])->first();
                $evento = DB::table('evento')->whereRaw('lower(titulo) = lower(?)',  $line['Evento'])->first();

                $presenca = new Presenca();
                $presenca->cr = $line['CR'] == "" ? null : $line['CR'];
                $presenca->users_id = $associado ? $associado->id : null;
                $presenca->data = $line['Data'];
                $presenca->arma = null;
                $presenca->quantidade_disparos = $line['Disparos'] != "" ? $line['Disparos'] : null;
                $presenca->local = $line['Local'];
                $presenca->pagina = $line['Página'];
                $presenca->evento_id = $evento ? $evento->id : null;
                try{
                    if($presenca->save()){
                        $status->sucessos++;
                    }else{
                        $status->falhas++;
                        $status->linhas.= $line['Linha']. " | ";
                    }
                }catch (QueryException $e){
                    $status->falhas++;
                    $status->linhas.= $line['Linha']." | ";
                }
            }
        });

        return back()->with(['results' => $status]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws IOException
     * @throws ReaderNotOpenedException
     * @throws UnsupportedTypeException
     */
    public function importacaoJantas(Request $request){
        if(!$request->hasFile('arquivo'))
            return back()->with(['type' => 'alert-warning', 'message' => 'Nenhum arquivo enviado']);

        $status = new StdClass();
        $status->sucessos = 0;
        $status->falhas = 0;
        $status->associados = 0;
        $status->nomes_falhas = "";
        (new FastExcel)->import($request->file('arquivo'), function ($line) use($status){
            if(!empty(array_filter($line))){
                try{
                    $associado = User::all()->where('nome', '=', strtoupper($line['nome']))->first();
                    if(!$associado){
                        $status->associados++;
                        $status->nomes_falhas.="<br/>". $line['nome'];
                        return;
                    }
                    $janta = Janta::all()->where('data', '=', $line['data']->format('Y-m-d H:i:s'))->first();
                    if(!$janta){
                        $janta = new Janta();
                        $janta->data = $line['data'];
                        $janta->save();
                    }
                    $participante = new JantaParticipantes();
                    $participante->janta_id = $janta->id;
                    $participante->users_id = $associado->id;
                    $participante->save();

                    $associado->janta = true;
                    $associado->save();

                    $status->sucessos++;
                }catch (QueryException $e){
                    $status->falhas++;
                    $status->nomes_falhas.="<br/>". $line['nome'];
                }
            }
        });

        return back()->with(['results' => $status]);
    }

    /**
     * @return false|string
     */
    public function presencasAnoAtual(){
        $meses = DB::select('select count(1) as quant, month(data) as mes from presenca where year(data) = year(now()) group by 2');
        $valores = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        if($meses){
            foreach ($meses as $mes){
                $valores[$mes->mes - 1] = $mes->quant;
            }
        }
        return json_encode($valores);
    }

    /**
     * @return false|string
     */
    public function declaracoesAnoAtual(){
        $meses = DB::select('select count(1) as quant, month(created_at) as mes from declaracao where year(created_at) = year(now()) group by 2');
        $valores = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        if($meses){
            foreach ($meses as $mes){
                $valores[$mes->mes - 1] = $mes->quant;
            }
        }
        return json_encode($valores);
    }

    public function emailVencimento(){

        $cur_date = Carbon::now();
        $vencimento = Carbon::now()->addDays(90);

        $data = DB::table('users as u')
        ->select('u.*')
        ->whereBetween('validade_cr', [$cur_date->format('Y-m-d'), $vencimento->format('Y-m-d')])
        ->get()->toArray();

        foreach ($data as $key => $user) {
            $validade = Carbon::createFromFormat('Y-m-d H:i:s', $user->validade_cr);
            $result = $validade->diffInDays($cur_date);

            if ($validade->gte($cur_date) || $result == 0) {
                if ($result <= 90 && ($result % 30 == 0 || $result == 10)) {
                    Mail::send('layouts.emails.vencimento-cr', [
                        'nome' => $user->nome,
                        'dias' => $result,
                        'user' => $user
                        ], function ($m) use ($user) {
                        $m->from('contato@clubedetirorsl.com.br', 'Clube de Tiro - Rio do Sul');
                        $m->to($user->email, $user->nome)->subject('Vencimento do seu CR');
                    });
                }
            }
        }
    }
}
