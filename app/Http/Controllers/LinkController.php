<?php

namespace App\Http\Controllers;

use App\Links;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LinkController extends Controller
{
    public function index()
    {
        $links = Links::all();
        return view('painel.links.listagem', ['links' => $links]);
    }

    public function create()
    {
        return view('painel.links.cadastro');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $link = new Links;
        $link->titulo = $request->title;
        $link->email =  $request->email;
        $link->telefone = $request->telefone;
        $link->link = $request->link;
        $link->descricao = $request->descricao;

        if ($link->save()) {
            return redirect()->route('link.index')->with(['type' => 'alert-success', 'message' => 'Criado com sucesso!']);
        }
    }

    public function edit($id)
    {
        $link = Links::find($id);
        return view('painel.links.editar', ['link' => $link]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $link = Links::find($id);
        $link->titulo = $request->title;
        $link->email =  $request->email;
        $link->telefone = $request->telefone;
        $link->link = $request->link;
        $link->descricao = $request->descricao;

        if ($link->save()) {
            return redirect()->route('link.index')->with(['type' => 'alert-success', 'message' => 'Atualizado com sucesso!']);
        }
    }

    public function destroy($id)
    {
        $link = Links::find($id);
        if ($link->delete()) {
            return redirect()->route('link.index')->with(['type' => 'alert-success', 'message' => 'Link deletado com sucesso!']);
        }
        return redirect()->route('link.index')->with(['type' => 'alert-error', 'message' => 'Ocorreu um erro!']);
    }
}
