<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Helpers\AppHelper;
use App\User;
use Carbon;


class PainelController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request){

        if (Auth::user()->tipo === 'auxiliar') {
            return redirect()->route('associados');
        } elseif (\App\Helpers\AppHelper::isAdmin()) {
            return view('painel.index-admin');
        } else {
            if (!empty(Auth::user()->validade_cr)) {
                $now = Carbon\Carbon::now();
                $validade = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', Auth::user()->validade_cr);
                if ($validade->gt($now)) {
                    $result = $validade->diffInDays($now);
                    if ($result < 90) {
                        return view('painel.index-associado', ['type' => 'alert-warning', 'message' => 'Seu CR vencerá em '.$result.' dias. Lembre-se de encaminhar a renovação.']);
                    }else{
                        return view('painel.index-associado');
                    }
                }else{
                    return view('painel.index-associado', ['type' => 'alert-danger', 'message' => 'Seu CR está vencido. Encaminhe a renovação.']);
                }
            }else{
                return view('painel.index-associado');
            }
        }
    }

}
