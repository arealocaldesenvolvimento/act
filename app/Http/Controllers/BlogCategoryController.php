<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class BlogCategoryController extends Controller
{
    public function index($id)
    {
        $posts = Post::select('*')
        ->where('post_category_id', '=', $id)
        ->orderBy('created_at', 'desc')
        ->simplePaginate(9);

        return view('landing.noticias', ['posts' => $posts]);
    }
}
