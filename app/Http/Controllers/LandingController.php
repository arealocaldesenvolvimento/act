<?php

namespace App\Http\Controllers;

use App\Links;
use Illuminate\Http\Request;

class LandingController extends Controller
{
    public function contato()
    {
        return view('landing.home');
    }

    public function sobre()
    {
        return view('landing.sobre');
    }

    public function links()
    {
        $links =  Links::all();
        return view('landing.links', ['links' => $links]);
    }
}
