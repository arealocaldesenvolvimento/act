<?php

namespace App\Http\Controllers;

use App\Arma;
use App\Helpers\AppHelper;
use App\Presenca;
use App\Anuidade;
use App\User;
use App\Clube;
use App\Conta;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

use function back;
use function is_null;
use function now;
use function redirect;
use function view;

class PresencaController extends Controller
{
    /**
     * PresencaController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('presidente-secretario')->except('listagemAssociado', 'cadastroQRCode', 'storeQRCode', 'storeLog');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listagem(){
        $presencas = DB::table('presenca')
            ->selectRaw('presenca.id, date(presenca.data) as data, presenca.cr, presenca.calibre, presenca.quantidade_disparos,
                presenca.local, presenca.pagina, presenca.calibre, presenca.tipo_arma as tipo, presenca.marca_arma as marca, presenca.modelo_arma as modelo, presenca.validade, presenca.calibre_restrito,
                   users.nome, evento.titulo, DATE_FORMAT(presenca.created_at, "%d/%m/%Y") as created_at,
                DATE_FORMAT(presenca.updated_at, "%d/%m/%Y") as updated_at, presenca.user_agent,presenca.plataforma,presenca.ip,presenca.latitude,presenca.longitude')
            ->LeftJoin('users', 'users.id', '=', 'presenca.users_id')
            ->LeftJoin('evento', 'evento.id', '=', 'presenca.evento_id')
            ->whereNull(['presenca.deleted_at', 'presenca.deleted_at'])
            ->orderByDesc('presenca.data')
            ->get();
        return view('painel.habitualidade.listagem', ['habitualidades' => $presencas]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listagemAssociado(){
        $presencas = DB::table('presenca')
            ->selectRaw("presenca.id,
                date(presenca.data) as data,
                presenca.cr,
                presenca.modelo_arma as modelo,
                presenca.marca_arma as marca,
                presenca.calibre,
                presenca.tipo_arma as tipo,
                presenca.validade,
                presenca.created_at,
                presenca.updated_at,
                presenca.user_agent,
                presenca.plataforma,
                presenca.ip,
                presenca.longitude,
                presenca.latitude,
                presenca.calibre_restrito,
                presenca.quantidade_disparos,
                presenca.local, presenca.pagina,
                users.nome,
                evento.titulo")
            ->LeftJoin('users', 'users.id', '=', 'presenca.users_id')
            ->LeftJoin('evento', 'evento.id', '=', 'presenca.evento_id')
            ->where('users_id', '=', Auth::id())
            ->whereNull(['presenca.deleted_at', 'presenca.deleted_at'])
            ->orderByDesc('data')
            ->get();
        return view('painel.habitualidade.listagem', ['habitualidades' => $presencas]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cadastro(){
        $last = Presenca::all()->sortByDesc('created_at')->first();
        return view('painel.habitualidade.cadastro', [
            'last' => $last
        ]);
    }

    /**
     * @return mixed
     */
    public function cadastroQRCode($qrcode){
        $clube = Clube::all()->sortByDesc('created_at')->first();
        $anuidade =  Anuidade::select("valor")->where("ano", date("Y"))->first();
        if($anuidade==null){
            $anuidade=0;
        }else{
            $anuidade= $anuidade->valor;
        }
        $saldo = (number_format(AppHelper::getSaldo(Auth::id()) + $anuidade))>= 0 ? true : false;
        if ($clube->qr_code == $qrcode) {
            $last = Presenca::all()->sortByDesc('created_at')->first();
            return view('painel.habitualidade.cadastro-qr-code', [
                'last' => $last, 
                'qr_code' => $qrcode,
                'saldo' => $saldo
            ]);
        }else{
            return redirect()->route('painel-index')->with(['type' => 'alert-danger', 'message' => 'Impossível criar habitualidade. QR Code inválido.']);
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function imprimirQRCode(){
        $clube = Clube::all()->sortByDesc('created_at')->first();
        if (empty($clube->qr_code)) {
            $clube->qr_code = base64_encode(Carbon::now());
            $clube->save();
        }
        return view('painel.habitualidade.imprimir-qr-code', ['qr_code' => $clube->qr_code]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function gerarQRCode(){
        $clube = Clube::all()->sortByDesc('created_at')->first();
        $clube->qr_code = base64_encode(Carbon::now());
        $clube->save();
        return redirect()->route('painel-habitualidade-imprimir-qr-code')->with(['type' => 'alert-success', 'message' => 'QR Code gerado com sucesso!', 'qr_code' => $clube->qr_code]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
        $data = $request->validate([
            'evento_id'             => 'required',
            'data'                  => 'required',
            'cr'                    => 'required',
            'quantidade_disparos'   => 'required',
            'users_id'              => 'required',
            'local'                 => 'required',
            // 'pagina'                => 'required',
            'data'                  => 'required',
        ]);

        $idArma = $request->suas_armas ?: $request->armas_clube;
        $arma = Arma::find($idArma);

        $presenca = new Presenca();
        $presenca->fill($data);

        if ($request->has('pagina')) {
            $presenca->pagina = $request->pagina;
        }

        $presenca->calibre          = $arma->calibre;
        $presenca->tipo_arma        = $arma->tipo_arma;
        $presenca->marca_arma       = $arma->marca;
        $presenca->modelo_arma      = $arma->modelo;
        $presenca->calibre_restrito = $arma->calibre_restrito;
        $presenca->validade         = $arma->validade;
        $presenca->numero_gte       = $arma->numero_gte;
        $presenca->numero_sigma     = $arma->numero_sigma;
        $presenca->numero_serie     = $arma->numero_serie;


        $user = User::find($presenca->users_id);

        if (!empty($user->validade_cr)) {

            $now = Carbon::now();
            $validade = Carbon::createFromFormat('Y-m-d H:i:s', $user->validade_cr);
            $result = $validade->gt($now);

            if (!$result) {
                return redirect()->route('painel-habitualidade-cadastro')->with(['type' => 'alert-danger', 'message' => 'Falha ao criar habitualidade! CR expirado.']);
            }
        }

        $anuidade =  Anuidade::select("valor")->where("ano", date("Y"))->first();
        if($anuidade==null){
            $anuidade=0;
        }else{
            $anuidade= $anuidade->valor;
        }
        $saldo = (number_format(AppHelper::getSaldo($user->id) + $anuidade))>= 0 ? true : false;
        if (!$saldo) {
            return redirect()->route('painel-habitualidade-cadastro')->with(['type' => 'alert-danger', 'message' => 'Falha ao criar habitualidade! Saldo Negativo.']);
        }

        if($presenca->save()){
            if(is_null($user->data_primeira_habitualidade)){
                $user->data_primeira_habitualidade = $presenca->data;
                $user->save();
            }
            return redirect()->route('painel-habitualidade-cadastro')->with(['type' => 'alert-success', 'message' => 'Habitualidade criada com sucesso!']);
        }

        return redirect()->route('painel-habitualidade-cadastro')->with(['type' => 'alert-danger', 'message' => 'Falha ao criar habitualidade! tente novamente mais tarde.']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeQRCode(Request $request){
        $data = $request->validate(
            [
                'evento_id'             => 'required',
                'data'                  => 'required',
                'users_id'              => 'required',
                'cr'                    => 'required',
                'local'                 => 'required',
                'quantidade_disparos'   => 'required',
                'user_agent'    => 'required',
                'plataforma'    => 'required',
                'ip'       => 'required',
                'latitude'      => 'required',
                'longitude'     => 'required',
            ],
            [
                'latitude.required'  => 'Você precisa permitir a localização para poder cadastrar habitualidades.',
                'longitude.required' => 'Você precisa permitir a localização para poder cadastrar habitualidades.',
            ]
        );

        $isInsidePolygon = '';
        if($_SERVER["REMOTE_ADDR"]=='186.209.28.111'){
            // Teste de coordenadas que pertencem a area do clube
            $isInsidePolygon = $this->isCoordinateInsidePolygon(-27.15911, -49.59396);
        }else{
            $isInsidePolygon = $this->isCoordinateInsidePolygon($data['latitude'], $data['longitude']);
        }

        if (!$isInsidePolygon) {
            return redirect()->route('painel-habitualidade-listagem-associado')->with(['type' => 'alert-danger', 'message' => 'Não foi possível cadastrar a habitualidade pois você não está nas dependências do clube.']);
        }

        $user = User::find($request->users_id);

        /* Valida Nº CR */
        if(!$user->cr){
            return redirect()->route('painel-habitualidade-listagem-associado')->with(['type' => 'alert-danger', 'message' => 'Número do CR inválido.']);
        }

        /* Valida Data do CR*/
        if($user->validade_cr < now()){
            return redirect()->route('painel-habitualidade-listagem-associado')->with(['type' => 'alert-danger', 'message' => 'Validade do CR inválida']);
        }

        /* Valida o Saldo Devedor */
        $ultimaAnuidade = Conta::where('descricao', 'ANUIDADE '. date('Y'))->where('users_id', $user->id)->first();
        $divida = AppHelper::getSaldo($user->id);

        /* pode ficar devendo apenas o valor da anuidade atual */

        if ($ultimaAnuidade !== null && is_object($ultimaAnuidade->debito)) {
            if(-$divida > $ultimaAnuidade->debito ){
                return redirect()->route('painel-habitualidade-listagem-associado')->with(['type' => 'alert-danger', 'message' => 'Você possui pendências financeiras com o clube.']);
            }
        }

        $now = Carbon::now();
        $validade = Carbon::createFromFormat('Y-m-d H:i:s', $user->validade_cr);
        $result = $validade->gt($now);

        if (!$result) {
            return redirect()->route('painel-habitualidade-listagem-associado')->with(['type' => 'alert-danger', 'message' => 'Falha ao criar habitualidade! CR expirado.']);
        }


        $idArma = $request->suas_armas ?: $request->armas_clube;

        $arma = Arma::find($idArma);

        $presenca = new Presenca();
        $presenca->fill($data);

        $presenca->calibre          = $arma->calibre;
        $presenca->tipo_arma        = $arma->tipo_arma;
        $presenca->marca_arma       = $arma->marca;
        $presenca->modelo_arma      = $arma->modelo;
        $presenca->calibre_restrito = $arma->calibre_restrito;
        $presenca->validade         = $arma->validade;
        $presenca->numero_gte       = $arma->numero_gte;
        $presenca->numero_sigma     = $arma->numero_sigma;
        $presenca->numero_serie     = $arma->numero_serie;

        if($presenca->save()){
            if(is_null($user->data_primeira_habitualidade)){
                $user->data_primeira_habitualidade = $presenca->data;
                $user->save();
            }
            return redirect()->back()->with(['isPopUp' => 'true', 'type' => 'alert-success', 'message' => 'Habitualidade criada com sucesso!']);
        }

        return redirect()->route('painel-habitualidade-listagem-associado')->with(['type' => 'alert-danger', 'message' => 'Falha ao criar habitualidade! tente novamente mais tarde.']);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editar($id){
        return view('painel.habitualidade.editar', ['presenca' => Presenca::find($id)]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request){
        $data = $request->validate([
            'evento_id'           => 'required',
            'data'                => 'required',
            'cr'                  => 'required',
            'associado_id'        => 'required',
            'local'               => 'required',
            'quantidade_disparos'   => 'required',
            // 'pagina'              => 'required',
        ]);

        $idArma = $request->suas_armas ?: $request->armas_clube;
        $arma = Arma::find($idArma);

        $presenca = Presenca::find($request->id);
        if (!$presenca) {
            return redirect()->back()->with(['type' => 'alert-danger', 'message' => 'Habitualidade não encontrada!']);
        }

        $presenca->fill($data);

        if ($request->has('pagina')) {
            $presenca->pagina = $request->pagina;
        }

        if ($arma) {
            $presenca->calibre          = $arma->calibre;
            $presenca->tipo_arma        = $arma->tipo_arma;
            $presenca->marca_arma       = $arma->marca;
            $presenca->modelo_arma      = $arma->modelo;
            $presenca->calibre_restrito = $arma->calibre_restrito;
            $presenca->validade         = $arma->validade;
            $presenca->numero_gte       = $arma->numero_gte;
            $presenca->numero_sigma     = $arma->numero_sigma;
            $presenca->numero_serie     = $arma->numero_serie;
        }

        if ($presenca->save()) {
            return redirect()->route('painel-habitualidade-editar', $request->id)->with(['type' => 'alert-success', 'message' => 'Habitualidade atualizada com sucesso!']);
        }

        return redirect()->route('painel-habitualidade-editar', $request->id)->with(['type' => 'alert-danger', 'message' => 'Falha ao atualizar habitualidade! Tente novamente mais tarde.']);
    }

        /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request){
        $request->validate([
            'id' => 'required'
        ]);

        $presenca = Presenca::find($request->id);
        if($presenca->delete())
            return redirect()->route('painel-habitualidade-listagem')->with(['type' => 'alert-success', 'message' => 'Habitualidade removido com sucesso!']);

        return redirect()->route('painel-habitualidade-listagem')->with(['type' => 'alert-danger', 'message' => 'Falha ao remover habitualidade! tente novamente mais tarde.']);
    }

     /**
     * @param $id
     * @return JsonResponse
     */
    public function getDetalhes($id){
        if (!empty($id)) {
            $presenca = DB::table('presenca')
            ->selectRaw('users.nome as "Associado",
                users.cpf as "CPF",
                users.telefone as "Telefone",
                presenca.cr as "CR",
                evento.titulo as "Evento",
                presenca.calibre as "Arma",
                presenca.quantidade_disparos as "Quantidade de disparos",
                presenca.local as "Local",
                presenca.pagina as "Página",
                DATE_FORMAT(presenca.data, "%d/%m/%Y") as "Data do registro",
                DATE_FORMAT(presenca.updated_at, "%d/%m/%Y") as "Modificado em",
                presenca.user_agent as "Navegador",
                presenca.plataforma as "Sistema Operacional",
                presenca.ip as "Endereço IP",
                presenca.latitude as "Latitude",
                presenca.longitude as "Longitude"')
            ->LeftJoin('users', 'users.id', '=', 'presenca.users_id')
            ->LeftJoin('evento', 'evento.id', '=', 'presenca.evento_id')
            ->where('presenca.id', '=', $id)
            ->whereNull(['presenca.deleted_at', 'presenca.deleted_at'])
            ->first();
            return json_encode($presenca);
        }
    }

    function isCoordinateInsidePolygon($latitude, $longitude)
    {
        $polygon = json_decode(env('COORDINATES'), true);

        $inside = false;
        $j = count($polygon) - 1;

        for ($i = 0; $i < count($polygon); $i++) {
            if (($polygon[$i][1] < $latitude && $polygon[$j][1] >= $latitude ||
                $polygon[$j][1] < $latitude && $polygon[$i][1] >= $latitude) &&
                ($polygon[$i][0] + ($latitude - $polygon[$i][1]) / ($polygon[$j][1] - $polygon[$i][1]) * ($polygon[$j][0] - $polygon[$i][0]) < $longitude)) {
                $inside = !$inside;
            }
            $j = $i;
        }

        return $inside;
    }

    public function storeLog(Request $request)
    {
        Log::error('Erro de Geolocalização: ' . $request->error . ', Latitude: '. $request->latitude . ', Longitude: '. $request->longitude . ', User: '. $request->user . '!');
        return response()->json(['success' => true]);
    }

}
