<?php

namespace App\Http\Controllers;

use File;
use App\Post;
use App\PostCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File as FacadesFile;
use Illuminate\Support\Facades\Validator;

class NoticiasController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        return view('painel.noticias.listagem', ['posts' => $posts]);
    }

    public function create()
    {
        return view('painel.noticias.cadastro');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
            'category_id' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $post = new Post;
        $post->title = $request->title;
        $post->post_category_id = $request->category_id;
        $post->description = $request->content;
        if ($post->save()) {
            if ($request->file()) {
                FacadesFile::makeDirectory(public_path() . '/images/noticias/' . $post->id, $mode = 0755, $recursive = false, $force = false);
                $request->file('file')->move(public_path() . '/images/noticias/' . $post->id, $request->file('file')->getClientOriginalName());
                $path = '/images/noticias/' . $post->id . '/' . $request->file('file')->getClientOriginalName();
                $post->thumbnail = $path;
                $post->save();
            }
            return redirect()->route('noticias.index')->with(['type' => 'alert-success', 'message' => 'Post criado com sucesso!']);
        }
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $post = Post::find($id);
        return view('painel.noticias.editar', ['post' => $post]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
            'category_id' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $old_thumb = Post::find($id)->thumbnail;
        if (!empty($old_thumb) && !empty($request->file())) {
            FacadesFile::delete($old_thumb);
        }

        if ($request->file() && $request->file('file')->isValid()) {
            $request->file->storeAs('/noticias' . '/' . $id, $request->file->getClientOriginalName());
            $request->file('file')->move(public_path() . '/images/noticias/' . $id, $request->file('file')->getClientOriginalName());
            $path = '/images/noticias/' . $id . '/' . $request->file('file')->getClientOriginalName();
        }

        $post = Post::find($id);
        $post->title = $request->title;
        $post->post_category_id = $request->category_id;
        $post->description =  $request->content;
        !empty($request->file('file')) ? $post->thumbnail = $path : '';

        if ($post->save()) {
            return redirect()->route('noticias.index')->with(['type' => 'alert-success', 'message' => 'Notícia atualizada com sucesso!']);
        }

        return redirect()->route('noticias.index')->with(['type' => 'alert-error', 'message' => 'Ocorreu um erro!']);
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        if ($post->delete()) {
            return redirect()->route('noticias.index')->with(['type' => 'alert-success', 'message' => 'Notícia deletada com sucesso!']);
        }
        return redirect()->route('noticias.index')->with(['type' => 'alert-error', 'message' => 'Ocorreu um erro!']);
    }
}
