<?php

namespace App\Http\Controllers;

use App\Evento;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use function redirect;
use function view;

class EventoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'presidente-secretario']);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listagem(){
        return view('painel.evento.listagem');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
        $request->validate([
            'titulo' => 'required|max:255',
            'observacao' => 'nullable|max:255'
        ]);

        $evento = new Evento();
        $evento->titulo = $request->titulo;
        $evento->observacoes = $request->observacao;

        if($evento->save())
            return redirect()->route('painel-evento-listagem')->with(['type' => 'alert-success', 'message' => 'Evento criado com sucesso!']);

        return redirect()->route('painel-evento-listagem')->with(['type' => 'alert-danger', 'message' => 'Falha ao criar evento! tente novamente mais tarde.']);
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request){
        $request->validate([
            'id' => 'required'
        ]);

        $evento = Evento::find($request->id);
        if($evento->delete())
            return redirect()->route('painel-evento-listagem')->with(['type' => 'alert-success', 'message' => 'Evento removido com sucesso!']);

        return redirect()->route('painel-evento-listagem')->with(['type' => 'alert-danger', 'message' => 'Falha ao remover evento! tente novamente mais tarde.']);
    }

    /**
     * @return Factory|View
     */
    public function editar($id){
        $evento = Evento::find($id);
        return view('painel.evento.editar', ['evento' => $evento]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request){
        $request->validate([
            'id' => 'required',
            'titulo' => 'required|max:255',
            'observacao' => 'nullable|max:255'
        ]);

        $evento = Evento::find($request->id);
        $evento->titulo = $request->titulo;
        $evento->observacoes = $request->observacao;

        if($evento->save())
            return redirect()->route('painel-evento-editar', $evento->id)->with(['type' => 'alert-success', 'message' => 'Evento atualizado com sucesso!']);

        return redirect()->route('painel-evento-editar', $evento->id)->with(['type' => 'alert-danger', 'message' => 'Falha ao atualizar evento! tente novamente mais tarde.']);
    }
}
