<?php

namespace App\Http\Controllers;

use App\Janta;
use App\JantaParticipantes;
use App\User;
use Illuminate\Http\Request;
use function redirect;
use function view;

class JantaParticipantesController extends Controller
{
    /**
     * JantaParticipantesController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'presidente-secretario']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function participantes($id){
        $janta = Janta::find($id);
        $participantes = JantaParticipantes::all();
        return view('painel.janta.participantes', ['janta' => $janta, 'participantes' => $participantes]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
        $request->validate([
            'associado_id' => 'required',
            'janta_id' => 'required',
        ]);

        $participante = new JantaParticipantes();
        $participante->janta_id = $request->janta_id;
        $participante->users_id = $request->associado_id;

        $user = User::find($participante->users_id);
        $user->janta = true;
        $user->save();

        if($participante->save())
            return redirect()->route('painel-janta-participantes', $request->janta_id)->with(['type' => 'alert-success', 'message' => 'Associado registrado com sucesso!']);

        return redirect()->route('painel-janta-participantes', $request->janta_id)->with(['type' => 'alert-danger', 'message' => 'Falha ao registrar associado! tente novamente mais tarde.']);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $id){
        $request->validate([
            'janta_id' => 'required'
        ]);

        $jantaParticipante = JantaParticipantes::find($request->janta_id);

        $user = User::find($jantaParticipante->users_id);
        $user->janta = false;
        $user->save();

        if($jantaParticipante->delete())
            return redirect()->route('painel-janta-participantes', $id)->with(['type' => 'alert-success', 'message' => 'Participante removido com sucesso!']);

        return redirect()->route('painel-janta-participantes', $id)->with(['type' => 'alert-danger', 'message' => 'Falha ao remover participante! tente novamente mais tarde.']);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function alteraPagou(Request $request, $id){
        $request->validate([
            'associado_id' => 'required'
        ]);

        $jantaParticipante = JantaParticipantes::all()->where('janta_id', '=', $id)->where('users_id', '=', $request->associado_id)->first();
        $jantaParticipante->pago = !$jantaParticipante->pago;

        if($jantaParticipante->save())
            return redirect()->route('painel-janta-participantes', $id)->with(['type' => 'alert-success', 'message' => 'Informação alterada com sucesso!']);

        return redirect()->route('painel-janta-participantes', $id)->with(['type' => 'alert-danger', 'message' => 'Falha ao alterar participação! tente novamente mais tarde.']);
    }
}
