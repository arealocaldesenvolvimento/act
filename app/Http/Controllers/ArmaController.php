<?php

namespace App\Http\Controllers;

use App\Arma;
use App\Calibre;
use App\TipoArma;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use function view;

class ArmaController extends Controller
{
    public function __construct()
    {
        $this->middleware('presidente-secretario')->except(['minhasArmas', 'cadastroArmaAssoc', 'storeArmaAssoc', 'editarArma', 'updateArma', 'destroyArma']);
    }

    public function index()
    {
        return view('painel.armas.menu');
    }

    public function minhasArmas()
    {
        return view('painel.armas.minhas-armas', ['armas' => Arma::where('users_id', Auth::user()->id)->get()]);
    }

    public function listagemArma()
    {
        return view('painel.armas.arma', ['armas' => Arma::where('users_id', 'clube')->get()]);
    }

    public function cadastroArmaAssoc()
    {
        return view('painel.armas.cadastro-arma-assoc');
    }

    public function cadastroArmaAdmin()
    {
        return view('painel.armas.cadastro-arma-admin');
    }

    public function storeArmaAdmin(Request $request)
    {
        $data = $request->validate([
            'tipo_arma'     => 'required|string',
            'calibre'       => 'required|string',
            'calibre_restrito' => 'required|string',
            'marca'         => 'required|string',
            'modelo'        => 'required|string',
            'numero_serie'  => 'required|string',
            'numero_sigma'  => 'required|string',
            'numero_gte'    => 'nullable|string',
            'validade'      => 'nullable',
        ]);

        $data['marca']    = strtoupper($data['marca']);
        $data['modelo']   = strtoupper($data['modelo']);
        $data['users_id'] = 'clube';

        $arma = new Arma();
        $arma->fill($data);

        $arma->save();

        return redirect()->route('painel-arma-listagem')->with(['type' => 'alert-success', 'message' => 'Arma cadastrada com sucesso!']);
    }


    public function storeArmaAssoc(Request $request)
    {
        $data = $request->validate([
            'tipo_arma'     => 'required|string',
            'calibre'       => 'required|string',
            'calibre_restrito' => 'required|string',
            'marca'         => 'required|string',
            'modelo'        => 'required|string',
            'numero_serie'  => 'required|string',
            'numero_sigma'  => 'required|string',
            'numero_gte'    => 'nullable|string',
            'validade'      => 'required',
        ]);

        $data['marca']    = strtoupper($data['marca']);
        $data['modelo']   = strtoupper($data['modelo']);
        $data['users_id'] = Auth::user()->id;

        $arma = new Arma();
        $arma->fill($data);
        $arma->save();

        return redirect()->route('painel-minhas-armas')->with(['type' => 'alert-success', 'message' => 'Arma cadastrada com sucesso!']);
    }

    public function editarArma($id)
    {
        $arma = Arma::find($id);

        if (!$arma) {
            return view('errors.404');
        }
        return view('painel.armas.editar-arma', ['arma' => $arma]);
    }

    public function updateArma($id, Request $request)
    {
        try {
            $data = $request->validate([
                'tipo_arma'     => 'required',
                'calibre'       => 'required|string',
                'calibre_restrito' => 'required|string',
                'marca'         => 'required|string',
                'modelo'        => 'required|string',
                'numero_serie'  => 'required|string',
                'numero_sigma'  => 'required|string',
                'numero_gte'    => 'nullable|string',
                'validade'      => 'required',
            ]);

            if (!$arma = Arma::find($id)) {
                if (Auth::user()->tipo == 'secretario' || Auth::user()->tipo == 'presidente') {
                    return redirect()->route('painel-arma-listagem')->with(['type' => 'alert-danger', 'message' => 'Ocorreu um erro ao editar a arma. Tente novamente mais tarde!']);
                } else {
                    return redirect()->route('painel-minhas-armas')->with(['type' => 'alert-danger', 'message' => 'Ocorreu um erro ao editar a arma. Tente novamente mais tarde!']);
                }
            }
            $arma->update($data);
            if (Auth::user()->tipo == 'secretario' || Auth::user()->tipo == 'presidente') {
                return redirect()->route('painel-arma-listagem')->with(['type' => 'alert-success', 'message' => 'A arma foi atualizada com sucesso!']);
            } else {
                return redirect()->route('painel-minhas-armas')->with(['type' => 'alert-success', 'message' => 'A arma foi atualizada com sucesso!']);
            }
        } catch (Exception $ex) {
            dd($ex);
            Log::error('ArmaController:update() - ' . $ex->getMessage());
            if (Auth::user()->tipo == 'secretario' || Auth::user()->tipo == 'presidente') {
                return redirect()->route('painel-arma-listagem')->with(['type' => 'alert-danger', 'message' => 'Erro ao editar a arma. Tente novamente mais tarde.']);
            } else {
                return redirect()->route('painel-minhas-armas')->with(['type' => 'alert-danger', 'message' => 'Erro ao editar a arma. Tente novamente mais tarde.']);
            }
        }
    }


    public function destroyArma(Request $request)
    {
        try {
            $arma = Arma::findOrFail($request->armaId);
            $arma->delete();

            return response()->json(['titulo' => 'Sucesso', 'tipo' => 'success', 'mensagem' => 'Arma excluida com sucesso!']);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['Alerta' => 'Sucesso', 'tipo' => 'warning', 'mensagem' => 'Arma não encontrada.']);
        } catch (\Exception $ex) {
            Log::error('Erro ao excluir arma. ArmaController.destroyArma(). Erro:  ' . $ex->getMessage());
            return response()->json(['Erro' => 'Sucesso', 'tipo' => 'error', 'mensagem' => 'Erro ao excluir arma.']);
        }
    }


    public function listagemCalibre()
    {
        return view('painel.armas.calibre', ['calibres' => Calibre::get()]);
    }

    public function storeCalibre(Request $request)
    {
        $calibre = new Calibre();
        $calibre->nome = $request->calibre;
        $calibre->restrito = $request->restrito;
        $calibre->save();

        return response()->json(['message' => 'Calibre cadastrado com sucesso']);
    }

    public function destroyCalibre(Request $request)
    {
        try {
            $calibre = Calibre::findOrFail($request->calibreId);
            $calibre->delete();

            return response()->json(['titulo' => 'Sucesso', 'tipo' => 'success', 'mensagem' => 'Calibre excluido com sucesso!']);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['Alerta' => 'Sucesso', 'tipo' => 'warning', 'mensagem' => 'Calibre não encontrado.']);
        } catch (\Exception $ex) {
            Log::error('Erro ao excluir calibre. ArmaController.destroyCalibre(). Erro:  ' . $ex->getMessage());
            return response()->json(['Erro' => 'Sucesso', 'tipo' => 'error', 'mensagem' => 'Erro ao excluir calibre.']);
        }
    }



    public function listagemTipoArma()
    {
        return view('painel.armas.tipo-arma', ['tiposArma' => TipoArma::get()]);
    }

    public function storeTipoArma(Request $request)
    {
        $tipoArma = new TipoArma();
        $tipoArma->nome = $request->tipoArma;
        $tipoArma->save();

        return response()->json(['message' => 'Tipo de arma cadastrado com sucesso']);
    }

    public function destroyTipoArma(Request $request)
    {
        try {
            $tipoArma = TipoArma::findOrFail($request->tipoArmaId);
            $tipoArma->delete();

            return response()->json(['titulo' => 'Sucesso', 'tipo' => 'success', 'mensagem' => 'Tipo de arma excluido com sucesso!']);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['Alerta' => 'Sucesso', 'tipo' => 'warning', 'mensagem' => 'Tipo de arma não encontrado.']);
        } catch (\Exception $ex) {
            Log::error('Erro ao excluir tipo de arma. ArmaController.destroyTipoArma(). Erro:  ' . $ex->getMessage());
            return response()->json(['Erro' => 'Sucesso', 'tipo' => 'error', 'mensagem' => 'Erro ao excluir tipo de arma.']);
        }
    }

    public function armaByUser(Request $request)
    {
        $armas = Arma::where('users_id', $request->userId)->get();
        if ($armas->isEmpty()) {
            return response()->json(['mensagem' => 'O usuário não possui armas cadastradas.']);
        }
        return response()->json($armas);
    }
}
