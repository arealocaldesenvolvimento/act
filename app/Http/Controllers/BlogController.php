<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        $posts = Post::select('*')->orderBy('created_at', 'desc')->simplePaginate(9);
        return view('landing.noticias', ['posts' => $posts]);
    }

    public function show($id)
    {
        $post = Post::find($id);
        return view('landing.noticia', ['post' => $post]);
    }
}
