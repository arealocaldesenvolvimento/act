<?php

namespace App\Http\Controllers;

use App\Helpers\AppHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;
use function back;
use function dump;
use function view;

/**
 * Class CarteirinhaController
 * @package App\Http\Controllers
 */
class CarteirinhaController extends Controller
{
    /**
     * CarteirinhaController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        return view('painel.carteirinha.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function geraCarteirinha(Request $request){
        $request->validate([
            'foto' => 'required'
        ]);
        // if(AppHelper::getSaldo(Auth::id()) < 0)
        //     return back()->with(['type' => 'alert-warning', 'message' => 'Contas pendentes, entre em contato com o clube para regularizar.']);

        if($request->hasFile('foto'))
            $foto = $request->file('foto')->store('temp');

        return view('painel.carteirinha.modelo.carteirinha', ['foto' => $foto]);
    }
}
