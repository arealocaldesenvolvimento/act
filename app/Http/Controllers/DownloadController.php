<?php

namespace App\Http\Controllers;

use App\Documento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use function redirect;
use function view;

/**
 * Class DownloadController
 * @package App\Http\Controllers
 */
class DownloadController extends Controller
{
    /**
     * DownloadController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('presidente-secretario')->except(['listagemCli', 'download']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listagem(){
        return view('painel.downloads.listagem');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listagemCli(){
        return view('painel.downloads.listagem-cli');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
        $request->validate([
            'arquivo' => 'required'
        ]);

        if($request->hasFile('arquivo')){
            $documento = new Documento();
            $documento->diretorio = $request->file('arquivo')->store('downloads');
            $documento->nome_original = $request->file('arquivo')->getClientOriginalName();
            $documento->categoria = "download";
            if($documento->save())
                return redirect()->route('painel-download-listagem')->with(['type' => 'alert-success', 'message' => 'Documento salvo com sucesso!']);
        }
        return redirect()->route('painel-download-listagem')->with(['type' => 'alert-danger', 'message' => 'Falha ao salvar documento! tente novamente mais tarde.']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request){
        $request->validate([
            'id' => 'required'
        ]);

        $documento = Documento::find($request->id);

        if($documento->delete()){
            try {
                Storage::delete($documento->diretorio);
            } catch (\Throwable $th) {
                return redirect()->route('painel-download-listagem-cli')->with(['type' => 'alert-danger', 'message' => 'Falha ao baixar o arquivo do servidor!']);
            }
            return redirect()->route('painel-download-listagem')->with(['type' => 'alert-success', 'message' => 'Documento removido com sucesso!']);
        }
        return redirect()->route('painel-download-listagem')->with(['type' => 'alert-danger', 'message' => 'Falha ao salvar documento! tente novamente mais tarde.']);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function download($id){
        $documento = Documento::find($id);
        $documento->downloads++;
        $retorno;
        try {
            $retorno = Storage::download($documento->diretorio, $documento->nome_original);
        } catch (\Throwable $th) {
            return redirect()->route('painel-download-listagem-cli')->with(['type' => 'alert-danger', 'message' => 'Falha ao baixar o arquivo do servidor!']);
        }
        $documento->save();
        return $retorno;
    }
}
