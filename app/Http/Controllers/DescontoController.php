<?php

namespace App\Http\Controllers;

use App\Anuidade;
use App\Desconto;
use Illuminate\Http\Request;
use function count;
use function redirect;
use function view;

class DescontoController extends Controller
{
    /**
     * DescontoController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'presidente-secretario']);
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listagem(){
        if(count(Anuidade::all()) == 0)
            return redirect()->route('painel-anuidade-listagem')->with(['type' => 'alert-danger', 'message' => 'Tenha ao menos uma anuidade cadastrada para continuar!']);

        $anuidade = Anuidade::all()->where('ano', '=', date('Y'))->first();

        return view('painel.desconto.listagem', ['anuidade' => $anuidade]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
        $request->validate([
            'data' => 'required',
            'desconto' => 'required|min:0'
        ]);

        $desconto = new Desconto();
        $desconto->valor = $request->desconto;
        $desconto->data_fim = $request->data;

        if($desconto->save())
            return redirect()->route('painel-desconto-listagem')->with(['type' => 'alert-success', 'message' => 'Desconto criado com sucesso!']);

        return redirect()->route('painel-desconto-listagem')->with(['type' => 'alert-danger', 'message' => 'Falha ao criar desconto! tente novamente mais tarde.']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request){
        $request->validate([
            'id' => 'required'
        ]);

        $desconto = Desconto::find($request->id);
        if($desconto->delete())
            return redirect()->route('painel-janta-listagem')->with(['type' => 'alert-success', 'message' => 'Desconto removido com sucesso!']);

        return redirect()->route('painel-janta-listagem')->with(['type' => 'alert-danger', 'message' => 'Falha ao remover desconto! tente novamente mais tarde.']);
    }
}
