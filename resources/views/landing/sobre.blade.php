@extends('landing.index')
@section('content')
<section class="sobre container clearfix">
  @desktop
  <div class="left-right-content">
      <div class="left">
          <img src="{{ asset('images/sobre.png') }}" alt="sobre a associação">
      </div>
      <div class="right">
          <h1>SOBRE A ASSOCIAÇÃO</h1>
          <p>Fundada em 1990, a associação TIRO AO PRATO - RIO DO SUL conta hoje com
              dois estandes para a prática de tiro ao prato, sendo um de TRAP single e
              outro de TRAP double, e um estande para prática de tiro bala. Também
              possui máquinas para recarga de cartuchos para espingarda calibre 12 e
              para armas raiadas de vários calibres. Venha participar. Entre em
              contato conosco e associe-se.</p>
      </div>
  </div>
  @elsedesktop
      <h1>SOBRE A ASSOCIAÇÃO</h1>
      <img src="{{ asset('images/sobre.png') }}" alt="sobre a associação">
      <p>Fundada em 1990, a associação TIRO AO PRATO - RIO DO SUL conta hoje com dois estandes para a prática de tiro ao prato, sendo um de TRAP single e outro de TRAP double, e um estande para prática de tiro bala. Também possui máquinas para recarga de cartuchos para espingarda calibre 12 e para armas raiadas de vários calibres. Venha participar. Entre em contato conosco e associe-se.</p>
  @enddesktop
</section>
@endsection