@extends('landing.index')
@section('content')
  <section class="blog clearfix container">
    <h1>NOTÍCIAS</h1>
    <div class="left-right-content">
      <div class="left">
        @isset($posts)
          <div class="posts">
            @forelse($posts as $post)
                <article class="post-{{ $post->id }}">
                  <div class="thumbnail">
                    @if ($post->thumbnail)
                        <img src="{{ $post->thumbnail }}" alt="">
                    @else
                        <img class="logo" src="{{asset('images/logo.png')}}" alt="">
                    @endif
                  </div>
                  <div class="content">
                    <h2>{{ $post->title }}</h2>
                    <div class="text">
                        {!! $post->description !!}
                    </div>
                    <div class="ler">
                      <a href="{{ route('blog.show', $post->id) }}">Leia mais</a>
                    </div>
                  </div>
                </article>
                @empty
                <p>Nenhuma notícia cadastrada no momento...</p>
              @endforelse
          </div>
          @if ($posts->nextPageUrl())
            <div class="load-more">
              <a href="{{ $posts->nextPageUrl() }}">Carregar mais</a>
            </div>
          @endif
        @endisset
      </div>
      <div class="right">
        <div class="categorys">
          <ul>
            <h2>CATEGORIAS</h2>
            @foreach(\App\PostCategory::all()->sortBy('nome') as $category)
              <a href="{{ route('blog.category.index', $category->id) }}">
                <li>{{ $category->name }}</li>
              </a>
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </section>
@endsection

@section('script')
  <script>
    window.onload = async () => {
      const load_more = document.querySelector('.load-more a')

      if (load_more != null) {
        load_more.addEventListener('click', async event => {
        event.preventDefault()
        await $.get(event.target.href)
        .done( response => {
          const nextUrl = $(response).find('.load-more a').attr('href')
          if (nextUrl) {
            $('.load-more a').attr('href', nextUrl)
          } else {
            $('.load-more').css('display', 'none')
          }
          $('.posts').append(
            $(response).find('.posts').html()
          )
        })
      })
      }
    }
  </script>
@endsection
