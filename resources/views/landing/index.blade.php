<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('fonts/fontawesome/css/all.min.css') }}">
        <title>{{ env('APP_NAME') }}</title>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-50664921-4"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-60691549-37"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-60691549-37');
        </script>
    </head>
    <body class="{{ Request::segment(1) }}">
        @mobile
            <div class="menu-mobile">
                <div class="header">
                    <div class="burger-container">
                        <div id="burger">
                            <div class="bar topBar"></div>
                            <div class="bar btmBar"></div>
                        </div>
                    </div>
                    <div class="icon icon-apple"></div>
                    <ul class="menu">
                        <li class="menu-item"><a href="{{ route('contato') }}"><i class="fas fa-id-card-alt"></i>&nbsp; Contato</a></li>
                        <li class="menu-item"><a href="{{ route('sobre') }}"><i class="far fa-handshake"></i>&nbsp; Sobre</a></li>
                        <li class="menu-item"><a href="{{ route('links') }}"><i class="fas fa-link"></i>&nbsp; Links</a></li>
                        @guest
                            <li class="menu-item"><a href="{{ route('login') }}"><i class="fas fa-desktop"></i>&nbsp; Login</a></li>
                        @else
                            <li class="menu-item"><a href="{{ route('painel-index') }}"><i class="fas fa-desktop"></i>&nbsp; Painel - {{ explode(' ',trim(\Illuminate\Support\Facades\Auth::user()->nome))[0]  }}</a></li>
                        @endguest
                        <li class="menu-item"><a href="#" onclick="document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i> &nbsp; Sair</a></li>
                    </ul>
                </div>
            </div>
        @endmobile

        <nav>
            <div class="content">
                <div class="content-menu">
                    <a href="{{ route('contato') }}">Contato</a>
                    <a href="{{ route('sobre') }}">Sobre</a>
                    <a href="{{ route('links') }}">Links</a>
                </div>
                <a href="{{ route('index') }}">
                    <img src="{{ asset('images/logo.png') }}" alt="logomarca">
                </a>
                @desktop
                    @guest
                        <div class="options">
                            <a href="{{ route('login') }}" class="login"><i class="fas fa-user"></i> &nbsp;Login</a>
                        </div>
                    @else
                        <div class="options">
                            <a href="JavaScript:Void(0);" class="button-opcoes">Opções &nbsp;<i class="fas fa-caret-down"></i> </a>
                            <ul>
                                @guest
                                    <li class="menu-item"><a href="{{ route('login') }}"><i class="fas fa-desktop"></i>&nbsp; Login</a></li>
                                @else
                                    <li class="menu-item"><a href="{{ route('painel-index') }}"><i class="fas fa-desktop"></i>&nbsp; Painel - {{ explode(' ',trim(\Illuminate\Support\Facades\Auth::user()->nome))[0]  }}</a></li>
                                @endguest
                                <li class="menu-item"><a href="#" onclick="document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i> &nbsp; Sair</a></li>
                            </ul>
                        </div>
                    @endguest
                @enddesktop
            </div>
            <div class="rectangle"><div></div></div>
        </nav>
        <div class="banner">
            @if (Request::segment(1) === 'blog')
                <img src="{{ asset('images/blog.png') }}" alt="banner">
            @else
                <img src="{{ asset('images/banner.jpg') }}" alt="banner">
            @endif
        </div>

        <main class="py-4">
            @yield('content')
        </main>

        <footer>
            <img src="{{ asset('images/logo-rodape.png') }}" alt="logo rodape">
            <div class="arealocal"><a href="arealocal.com.br">Desenvolvido por: Área Local</a></div>
        </footer>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
        <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
        <script src="{{ asset('js/jquery.mask.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        <script>
            $('div.options').click(function () {
                $('div.options > ul').fadeToggle();
            });

            $('.burger-container').click(function () {
                $('.header').toggleClass('menu-opened')
            });
        </script>
        @yield('script')
    </body>
</html>
