@extends('landing.index')
@section('content')
@isset($links)

<section class="links container clearfix">
  <h1>LINKS ÚTEIS</h1>
  <ul>
    @forelse($links as $link)
      <li>
        <a class="expand" href="#">
          <h2>{{$link->titulo}}</h2>
          <span class="down">
            <i class="fas fa-angle-down"></i>
          </span>
        </a>
        <div class="hidden">
          <p>Email: {{ $link->email }}</p>
          <p>Telefone: {{ $link->telefone }}</p>
          <p> Link: <a target="_blank" href="https://{{ $link->link }}">{{ $link->link }}</a></p>
          <p>{{ $link->descricao }}</p>
        </div>
      </li>
    @empty
      <p>Não há links cadastrados no momento...</p>
    @endforelse
  </ul>
</section>
@endisset
@endsection

@section('script')
  <script>
    window.onload = () => {
        const btndown = document.querySelectorAll('li a.expand')

        btndown.forEach(item => {
          item.addEventListener('click', event => {
            event.preventDefault()
            if ( event.target.parentElement.nextElementSibling.classList.contains('show') ) {
              event.target.nextElementSibling.classList.remove('active')
              event.target.parentElement.nextElementSibling.classList.remove('show')
            } else {
              event.target.parentElement.parentElement.classList.add('active')
              event.target.parentElement.nextElementSibling.classList.add('show')
              event.target.nextElementSibling.classList.add('active')
            }
          })
        });

    }
  </script>
@endsection