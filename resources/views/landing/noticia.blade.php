@extends('landing.index')
@section('content')
  @isset($post)
    <section class="single container clearfix">
      <div class="left-right-content">
        <div class="left">
          <article>
            <h2>{{ $post->title }}</h2>
            <div class="thumbnail">
              @if ($post->thumbnail) 
                <img src="{{ $post->thumbnail }}" alt="">
              @else 
              <img class="logo" src="{{asset('images/logo.png')}}" alt="">
              @endif
            </div>
            <div class="content">
              {!! $post->description !!}
            </div>
          </article>
        </div>
        <div class="right">
          <div class="categorys">
            <ul>
              <h2>CATEGORIAS</h2>
              @foreach(\App\PostCategory::all() as $category)
                <a href="{{ route('blog.category.index', $category->id) }}">
                  <li>{{ $category->name }}</li>
                </a>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    </section>
  @endisset
@endsection