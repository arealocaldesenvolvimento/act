<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <title>{{ env('APP_NAME') }} - VALIDA DECLARAÇÕES</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="{{ asset('fonts/fontawesome/css/all.min.css')  }}">
        <!-- Material Kit CSS -->
        <link href="{{ asset('css/material-dashboard.min.css?v=2.1.1') }}" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('css/bootstrap-select.min.css') }}">

        <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
        <link href="{{asset('css/select2-materialize.css')}}" rel="stylesheet" />

        <style>
            .btn-view-document {
                display: inline-block;
                padding: 10px 20px;
                background-color: #00bcd4;
                color: #fff;
                text-decoration: none;
                font-size: 16px;
                border-radius: 5px;
                transition: all .3s;
            }

            .btn-view-document .icon {
                margin-right: 5px;
                font-size: 18px;
            }

            .btn-view-document:hover {
                transform: translateY(-10px);

                background-color: #008ba3;
                color: #000;
                box-shadow: 0 14px 26px -12px rgba(6, 219, 219, 0.42), 0 4px 23px 0px rgba(0,0,0,0.12), 0 8px 10px -5px rgba(76, 175, 157, 0.2);
            }
            </style>
    </head>

    <body>
        <div class="container">
            <div class="text-center w-100 my-5">
                <a href="{{route('index')}}">
                    <img src="{{asset('images/logo.png')}}">
                </a>
            </div>
            <div class="row">
                <div class="col-12 col-md-8 @notmobile offset-2 @endnotmobile">
                    <div class="card">
                        <div class="card-header card-header-text card-header-primary">
                            <div class="card-text">
                                <h4 class="card-title">VALIDAÇÃO DE DECLARAÇÕES</h4>
                            </div>
                        </div>
                        <div class="card-body">
                           <form method="get">
                               <div class="row">
                                   <div class="col-12 col-md-8">
                                       <div class="form-group bmd-form-group">
                                           <label>CÓDIGO DA DECLARAÇÃO</label>
                                           <input type="text" class="form-control mask-onlynumber" name="codigo" required maxlength="15" value="@isset($declaracao){{$declaracao->id ?? ""}}@endisset">
                                       </div>
                                   </div>
                                   <div class="col-12 col-md-4">
                                       <button type="submit" class="btn btn-success btn-block">BUSCAR</button>
                                   </div>
                               </div>
                           </form>
                        </div>
                    </div>
                </div>
            </div>
            @isset($declaracao)
                @if($declaracao != false)
                <div class="row">
                    <div class="col-12 col-md-8 @notmobile offset-2 @endnotmobile">
                        <div class="card m-0">
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div class="author">
                                            @if(now() < $declaracao->data_validade)
                                                <img width="40" src="{{asset('images/valido.svg')}}" alt="arquivo doc" style="vertical-align: middle">
                                                <span style="font-size: 20px; vertical-align: middle" class="text-success"> <strong>CERTIFICADO VÁLIDO</strong></span>
                                            @else
                                                <img width="40" src="{{asset('images/invalido.svg')}}" alt="arquivo doc" style="vertical-align: middle">
                                                <span style="font-size: 20px; vertical-align: middle" class="text-danger"> <strong>CERTIFICADO INVÁLIDO</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4 text-muted @notmobile text-right @elsenotmobile mt-2 text-left @endnotmobile">
                                        Disponibilizado em: {{\Carbon\Carbon::parse($declaracao->created_at)->format('d/m/Y')}}
                                        @mobile
                                        <br/>
                                        @endmobile
                                        <span class="@if(now() < $declaracao->data_validade) text-dark @else text-danger @endif">Válido até: {{\Carbon\Carbon::parse($declaracao->data_validade)->format('d/m/Y')}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                @notmobile
                                    <span class="text-muted">Emitido por&nbsp;</span>{{\App\User::find($declaracao->users_id)->nome}} <span class="text-muted">&nbsp;portador do CR:&nbsp;</span> {{\App\User::find($declaracao->users_id)->cr}}
                                    <div class="stats ml-auto" title="TOTAL DE DOWNLOADS">
                                        <span class="text-muted text-uppercase">Tipo:&nbsp;{{$declaracao->tipo}}</span>
                                    </div>
                                @elsenotmobile
                                    <div class="row">
                                        <div class="col-12">
                                            <span class="text-muted">Emitido por&nbsp;</span>{{\App\User::find($declaracao->users_id)->nome}} <span class="text-muted">&nbsp;portador do CR:&nbsp;</span> {{\App\User::find($declaracao->users_id)->cr}}
                                        </div>
                                        <div class="col-12">
                                            <span class="text-muted text-uppercase">Tipo:&nbsp;{{$declaracao->tipo}}</span>
                                        </div>
                                    </div>
                                @endnotmobile
                            </div>
                            @if ($declaracao->nome_arquivo)
                                <a href="{{ route('painel-ver-documento', $declaracao->id) }}" target="_blank" class="btn-view-document">
                                    <span class="icon">&#x2197;</span> Visualizar o documento
                                </a>
                            @endif

                        </div>
                    </div>
                </div>
                @endif
            @else
                <div class="alert alert-danger" role="alert">
                    Declaração não localizada!
                </div>
            @endisset
            <div class="row mt-5 mb-5">
                <div class="col text-center">
                    <p style="line-height: 1.4; margin: 0" class="text-muted">Para mais informações entre em contato com o clube ou acesse <a href="{{route('index')}}">www.clubedetirorsl.com.br</a></p>
                    <p style="line-height: 1.4; margin: 0" class="text-muted">(47) 3521 1974 - contferreira@uol.com.br</p>
                    <p style="line-height: 1.4; margin: 0" class="text-muted">Rua Antonio Dolzani, nº 970 -
                        Bairro Valada Sao Paulo - Rio do Sul - SC - 89162.190</p>
                </div>
            </div>
        </div>
    </body>

    <!--   Core JS Files   -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-material-design.min.js') }}"></script>

    <!-- Plugin for the Perfect Scrollbar -->
    <script src="{{ asset('js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>

    {{--<!-- Plugin for the momentJs  -->--}}
    {{--<script src="{{ asset('js/plugins/moment.min.js') }}"></script>--}}

    <!--  Plugin for Sweet Alert -->
    <script src="{{ asset('js/plugins/sweetalert2.js') }}"></script>

    {{--<!-- Forms Validations Plugin -->--}}
    {{--<script src="{{ asset('js/plugins/jquery.validate.min.js') }}"></script>--}}

    {{--<!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->--}}
    {{--<script src="{{ asset('js/plugins/jquery.bootstrap-wizard.js') }}"></script>--}}

    <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
    <script src="{{ asset('js/plugins/bootstrap-selectpicker.js') }}" ></script>

    {{--<!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->--}}
    {{--<script src="{{ asset('js/plugins/bootstrap-datetimepicker.min.js') }}"></script>--}}

    {{--<!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->--}}
    {{--<script src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>--}}

    <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
    <script src="{{ asset('js/plugins/bootstrap-tagsinput.js') }}"></script>

    <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
    <script src="{{ asset('js/plugins/jasny-bootstrap.min.js') }}"></script>

    {{--<!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->--}}
    {{--<script src="{{ asset('js/plugins/fullcalendar.min.js') }}"></script>--}}

    {{--<!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->--}}
    {{--<script src="{{ asset('js/plugins/jquery-jvectormap.js') }}"></script>--}}

    {{--<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->--}}
    {{--<script src="{{ asset('js/plugins/nouislider.min.js') }}" ></script>--}}

    {{--<!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>--}}

    {{--<!-- Library for adding dinamically elements -->--}}
    {{--<script src="{{ asset('js/plugins/arrive.min.js') }}"></script>--}}

    <!-- Chartist JS -->
    <script src="{{ asset('js/plugins/chartist.min.js') }}"></script>

    {{--<!--  Notifications Plugin    -->--}}
    {{--<script src="{{ asset('js/plugins/bootstrap-notify.js') }}"></script>--}}

    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{ asset('js/material-dashboard.js?v=2.1.1') }}" type="text/javascript"></script>

    <!-- FUNÇÕES AREA LOCAL -->
    <script src="{{ asset('js/funcoes.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="{{asset('js/select2.full.min.js')}}"></script>
    <script src="{{asset('js/select2-materialize.js')}}"></script>
    <script src="{{asset('js/jquery.mask.js')}}"></script>

    @yield('script')

</html>
