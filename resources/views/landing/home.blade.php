@extends('landing.index')
@section('content')

{{--        <section class="faca-parte-equipe">--}}
{{--            <img src="{{ asset('images/faca-parte-equipe.png') }}" alt="faça parte da equipe">--}}
{{--        </section>--}}
        <section class="contato container">
            <h1>ENTRE EM CONTATO</h1>
            <div class="left">
                <p><strong>CONTATO | WHATSAPP</strong></p>
                <p><i class="fab fa-whatsapp"></i> (47) 3521 1974</p>
                <br/>

                <p><strong>E-MAIL</strong></p>
                <p><i class="fas fa-envelope"></i> contferreira@uol.com.br</p>
                <br/>

                <p><strong>NOSSO ENDREREÇO</strong></p>
                <p><i class="fas fa-map-marked-alt"></i> Rua Antonio Dolzani, nº 970 - <br/> Bairro Valada Sao Paulo - Rio do Sul - SC - 89162.190</p>
            </div>
{{--            <div class="right">--}}
{{--                <div class="form-contato">--}}
{{--                    <form class="contato">--}}
{{--                        @csrf--}}
{{--                        <input type="text" placeholder="Nome:" name="nome" class="nome" required>--}}
{{--                        <input type="email" placeholder="E-mail:" name="email" class="email" required>--}}
{{--                        <input type="tel" placeholder="Telefone:" name="telefone" class="telefone" required>--}}
{{--                        <select name="uf" class="uf" id="estados" required>--}}
{{--                            <option disabled selected>UF:</option>--}}
{{--                        </select>--}}
{{--                        <select name="cidade" class="cidade" id="cidades" required>--}}
{{--                            <option disabled selected>Cidade:</option>--}}
{{--                        </select>--}}
{{--                        <textarea placeholder="Digite aqui sua mensagem:" name="mensagem" class="mensagem" rows="4" required></textarea>--}}
{{--                        <button type="submit" class="enviar">Enviar</button>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
        </section>
{{--        <section class="parte-equipe container" style="display: none">--}}
{{--            <div class="fechar">--}}
{{--                <i class="fas fa-times"></i> Fechar--}}
{{--            </div>--}}
{{--            <h1>Envia sua solicitação para entrar em nossa equipe</h1>--}}
{{--            <div class="form-contato">--}}
{{--                <form class="contato">--}}
{{--                    @csrf--}}
{{--                    <input type="hidden" value="true" name="equipe">--}}
{{--                    <input type="text" placeholder="Nome:" name="nome" class="nome" required>--}}
{{--                    <input type="email" placeholder="E-mail:" name="email" class="email" required>--}}
{{--                    <input type="tel" placeholder="Telefone:" name="telefone" class="telefone" required>--}}
{{--                    <select name="uf" class="uf" id="estados" required>--}}
{{--                        <option disabled selected>UF:</option>--}}
{{--                    </select>--}}
{{--                    <select name="cidade" class="cidade" id="cidades" required>--}}
{{--                        <option disabled selected>Cidade:</option>--}}
{{--                    </select>--}}
{{--                    <textarea placeholder="Digite aqui sua mensagem:" name="mensagem" class="mensagem" rows="4" required></textarea>--}}
{{--                    <button type="submit" class="enviar">Enviar</button>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </section>--}}
        <section class="maps">
            <div class="blank"></div>
            <iframe src="https://www.google.com/maps/d/embed?mid=1oCkGqTWyUQEb0MsxkwMVKorAdkfjbfbL" width="100%" height="480"></iframe>
        </section>
@endsection

@section('script')
<script>
  $(document).ready(()=>{

    var telefoneMaskBehavior = val =>
        val.replace(/\D/g, '').length <= 10 ? '(00) 0000-00009' : '(00) 0 0000-0000';

    telefoneOptions = {
        onKeyPress: function (val, e, field, options) {
            field.mask(telefoneMaskBehavior.apply({}, arguments), options);
        }
    };

    // Coloca mask nos campos:
    $(':input[name=telefone]').mask(telefoneMaskBehavior, telefoneOptions);

    $('form.contato').submit(function (event) {
        event.preventDefault();
        var enviar = $('button.enviar');

        enviar.prop('disabled', true).html('<i class="fas fa-spinner fa-spin"></i> Enviando...');
        $.post('{{ route('email-contato') }}', $(this).serialize())
        .done(function () {
            Swal.fire({
                type: 'success',
                title: 'Contato efetuado com sucesso!',
            });
            $('form.contato')[0].reset();
        })
        .fail(function () {
            Swal.fire(
                'Ooops!',
                'Falha ao entrar em contato, tente novamente mais tarde ou utilize outro meio.',
                'error'
            )
        });

        enviar.prop('disabled', false).html('Enviar');
    });

    $.getJSON('{{ asset('estados_cidades.json') }}', function (data) {
        var items = [];
        var options = '<option selected disabled>UF: </option>';
        $.each(data, function (key, val) {
            options += '<option value="' + val.nome + '">' + val.sigla + '</option>';
        });
        $("#estados").html(options);

        $("#estados").change(function () {

            var options_cidades = '<option selected disabled>Cidade: </option>';
            var str = "";

            $("#estados option:selected").each(function () {
                str += $(this).text();
            });

            $.each(data, function (key, val) {
                if(val.sigla == str) {
                    $.each(val.cidades, function (key_city, val_city) {
                        options_cidades += '<option value="' + val_city + '">' + val_city + '</option>';
                    });
                }
            });
            $("#cidades").html(options_cidades);

        }).change();

    });

    $('section.faca-parte-equipe > img').click(function () {
        $('section.parte-equipe').show('slow');
        $('section.contato').hide('slow');
    });
    
    $('section.parte-equipe div.fechar').click(function () {
        $('section.parte-equipe').hide('slow');
        $('section.contato').show('slow');
    });

    $('.burger-container').click(function () {
      $('.header').toggleClass('menu-opened')
    });
  });
</script>
@endsection