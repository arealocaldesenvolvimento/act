<style>
    .sidebar .nav i.icon-menu{
        color: #3C4858;
    }

    .sidebar .nav .nav-item.active  i.icon-menu img{
        filter: brightness(0) saturate(100%) invert(100%) sepia(0%) saturate(7500%) hue-rotate(50deg) brightness(112%) contrast(112%);
    }


</style>
@if (Route::current()->getName() !== 'atualizar-dados')
    <ul class="nav">
        @if (\Illuminate\Support\Facades\Auth::user()->tipo != 'auxiliar')
            <li class="nav-item  @if (Route::current()->getName() == 'painel-index') active @endif">
                <a class="nav-link" href="{{ route('painel-index') }}">
                    <i class="material-icons icon-menu">dashboard</i>
                    <p>Início</p>
                </a>
            </li>
            @if (\Illuminate\Support\Facades\Auth::user()->tipo == 'tesoureiro')
                <li class="nav-item  @if (Route::current()->getName() == 'painel-conta-listagem' or
                        Route::current()->getName() == 'painel-conta-historico' or
                        Route::current()->getName() == 'painel-conta-debitar-creditar') active @endif">
                    <a class="nav-link" href="{{ route('painel-conta-listagem') }}">
                        <i class="material-icons icon-menu">credit_card</i>
                        <p>Contas</p>
                    </a>
                </li>
            @endif
            <li class="nav-item  @if (Route::current()->getName() == 'painel-conta-historico') active @endif">
                <a class="nav-link"
                    href="{{ route('painel-conta-historico', \Illuminate\Support\Facades\Auth::id()) }}">
                    <i class="material-icons icon-menu">attach_money</i>
                    <p>Hist. Financeiro</p>
                </a>
            </li>
            <li class="nav-item  @if (Route::current()->getName() == 'painel-habitualidade-listagem-associado') active @endif">
                <a class="nav-link" href="{{ route('painel-habitualidade-listagem-associado') }}">
                    <i class="material-icons icon-menu">fingerprint</i>
                    <p>Habitualidades</p>
                </a>
            </li>
            <li class="nav-item  @if (Route::current()->getName() == 'painel-minhas-armas' or
                    Route::current()->getName() == 'painel-minha-arma-cadastro' or
                    Route::current()->getName() == 'painel-arma-store-assoc' or
                    Route::current()->getName() == 'painel-arma-cadastro' or
                    Route::current()->getName() == 'painel-armas') active @endif">
                <a class="nav-link" href="{{ route('painel-minhas-armas') }}">
                    <i class="material-icons icon-menu">
                        <img width="30" class="mb-3" src="{{asset('images/arma.svg')}}">
                    </i>
                    <p>Armas</p>
                </a>
            </li>
            <li class="nav-item  @if (Route::current()->getName() == 'painel-download-listagem-cli') active @endif">
                <a class="nav-link" href="{{ route('painel-download-listagem-cli') }}">
                    <i class="material-icons icon-menu">cloud_download</i>
                    <p>Downloads</p>
                </a>
            </li>
            <li class="nav-item  @if (Route::current()->getName() == 'painel-declaracoes-listagem-cli') active @endif">
                <a class="nav-link" href="{{ route('painel-declaracoes-listagem-cli') }}">
                    <i class="material-icons icon-menu">card_membership</i>
                    <p>Declarações</p>
                </a>
            </li>
            <li class="nav-item  @if (Route::current()->getName() == 'painel-usuario-meus-dados') active @endif">
                <a class="nav-link" href="{{ route('painel-usuario-meus-dados') }}">
                    <i class="material-icons icon-menu">account_circle</i>
                    <p>Meus Dados</p>
                </a>
            </li>
            <li class="nav-item  @if (Route::current()->getName() == 'painel-declaracoes-carteirinha') active @endif">
                <a class="nav-link" href="{{ route('painel-declaracoes-carteirinha') }}">
                    <i class="material-icons icon-menu">assignment_ind</i>
                    <p>Carteirinha</p>
                </a>
            </li>
            <li class="nav-item  @if (Route::current()->getName() == 'painel-janta-associado') active @endif">
                <a class="nav-link" href="{{ route('painel-janta-associado') }}">
                    <i class="material-icons icon-menu">restaurant</i>
                    <p>Janta</p>
                </a>
            </li>
        @endif
        <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="material-icons icon-menu">exit_to_app</i>
                Sair
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
@endif
