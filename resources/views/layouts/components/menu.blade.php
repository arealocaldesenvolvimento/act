<style>

.sidebar .nav i.icon-menu{
    color: #3C4858;
}
.sidebar .nav .nav-item.active  i.icon-menu img{
    filter: brightness(0) saturate(100%) invert(100%) sepia(0%) saturate(7500%) hue-rotate(50deg) brightness(112%) contrast(112%);
}
</style>
<ul class="nav">
    <li class="nav-item  @if(Route::current()->getName() == 'painel-index') active @endif">
        <a class="nav-link" href="{{ route('painel-index') }}">
            <i class="material-icons icon-menu">dashboard</i>
            <p>Início</p>
        </a>
    </li>
    <li class="nav-item  @if(Route::current()->getName() == 'painel-tipo-arma-listagem' or
                            Route::current()->getName()  == 'painel-calibre-listagem' or
                            Route::current()->getName()  == 'painel-arma-listagem' or
                            Route::current()->getName()  == 'painel-arma-cadastro' or
                            Route::current()->getName()  == 'painel-minhas-armas' or
                            Route::current()->getName()  == 'painel-minha-arma-cadastro' or
                            Route::current()->getName()  == 'painel-tipo-arma-listagem' or
                            Route::current()->getName()  == 'painel-armas') active @endif">
        <a class="nav-link" href="{{ route('painel-armas') }}">
            <i class="material-icons icon-menu">
                <img width="30" class="mb-3" src="{{asset('images/arma.svg')}}">
            </i>
            <p>Armas</p>
        </a>
    </li>
    <li class="nav-item  @if(Route::current()->getName() == 'painel-habitualidade-listagem' or
                            Route::current()->getName() == 'painel-habitualidade-cadastro') active @endif">
        <a class="nav-link" href="{{ route('painel-habitualidade-listagem') }}">
            <i class="material-icons icon-menu">fingerprint</i>
            <p>Habitualidade</p>
        </a>
    </li>
    <li class="nav-item  @if(Route::current()->getName() == 'painel-habitualidade-imprimir-qr-code') active @endif">
        <a class="nav-link" href="{{ route('painel-habitualidade-imprimir-qr-code') }}">
        <i class="material-icons icon-menu">qr_code</i>
        <p>QR code</p>
        </a>
    </li>
    <li class="nav-item  @if(Route::current()->getName() == 'painel-conta-listagem' or
                            Route::current()->getName() == 'painel-conta-historico' or
                            Route::current()->getName() == 'painel-conta-debitar-creditar') active @endif">
        <a class="nav-link" href="{{ route('painel-conta-listagem') }}">
            <i class="material-icons icon-menu">credit_card</i>
            <p>Contas</p>
        </a>
    </li>
    <li class="nav-item @if(Route::current()->getName() == 'painel-usuarios-listagem' or
                Route::current()->getName() == 'painel-usuarios-cadastro' or
                Route::current()->getName() == 'painel-usuarios-editar' or
                Route::current()->getName() == 'painel-usuarios-lixeira' ) active @endif">
        <a class="nav-link" href="{{ route('painel-usuarios-listagem') }}">
            <i class="material-icons icon-menu">people_alt</i>
            <p>Usuários</p>
        </a>
    </li>
    <li class="nav-item  @if(Route::current()->getName() == 'painel-sobre-clube') active @endif">
        <a class="nav-link" href="{{ route('painel-sobre-clube') }}">
            <i class="material-icons icon-menu">storefront</i>
            <p>Clube</p>
        </a>
    </li>
    {{-- <li class="nav-item  @if(Route::current()->getName() == 'painel-certificado') active @endif">
        <a class="nav-link" href="{{ route('painel-certificado') }}">
            <i class="material-icons icon-menu">key</i>
            <p>Certificado</p>
        </a>
    </li> --}}
    <li class="nav-item  @if(Route::current()->getName() == 'painel-anuidade-listagem' or
                            Route::current()->getName() == 'painel-anuidade-cadastro' or
                            Route::current()->getName() == 'painel-desconto-listagem' or
                            Route::current()->getName() == 'painel-desconto-cadastro') active @endif">
        <a class="nav-link" href="{{ route('painel-anuidade-listagem') }}">
            <i class="material-icons icon-menu">attach_money</i>
            <p>Anuidade</p>
        </a>
    </li>
{{--    <li class="nav-item  @if(Route::current()->getName() == 'painel-desconto-listagem' or--}}
{{--                            Route::current()->getName() == 'painel-desconto-cadastro') active @endif">--}}
{{--        <a class="nav-link" href="{{ route('painel-desconto-listagem') }}">--}}
{{--            <i class="material-icons icon-menu">event</i>--}}
{{--            <p>Descontos</p>--}}
{{--        </a>--}}
{{--    </li>--}}
    <li class="nav-item  @if(Route::current()->getName() == 'painel-evento-listagem' or
                             Route::current()->getName() == 'painel-evento-cadastro') active @endif">
        <a class="nav-link" href="{{ route('painel-evento-listagem') }}">
            <i class="material-icons icon-menu">emoji_events</i>
            <p>Eventos</p>
        </a>
    </li>
    <li class="nav-item  @if(Route::current()->getName() == 'painel-janta-listagem' or
                            Route::current()->getName() == 'painel-janta-participantes') active @endif">
        <a class="nav-link" href="{{ route('painel-janta-listagem') }}">
            <i class="material-icons icon-menu">restaurant</i>
            <p>Janta</p>
        </a>
    </li>
    <li class="nav-item  @if(Route::current()->getName() == 'painel-janta-associado') active @endif">
        <a class="nav-link" href="{{ route('painel-janta-associado') }}">
            <i class="material-icons icon-menu">restaurant_menu</i>
            <p>Minha Janta</p>
        </a>
    </li>
    <li class="nav-item  @if(Route::current()->getName() == 'painel-download-listagem') active @endif">
        <a class="nav-link" href="{{ route('painel-download-listagem') }}">
            <i class="material-icons icon-menu">cloud_download</i>
            <p>Gerenciar Down.</p>
        </a>
    </li>
    <li class="nav-item  @if(Route::current()->getName() == 'painel-download-listagem-cli') active @endif">
        <a class="nav-link" href="{{ route('painel-download-listagem-cli') }}">
            <i class="material-icons icon-menu">cloud_download</i>
            <p>Downloads</p>
        </a>
    </li>
    <li class="nav-item  @if(Route::current()->getName() == 'painel-declaracoes-listagem-cli') active @endif">
        <a class="nav-link" href="{{ route('painel-declaracoes-listagem-cli') }}">
            <i class="material-icons icon-menu">card_membership</i>
            <p>Declarações</p>
        </a>
    </li>
    <li class="nav-item  @if(Route::current()->getName() == 'painel-declaracoes-carteirinha') active @endif">
        <a class="nav-link" href="{{ route('painel-declaracoes-carteirinha') }}">
            <i class="material-icons icon-menu">assignment_ind</i>
            <p>Carteirinha</p>
        </a>
    </li>
    <li class="nav-item  @if(Route::current()->getName() == 'painel-usuario-meus-dados') active @endif">
        <a class="nav-link" href="{{ route('painel-usuario-meus-dados') }}">
            <i class="material-icons icon-menu">face</i>
            <p>Meus Dados</p>
        </a>
    </li>

    <ul class="nav institucional border pb-3">
        <h3 class="text-center">Institucional</h3>
        <li class="nav-item  @if(Route::current()->getName() == 'noticias.index') active @endif">
            <a class="nav-link" href="{{ route('noticias.index') }}">
                <i class="material-icons icon-menu">create</i>
                <p>Notícias</p>
            </a>
        </li>
        <li class="nav-item  @if(Route::current()->getName() == 'link.index') active @endif">
            <a class="nav-link" href="{{ route('link.index') }}">
                <i class="material-icons icon-menu">link</i>
                <p>Links</p>
            </a>
        </li>
    </ul>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="material-icons icon-menu">exit_to_app</i>
            Sair
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </li>
</ul>
