<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>{{ env('APP_NAME') }} - @yield('title')</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="{{ asset('fonts/fontawesome/css/all.min.css')  }}">
    <!-- Material Kit CSS -->
    <link href="{{ asset('css/material-dashboard.min.css?v=2.1.1') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.min.css') }}">

    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
    <link href="{{asset('css/select2-materialize.css')}}" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-colvis-1.6.1/b-html5-1.6.1/datatables.min.css"/>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-60691549-37"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-60691549-37');
    </script>

</head>

<body>
<div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="{{ asset('images/menu.jpg') }}">
        <div class="logo">
            <a href="{{route('painel-index')}}" class="simple-text logo-mini">
                <img src="{{ asset('images/logo.png') }}" width="200" alt="logo">
            </a>
            <a href="JavaScript:Void(0);" class="simple-text logo-normal">
                {{ \Illuminate\Support\Facades\Auth::user()->nome }}
            </a>
        </div>
        <div class="sidebar-wrapper">
            @if(\App\Helpers\AppHelper::isAdmin())
                @include('layouts.components.menu')
            @else
                @include('layouts.components.menu-associado')
            @endif
        </div>
    </div>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <a class="navbar-brand" href="#">@yield('title')</a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                </button>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="content">
            <div class="container">
                @if(session()->get('message') && session()->get('type'))
                    <div class="alert {{ session()->get('type') }} mb-5">
                        <strong>{{ session()->get('message') }}</strong>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-warning alert-dismissible fade show mb-5" role="alert" style="z-index: 2;">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                @yield('content')
            </div>
        </div>
        <footer class="footer">
            <div class="container-fluid">
                <div class="copyright float-right">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, criado com <i class="material-icons">favorite</i> por
                    <a href="https://www.arealocal.com.br" target="_blank">Área Local</a>.
                </div>
                <!-- your footer here -->
            </div>
        </footer>
    </div>
</div>
</body>

<!--   Core JS Files   -->
<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-material-design.min.js') }}"></script>

<!-- Plugin for the Perfect Scrollbar -->
<script type="text/javascript" src="{{ asset('js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>

<!--  Plugin for Sweet Alert -->
<script type="text/javascript" src="{{ asset('js/plugins/sweetalert2.js') }}"></script>

<!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
<script type="text/javascript" src="{{ asset('js/plugins/bootstrap-selectpicker.js') }}" ></script>

{{--<!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->--}}
<script type="text/javascript" src="{{ asset('js/plugins/jquery.dataTables.min.js') }}"></script>

<!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
<script src="{{ asset('js/plugins/bootstrap-tagsinput.js') }}"></script>

<!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script type="text/javascript" src="{{ asset('js/plugins/jasny-bootstrap.min.js') }}"></script>

<!-- Chartist JS -->
<script type="text/javascript" src="{{ asset('js/plugins/chartist.min.js') }}"></script>

<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script type="text/javascript" src="{{ asset('js/material-dashboard.js?v=2.1.1') }}" type="text/javascript"></script>

<!-- Plugin para editor de noticias -->
<!-- <script src="https://cdn.tiny.cloud/1/yly2g4z0rkj63cytvg0vj9xke920skmv971u693s7sbnqafj/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.7.0/tinymce.min.js" integrity="sha512-XaygRY58e7fVVWydN6jQsLpLMyf7qb4cKZjIi93WbKjT6+kG/x4H5Q73Tff69trL9K0YDPIswzWe6hkcyuOHlw==" crossorigin="anonymous"></script>

<!-- FUNÇÕES AREA LOCAL -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{asset('js/select2.full.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/select2-materialize.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.mask.js')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-html5-1.6.1/b-print-1.6.1/datatables.min.js"></script>

<script type="text/javascript" src="{{ asset('js/funcoes.js') }}"></script>

@yield('script')

</html>
