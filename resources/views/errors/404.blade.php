<!DOCTYPE html>
<html lang="pt">
<head>
    <title>{{ env('APP_NAME') }}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/login-util.css')  }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/login-main.css')  }}">
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-form-title" style="background-image: url({{ asset('images/banner.jpg') }});">
                <span class="login100-form-title-1">
                    <img width="200" src="{{ asset('images/logo-texto-branco.png') }}" alt="logo">
                </span>
            </div>

            <div style=" padding: 25px; text-align: center">
                <h3>Oooops.</h3>
                <p>Não localizamos a página solicitada.</p>
                <br/>
                <a href="{{ route('index') }}" style="width: 50%; margin: 0 auto" class="login100-form-btn-back">
                    INÍCIO
                </a>
            </div>
        </div>
    </div>
</div>

</body>
</html>
