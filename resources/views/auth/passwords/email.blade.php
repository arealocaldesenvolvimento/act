<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <title>{{ env('APP_NAME') }} - Redefinir senha</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/login-util.css')  }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/login-main.css')  }}">
        <!--===============================================================================================-->
    </head>
    <body>
        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    <div class="login100-form-title" style="background-image: url({{ asset('images/banner.jpg') }});">
                        <span class="login100-form-title-1">
                            <img width="200" src="{{ asset('images/logo-texto-branco.png') }}" alt="logo">
                        </span>
                    </div>
                    <form method="POST" action="{{ route('password.email') }}" class="login100-form validate-form">
                        @csrf
                        <h1 class="txt1">RECUPERAR SENHA</h1>
                        <br/>
                        <p class="txt1">Caso o e-mail exista será enviado um link para redefinir a senha.</p>
                        <div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
                            <span class="label-input100">E-mail</span>
                            <input id="email" type="email" class="input100 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            <span class="focus-input100"></span>
                        </div>

                        <div class="container-login100-form-btn">
                            <button class="login100-form-btn" type="submit">
                                Enviar
                            </button>

                            <a href="{{ route('login') }}" class="login100-form-btn-back">
                                Voltar
                            </a>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </body>
</html>

