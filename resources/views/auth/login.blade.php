<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ env('APP_NAME') }}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/login-util.css')  }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/login-main.css')  }}">
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-form-title" style="background-image: url({{ asset('images/banner.jpg') }});">
                <span class="login100-form-title-1">
                    <img width="200" src="{{ asset('images/logo-texto-branco.png') }}" alt="logo">
                </span>
            </div>

            <form class="login100-form validate-form" method="POST" action="{{ route('login') }}">
                @csrf
                @if ($errors->any())
                    @error('cpf')
                        <p style="color: red; font-size:15px"> Credencial inválida!</p>
                    @enderror
                @endif

                @error('cpf')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    <style>
                        .focus-input100::before{
                            background-color: red;
                        }
                        .input100 + .focus-input100::before {
                            width: 100%;
                        }
                    </style>
                @enderror

                <div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">
                    <span class="label-input100">CPF</span>
                    <input class="input100 mask-cpf" type="text" name="cpf" placeholder="Digite seu CPF" value="{{ old('cpf') }}" required>
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input m-b-18" data-validate = "Password is required">
                    <span class="label-input100">SENHA</span>
                    <input class="input100" type="password" name="password" placeholder="Digite sua senha" value="{{ old('senha') }}" required>
                    <span class="focus-input100"></span>
                </div>

                <div class="flex-sb-m w-full p-b-30">
                    <div class="contact100-form-checkbox">
                        <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="label-checkbox100" for="ckb1">
                            Lembrar-me
                        </label>
                    </div>

                    @if (Route::has('password.request'))
                        <div>
                            <a href="{{ route('password.request') }}" class="txt1">
                                Esqueceu a senha?
                            </a>
                        </div>
                    @endif
                </div>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        Entrar
                    </button>

                    <a href="{{ route('index') }}" class="login100-form-btn-back">
                        Voltar
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{asset('js/jquery.mask.js')}}"></script>
<script>
    $(document).ready(() => {
        $('.mask-cpf').mask('000.000.000-00');
    })
</script>

</body>
</html>
