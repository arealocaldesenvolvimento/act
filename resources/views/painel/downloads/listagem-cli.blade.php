@extends('layouts.material')
@section('title', 'Downloads')
@section('content')
    <div class="card m-0">
        <div class="card-body bg-light">
            <h3>Lista de downloads disponíveis para você!</h3>
            <div class="row">
                @forelse(\App\Documento::all()->where('categoria', '=', 'download')->sortBy('nome_original') as $documento)
                    <div class="col-12">
                        <div class="card mt-0 mb-3">
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-12 col-md-8">
                                        <div class="author">
                                            <a href="{{route('painel-download-download', $documento->id)}}">
                                                @if(\Illuminate\Support\Str::contains($documento->nome_original, ".pdf"))
                                                    <img width="40" src="{{asset('images/pdf.svg')}}" alt="arquivo pdf">
                                                @elseif(\Illuminate\Support\Str::contains($documento->nome_original, ['jpg', 'jpeg', 'png', 'gif']))
                                                    <img width="40" src="{{asset('images/picture.svg')}}" alt="arquivo de imagem">
                                                @else
                                                    <img width="40" src="{{asset('images/doc.svg')}}" alt="arquivo doc">
                                                @endif
                                                <span style="font-size: 20px"> {{$documento->nome_original}}</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-4 text-muted text-right">
                                        Disponibilizado em: {{\Carbon\Carbon::parse($documento->created_at)->format('d/m/Y')}}
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <a href="{{route('painel-download-download', $documento->id)}}" class="btn btn-success btn-sm">BAIXAR</a>
                                <div class="stats ml-auto" title="TOTAL DE DOWNLOADS">
                                    <i class="material-icons">cloud_download</i> {{$documento->downloads}}
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="col-12">
                        <p>Nenhum desconto cadastrado</p>
                    </div>
                @endforelse
            </div>
        </div>
    </div>
@endsection
