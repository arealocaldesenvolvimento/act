@extends('layouts.material')
@section('title', 'Downloads')
@section('content')
    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="{{route('painel-download-listagem')}}"><i class="material-icons">attach_money</i> <strong>DOWNLOADS</strong></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content">
                <div class="row">
                    <div class="col">
                        <div class="card" style="background-color: rgba(28, 92, 170, 0.1)">
                            <div class="card-body">
                                <label><strong>Arquivos cadastrados serão disponibilizados para os associados efetuar download.</strong></label>
                                <form method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row mt-3">
                                        <div class="col-12 col-md-10">
                                            <div class="form-group form-file-upload form-file-multiple">
                                                <input type="file" class="inputFileHidden"  accept=".xlsx, .xls, .doc, .docx,.ppt, .pptx, .txt, .pdf, image/*"  name="arquivo">
                                                <div class="input-group">
                                                    <input type="text" class="form-control inputFileVisible" placeholder="Arquivo">
                                                    <span class="input-group-btn">
                                                        <button type="button" class="btn btn-fab btn-round btn-primary">
                                                            <i class="material-icons">attach_file</i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-1 mt-md-0 mt-3">
                                            <button type="submit" class="btn btn-success pull-right">Enviar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col">
                        <label><strong>Listagem</strong></label>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ARQUIVO</th>
                                    <th class="text-center"><i class="material-icons">cloud_download</i></th>
                                    <th class="text-right">OPÇÕES</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse(App\Documento::all()->where('categoria', '=', 'download')->sortByDesc('nome_original') as $documento)
                                    <tr>
                                        <td>{{$documento->nome_original}}</td>
                                        <td class="text-center">{{$documento->downloads}}</td>
                                        <td class="td-actions text-right">
                                            <form class="delete-form" action="{{route('painel-download-destroy')}}" method="post">
                                                @csrf
                                                <input type="hidden" value="{{$documento->id}}" name="id">
                                            </form>
                                            <button type="button" class="btn btn-danger delete">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                    <td colspan="3"><p><strong>Nenhum arquivo cadastrado.</strong></p></td>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(()=>{
            $('button.delete').click(function () {
                var button = $(this);
                Swal.fire({
                    title: 'Você tem certeza que deseja remover esse documento?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#c0cdd6',
                    confirmButtonText: 'Sim, remover!',
                    cancelButtonText: 'Cancelar!'
                }).then((result) => {
                    if (result.value) {
                        button.siblings('form.delete-form').submit();
                    }
                })
            });
        });
    </script>
@endsection
