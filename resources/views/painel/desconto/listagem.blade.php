@extends('layouts.material')
@section('title', 'Desconto')
@section('content')
    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('painel-anuidade-listagem')}}"><i class="material-icons">attach_money</i> <strong>ANUIDADES</strong></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="{{route('painel-desconto-listagem')}}"><i class="material-icons">view_list</i> Descontos</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content">
                <div class="card" style="background-color: rgba(28, 92, 170, 0.1)">
                    <div class="card-body">
                        <label><strong>Cadastrar</strong></label>
                        <p>Anuidade atual de R$ <span class="valoranuidade">{{number_format($anuidade->valor, 2, '.', '')}}</span> para o ano de {{$anuidade->ano}}.</p>
                        <form method="post">
                            @csrf
                            <div class="row mt-3">
                                <div class="col-md-3">
                                    <div class="form-group bmd-form-group @error('data') has-danger @enderror">
                                        <label>DATA LIMITE*</label>
                                        <input type="date" class="form-control" required name="data" value="{{old('data')}}">
                                        @error('data')<span class="material-icons form-control-feedback">clear</span>@enderror
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group bmd-form-group @error('desconto') has-danger @enderror">
                                        <label>DESCONTO(R$)*</label>
                                        <input type="text" class="form-control mask-money" required name="desconto" value="{{old('desconto')}}">
                                        @error('desconto')<span class="material-icons form-control-feedback">clear</span>@enderror
                                    </div>
                                </div>
                                <div class="col">
                                    <p style="margin:0">Porcentagem de desconto: <span class="porcentagem">0</span>%</p>
                                    <p style="margin:0"><strong>Valor final: R$<span class="valorfinal">{{number_format($anuidade->valor, 2, '.', '')}}</span></strong></p>
                                </div>
                                <div class="col text-right">
                                    <button type="submit" class="btn btn-success pull-right">Criar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col">
                        <label><strong>Listagem</strong></label>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>DATA FIM</th>
                                    <th>VALOR DESCONTO</th>
                                    <th class="text-right">OPÇÕES</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse(App\Desconto::all()->sortBy('data_fim') as $desconto)
                                    <tr>
                                        <td>{{\Carbon\Carbon::parse($desconto->data_fim)->format('d/m/Y')}}</td>
                                        <td>R$ {{number_format($desconto->valor, 2, '.', ',')}}</td>
                                        <td class="td-actions text-right">
                                            <form class="delete-form" action="{{route('painel-desconto-destroy')}}" method="post">
                                                @csrf
                                                <input type="hidden" value="{{$desconto->id}}" name="id">
                                            </form>
                                            <button type="button" class="btn btn-danger delete">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                    <td colspan="3"><p><strong>Nenhum desconto cadastrado.</strong></p></td>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(()=>{
            $('button.delete').click(function () {
                var button = $(this);
                Swal.fire({
                    title: 'Você tem certeza que deseja remover esse desconto?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#c0cdd6',
                    confirmButtonText: 'Sim, remover!',
                    cancelButtonText: 'Cancelar!'
                }).then((result) => {
                    if (result.value) {
                        button.siblings('form.delete-form').submit();
                    }
                })
            });

            $('input[name="desconto"]').keyup(function(){
                var anuidade = parseFloat($('span.valoranuidade').text());
                var desconto = $(this).val();
                var porcentagem =  ((anuidade - desconto) * 100) / anuidade;
                $('span.porcentagem').text(parseFloat(100.00 - porcentagem).toFixed(2));
                $('span.valorfinal').text(parseFloat(anuidade - desconto).toFixed(2));
            });
        });
    </script>
@endsection
