<!DOCTYPE html>
<html lang="pt">
<head>
    <title>CARTEIRINHA</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <!--===============================================================================================-->
    <style>
        .back p{
            line-height: 1.5;
            margin: 0;
            padding: 0;
            font-size: 14px;
        }
        h4{
            font-size: 16px;
            margin: 20px 0 0 0;
        }
        p{
            margin: 0;
            font-size: 12px;
        }
        .card-body img.user{
            border-radius: 10%;
            background-color: white;
            height: 100px;
            width: 100px;
            object-fit: cover;
            object-position: center;
        }
        .cartao{
            width: 224px;
            height: 340px;
            min-width: 195px;
            min-height: 300px;
            border: 1px solid #9e9e9e;
            margin: 20px;
            position: relative;
        }
    </style>
</head>
    <body>
        <p class="m-3 text-muted">Este cartão cabe no seu bolso! Imprima e plastifique para estar sempre junto a você!</p>
        <hr/>
        <div class="cartao rounded" style="margin-bottom: 0; background-color: white">
            <div class="card-body text-center">
                <img class="user"
                     src="data:image/png;base64, {{base64_encode(file_get_contents(storage_path(). '/app/'.$foto))}}">
                <h4 style="color:#EF9525">{{strtoupper(\Illuminate\Support\Facades\Auth::user()->nome)}}</h4>
                <br/>
                <p>CR: {{\Illuminate\Support\Facades\Auth::user()->cr ?? "N/A"}} </p>
                <p>CÓDIGO: {{\Illuminate\Support\Facades\Auth::user()->cadastro ?? "N/A"}} <p>
                <p>FILIAÇÃO: {{\Carbon\Carbon::parse(\Illuminate\Support\Facades\Auth::user()->data_filiacao)->format('d/m/Y') ?? "N/A"}}</p>
                <br/>
                <br/>
                <p>ASSOCIADO</p>
                <p>VÁLIDA ATÉ 31/12/{{date('Y')}}</p>
            </div>
        </div>
        <div class="cartao rounded" style="transform: rotate(180deg); margin-top: 0">
            <div class="card-body text-center">
                <br/>
                <img width="150"  class="mb-2" src="{{asset('images/logo.png')}}">
                <p>(47) 3521 1974</p>
                <p>contferreira@uol.com.br</p>
                <br/>
                <p>Rua Antonio Dolzani, nº 970 -
                    Bairro Valada Sao Paulo - Rio do Sul - SC - 89162.190</p>
            </div>
        </div>
    </body>
</html>
