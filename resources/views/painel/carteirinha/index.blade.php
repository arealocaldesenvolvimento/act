@extends('layouts.material')
@section('title', 'Gerar Carteirinha')

@section('content')
    <style>
        .cartao .back p{
            line-height: 1.5;
            margin: 0;
            padding: 0;
            font-size: 14px;
        }
        .cartao h4{
            font-size: 16px;
            margin: 20px 0 0 0 !important;
        }
        .cartao p{
            margin: 0 !important;
            font-size: 12px !important;
        }
        .cartao img{
            max-width: 100%;
        }
        .cartao{
            width: 224px;
            height: 340px;
            min-width: 195px;
            min-height: 300px;
            border: 1px solid #9e9e9e;
            margin: 20px;
            position: relative;
        }
    </style>
    <div class="card @desktop w-50 @enddesktop">
        <div class="card-body">
        <label>Utuilize uma foto para identificar-se.</label>
            <form method="post" enctype="multipart/form-data" target="_blank">
                @csrf
                <div class="row">
                    <div class="col-12 col-md">
                        <div class="form-group form-file-upload form-file-multiple">
                            <input type="file" class="inputFileHidden" accept="image/x-png,image/gif,image/jpeg" name="foto" required>
                            <div class="input-group">
                                <input type="text" class="form-control inputFileVisible" placeholder="Foto*">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-fab btn-round btn-primary">
                                        <i class="material-icons">attach_file</i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="col-12 col-md text-right">
                            <button type="submit" class="btn btn-success">GERAR CARTEIRINHA</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-3">
            <div class="cartao rounded" style="margin-bottom: 0; background-color: white">
                <div class="card-body text-center">
                    <img width="100" height="100" style="border-radius: 10%; background-color: white;"
                         src="{{asset('images/default-user.png')}}">
                    <h4 style="color:#EF9525">{{strtoupper(\Illuminate\Support\Facades\Auth::user()->nome)}}</h4>
                    <br/>
                    <p>CR: {{\Illuminate\Support\Facades\Auth::user()->cr ?? "N/A"}} </p>
                    <p>CÓDIGO: {{\Illuminate\Support\Facades\Auth::user()->cadastro ?? "N/A"}} <p>
                    <p>FILIAÇÃO: {{\Carbon\Carbon::parse(\Illuminate\Support\Facades\Auth::user()->data_filiacao)->format('d/m/Y') ?? "N/A"}}</p>
                    <br/>
                    <p>ASSOCIADO</p>
                    <p>VÁLIDA ATÉ 31/12/{{date('Y')}}</p>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3">
            <div class="cartao rounded" style="background-color: white">
                <div class="card-body text-center">
                    <br/>
                    <img width="150"  class="mb-2" src="{{asset('images/logo.png')}}">
                    <p>(47) 3521 1974</p>
                    <p>contferreira@uol.com.br</p>
                    <br/>
                    <p>Rua Antonio Dolzani, nº 970 -
                        Bairro Valada Sao Paulo - Rio do Sul - SC - 89162.190</p>
                </div>
            </div>
        </div>
    </div>
@endsection
