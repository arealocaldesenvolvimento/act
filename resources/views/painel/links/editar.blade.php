@extends('layouts.material')
@section('title', 'Notícias - Editar')
@section('content')
@isset($link)
  <div class="card card-nav-tabs">
      <div class="card-header card-header-primary">
          <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
          <div class="nav-tabs-navigation">
              <div class="nav-tabs-wrapper">
                  <ul class="nav nav-tabs" data-tabs="tabs">
                      <li class="nav-item">
                          <a class="nav-link" href="{{route('link.index')}}"><i class="material-icons">view_list</i> Listagem de links</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="{{route('link.create')}}"><i class="material-icons">add</i> Cadastrar novo link</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link active" href="#"><i class="material-icons">edit</i>EDITANDO</a>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
          <div class="card-body">
              <form method="post" action="{{route('link.update', $link->id)}}" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <input type="hidden" value="{{$link->id}}" name="id">
                  <div class="row">
                    <div class="col-12 col-md-8 mx-auto">
                      <label class="mb-2"><strong>Informações</strong></label>
                      <div class="row">

                        <div class="col-12 mt-3">
                          <div class="form-group bmd-form-group @error('data') has-danger @enderror">
                            <label for="title">Título*</label>
                            <input type="text" class="form-control" required name="title" value="{{old('title') ? old('title') : $link->titulo}}">
                            @error('title')<span class="material-icons form-control-feedback">clear</span>@enderror
                          </div>
                        </div>

                        <div class="col-12 mt-3">
                          <div class="form-group bmd-form-group @error('data') has-danger @enderror">
                            <label for="title">E-mail*</label>
                            <input type="email" class="form-control" name="email" value="{{old('email') ? old('email') : $link->email}}">
                            @error('email')<span class="material-icons form-control-feedback">clear</span>@enderror
                          </div>
                        </div>

                        <div class="col-12 mt-3">
                          <div class="form-group bmd-form-group @error('data') has-danger @enderror">
                            <label for="title">Telefone*</label>
                            <input type="text" class="form-control mask-phone" name="telefone" value="{{old('telefone') ? old('telefone') : $link->telefone}}">
                            @error('telefone')<span class="material-icons form-control-feedback">clear</span>@enderror
                          </div>
                        </div>

                        <div class="col-12 mt-3">
                          <div class="form-group bmd-form-group @error('data') has-danger @enderror">
                            <label for="title">Link*</label>
                            <input type="text" class="form-control" name="link" value="{{old('link') ? old('link') : $link->link}}">
                            @error('link')<span class="material-icons form-control-feedback">clear</span>@enderror
                          </div>
                        </div>

                        <div class="col-12 mx-auto mt-3">
                          <div class="form-group">
                            <textarea class="form-control" name="descricao" id="descricao" cols="30" rows="10">
                              {{ old('descricao') ? old('descricao') : $link->descricao }}
                            </textarea>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12 text-right">
                      <button type="submit" class="btn btn-success pull-right">Atualizar</button>
                    </div>
                  </div>
              </form>
          </div>
      </div>
    @else
        <p>Nenhum link encontrado neste endereço. Tente novamente ou volte a listagem de links.</p>
    @endisset

@endsection

