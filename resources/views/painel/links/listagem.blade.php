@extends('layouts.material')
@section('title', 'Links - Listagem')
@section('content')
<style>
    .dataTable>thead>tr>th,
    .dataTable>tbody>tr>th,
    .dataTable>tfoot>tr>th,
    .dataTable>thead>tr>td,
    .dataTable>tbody>tr>td,
    .dataTable>tfoot>tr>td {
        min-height: 55px;
    }

    .td-actions.text-right {
        display: flex;
        justify-content: flex-end;
        align-items: center;
        gap: 5px;
    }
</style>
<div class="card card-nav-tabs">
  <div class="card-header card-header-primary">
      <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
      <div class="nav-tabs-navigation">
          <div class="nav-tabs-wrapper">
              <ul class="nav nav-tabs" data-tabs="tabs">
                  @if(\Illuminate\Support\Facades\Auth::user()->tipo == "presidente" || \Illuminate\Support\Facades\Auth::user()->tipo == "secretario")
                      <li class="nav-item">
                          <a class="nav-link active" href="{{route('link.index')}}"><i class="material-icons">view_list</i> Listagem de links</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('link.create') }}"><i class="material-icons">add</i> Cadastrar novo link</a>
                      </li>
                  @endif
              </ul>
          </div>
      </div>
  </div>
  <div class="card-body ">
      <div class="tab-content">
          <div class="table-responsive">
              <table id="tabela-links" class="table datatable d-none">
              <thead>
                <tr>
                    <th>TÍTULO</th>
                    <th style="width: 150px;">DATA</th>
                    <th style="width: 130px;">OPÇÕES</th>
                </tr>
                </thead>
                <tbody>
                    @forelse($links as $link)
                        <tr>
                            <td>{{$link->titulo}}</td>
                            <td>{{$link->created_at}}</td>
                            <td class="td-actions text-right">
                                <input id="id_link" type="hidden" value="{{ $link->id }}">
                                <a href="{{ route('link.edit', $link->id) }}" class="btn btn-success">
                                    <i class="material-icons">edit</i>
                                </a>
                                <form action="{{ url('/link', $link->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger delete">
                                        <i class="material-icons">delete</i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @empty
                    @endforelse
                </tbody>
              </table>
          </div>
          <div class="row spinner">
              <div class="col-12 text-center">
                  <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
              </div>
          </div>
      </div>
  </div>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(()=>{
            $('button.delete').click(function(event) {
                event.preventDefault();
                var button = $(this);
                Swal.fire({
                    title: 'Você tem certeza que deseja remover esse post?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#c0cdd6',
                    confirmButtonText: 'Sim, remover!',
                    cancelButtonText: 'Cancelar!'
                }).then((result) => {
                    if (result.value == true) {
                        console.log(true)
                        button.closest('form').submit();
                    }
                })
            });
            configDatatables['initComplete'] = function() {
                $('div.dt-buttons').css('display','none')
                $('#tabela-links').removeClass('d-none');
                $('div.spinner').remove();
            };

            $('.datatable').DataTable(configDatatables);
        });
    </script>
@endsection
