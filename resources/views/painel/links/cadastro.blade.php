@extends('layouts.material')
@section('title', 'Links - Listagem')
@section('content')
<div class="card card-nav-tabs">
  <div class="card-header card-header-primary">
    <div class="nav-tabs-navigation">
      <div class="nav-tabs-wrapper">
        <ul class="nav nav-tabs" data-tabs="tabs">
          <li class="nav-item">
            <a class="nav-link" href="{{route('link.index')}}"><i class="material-icons">view_list</i> Listagem de links</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="{{route('link.create')}}"><i class="material-icons">add</i> Cadastrar novo link</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="card-body ">
    <form method="post" id="new-link" action="{{ route('link.store') }}" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col-12 mx-auto">
          <div class="form-group">
            <input value="{{ old('title') }}" name="title" type="text" class="form-control" placeholder="Título" required>
          </div>
        </div>
        <div class="col-12 mx-auto">
          <div class="form-group">
            <input value="{{ old('email') }}" name="email" type="email" class="form-control" placeholder="E-mail">
          </div>
        </div>
        <div class="col-12 mx-auto">
          <div class="form-group">
            <input maxlength="12" value="{{ old('telefone') }}" name="telefone" type="text" class="form-control mask-phone" placeholder="Telefone">
          </div>
        </div>
        <div class="col-12 mx-auto">
          <div class="form-group">
            <input value="{{ old('link') }}" name="link" type="text" class="form-control" placeholder="Link">
          </div>
        </div>
        <div class="col-12 mx-auto">
          <div class="form-group">
            <textarea placeholder="Descrição" class="form-control" name="descricao" id="descricao" cols="30" rows="10"></textarea>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 text-right">
          <button type="submit" form="new-link" class="btn btn-success pull-right">Criar</button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection