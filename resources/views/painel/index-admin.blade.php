@extends('layouts.material')
@section('title', 'Início')


@section('content')
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">people_alt</i>
                    </div>
                    <p class="card-category">Associados ativos</p>
                    <h3 class="card-title">{{\App\User::all()->count()}}</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <a class="text-muted" href="{{route('painel-usuarios-listagem')}}">Ver lista</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-danger card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">info_outline</i>
                    </div>
                    <p class="card-category">Jantas não pagas</p>
                    <h3 class="card-title">{{\App\JantaParticipantes::all()->where('pago', '=', false)->count()}}</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <a class="text-muted" href="{{route('painel-janta-listagem')}}">Ver jantas</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">card_membership</i>
                    </div>
                    <p class="card-category">Declarações</p>
                    <h3 class="card-title">{{ \App\Declaracao::all()->count() }}</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        Total de registros, sem filtro
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header card-chart card-header-success">
                    <div class="ct-chart" id="frequencias"></div>
                </div>
                <div class="card-body">
                    <h4 class="card-title">FREQUÊNCIAS</h4>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        Informações refente ao ano de {{date('Y')}}
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-header card-chart card-header-danger">
                    <div class="ct-chart" id="declaracoes"></div>
                </div>
                <div class="card-body">
                    <h4 class="card-title">DECLARAÇÕES GERADAS</h4>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        Informações refente ao ano de {{date('Y')}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(()=>{
                $.get('{{route('api-presenca-ano')}}')
                    .done(function (data) {
                        data = JSON.parse(data);
                        console.log(data);
                        dataFrequencia = {
                            labels: ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'],
                            series: [data]
                        };

                        optionsFrequencia = {
                            lineSmooth: Chartist.Interpolation.cardinal({
                                tension: 0
                            }),
                            low: 0,
                            high: Math.max.apply(Math, data),
                            chartPadding: {
                                top: 0,
                                right: 0,
                                bottom: 0,
                                left: 0
                            },
                        };
                        new Chartist.Line('#frequencias', dataFrequencia, optionsFrequencia);
                    });

            $.get('{{route('api-declaracoes-ano')}}')
                .done(function (data) {
                    data = JSON.parse(data);

                    dataDeclaracoes = {
                        labels: ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'],
                        series: [data]
                    };

                    var optionsDeclaracoes = {
                        axisX: {
                            showGrid: false
                        },
                        low: 0,
                        high: Math.max.apply(Math, data),
                        chartPadding: {
                            top: 0,
                            right: 5,
                            bottom: 0,
                            left: 0
                        }
                    };

                    var responsiveOptions = [
                        ['screen and (max-width: 640px)', {
                            seriesBarDistance: 5,
                            axisX: {
                                labelInterpolationFnc: function(value) {
                                    return value[0];
                                }
                            }
                        }]
                    ];

                    new Chartist.Bar('#declaracoes', dataDeclaracoes, optionsDeclaracoes, responsiveOptions);
                });


        });
    </script>
@endsection
