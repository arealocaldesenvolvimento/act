@extends('layouts.material')
@section('title', 'Armas')
@section('content')
<style>
.filter-option-inner-inner {
    color: #495057;
}
</style>

    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <div class="nav-tabs-navigation">
                <div>
                    <ul class="mb-0" data-tabs="tabs">
                       <h3 class="m-0 text-center"><strong>Cadastrar nova Arma do clube</strong></h3>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form action="{{route('painel-arma-store-admin')}}" method="POST" enctype="multipart/form-data"
                class="multisteps-form__form">
                @csrf

                <label><strong>Dados da Arma</strong></label>

                    <div class="row mt-4">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tipo Arma *</label>
                                <select class="form-control selectpicker" data-style="btn btn-link" name="tipo_arma" required>
                                    <option disabled selected value="">SELECIONE</option>
                                    @foreach(App\TipoArma::all() as $tipo)
                                        <option value="{{$tipo->nome}}">{{$tipo->nome}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Calibre *</label>
                                <select class="form-control selectpicker" data-style="btn btn-link" name="calibre" required>
                                    <option value="" disabled selected>SELECIONE</option>
                                    @foreach(App\Calibre::all() as $calibre)
                                        <option value="{{$calibre->nome}}" data-restrito="{{$calibre->restrito}}">{{$calibre->nome}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" name="calibre_restrito" id="calibre_restrito">
                            </div>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group @error('marca') has-danger @enderror">
                                <label>Marca *</label>
                                <input type="text" class="form-control text-uppercase" required name="marca" maxlength="60" value="{{old('marca')}}">
                                @error('marca')<span class="material-icons form-control-feedback">clear</span>@enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group bmd-form-group @error('modelo') has-danger @enderror">
                                <label>Modelo *</label>
                                <input type="text" class="form-control text-uppercase" required name="modelo" maxlength="60" value="{{old('modelo')}}">
                                @error('modelo')<span class="material-icons form-control-feedback">clear</span>@enderror
                            </div>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group @error('numero_serie') has-danger @enderror">
                                <label>Nº série *</label>
                                <input type="text" class="form-control" required name="numero_serie" >
                                @error('numero_serie')<span class="material-icons form-control-feedback">clear</span>@enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group @error('numero_sigma') has-danger @enderror">
                                <label>Nº sigma *</label>
                                <input type="text" class="form-control mask-onlynumber" required name="numero_sigma">
                                @error('numero_sigma')<span class="material-icons form-control-feedback">clear</span>@enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group @error('numero_gte') has-danger @enderror">
                                <label>Nº GTE</label>
                                <input type="text" class="form-control mask-onlynumber" name="numero_gte" >
                                @error('numero_gte')<span class="material-icons form-control-feedback">clear</span>@enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group bmd-form-group @error('cr') has-danger @enderror">
                                <label>Data de Validade</label>
                                <input type="date" class="form-control data-validade" name="validade">
                                @error('validade')<span class="material-icons form-control-feedback">clear</span>@enderror
                            </div>
                        </div>
                    </div>


                    <div class="row mt-5">
                        <div class="col-6 text-left">
                           <a href="{{route('painel-arma-listagem')}}"> <button type="button" class="btn btn-outline-dark pull-right">Voltar</button></a>
                        </div>

                        <div class="col-6 text-right">
                            <button type="submit" class="btn btn-success pull-right">Cadastrar</button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
<script>
    $('select[name="calibre"]').change(function(){
        $('#calibre_restrito').val($(this).find('option:selected').attr('data-restrito'))
    })
</script>
@endsection
