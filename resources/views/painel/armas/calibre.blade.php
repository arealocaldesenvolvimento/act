@extends('layouts.material')
@section('title', 'Calibres')
@section('content')

    <style>
        .container-checkbox {
            display: block;
            position: relative;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 22px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            padding-top: 10px;
        }

        /* Hide the browser's default checkbox */
        .container-checkbox input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 10px;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .container-checkbox:hover input~.checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .container-checkbox input:checked~.checkmark {
            background-color: #2196F3;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .container-checkbox input:checked~.checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .container-checkbox .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }

        .badge {
            font-size: 90% !important;
        }
    </style>



    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <div class="nav-tabs-navigation">
                <div>
                    <ul class="mb-0" data-tabs="tabs">
                        <h3 class="m-0 text-center"><strong>Calibres</strong></h3>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content">
                <br />
                <div class="row">
                    <div class="col">
                        <label><strong>Listagem</strong></label>
                        <button style="float:right; padding:0.8rem 1.7rem" type="button"
                            class="btn btn-primary btn-lg gerar add">Cadastrar novo Calibre</button>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Restrição</th>
                                        <th>Data</th>
                                        <th class="text-right">OPÇÕES</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($calibres as $calibre)
                                        <tr>
                                            <td>{{ $calibre->nome }}</td>
                                            <td>
                                                @if ($calibre->restrito)
                                                <span class="badge badge-danger">Restrito</span>
                                                @else
                                                <span class="badge badge-success">Liberado</span>

                                                @endif
                                            </td>
                                            <td>{{ \Carbon\Carbon::parse($calibre->created_at)->format('d/m/Y') }}</td>
                                            <td class="td-actions text-right">
                                                <button type="button" class="btn btn-danger delete"
                                                    data-calibre-id="{{ $calibre->id }}">
                                                    <i class="material-icons">delete</i>
                                                </button>
                                            </td>
                                        </tr>
                                    @empty
                                        <td colspan="3">
                                            <p><strong>Nenhum calibre cadastrado.</strong></p>
                                        </td>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <!-- Inclua a biblioteca axios -->
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        $(document).ready(() => {
            $('button.add').click(function() {
                var button = $(this);
                Swal.fire({
                    title: "Cadastrar novo Calibre",
                    html: '<input id="input-text" name="calibre" class="swal2-input" placeholder="Informe o Calibre">' +
                        '<label class="container-checkbox">Assinale se o calibre for restrito' +
                        '<input id="checkbox-restrito" type="checkbox" name="restrito">' +
                        '<span class="checkmark"></span>' +
                        '</label>',

                    inputPlaceholder: "Digite o calibre",
                    confirmButtonText: "Cadastrar",
                    showCloseButton: true,
                    inputAttributes: {
                        maxlength: "10",
                        autocapitalize: "off",
                        autocorrect: "off"
                    }
                }).then((result) => {
                    if (result.value) {
                        const urlPrefixo = '{{ env('URL_LOCAL') }}';
                        const urlCadastro = `${urlPrefixo}/painel/calibre/cadastro`;

                        const calibre = document.getElementById('input-text').value;
                        const restrito = document.getElementById('checkbox-restrito').checked;

                        axios.post(urlCadastro, {
                                calibre: calibre,
                                restrito: restrito
                            })
                            .then((response) => {
                                Swal.fire("Sucesso", "Calibre cadastrado com sucesso!",
                                    "success").then(() => {
                                    location.reload();
                                });
                            })
                            .catch((error) => {
                                console.error(error);
                                Swal.fire("Erro", "Ocorreu um erro ao cadastrar o calibre",
                                    "error");
                            });
                    }
                });
            });
            $('button.delete').click(function() {
                var button = $(this);
                var calibreId = button.data('calibre-id');
                Swal.fire({
                    title: "Deseja mesmo excluir?",
                    icon: 'question',
                    showCancelButton: true,

                    confirmButtonText: "Sim",
                    cancelButtonText: "Cancelar",
                    cancelButtonColor: "#d33",
                    showCloseButton: true,
                }).then((result) => {
                    if (result.value) {
                        const urlPrefixo = '{{ env('URL_LOCAL') }}';
                        const urlCadastro = `${urlPrefixo}/painel/calibre/destroy`;

                        axios.post(urlCadastro, {
                                calibreId: calibreId
                            })
                            .then((response) => {
                                Swal.fire(response.data.titulo, response.data.mensagem, response
                                    .data.tipo).then(() => {
                                    location.reload();
                                });
                            })
                            .catch((error) => {
                                console.error(error);
                                Swal.fire("Erro", "Ocorreu um erro ao cadastrar o tipo de arma",
                                    "error");
                            });
                    }
                });
            });
        });
    </script>
@endsection
