@extends('layouts.material')
@section('title', 'Início')
<style>
    .card-stats:hover{
        transform: translateY(-10px)
    }

    .card-stats{
        transition: all linear 0.2s
    }

</style>

@section('content')
    <div class="row">
        <div class="col-md-6 col-sm-6">
            <a href="{{route('painel-minhas-armas')}}" class="link-card">
                <div class="card card-stats">
                    <div class="card-header card-header-warning card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">
                                <img width="58" class="mb-3" src="{{asset('images/arma_w.svg')}}">
                            </i>
                        </div>
                        <p class="card-category">Minhas armas</p>
                        <h3 class="card-title">{{\App\Arma::all()->where('users_id', '=', Auth::user()->id)->count()}}</h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <p class="mb-0 text-muted">Ver listagem</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-6 col-sm-6">
            <a href="{{route('painel-arma-listagem')}}" class="link-card">
                <div class="card card-stats">
                    <div class="card-header card-header-success card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">
                                <img width="58" class="mb-3" src="{{asset('images/arma_w.svg')}}">
                            </i>
                        </div>
                        <p class="card-category">Armas do Clube</p>
                        <h3 class="card-title">{{\App\Arma::all()->where('users_id', '=', 'clube')->count()}}</h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <p class="mb-0 text-muted">Ver listagem</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6 col-sm-6">
            <a href="{{route('painel-tipo-arma-listagem')}}" class="link-card">

                <div class="card card-stats">
                    <div class="card-header card-header-danger card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">
                                <img width="58" class="mb-3" src="{{asset('images/tipo_w.svg')}}">
                            </i>
                        </div>
                        <p class="card-category">Tipos de Armas</p>
                        <h3 class="card-title">{{\App\TipoArma::all()->count()}}</h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <p class="mb-0 text-muted">Ver listagem</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-6 col-sm-6">
            <a href="{{route('painel-calibre-listagem')}}" class="link-card">
                <div class="card card-stats">
                    <div class="card-header card-header-info card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">
                                <img width="58" class="mb-3" src="{{asset('images/calibre_w.svg')}}">
                            </i>
                        </div>
                        <p class="card-category">Calibres</p>
                        <h3 class="card-title">{{ \App\Calibre::all()->count() }}</h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <p class="mb-0 text-muted">Ver listagem</p>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(()=>{
                $.get('{{route('api-presenca-ano')}}')
                    .done(function (data) {
                        data = JSON.parse(data);
                        console.log(data);
                        dataFrequencia = {
                            labels: ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'],
                            series: [data]
                        };

                        optionsFrequencia = {
                            lineSmooth: Chartist.Interpolation.cardinal({
                                tension: 0
                            }),
                            low: 0,
                            high: Math.max.apply(Math, data),
                            chartPadding: {
                                top: 0,
                                right: 0,
                                bottom: 0,
                                left: 0
                            },
                        };
                        new Chartist.Line('#frequencias', dataFrequencia, optionsFrequencia);
                    });

            $.get('{{route('api-declaracoes-ano')}}')
                .done(function (data) {
                    data = JSON.parse(data);

                    dataDeclaracoes = {
                        labels: ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'],
                        series: [data]
                    };

                    var optionsDeclaracoes = {
                        axisX: {
                            showGrid: false
                        },
                        low: 0,
                        high: Math.max.apply(Math, data),
                        chartPadding: {
                            top: 0,
                            right: 5,
                            bottom: 0,
                            left: 0
                        }
                    };

                    var responsiveOptions = [
                        ['screen and (max-width: 640px)', {
                            seriesBarDistance: 5,
                            axisX: {
                                labelInterpolationFnc: function(value) {
                                    return value[0];
                                }
                            }
                        }]
                    ];

                    new Chartist.Bar('#declaracoes', dataDeclaracoes, optionsDeclaracoes, responsiveOptions);
                });


        });
    </script>
@endsection
