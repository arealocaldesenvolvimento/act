@extends('layouts.material')
@section('title', 'Tipos de Armas')
@section('content')
    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <div class="nav-tabs-navigation">
                <div>
                    <ul class="mb-0" data-tabs="tabs">
                       <h3 class="m-0 text-center"><strong>Tipos de Arma</strong></h3>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content">
                <br/>
                <div class="row">
                    <div class="col">
                        <label><strong>Listagem</strong></label>
                        <button style="float:right; padding:0.8rem 1.7rem" type="button" class="btn btn-primary btn-lg gerar add">Cadastrar novo tipo de arma</button>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Data</th>
                                    <th class="text-right">OPÇÕES</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($tiposArma as $tipo)
                                    <tr>
                                        <td>{{$tipo->nome}}</td>
                                        <td>{{\Carbon\Carbon::parse($tipo->created_at)->format('d/m/Y')}}</td>
                                        <td class="td-actions text-right">
                                            <button type="button" class="btn btn-danger delete" data-tipo-id="{{ $tipo->id }}">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                    <td colspan="3"><p><strong>Nenhum tipo de arma cadastrado.</strong></p></td>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<!-- Inclua a biblioteca axios -->
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        $(document).ready(()=>{
            $('button.add').click(function () {
                var button = $(this);
                Swal.fire({
                    title: "Cadastrar novo tipo de arma",
                    input: "text",
                    inputLabel: "tipo",
                    inputPlaceholder: "Digite o tipo da arma",
                    confirmButtonText: "Cadastrar",
                    showCloseButton: true,
                    inputAttributes: {
                        maxlength: "10",
                        autocapitalize: "off",
                        autocorrect: "off"
                    }
                }).then((result) => {
                    if (result.value) {
                        const urlPrefixo = '{{ env("URL_LOCAL") }}';
                        const urlCadastro = `${urlPrefixo}/painel/tipo-arma/cadastro`;

                        axios.post(urlCadastro, {
                            tipoArma: result.value
                        })
                        .then((response) => {
                            Swal.fire("Sucesso", "Tipo de arma cadastrado com sucesso!", "success").then(() => {
                                location.reload();
                            });
                        })
                        .catch((error) => {
                            console.error(error);
                            Swal.fire("Erro", "Ocorreu um erro ao cadastrar o tipo de arma", "error");
                        });
                    }
                });
            });
            $('button.delete').click(function () {
                var button = $(this);
                var tipoId = button.data('tipo-id'); // Obtém o ID do botão clicado
                Swal.fire({
                    title: "Deseja mesmo excluir?",
                    icon: 'question',
                    showCancelButton: true,

                    confirmButtonText: "Sim",
                    cancelButtonText: "Cancelar",
                    cancelButtonColor: "#d33",
                    showCloseButton: true,
                }).then((result) => {
                    if (result.value) {
                        const urlPrefixo = '{{ env("URL_LOCAL") }}';
                        const urlCadastro = `${urlPrefixo}/painel/tipo-arma/destroy`;

                        axios.post(urlCadastro, {
                            tipoArmaId: tipoId
                        })
                        .then((response) => {
                            Swal.fire(response.data.titulo, response.data.mensagem, response.data.tipo).then(() => {
                                location.reload();
                            });
                        })
                        .catch((error) => {
                            console.error(error);
                            Swal.fire("Erro", "Ocorreu um erro ao cadastrar o tipo de arma", "error");
                        });
                    }
                });
            });
        });
    </script>
@endsection
