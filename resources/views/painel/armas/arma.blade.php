@extends('layouts.material')
@section('title', 'Armas')
@section('content')
    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <div class="nav-tabs-navigation">
                <div>
                    <ul class="mb-0" data-tabs="tabs">
                       <h3 class="m-0 text-center"><strong>Armas do clube</strong></h3>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content">
                <br/>
                <div class="row">
                    <div class="col">
                        <label><strong>Listagem</strong></label>
                        <a href="{{route('painel-arma-cadastro')}}">
                            <button style="float:right; padding:0.8rem 1.7rem" type="button" class="btn btn-primary btn-lg gerar add">Cadastrar nova Arma</button>
                        </a>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Tipo</th>
                                    <th>Calibre</th>
                                    <th>Marca / Modelo</th>
                                    <th>Nº série</th>
                                    <th>Nº sigma</th>
                                    <th>Validade</th>
                                    <th class="text-right">OPÇÕES</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($armas as $arma)
                                    <tr>
                                        <td>{{$arma->tipo_arma}}</td>
                                        <td>{{$arma->calibre}}</td>
                                        <td class="text-uppercase" >{{$arma->marca}} / {{$arma->modelo}}</td>
                                        <td>{{$arma->numero_serie}}</td>
                                        <td>{{$arma->numero_sigma}}</td>
                                        <td>{{\Carbon\Carbon::parse($arma->validade)->format('d/m/Y')}}</td>

                                        <td class="td-actions text-right">
                                            <a href="{{route('painel-arma-editar', $arma->id)}}">
                                                <button type="button" class="btn btn-info">
                                                    <i class="material-icons">edit</i>
                                                </button>
                                            </a>
                                            <button type="button" class="btn btn-danger delete" data-arma-id="{{ $arma->id }}">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                    <td colspan="3"><p><strong>Nenhuma arma cadastrado.</strong></p></td>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<!-- Inclua a biblioteca axios -->
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        $(document).ready(()=>{
            $('button.delete').click(function () {
                var button = $(this);
                var armaId = button.data('arma-id');
                Swal.fire({
                    title: "Deseja mesmo excluir?",
                    icon: 'question',
                    showCancelButton: true,

                    confirmButtonText: "Sim",
                    cancelButtonText: "Cancelar",
                    cancelButtonColor: "#d33",
                    showCloseButton: true,
                }).then((result) => {
                    if (result.value) {
                        const urlPrefixo = '{{ env("URL_LOCAL") }}';
                        const urlCadastro = `${urlPrefixo}/painel/arma/destroy`;

                        axios.post(urlCadastro, {
                            armaId: armaId
                        })
                        .then((response) => {
                            Swal.fire(response.data.titulo, response.data.mensagem, response.data.tipo).then(() => {
                                location.reload();
                            });
                        })
                        .catch((error) => {
                            console.error(error);
                            Swal.fire("Erro", "Ocorreu um erro ao excluir a arma", "error");
                        });
                    }
                });
            });
        });
    </script>
@endsection
