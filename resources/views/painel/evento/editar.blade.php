@extends('layouts.material')
@section('title', 'Editar evento')
@section('content')
    @isset($evento)
        <div class="card">
                <div class="card-header card-header-icon card-header-primary">
                    <div class="card-icon">
                        <i class="material-icons">event</i>
                    </div>
                    <div class="card-text">
                        <h4 class="card-title">{{ $evento->titulo }}</h4>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{route('painel-evento-update')}}">
                        @csrf
                        <label><strong>EDITAR EVENTO</strong></label>
                        <input type="hidden" value="{{$evento->id}}" name="id">
                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="form-group bmd-form-group @error('titulo') has-danger @enderror">
                                    <label>TÍTULO*</label>
                                    <input type="text" class="form-control" required name="titulo" value="@if(old('titulo')){{old('titulo')}}@else{{$evento->titulo}}@endif" maxlength="255">
                                    @error('titulo')<span class="material-icons form-control-feedback">clear</span>@enderror
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group bmd-form-group @error('desconto') has-danger @enderror">
                                    <label>OBSERVAÇÃO</label>
                                    <textarea class="form-control" name="observacao" maxlength="255">@if(old('observacao')){{old('observacao')}}@else{{$evento->observacoes}}@endif</textarea>
                                    @error('observacao')<span class="material-icons form-control-feedback">clear</span>@enderror
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-6">
                                <a href="{{route('painel-evento-listagem')}}" class="btn btn-secondary">Voltar</a>
                            </div>
                            <div class="col-6 text-right">
                                <button type="submit" class="btn btn-success pull-right">Atualizar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    @endisset
@endsection
