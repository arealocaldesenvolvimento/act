@extends('layouts.material')
@section('title', 'Eventos')
@section('content')
    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="{{route('painel-evento-listagem')}}"><i class="material-icons">view_list</i> Eventos</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content">
                <div class="row">
                    <div class="col">
                        <div class="card" style="background-color: rgba(28, 92, 170, 0.1)">
                            <div class="card-body">
                                <label><strong>Cadastrar</strong></label>
                                <form method="post">
                            @csrf
                            <div class="row mt-3">
                                <div class="col-12">
                                    <div class="form-group bmd-form-group @error('titulo') has-danger @enderror">
                                        <label>TÍTULO*</label>
                                        <input type="text" class="form-control" required name="titulo" value="{{old('titulo')}}" maxlength="255">
                                        @error('titulo')<span class="material-icons form-control-feedback">clear</span>@enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group bmd-form-group @error('desconto') has-danger @enderror">
                                        <label>OBSERVAÇÃO</label>
                                        <textarea class="form-control" name="observacao" value="{{old('observacao')}}" maxlength="255"></textarea>
                                        @error('observacao')<span class="material-icons form-control-feedback">clear</span>@enderror
                                    </div>
                                </div>
                                <div class="col-12 text-right">
                                    <button type="submit" class="btn btn-success pull-right">Criar</button>
                                </div>
                            </div>
                        </form>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col">
                        <label><strong>Listagem</strong></label>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>NOME</th>
                                    <th>OBSERVAÇÃO</th>
                                    <th class="text-right">OPÇÕES</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse(App\Evento::all()->sortBy('descricao') as $evento)
                                    <tr>
                                        <td>{{$evento->titulo}}</td>
                                        <td>{{$evento->observacoes}}</td>
                                        <td class="td-actions text-right">
                                            <form class="delete-form" action="{{route('painel-evento-destroy')}}" method="post">
                                                @csrf
                                                <input type="hidden" value="{{$evento->id}}" name="id">
                                            </form>
                                            <a href="{{ route('painel-evento-editar', $evento->id) }}" class="btn btn-success">
                                                <i class="material-icons">edit</i>
                                            </a>
                                            <button type="button" class="btn btn-danger delete">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                    <td colspan="3"><p><strong>Nenhum evento cadastrado.</strong></p></td>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(()=>{
            $('button.delete').click(function () {
                var button = $(this);
                Swal.fire({
                    title: 'Você tem certeza que deseja remover esse evento?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#c0cdd6',
                    confirmButtonText: 'Sim, remover!',
                    cancelButtonText: 'Cancelar!'
                }).then((result) => {
                    if (result.value) {
                        button.siblings('form.delete-form').submit();
                    }
                })
            });
        });
    </script>
@endsection
