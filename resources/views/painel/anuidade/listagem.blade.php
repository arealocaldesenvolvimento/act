@extends('layouts.material')
@section('title', 'Anuidade')
@section('content')
    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="{{route('painel-anuidade-listagem')}}"><i class="material-icons">attach_money</i> <strong>ANUIDADES</strong></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('painel-desconto-listagem')}}"><i class="material-icons">view_list</i> Descontos</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content">
                <div class="row">
                    <div class="col">
                        <div class="card" style="background-color: rgba(28, 92, 170, 0.1)">
                            <div class="card-body">
                                <label><strong>Cadastrar</strong></label>
                                <form method="post">
                                    @csrf
                                    <div class="row mt-3">
                                        <div class="col-md-2">
                                            <div class="form-group bmd-form-group @error('ano') has-danger @enderror">
                                                <label>ANO*</label>
                                                <input type="text" class="form-control" required name="ano" maxlength="4" value="{{old('ano')}}">
                                                @error('ano')<span class="material-icons form-control-feedback">clear</span>@enderror
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group bmd-form-group @error('valor') has-danger @enderror">
                                                <label>VALOR(R$)*</label>
                                                <input type="number" step=".01" class="form-control" required name="valor" value="{{old('valor')}}">
                                                @error('valor')<span class="material-icons form-control-feedback">clear</span>@enderror
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <button type="submit" class="btn btn-success pull-right">Criar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col">
                        <label><strong>Listagem</strong></label>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ANO</th>
                                    <th>VALOR</th>
                                    <th class="text-right">OPÇÕES</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse(App\Anuidade::all()->sortByDesc('ano') as $anuidade)
                                    <tr>
                                        <td>{{$anuidade->ano}}</td>
                                        <td>R$ {{number_format($anuidade->valor, 2)}}</td>
                                        <td class="td-actions text-right">
                                            <form class="delete-form" action="{{route('painel-anuidade-destroy')}}" method="post">
                                                @csrf
                                                <input type="hidden" value="{{$anuidade->id}}" name="id">
                                            </form>
                                            <button type="button" class="btn btn-danger delete">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                    <td colspan="3"><p><strong>Nenhuma anuidade cadastrada.</strong></p></td>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(()=>{
            $('button.delete').click(function () {
                var button = $(this);
                Swal.fire({
                    title: 'Você tem certeza que deseja remover essa anuidade?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#c0cdd6',
                    confirmButtonText: 'Sim, remover!',
                    cancelButtonText: 'Cancelar!'
                }).then((result) => {
                    if (result.value) {
                        button.siblings('form.delete-form').submit();
                    }
                })
            });
        });
    </script>
@endsection
