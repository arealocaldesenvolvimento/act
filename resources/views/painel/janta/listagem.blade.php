@extends('layouts.material')
@section('title', 'Janta')
@section('content')
    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="{{route('painel-janta-listagem')}}"><i class="material-icons">view_list</i> <strong>JANTAS</strong></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('painel-janta-novoano')}}"><i class="material-icons">view_module</i> GERAR NOVO ANO</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content">
                <div class="card" style="background-color: rgba(28, 92, 170, 0.1)">
                    <div class="card-body">
                        <label><strong>Cadastrar</strong></label>
                        <form method="post">
                            @csrf
                            <div class="row mt-3">
                                <div class="col-12 col-md-3">
                                    <div class="form-group bmd-form-group @error('data') has-danger @enderror">
                                        <label>DATA*</label>
                                        <input type="date" class="form-control" required name="data">
                                        @error('data')<span class="material-icons form-control-feedback">clear</span>@enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group bmd-form-group @error('observacao') has-danger @enderror">
                                        <label>OBSERVAÇÃO</label>
                                        <input type="text" class="form-control" name="observacao" value="{{old('observacao')}}">
                                        @error('observacao')<span class="material-icons form-control-feedback">clear</span>@enderror
                                    </div>
                                </div>
                                <div class="col-1">
                                    <button type="submit" class="btn btn-success pull-right">Criar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
{{--                <label><strong>Filtrar por ano:</strong></label>--}}
{{--                <form>--}}
{{--                    <div class="row my-3">--}}
{{--                        <div class="col-2">--}}
{{--                            <div class="form-group bmd-form-group @error('anoSelecionado') has-danger @enderror">--}}
{{--                                <input type="text" class="form-control" required name="anoSelecionado" placeholder="Digite um ano..." value="{{$anoSelecionado ?? null}}">--}}
{{--                                @error('anoSelecionado')<span class="material-icons form-control-feedback">clear</span>@enderror--}}
{{--                            </div>--}}
{{--                            </div>--}}
{{--                            <div class="col">--}}
{{--                                <button type="submit" class="btn btn-success pull-right">Filtrar</button>--}}
{{--                                <a href="{{route('painel-janta-listagem')}}?anoSelecionado={{date('Y')}}" class="btn btn-secondary">ANO ATUAL ({{date('Y')}})</a>--}}
{{--                                @if($anoSelecionado)--}}
{{--                                    <a href="{{route('painel-janta-listagem')}}" class="btn btn-danger">Limpar filtro</a>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                </form>--}}
                <br/>
                <div class="row">
                    <div class="col">
                        <label><strong>Listagem</strong></label>
                        <a style="float:right" href="{{route('painel-janta-exportacao-completa')}}" class="btn btn-primary">Exportação completa</a>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>DATA</th>
                                    <th>OBSERVAÇÃO</th>
                                    <th>Q. PARTIC.</th>
                                    <th>FINANCEIRO</th>
                                    <th class="text-right">OPÇÕES</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    use Illuminate\Support\Facades\DB;if($anoSelecionado){
                                        $jantas = DB::table('janta')->whereNull('deleted_at')->whereYear('data', $anoSelecionado)->orderBy('data')->get();
                                    }else{
                                        $jantas = DB::table('janta')->whereNull('deleted_at')->orderBy('data')->get();
                                    }
                                @endphp
                                @forelse($jantas as $janta)
                                    <tr>
                                        <td>{{\Carbon\Carbon::parse($janta->data)->format('d/m/Y')}}</td>
                                        <td>
                                            {{$janta->observacao ?? "Sem Observação"}}
                                        </td>
                                        <td>{{count(\App\JantaParticipantes::all()->where('janta_id','=', $janta->id))}}</td>
                                        <td>
                                            @php
                                                $naopagantes = count(\App\JantaParticipantes::all()->where('janta_id','=', $janta->id)->where('pago', '=', false))
                                            @endphp
                                            @if($naopagantes > 0)
                                                <span class="text-danger font-weight-bold">{{$naopagantes}} NÃO PAGANTE(S)</span>
                                            @else
                                                <span class="text-muted">TODOS PAGARAM</span>
                                            @endif
                                        </td>
                                        <td class="td-actions text-right">
                                            <form class="delete-form" action="{{route('painel-janta-destroy')}}" method="post">
                                                @csrf
                                                <input type="hidden" value="{{$janta->id}}" name="id">
                                            </form>
                                            <a href="{{route('painel-janta-participantes', $janta->id)}}" class="btn btn-primary"><i class="material-icons">people_alt</i></a>
                                            <button type="button" class="btn btn-danger delete">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                    <td colspan="3"><p><strong>Nenhuma janta cadastrada.</strong></p></td>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(()=>{
            $('button.delete').click(function () {
                var button = $(this);
                Swal.fire({
                    title: 'Você tem certeza que deseja remover essa janta?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#c0cdd6',
                    confirmButtonText: 'Sim, remover!',
                    cancelButtonText: 'Cancelar!'
                }).then((result) => {
                    if (result.value) {
                        button.siblings('form.delete-form').submit();
                    }
                })
            });
        });
    </script>
@endsection
