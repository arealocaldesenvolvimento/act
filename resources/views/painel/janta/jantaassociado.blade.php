@extends('layouts.material')
@section('title', 'Janta')
@section('content')

    <div class="card">
        <img class="card-img-top" src="{{asset('images/janta.jpg')}}" alt="Card image cap">
        <div class="card-body text-center">
            @if(\Illuminate\Support\Facades\Auth::user()->janta)
                <label class="m-0">Sua janta será:</label>
                <h2 class="card-text m-0" style="line-height: 1">{{\Carbon\Carbon::parse($data)->format('d/m/Y')}}</h2>
            @else
                <h2 class="card-text m-0" style="line-height: 1">Você ainda não faz parte de nenhuma equipe de janta. Mas, fique atento! A qualquer momento seu nome pode ser incluído em alguma equipe caso abra uma vaga. Você será avisado por e-mail caso isso ocorra.</h2>
            @endif
        </div>
    </div>

    @if(\Illuminate\Support\Facades\Auth::user()->janta)
        <div class="card w-50" style="margin: 0 auto">
            <div class="card-body text-center">
                <h3 class="m-0 mb-5">PARTICIPANTES</h3>
                @foreach($participantes as $participante)
                    <p style="line-height: 1.5; margin-bottom: 0;">{{$participante->nome}} <span style="font-size: 12px" class="text-muted">@isset($participante->telefone){{$participante->telefone}}@endisset @isset($participante->telefone2)| {{$participante->telefone2}}@endisset @isset($participante->telefone3)| {{$participante->telefone3}}@endisset</span></p>
                @endforeach
                <div class="row mt-5">
                    <div class="col text-center">
                        <a href="{{route('painel-janta-associado-download')}}" class="btn btn-primary ">Baixar lista completa</a>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

