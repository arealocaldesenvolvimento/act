@extends('layouts.material')
@section('title', 'Janta - Gerar novo ano')
@section('content')
    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('painel-janta-listagem')}}"><i class="material-icons">view_list</i> <strong>JANTAS</strong></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="{{route('painel-janta-novoano')}}"><i class="material-icons">view_module</i> GERAR NOVO ANO</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content">
                <div class="row">
                    <div class="col">
                        <div class="jumbotron">
                            <h1 class="display-4">Gerador de novo ano de janta!</h1>
                            <p class="lead">Lembre-se: ao utilizar o gerador todas as operações executadas <strong class="text-danger">não poderam ser desfeitas</strong>!</p>
                            <hr class="my-4">
                            <button class="btn btn-primary btn-lg gerar">GERAR ANO</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>DATA</th>
                                    <th>PARTICIPANTES</th>
                                    <th>STATUS</th>
                                </tr>
                                </thead>
                                <tbody id="results">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a style="display: none" download  id="exporta" href="{{route('painel-janta-exportacao-completa')}}">a</a>
@endsection
@section('script')
    <script src="https://momentjs.com/downloads/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/promise.prototype.finally/1.0.1/finally.js"></script>
    <script>
        $(document).ready(()=>{
            $('button.gerar').click(function () {
                let botao = $(this);
                botao.prop('disabled', true).text('Gerando, aguarde o relatório e o download...');
                  Swal.fire({
                    title: 'Digite a data Inicial',
                    input: 'text',
                    inputAttributes: {
                        autocapitalize: 'off'
                    },
                    showCancelButton: true,
                    confirmButtonText: 'Gerar Datas',
                    showLoaderOnConfirm: true,
                    onBeforeOpen: () =>{
                        $('.swal2-input').mask('00/00/0000');
                    },
                    preConfirm: (data) => {
                        data = data.replace(/[/]/g, '-');
                        return fetch(`/api/janta/getdatas/${data}`)
                            .then(response => {
                                if (!response.ok) {
                                    throw new Error(response.statusText)
                                }
                                $('.swal2-input').css('border', '1px solid red');
                                return response.json()
                            })
                            .catch(error => {
                                alert('Error: '+error);
                            })
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.value) {
                        var html ="";
                        var primeiradata = "";
                        $.each(result.value, function (key, value) {
                            if(key === 0)
                                primeiradata = value;

                            html += moment(value.date).format('DD/MM/YYYY') + '<br/>';
                        });
                        Swal.fire({
                            title: 'CONFIMAR INFORMAÇÕES',
                            html: `<h3>Datas a serem geradas: </h3><div style="columns: 3"> ${html}</div><br/> <h4 class="text-danger" style="font-weight: 800;">Todos os dados do ano atual serão apagados e exportados em uma planilha!<br/> As modificações não poderam ser desfeitas!</strong></h4>`,
                            showCancelButton: true,
                            confirmButtonColor: '#279437',
                            cancelButtonColor: '#c0cdd6',
                            confirmButtonText: 'Sim, gerar!',
                            cancelButtonText: 'Cancelar!'
                        }).then((result) => {
                            if (result.value) {
                                document.querySelector('#exporta').click();
                                setTimeout(function (){
                                    $.get('/painel/janta/gera-novo-ano/'+ moment(primeiradata.date).format('YYYY-MM-DD'))
                                        .done(function (data) {
                                            data = JSON.parse(data);
                                            $.each(data, function (key, value) {
                                                $('tbody#results').append('<tr><td>'+moment(value.data.date).format('DD/MM/YYYY')+'</td><td>'+value.quant_part+'</td><td>'+(value.status ? "SUCESSO": "FALHA")+'</td></tr>');
                                            })
                                            botao.text('Geração completa!');
                                    })
                                },1000)
                            }
                        });
                    }
                });
            });
        });
    </script>
@endsection
