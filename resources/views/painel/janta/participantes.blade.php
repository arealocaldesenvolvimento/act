@extends('layouts.material')
@section('title', 'Janta - Participantes')
@section('content')
    <a href="{{ route('painel-janta-listagem') }}" class="btn btn-primary mb-3"><i class="material-icons">arrow_back</i> Voltar a listagem</a>
    <div class="card">
            <div class="card-header card-header-icon card-header-primary">
                @desktop
                    <div class="card-icon">
                        <i class="material-icons">people_alt</i>
                    </div>
                @enddesktop
                <div class="card-text">
                    <h4 class="card-title">Participantes @isset($janta)- Janta {{\Carbon\Carbon::parse($janta->data)->format('d/m/Y')}}@endisset</h4>
                </div>
            </div>
            <div class="card-body">
                <div class="card" style="background-color: rgba(28, 92, 170, 0.1)">
                    <div class="card-body">
                        <label><strong>Cadastrar</strong></label>
                        <form method="post">
                            @csrf
                            <input type="hidden" name="janta_id" value="{{$janta->id}}">
                            <div class="row mt-3">
                                <div class="col-12 col-md-10">
                                    <select class="select-user" required name="associado_id" placeholder="SELECIONE UM ASSOCIADO*">
                                        <option disabled selected placeholder value="none" selected>ASSOCIADO*</option>
                                        @foreach(\App\User::all()->where('janta', '=', false)->sortByDesc('created_at') as $usuario)
                                            <option @if(old('associado_id') == $usuario->id) selected @endif value="{{$usuario->id}}">{{$usuario->nome}} ({{$usuario->cadastro}})</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-12 col-md-1 mt-md-0 mt-4">
                                    <button type="submit" class="btn btn-success pull-right">INSCREVER</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <br/>
                <div class="table-responsive">
                    <p>Participantes: {{count($participantes)}}</p>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>NOME</th>
                                <th>DATA ADIÇÃO</th>
                                <th>PAGO</th>
                                <th class="text-right">OPCÕES</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse(\App\JantaParticipantes::all()->where('janta_id', '=', $janta->id) as $participante)
                                <tr>
                                    <td>{{\App\User::find($participante->users_id)->nome}}</td>
                                    <td>{{\Carbon\Carbon::parse($participante->created_at)->format('d/m/Y')}}</td>
                                    <td class="text-success">@if($participante->pago) SIM @else <strong><span class="text-danger">NÃO</span></strong> @endif</td>
                                    <td class="td-actions text-right">
                                        <form class="delete-form" action="{{route('painel-janta-participante-destroy', $participante->janta_id)}}" method="post">
                                            @csrf
                                            <input type="hidden" value="{{$participante->id}}" name="janta_id">
                                        </form>
                                        <form class="pagou-form" action="{{route('painel-janta-participante-pagou', $participante->janta_id)}}" method="post">
                                            @csrf
                                            <input type="hidden" value="{{$participante->users_id}}" name="associado_id">
                                        </form>
                                        @if($participante->pago)
                                        <button title="ALTERAR PARA NÃO PAGO" type="button" class="btn btn-warning pagou">
                                            <i class="material-icons">thumb_down_alt</i>
                                        </button>
                                        @else
                                            <button title="ALTERAR PARA PAGO" type="button" class="btn btn-success pagou">
                                                <i class="material-icons">thumb_up_alt</i>
                                            </button>
                                        @endif
                                        <button title="REMOVER" type="button" class="btn btn-danger delete">
                                            <i class="material-icons">delete</i>
                                        </button>
                                    </td>
                                </tr>
                            @empty
                                <td colspan="3"><p><strong>Nenhum participante.</strong></p></td>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <a href="{{ route('painel-janta-listagem') }}" class="btn btn-primary"><i class="material-icons">arrow_back</i> Voltar a listagem</a>
@endsection
@section('script')
    <script>
        $(document).ready(()=>{
            $('select.select-user').sm_select();
        });

        $('button.delete').click(function () {
            var button = $(this);
            Swal.fire({
                title: 'Você tem certeza que deseja remover esse participante?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#c0cdd6',
                confirmButtonText: 'Sim, remover!',
                cancelButtonText: 'Cancelar!'
            }).then((result) => {
                if (result.value) {
                    button.siblings('form.delete-form').submit();
                }
            })
        });

        $('button.pagou').click(function () {
            $(this).siblings('form.pagou-form').submit();
        });
    </script>
@endsection
