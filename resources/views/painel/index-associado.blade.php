@extends('layouts.material')
@section('title', 'Início')

@section('content')
    <div class="container">
        @if(request()->has('type') && request()->has('message'))
            <div class="alert {{ request()->get('type') }} mb-5">
                <strong>{{ request()->get('message') }}</strong>
            </div>
        @endif
        @if(isset($type) && isset($message))
            <div class="alert {{ $type }} mb-5">
                <strong>{{ $message }}</strong>
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-12 col-md-3">
            <div class="card @if(\App\Helpers\AppHelper::getSaldo(\Illuminate\Support\Facades\Auth::id()) < 0) bg-danger @else bg-info @endif py-5">
                <div class="card-body text-center">
                    <h2 class="m-0">R$ {{number_format(\App\Helpers\AppHelper::getSaldo(\Illuminate\Support\Facades\Auth::id()), 2, ',', '') }}</h2>
                    <p class="m-0">Saldo Geral</p>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-9">
            <div class="card">
                <div class="card-body">
                    <label class="m-0">ULTIMAS OPERAÇÕES | <a href="{{route('painel-conta-historico', \Illuminate\Support\Facades\Auth::id())}}" class="text-muted"> Para ver todas operações clique aqui</a> </label>
                    <div class="table-responsive">
                        <table class="table m-0">
                            <thead>
                            <tr>
                                <th>DESCRIÇÃO</th>
                                <th>TIPO</th>
                                <th>VALOR</th>
                                <th>DATA</th>
                                <th>GERADOR</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse(\App\Conta::take(3)->where('users_id', '=',\Illuminate\Support\Facades\Auth::id())->orderByDesc('id')->get() as $row)
                                    <td class="py-1">{{$row->descricao ?? "Sem Descrição"}}</td>
                                    <td class="py-1">
                                        @if($row->debito != 0) <span class="text-danger">DÉBITO</span>@endif
                                        @if($row->credito != 0) <span class="text-success">CRÉDITO</span> @endif
                                    </td>
                                    <td class="py-1">
                                        @if($row->debito != 0) <span class="text-danger">- R${{number_format($row->debito, 2, ',', '')}}</span> @endif
                                        @if($row->credito != 0) <span class="text-success">+ R$ {{number_format($row->credito, 2, ',', '')}}</span> @endif
                                    </td>
                                    <td class="py-1">{{\Carbon\Carbon::parse($row->data)->format('d/m/Y')}}</td>
                                    <td class="py-1">{{ \App\User::find($row->gerador_id)->nome }}</td>
                                </tr>
                            @empty
                                <td colspan="3"><p><strong>Nenhuma movimentação.</strong></p></td>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            @if(\App\Helpers\AppHelper::getSaldo(\Illuminate\Support\Facades\Auth::id()) < 0)
                <div class="card">
                    <div class="card-body">
                        <p class="text-muted">Pague até as datas indicadas para receber <strong>desconto</strong>!</p>
                        <div class="row">
                            @forelse(\Illuminate\Support\Facades\DB::select('select * from desconto where YEAR(now()) = YEAR(data_fim) and desconto.deleted_at is null  order by data_fim') as $desconto)
                                <div class="col-12 col-md-4">
                                    <div class="card card-nav-tabs text-center">
                                        <div class="card-header  @if($desconto->data_fim < now()) card-header-danger @else card-header-success @endif">
                                            @if($desconto->data_fim < now()) <del> @endif
                                            <h4 class="m-0">{{\Carbon\Carbon::parse($desconto->data_fim)->format('d/m/Y')}}</h4>
                                            @if($desconto->data_fim < now()) </del> @endif
                                        </div>
                                        <div class="card-body text-center">
                                            <p class="text-muted m-0">Desconto de</p>
                                            @if($desconto->data_fim < now()) <del> @endif
                                            <h2 class="card-title">R$ {{number_format($desconto->valor, 2, ',', '')}}</h2>
                                            @if($desconto->data_fim < now()) </del> @endif
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <div class="col-12">
                                    <p>Nenhum desconto cadastrado</p>
                                </div>
                            @endforelse
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
{{--    <div class="row">--}}
{{--        <div class="col-12 col-md-6">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header card-chart card-header-rose">--}}
{{--                    <div class="ct-chart" id="dailySalesChart"></div>--}}
{{--                </div>--}}
{{--                <div class="card-body">--}}
{{--                    <h4 class="card-title">Frequência Mensal</h4>--}}
{{--                    <p class="card-category">Média: <span class="text-success">55</span></p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
@endsection
@section('script')
    <script>
        if ($('#dailySalesChart').length != 0) {
            /* ----------==========     Daily Sales Chart initialization For Documentation    ==========---------- */

            dataDailySalesChart = {
                labels: ['J', 'F', 'M', 'A', 'JN', 'JL', 'A', 'S', 'S', 'O', 'N', 'D'],
                series: [
                    [12, 17, 7, 17, 23, 18, 27, 10, 7, 1, 0, 0 ]
                ]
            };

            optionsDailySalesChart = {
                lineSmooth: Chartist.Interpolation.cardinal({
                    tension: 0
                }),
                low: 0,
                high: 30, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
                chartPadding: {
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0
                },
            };

            var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);
        }
        @if(!\Illuminate\Support\Facades\Auth::user()->first_login)
            $(document).ready(()=>{
                Swal.fire({
                    title: 'Bem vindo {{\Illuminate\Support\Facades\Auth::user()->nome}}',
                    html: 'Notamos que este é seu primeiro acesso ao sistema do clube! <strong>Gostaria de alterar sua senha?</strong>',
                    icon: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sim, gostaria!',
                    cancelButtonText: 'Não',
                    onClose: () => {
                        $.get('{{route('api-firstlogin')}}');
                    }
                }).then((result) => {
                    if (result.value) {
                        window.location.href = "{{route('painel-usuario-meus-dados')}}";
                    }
                })
            });
        @endif
    </script>
@endsection
