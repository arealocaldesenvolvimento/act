@extends('layouts.material')
@section('title', 'Lista associados')
@section('content')
  <div class="card card-nav-tabs">
    <div class="card-header card-header-primary">
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs" data-tabs="tabs">
                  <li class="nav-item">
                    <a class="nav-link active" href="{{ route('associados') }}"><i class="material-icons">view_list</i> Listagem de usuários</a>
                  </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card-body ">
        <div class="tab-content">
            <div class="table-responsive">
                <table id="tabela-associados" class="table datatable d-none">
                  <thead>
                    <tr>
                        <th>NOME</th>
                        <th>CÓDIGO</th>
                        <th>CR</th>
                        <th>FILIAÇÃO</th>
                        <th>CPF</th>
                        <th>TELEFONES</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse(App\User::all()->sortBy('nome') as $usuario)
                      <tr>
                        <td>{{$usuario->nome}}</td>
                        <td>{{$usuario->cadastro}}</td>
                        <td>{{$usuario->cr}}</td>
                        <td>{{\Carbon\Carbon::parse($usuario->data_filiacao)->format('d/m/Y')}}</td>
                        <td class="mask-cpf">{{$usuario->cpf}}</td>
                        <td class="mask-phone">{{$usuario->telefone}}</td>
                      </tr>
                    @empty
                        <td><p><strong>Nenhum usuário encontrado.</strong></p></td>
                    @endforelse
                  </tbody>
                </table>
            </div>
            <div class="row spinner">
                <div class="col-12 text-center">
                    <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection

@section('script')
  <script>
    $(document).ready(() => {
      $('button.delete').click(function () {
      });
      configDatatables['initComplete'] = function() {
          $('div.dt-buttons').css('display','none')
          $('#tabela-associados').removeClass('d-none');
          $('div.spinner').remove();
      };
      $('.datatable').DataTable(configDatatables);
    });
  </script>
@endsection
