@extends('layouts.material')
@section('title', 'Habitualidade - Cadastro')
@section('content')
    <style>
        .form-group .form-control {
            height: 43px;
        }

        .form-group:has(.select2-hidden-accessible)>label {
            margin-bottom: calc(0.5rem - 4px);
        }
    </style>
    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('painel-habitualidade-listagem') }}"><i
                                    class="material-icons">view_list</i> Listagem de habitualidades</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="{{ route('painel-habitualidade-cadastro') }}"><i
                                    class="material-icons">add</i> Cadastrar nova habitualidade</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content">
                <form method="post">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-4 border-right">
                            <label class="mb-2"><strong>Evento</strong></label>
                            @error('evento_id')
                                <div class="alert alert-danger" role="alert">Selecione um evento!</div>
                            @enderror
                            <div class="custom-radio @error('evento_id') border border-danger @enderror">
                                @foreach (\App\Evento::all()->sortBy('titulo') as $evento)
                                    <div class="inputGroup">
                                        <input id="radio-{{ $evento->id }}" value="{{ $evento->id }}" name="evento_id"
                                            type="radio" @if (isset($last) && $last->evento_id == $evento->id) checked @endif />
                                        <label for="radio-{{ $evento->id }}">{{ $evento->titulo }} @if ($evento->observacoes)
                                                <small><br /> {{ $evento->observacoes }}</small>
                                            @endif
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-12 col-md-8">
                            <label class="mb-2"><strong>Informações</strong></label>
                            <div class="row">
                                <div class="col-12 col-md-3">
                                    <div class="form-group bmd-form-group @error('data') has-danger @enderror">
                                        <label>DATA*</label>
                                        <input type="date" class="form-control" required name="data"
                                            value="{{ date('d/m/Y') }}">
                                        @error('data')
                                            <span class="material-icons form-control-feedback">clear</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-12 col-md-3">
                                    <div class="form-group bmd-form-group @error('cr') has-danger @enderror">
                                        <label>CR *</label>
                                        <input type="text" class="form-control" required name="cr" maxlength="11"
                                            value="{{ old('cr') }}">
                                        @error('cr')
                                            <span class="material-icons form-control-feedback">clear</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-9">
                                    <div class="form-group">
                                        <label>ASSOCIADO *</label>
                                        <select class="select-user form-control" required name="users_id"
                                            placeholder="SELECIONE UM ASSOCIADO*">
                                            <option disabled selected placeholder value="none" selected>SELECIONAR
                                            </option>
                                            @foreach (\App\User::all()->sortBy('nome') as $usuario)
                                                <option @if (old('users_id') == $usuario->id) selected @endif
                                                    value="{{ $usuario->id }}" data-cr="{{ $usuario->cr }}">
                                                    {{ $usuario->nome }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>SUAS ARMAS *</label>
                                        <select id="suas-armas" class="form-control" data-style="btn btn-link"
                                            name="suas_armas">
                                            <option value="" selected>SELECIONE</option>
                                            @foreach (App\Arma::where('users_id', Auth::user()->id)->get() as $arma)
                                                <option value="{{ $arma->id }}">{{ $arma->marca }} /
                                                    {{ $arma->modelo }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label for="armaClube">ARMAS DO CLUBE *</label>
                                        <select id="armas-clube" class="form-control" data-style="btn btn-link"
                                            name="armas_clube">
                                            <option value="" selected>SELECIONE</option>
                                            @foreach (App\Arma::where('users_id', 'clube')->get() as $arma)
                                                <option value="{{ $arma->id }}">{{ $arma->marca }} /
                                                    {{ $arma->modelo }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-3 col-12">
                                    <div
                                        class="form-group bmd-form-group @error('quantidade_disparos') has-danger @enderror">
                                        <label>DISPAROS</label>
                                        <input type="number" class="form-control" name="quantidade_disparos"
                                            value="0">
                                        @error('quantidade_disparos')
                                            <span class="material-icons form-control-feedback">clear</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-9 col-12">
                                    <div class="form-group bmd-form-group @error('local') has-danger @enderror">
                                        <label>LOCAL*</label>
                                        <input type="text" class="form-control" required name="local">
                                        @error('local')
                                            <span class="material-icons form-control-feedback">clear</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-3 col-12">
                                    <div class="form-group bmd-form-group @error('pagina') has-danger @enderror">
                                        <label>PÁGINA</label>
                                        <input type="text" class="form-control mask-onlynumber" name="pagina">
                                        @error('pagina')
                                            <span class="material-icons form-control-feedback">clear</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-right">
                            <button type="submit" class="btn btn-success pull-right">Criar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        $(document).ready(() => {
            $('select.select-user').sm_select();

            $('select[name="users_id"]').change(function() {
                $('input[name="cr"]').val($(this).find(':selected').data('cr'));
            });

            $('input[name="cr"]').focusout(function() {
                console.log($("select[name='users_id'] option[data-cr='" + $(this).val() + "']"));
                $($('select[name="users_id"]').val($("select[name='users_id'] > option[data-cr='" + $(this)
                    .val() + "']").val())).change();
            });

            $("#suas-armas").change(function() {
                $("#armas-clube").val("").selectpicker('refresh');
            });

            $("#armas-clube").change(function() {
                $("#suas-armas").val("").selectpicker('refresh');
            });

            $('#suas-armas').select2();
            $('#armas-clube').select2();

            $('select[name="users_id"]').change(function() {
                const urlPrefixo =
                    '{{ env('URL_LOCAL') }}';
                const userId = $(this).val();

                axios.get(urlPrefixo + '/painel/usuario/' + userId + '/armas')
                    .then(response => {
                        const armasSelect = $('#suas-armas');
                        armasSelect.empty().append('<option value="" selected>SELECIONE</option>');

                        if (response.data.mensagem) {
                            console.log(response.data.mensagem);
                            armasSelect.prop('disabled', true);
                        } else {
                            response.data.forEach(arma => {
                                armasSelect.append(new Option(`${arma.marca} / ${arma.modelo}`,
                                    arma.id));
                            });
                            armasSelect.prop('disabled', false);
                        }

                        armasSelect.trigger('change');
                    })
                    .catch(error => {
                        console.error('Erro ao buscar armas:', error);
                        $('#suas-armas').prop('disabled', true).trigger('change');
                    });
            });

            $('#suas-armas').on('change', function() {
                if ($(this).val() !== "") {
                    $('#armas-clube').val('').prop('disabled', true).selectpicker('refresh');
                    $('#select2-armas-clube-container').text('SELECIONE')
                } else {
                    $('#armas-clube').prop('disabled', false).selectpicker('refresh');
                }
            });
            $('#armas-clube').on('change', function() {
                if ($(this).val() !== "") {
                    $('#suas-armas').val('').prop('disabled', true).selectpicker('refresh');
                    $('#select2-suas-armas-container').text('SELECIONE')
                } else {
                    $('#suas-armas').prop('disabled', false).selectpicker('refresh');
                }
            });

            if ($('#suas-armas').val() !== "") {
                $('#armas-clube').val('').prop('disabled', true).selectpicker('refresh');
            }

            if ($('#armas-clube').val() !== "") {
                $('#suas-armas').val('').prop('disabled', true).selectpicker('refresh');
            }

        })
    </script>
@endsection
