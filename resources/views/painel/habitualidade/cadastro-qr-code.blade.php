@extends('layouts.material')
@section('title', 'Habitualidade - Cadastro por QR Code')
@section('content')

    @if (session()->get('isPopUp'))
        <div id="popUp-cadastro-novamente">
        </div>
    @endif


    <div class="card card-nav-tabs">
        <div class="card-body ">
            <div class="tab-content">
                <form method="post" action="{{ route('painel-habitualidade-cadastro_qr_code') }}">
                    @csrf
                    <input type="hidden" name="user_agent">
                    <input type="hidden" name="plataforma">
                    <input type="hidden" name="ip">
                    <input type="hidden" name="latitude">
                    <input type="hidden" name="longitude">
                    <div class="row">
                        <div class="col-12 col-md-4 border-right">
                            <label class="mb-2"><strong>Evento</strong></label>
                            @error('evento_id')
                                <div class="alert alert-danger" role="alert">Selecione um evento!</div>
                            @enderror
                            @if(!$saldo)
                                <div class="alert alert-danger" role="alert">Saldo negativo!</div>
                            @endif
                            <div class="custom-radio @error('evento_id') border border-danger @enderror">
                                @foreach (\App\Evento::all()->sortBy('titulo') as $evento)
                                    <div class="inputGroup">
                                        <input id="radio-{{ $evento->id }}" value="{{ $evento->id }}" name="evento_id"
                                            type="radio" @if (isset($last) && $last->evento_id == $evento->id) checked @endif />
                                        <label for="radio-{{ $evento->id }}">{{ $evento->titulo }} @if ($evento->observacoes)
                                                <small><br /> {{ $evento->observacoes }}</small>
                                            @endif
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-12 col-md-8">
                            <label class="mb-2"><strong>Informações</strong></label>
                            <div class="row">
                                <div class="col-12 col-md-4">
                                    <div class="form-group bmd-form-group @error('data') has-danger @enderror">
                                        <label>DATA*</label>
                                        <input type="date" readonly class="form-control" required name="data"
                                            value="{{ \Carbon\Carbon::parse(Carbon\Carbon::now())->format('Y-m-d') }}">
                                        @error('data')
                                            <span class="material-icons form-control-feedback">clear</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <?php $user = Auth::user(); ?>
                            <div class="row mt-3">
                                <div class="col-12 col-md-3">
                                    <div class="form-group bmd-form-group @error('cr') has-danger @enderror">
                                        <label>CR</label>
                                        <input type="text" readonly class="form-control" name="cr" maxlength="11"
                                            value="{{ $user->cr }}">
                                        @error('cr')
                                            <span class="material-icons form-control-feedback">clear</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12 col-md-9">
                                    <div class="form-group bmd-form-group @error('cr') has-danger @enderror">
                                        <label>ASSOCIADO*</label>
                                        <input type="hidden" readonly class="form-control" name="users_id"
                                            value="{{ $user->id }}" data-cr="{{ $user->cr }}">
                                        <input type="text" readonly class="form-control" name="associado_nome"
                                            value="{{ $user->nome }}" data-cr="{{ $user->cr }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>SUAS ARMAS *</label>
                                        <select id="suas-armas" class="form-control" data-style="btn btn-link" name="suas_armas">
                                            <option value="" selected>SELECIONE</option>
                                            @foreach (App\Arma::where('users_id', Auth::user()->id)->get() as $arma)
                                                @php
                                                    $is_valid = \Carbon\Carbon::now()->lessThan(\Carbon\Carbon::parse($arma->validade));
                                                @endphp
                                                <option value="{{ $arma->id }}" {{ $is_valid ? '' : 'disabled' }}>
                                                    {{ $arma->marca }} / {{ $arma->modelo }} {{ $is_valid ? '' : '(desabilitado)' }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label for="armaClube">ARMAS DO CLUBE *</label>
                                        <select id="armas-clube" class="form-control" data-style="btn btn-link"
                                            name="armas_clube">
                                            <option value="" selected>SELECIONE</option>
                                            @foreach (App\Arma::where('users_id', 'clube')->get() as $arma)
                                                @php
                                                    $is_valid = \Carbon\Carbon::now()->lessThan(
                                                        \Carbon\Carbon::parse($arma->validade),
                                                    );
                                                @endphp
                                                <option value="{{ $arma->id }}" {{ $is_valid ? '' : 'disabled' }}>
                                                    {{ $arma->marca }} / {{ $arma->modelo }} {{ $is_valid ? '' : '(desabilitado)' }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-3 col-12">
                                    <div
                                        class="form-group bmd-form-group @error('quantidade_disparos') has-danger @enderror">
                                        <label>DISPAROS</label>
                                        <input type="number" class="form-control" name="quantidade_disparos"
                                            value="{{ old('quantidade_disparos') ? old('quantidade_disparos') : (isset($last) ? $last->quantidade_disparos : 0) }}">
                                        @error('quantidade_disparos')
                                            <span class="material-icons form-control-feedback">clear</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-9 col-12">
                                    <div class="form-group bmd-form-group @error('local') has-danger @enderror">
                                        <label>LOCAL*</label>
                                        <input type="text" readonly class="form-control" required name="local"
                                            value="ACTRS">
                                        @error('local')
                                            <span class="material-icons form-control-feedback">clear</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-right">
                            <button type="submit" class="btn btn-success pull-right criar" {{!$saldo?"disabled":""}}>Criar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(() => {
            if ($('#popUp-cadastro-novamente').length) {
                Swal.fire({
                    title: 'Deseja cadastrar outra habitualidade?',
                    showCancelButton: true,
                    confirmButtonText: 'Sim',
                    cancelButtonText: 'Não',
                    showLoaderOnConfirm: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                }).then((result) => {
                    if (!result.value) {
                        window.location.href = '{{ route('painel-habitualidade-listagem-associado') }}';
                    }
                })
            }
            console.log(window.location.origin)

            if (window.location.origin !== 'http://192.168.0.39' && window.location.origin !==
                'http://25.60.73.76') {

                if ($('input[name="user_agent"]').length) {
                    $('input[name="user_agent"]').val(navigator.userAgent);
                }
                if ($('input[name="plataforma"]').length) {
                    $('input[name="plataforma"]').val(navigator.platform);
                }
                if ($('input[name="ip"]').length) {
                    $.get('https://www.cloudflare.com/cdn-cgi/trace', function(data) {
                        data = data.trim().split('\n').reduce(function(obj, pair) {
                            pair = pair.split('=');
                            return obj[pair[0]] = pair[1], obj;
                        }, {});
                        $('input[name="ip"]').val(data.ip);
                    });
                }
                if ($('input[name="latitude"]').length && $('input[name="longitude"]').length) {
                    const successCallback = (position) => {
                        $('input[name="latitude"]').val(position.coords.latitude);
                        $('input[name="longitude"]').val(position.coords.longitude)
                    };

                    const errorCallback = (error) => {
                        console.log(error)
                        let lat = 0;
                        let lon = 0;
                        if (!!$('input[name="latitude"]').val()) {
                            lat = $('input[name="latitude"]').val();
                        }
                        if (!!$('input[name="longitude"]').val()) {
                            lon = $('input[name="longitude"]').val();
                        }
                        $.ajax({
                            url: '{{ route('store-log') }}',
                            type: 'POST',
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            },
                            data: {
                                error: error.message,
                                latitude: lat,
                                longitude: lon,
                                user: '{{ $user->id }}'
                            },
                            success: function(response) {
                                console.log('Erro registrado no servidor', response);
                            },
                            error: function(xhr, status, error) {
                                console.log('Erro ao registrar o log no servidor', error);
                            }
                        });
                        window.location.href =
                            '{{ route('painel-index') }}?type=alert-danger&message=Você precisa permitir sua localização para poder cadastrar habitualidades. Libere acesso a localização e escaneie o QR Code novamente.';
                    };

                    navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
                }
            }
            $("#suas-armas").change(function() {
                $("#armas-clube").val("").selectpicker('refresh');
            });

            $("#armas-clube").change(function() {
                $("#suas-armas").val("").selectpicker('refresh');
            });

            $('#suas-armas').select2();
            $('#armas-clube').select2();

            $('#suas-armas').on('change', function() {
                if ($(this).val() !== "") {
                    $('#armas-clube').val('').prop('disabled', true).selectpicker('refresh');
                    $('#select2-armas-clube-container').text('SELECIONE')
                } else {
                    $('#armas-clube').prop('disabled', false).selectpicker('refresh');
                }
            });
            $('#armas-clube').on('change', function() {
                if ($(this).val() !== "") {
                    $('#suas-armas').val('').prop('disabled', true).selectpicker('refresh');
                    $('#select2-suas-armas-container').text('SELECIONE')
                } else {
                    $('#suas-armas').prop('disabled', false).selectpicker('refresh');
                }
            });

            if ($('#suas-armas').val() !== "") {
                $('#armas-clube').val('').prop('disabled', true).selectpicker('refresh');
            }

            if ($('#armas-clube').val() !== "") {
                $('#suas-armas').val('').prop('disabled', true).selectpicker('refresh');
            }

        });
    </script>
@endsection
