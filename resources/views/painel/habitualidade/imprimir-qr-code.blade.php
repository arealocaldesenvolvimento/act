@extends('layouts.material')
@section('title', 'Habitualidade - Cadastro por QR Code')
@section('content')
    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                            <a class="nav-link gerar-qr-code" href="{{route('painel-habitualidade-gerar-qr-code')}}"><i class="material-icons">add</i> Gerar novo QR Code</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content">
                <div class="row">
                    <div id="imprimir">
                        <h1 style="text-align: center; margin: 30px 0px;">Escaneie esse QR Code para registrar sua habitualidade:</h1>
                        <div style="text-align: center; margin-bottom: 30px" class="qr-code-body">
                            {!! QrCode::size(300)->generate(route('painel-habitualidade-qr-code', $qr_code)) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 text-right">
                        <button type="submit" class="btn btn-success pull-right">Imprimir</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(".btn-success").click(function () {
            var imprimir = document.querySelector("#imprimir").cloneNode(true);
            var printWindow = window.open('', 'my div', 'height=800,width=1200');
            printWindow.document.write('<html><head><title>Receita</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.body.appendChild(imprimir);
            printWindow.document.write('</body></html>');
            setTimeout(function () {
                printWindow.focus();
                printWindow.print();
                printWindow.close();
            }, 1000);
	    });
    </script>
@endsection
