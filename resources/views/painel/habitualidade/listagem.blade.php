@extends('layouts.material')
@section('title', 'Habitualidade - Listagem')
@section('content')
    <style>
        .swal2-container .card-header {
            background: linear-gradient(60deg, #2a6cbd, #003b84);
            border-radius: 3px;
            margin-top: -30px;
        }

        .swal2-container .swal2-title {
            color: white;
            margin-bottom: 0px;
        }

        .swal2-container .swal2-content {
            flex-grow: 1;
        }

        .swal2-container .swal2-close {
            top: -20px;
            right: 20px;
            color: white;
        }

        .swal2-popup {
            padding-top: 0px;
        }

        #tabela-hab {
            width: 100% !important;
        }

        #tabela-hab_wrapper .dt-buttons.btn-group.flex-wrap{
            gap: 20px;
        }

        #tabela-hab_wrapper .dt-buttons.btn-group.flex-wrap .btn.btn-secondary.buttons-html5{
            border-radius: 3px !important;
        }

        .table-responsive .filtro-data {
            display: flex;
            align-items: flex-end;
            justify-content: center;
            flex-direction: column;
            flex-wrap: wrap;
            margin-bottom: -56px;
        }

        .table-responsive .filtro-data .datas-container {
            display: flex;
            align-items: center;
            justify-content: center;
            flex-wrap: wrap;
            gap: 20px;
        }

        @media (max-width: 480px) {
            .table-responsive .filtro-data .datas-container {
                width: 100%;
            }

            .table-responsive .filtro-data .datas-container label {
                flex-grow: 1;
            }
        }
    </style>
    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        @if (
                            \Illuminate\Support\Facades\Auth::user()->tipo == 'presidente' ||
                                \Illuminate\Support\Facades\Auth::user()->tipo == 'secretario')
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ route('painel-habitualidade-listagem') }}"><i
                                        class="material-icons">view_list</i> Listagem de habitualidades</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('painel-habitualidade-cadastro') }}"><i
                                        class="material-icons">add</i> Cadastrar nova habitualidade</a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ route('painel-habitualidade-listagem-associado') }}"><i
                                        class="material-icons">view_list</i> Listagem de habitualidades</a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content">
                @if (count($habitualidades) <= 0)
                    <div class="alert alert-warning" role="alert">
                        Nenhuma habitualidade cadastrada
                    </div>
                @endif
                <div class="table-responsive">
                    <div class="filtro-data d-none">
                        <div class="datas-container">
                            <label>Data Inicial:
                                <input class="form-control form-control-sm" type="date" id="startDate" value="">
                            </label>
                            <label>Data Final:
                                <input class="form-control form-control-sm" type="date" id="endDate" value="">
                            </label>
                        </div>
                        <button class="btn btn-primary" id="dateFilter">Filtrar</button>
                    </div>
                    <table id="tabela-hab" class="table datatable d-none">
                        <thead>
                            <tr>
                                <th>DATA</th>
                                <th>ID</th>
                                <th>ASSOCIADO</th>
                                <th>CR</th>
                                <th>ARMA</th>
                                <th>QTD. DISP</th>
                                <th>EVENTO</th>
                                <th>LOCAL</th>
                                <th>PÁG</th>
                                <th>CALIBRE</th>
                                @if (
                                    \Illuminate\Support\Facades\Auth::user()->tipo == 'presidente' ||
                                        \Illuminate\Support\Facades\Auth::user()->tipo == 'secretario')
                                    <th>Tipo da Arma</th>
                                    <th>Validade da Arma</th>
                                    <th>Calibre é restrito</th>
                                    <th>Data do registro</th>
                                    <th>Modificado em</th>
                                    <th>Navegador</th>
                                    <th>Sist. OP</th>
                                    <th>Endereço IP</th>
                                    <th>Latitude</th>
                                    <th>Longitude</th>
                                    <th style="text-align: center">OPÇÕES</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($habitualidades as $habitualidade)
                                <tr>
                                    <td>{{ $habitualidade->data }}</td>
                                    <td>{{ $habitualidade->id }}</td>
                                    <td>{{ $habitualidade->nome }}</td>
                                    <td>{{ $habitualidade->cr }}</td>
                                    <td>{{ $habitualidade->marca }}/{{$habitualidade->modelo}}</td>
                                    <td>{{ $habitualidade->quantidade_disparos }}</td>
                                    <td>{{ $habitualidade->titulo }}</td>
                                    <td>{{ $habitualidade->local }}</td>
                                    <td>{{ $habitualidade->pagina }}</td>
                                    <td>{{ $habitualidade->calibre }}</td>
                                    @if (
                                        \Illuminate\Support\Facades\Auth::user()->tipo == 'presidente' ||
                                            \Illuminate\Support\Facades\Auth::user()->tipo == 'secretario')
                                        <td>{{ $habitualidade->tipo }}</td>
                                        <td>{{ $habitualidade->validade }}</td>
                                        <td>{{ $habitualidade->calibre_restrito ? 'sim' : 'nao' }}</td>
                                        <td>{{ $habitualidade->created_at }}</td>
                                        <td>{{ $habitualidade->updated_at }}</td>
                                        <td>{{ $habitualidade->user_agent }}</td>
                                        <td>{{ $habitualidade->plataforma }}</td>
                                        <td>{{ $habitualidade->ip }}</td>
                                        <td>{{ $habitualidade->latitude }}</td>
                                        <td>{{ $habitualidade->longitude }}</td>
                                        <td class="td-actions text-right">
                                            <input type="hidden" value="{{ $habitualidade->id }}" class="id">
                                            <a href="{{ route('painel-habitualidade-editar', $habitualidade->id) }}"
                                                class="btn btn-success">
                                                <i class="material-icons">edit</i>
                                            </a>
                                            <button type="button" class="btn btn-danger delete">
                                                <i class="material-icons">delete</i>
                                            </button>
                                            <button type="button" class="btn btn-primary info">
                                                <i class="material-icons">info</i>
                                            </button>
                                        </td>
                                    @endif
                                </tr>
                            @empty
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="row spinner">
                    <div class="col-12 text-center">
                        <div class="lds-spinner">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(() => {
            $('button.delete').click(function() {
                var button = $(this);
                Swal.fire({
                    title: 'Você tem certeza que deseja remover essa habitualidade?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#c0cdd6',
                    confirmButtonText: 'Sim, remover!',
                    cancelButtonText: 'Cancelar!'
                }).then((result) => {
                    if (result.value) {
                        $.post('{{ route('painel-habitualidade-destroy') }}', {
                            _token: '{{ csrf_token() }}',
                            id: button.siblings('.id').val()
                        });
                        button.parent().parent().find('*').css('text-decoration', 'line-through')
                            .hide('slow');
                    }
                })
            });

            $('button.info').click(function() {
                var button = $(this);
                $.ajax({
                    url: '{{ route('painel-index') }}/presenca/dados/' + button.parent().find(
                        'input.id').val(),
                    type: 'GET',
                    dataType: 'json',
                    cache: false,
                }).done(function(data) {
                    let text = '<div style="padding-top: 20px;">'
                    for (let x in data) {
                        if (!!data[x]) {
                            text += "<p><strong>" + x + ":</strong> " + data[x] + "</p>";
                        }
                    }
                    if (!!data.Latitude && data.Longitude) {
                        text +=
                            "<p><strong>Link: </strong> <a href='https://www.google.com/maps/search/?api=1&query=" +
                            data.Latitude + "," + data.Longitude +
                            "' target='_blank' rel='noopener noreferrer'>Veja no mapa</a></p>";
                    }
                    text += "</div>"
                    Swal.fire({
                        title: 'Informações detalhadas',
                        showCancelButton: false,
                        showConfirmButton: false,
                        showCloseButton: true,
                        html: text,
                        customClass: {
                            header: 'card-header card-header-primary'
                        }
                    })
                });

            });

            configDatatables["buttons"] = [{
                    extend: 'pdf',
                    text: 'Exportar para PDF',
                    orientation: 'landscape',
                    exportOptions: {
                        @if (
                            \Illuminate\Support\Facades\Auth::user()->tipo == 'presidente' ||
                                \Illuminate\Support\Facades\Auth::user()->tipo == 'secretario')
                            columns: [0, 1, 2, 3, 4, 5, 6, 7,8 ,9, 10, 11, 12],
                        @else
                            columns: [0, 1, 2, 3, 4, 5, 6, 7],
                        @endif
                    },
                    customize: function(doc) {
                        var table = doc.content[1].table;
                        @if (
                            \Illuminate\Support\Facades\Auth::user()->tipo == 'presidente' ||
                                \Illuminate\Support\Facades\Auth::user()->tipo == 'secretario')
                            // Get the table
                            for (var i = 0; i < table.body.length; i++) {
                                var row = table.body[i];
                                var cell = row[10];
                                var text = cell.text;
                                if (text.length > 11) {
                                    var newText = "";
                                    for (var j = 0; j < text.length; j++) {
                                        if (j > 0 && j % 11 == 0) {
                                            newText += "\n";
                                        }
                                        newText += text.charAt(j);
                                    }
                                    cell.text = newText;
                                }
                                var cell = row[11];
                                var text = cell.text;
                                if (text.length > 10) {
                                    var newText = "";
                                    for (var j = 0; j < text.length; j++) {
                                        if (j > 0 && j % 10 == 0) {
                                            newText += "\n";
                                        }
                                        newText += text.charAt(j);
                                    }
                                    cell.text = newText;
                                }
                                var cell = row[12];
                                var text = cell.text;
                                if (text.length > 10) {
                                    var newText = "";
                                    for (var j = 0; j < text.length; j++) {
                                        if (j > 0 && j % 10 == 0) {
                                            newText += "\n";
                                        }
                                        newText += text.charAt(j);
                                    }
                                    cell.text = newText;
                                }
                            }
                        @endif
                    }
                },
                {
                    extend: 'excel', // Usando a extensão 'excel'
                    text: 'Exportar para Excel',
                    exportOptions: {
                        @if (
                            \Illuminate\Support\Facades\Auth::user()->tipo == 'presidente' ||
                                \Illuminate\Support\Facades\Auth::user()->tipo == 'secretario')
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19],
                        @else
                            columns: [0, 1, 2, 3, 4, 5, 6, 7],
                        @endif
                    },
                    customize: function(xlsx) {
                        var table = xlsx.xl.worksheets['sheet1.xml'];

                    }
                }
            ];

            configDatatables['initComplete'] = function() {
                $('#tabela-hab').removeClass('d-none');
                $('.filtro-data').removeClass('d-none');
                $('div.spinner').remove();
            };
            configDatatables["order"] = [
                [0, 'desc']
            ];
            configDatatables['columnDefs'] = [{
                    "searchable": true,
                    "bVisible" : true,  //associado
                    "targets": 2
                },
                {
                    "searchable": true,
                    "bVisible" : false,
                    "targets": 3  //cr
                },
                {
                    "searchable": true,
                    "bVisible" : false,  //arma
                    "targets": 4
                },
                {
                    "searchable": true,
                    "bVisible" : true, //qtd Disparos
                    "targets": 5
                },
                {
                    "searchable": true,
                    "bVisible" : true, // evento
                    "targets": 6
                },
                {
                    "searchable": true,
                    "bVisible" : false,  //Local
                    "targets": 7
                },
                {
                    "searchable": true,
                    "bVisible" : false,
                    "targets": 8
                },
                @if (
                    \Illuminate\Support\Facades\Auth::user()->tipo == 'presidente' ||
                        \Illuminate\Support\Facades\Auth::user()->tipo == 'secretario')
                    {
                        "searchable": false,
                        "bVisible": false,
                        "targets": 8
                    }, {
                        "searchable": false,
                        "bVisible": true,
                        "targets": 9
                    }, {
                        "searchable": false,
                        "bVisible": false,
                        "targets": 10
                    }, {
                        "searchable": false,
                        "bVisible": false,
                        "targets": 11
                    }, {
                        "searchable": false,
                        "bVisible": false,
                        "targets": 12
                    }, {
                        "searchable": false,
                        "bVisible": false,
                        "targets": 13
                    }, {
                        "searchable": false,
                        "bVisible": false,
                        "targets": 14
                    }, {
                        "searchable": false,
                        "bVisible": false,
                        "targets": 15
                    }, {
                        "searchable": false,
                        "bVisible": false,
                        "targets": 16
                    }, {
                        "searchable": false,
                        "bVisible": false,
                        "targets": 17
                    }, {
                        "searchable": false,
                        "bVisible": false,
                        "targets": 18
                    }, {
                        "searchable": false,
                        "bVisible": false,
                        "targets": 19
                    },
                @endif
            ]

            $('.datatable').DataTable(configDatatables);

            // Definindo os event listeners para os campos de data e o botão de filtro
            $('#dateFilter').click(function() {
                $('.datatable').DataTable().draw();
            });

            $.fn.dataTable.ext.search.push(
                function(settings, data, dataIndex) {
                    var startDate = new Date($('#startDate').val()).getTime();
                    var endDate = new Date($('#endDate').val()).getTime();

                    // Supondo que a data na DataTable esteja na primeira coluna (índice 0) e no formato YYYY-MM-DD
                    var tableDate = new Date(data[0]).getTime();

                    // Se ambas as datas não forem definidas, retorne verdadeiro (não filtre nada)
                    if (isNaN(startDate) && isNaN(endDate)) {
                        return true;
                    }

                    if ((!isNaN(startDate) && !isNaN(endDate)) && (tableDate >= startDate && tableDate <=
                            endDate)) {
                        return true;
                    } else if (isNaN(startDate) && !isNaN(endDate) && tableDate <= endDate) {
                        return true;
                    } else if (!isNaN(startDate) && isNaN(endDate) && tableDate >= startDate) {
                        return true;
                    }
                    return false;
                }
            );
        });
    </script>
@endsection
