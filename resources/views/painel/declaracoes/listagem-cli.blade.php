{{-- PORTARIA 166 Fev. 2024
http://www.dfpc.eb.mil.br/documentos/portaria166.pdf --}}

@extends('layouts.material')
@section('title', 'Declarações')
@section('content')


<style>
    .container-checkbox {
        display: block;
        position: relative;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 22px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        padding-top: 10px;
    }

    .container-checkbox p{
        display: flex;
        margin-left: 35px;
    }

    /* Hide the browser's default checkbox */
    .container-checkbox input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
        position: absolute;
        top: 10px;
        left: 0;
        height: 25px;
        width: 25px;
        background-color: #eee;
    }

    /* On mouse-over, add a grey background color */
    .container-checkbox:hover input~.checkmark {
        background-color: #ccc;
    }

    /* When the checkbox is checked, add a blue background */
    .container-checkbox input:checked~.checkmark {
        background-color: #2196F3;
    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the checkmark when checked */
    .container-checkbox input:checked~.checkmark:after {
        display: block;
    }

    /* Style the checkmark/indicator */
    .container-checkbox .checkmark:after {
        left: 9px;
        top: 5px;
        width: 5px;
        height: 10px;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }

    .badge {
        font-size: 90% !important;
    }

    #swal2-content #calibre{
        padding: 10px;
        color: #666;
        border: none;
        border-bottom: 2px solid;
        background: white;
        box-shadow: 0px 0px 5px #6666666b;
    }
</style>



    <div class="card m-0">
        <div class="card-body bg-light">
            <h3>Emitir declarações</h3>
            <div class="row">
                {{--FILIACAO--}}
                @component('painel.declaracoes.componentes.card')
                    Filiação
                    @slot('link') {{route('painel-declaracoes-filiacao')}} @endslot
                    @slot('erros')
                        @if(is_null(Auth::user()->nome))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">NOME NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cpf))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">CPF NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cadastro))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">Nº CADASTRO NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->data_filiacao))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">DATA FILIAÇÃO NÃO CADASTRADA </p>
                        @endif
                        @if(!$saldo)
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">SALDO NEGATIVO</p>
                        @endif
                    @endslot
                @endcomponent


                {{--GUIA--}}
                {{-- @component('painel.declaracoes.componentes.card')
                    GUIA TRÁFEGO
                    @slot('link') {{route('painel-declaracoes-guiatrafego')}} @endslot
                    @slot('erros')
                        @if(is_null(Auth::user()->nome))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">NOME NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cpf))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">CPF NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cr))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">CR NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cadastro))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">Nº CADASTRO NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->data_filiacao))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">DATA FILIAÇÃO NÃO CADASTRADA </p>
                        @endif
                        @if(!$saldo)
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">SALDO NEGATIVO</p>
                        @endif
                    @endslot
                @endcomponent --}}


                {{--RANKING--}}
                {{-- @component('painel.declaracoes.componentes.card')
                    RANKING
                    @slot('link') {{route('painel-declaracoes-ranking')}} @endslot
                    @slot('erros')
                        @if(is_null(Auth::user()->nome))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">NOME NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cr))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">CR NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cpf))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">CPF NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cadastro))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">Nº CADASTRO NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->data_filiacao))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">DATA FILIAÇÃO NÃO CADASTRADA </p>
                        @endif
                        @if(!$saldo)
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">SALDO NEGATIVO</p>
                        @endif
                    @endslot
                @endcomponent --}}


                {{--HABITUALIDADE--}}
                {{-- @component('painel.declaracoes.componentes.card')
                    HABITUALIDADE
                    @slot('link') {{route('painel-declaracoes-habitualidade')}} @endslot
                    @slot('erros')
                        @if(is_null(Auth::user()->nome))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">NOME NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cr))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">CR NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cpf))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">CPF NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cadastro))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">Nº CADASTRO NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->data_filiacao))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">DATA FILIAÇÃO NÃO CADASTRADA </p>
                        @endif
                        @if(!$saldo)
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">SALDO NEGATIVO</p>
                        @endif
                        @if(is_null(Auth::user()->data_primeira_habitualidade))
                            @if($provas < 2)
                                <p class="text-danger m-0" style="font-size: 10px; line-height: 1">PRESENÇAS INSUFICIENTES</p>
                            @endif
                        @else
                            @if($presencaComDisparos < 8)
                                <p class="text-danger m-0" style="font-size: 10px; line-height: 1">PRESENÇAS INSUFICIENTES</p>
                            @endif
                        @endif
                    @endslot
                @endcomponent --}}
                {{--HABITUALIDADE - PORTARIA 150--}}
                {{-- @component('painel.declaracoes.componentes.card')
                    HABITUALIDADE PORTARIA 150
                    @slot('link') {{route('painel-declaracoes-habitualidade-portaria150')}} @endslot
                    @slot('erros')
                        @if(is_null(Auth::user()->nome))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">NOME NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cr))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">CR NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cpf))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">CPF NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cadastro))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">Nº CADASTRO NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->data_filiacao))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">DATA FILIAÇÃO NÃO CADASTRADA </p>
                        @endif
                        @if(!$saldo)
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">SALDO NEGATIVO</p>
                        @endif
                    @endslot
                @endcomponent --}}

                {{--HABITUALIDADE - PORTARIA 166--}}

                {{-- <div class="col-12 col-md-3">
                    <div class="card mt-0 mb-3 bg-light">
                        <div class="card-body text-center hab-166" style="position: relative; cursor:pointer ">
                            <img width="50" class="mb-3" src="{{asset('images/pdf.svg')}}" alt="arquivo doc">
                            <br/>
                            <h4 class="mb-0 text-muted">HABITUALIDADE PORTARIA 166</h4>
                            <i style="position:absolute;top: 10px; right:10px; font-size: 30px;" class="material-icons text-success">check</i>
                        </div>
                    </div>
                </div> --}}

                {{--MODALIDADE E PROVA--}}
                @component('painel.declaracoes.componentes.card')
                    HABITUALIDADE PORTARIA 166
                    @slot('link') JavaScript:Void(0) @endslot
                    @slot('atributos') {{!is_null(Auth::user()->cr)?'id=hab-166':""}} data-toggle="modal" data-target="#habitualidadeModal" @endslot
                    @slot('erros')
                        @if(is_null(Auth::user()->cr))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">CR NÃO CADASTRADO</p>
                        @endif
                        @if(!$saldo)
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">SALDO NEGATIVO</p>
                        @endif
                    @endslot
                @endcomponent

                {{--MODALIDADE E PROVA--}}
                @component('painel.declaracoes.componentes.card')
                    MODALIDADE E PROVA
                    @slot('link') JavaScript:Void(0) @endslot
                    @slot('atributos') data-toggle="modal" data-target="#modalideModal" @endslot
                    @slot('erros')
                        @if(is_null(Auth::user()->nome))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">NOME NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cr))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">CR NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cpf))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">CPF NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cadastro))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">Nº CADASTRO NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->data_filiacao))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">DATA FILIAÇÃO NÃO CADASTRADA </p>
                        @endif
                        @if(!$saldo)
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">SALDO NEGATIVO</p>
                        @endif
                    @endslot
                @endcomponent


                {{--SEGURANÇA E ACERVO--}}
                {{-- @component('painel.declaracoes.componentes.card')
                    SEGURANÇA E ACERVO
                    @slot('link') {{route('painel-seguranca-acervo')}} @endslot
                    @slot('erros')
                        @if(is_null(Auth::user()->nome))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">NOME NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cr))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">CR NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cpf))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">CPF NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cadastro))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">Nº CADASTRO NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->data_filiacao))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">DATA FILIAÇÃO NÃO CADASTRADA </p>
                        @endif
                        @if(!$saldo)
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">SALDO NEGATIVO</p>
                        @endif
                    @endslot
                @endcomponent --}}


                <div class="col-12 col-md-3">
                    <div class="card mt-0 mb-3 @if(!$saldo) bg-light @endif">
                        <div class="card-body text-center {{$saldo?"click-seguranca":""}}" style="position: relative; cursor:pointer ">
                            <img width="50" class="mb-3" src="{{asset('images/pdf.svg')}}" alt="arquivo doc">
                            <br/>
                            <h4 class="mb-0 text-muted">SEGURANÇA E ACERVO</h4>
                            <i style="position:absolute;top: 10px; right:10px; font-size: 30px;" class="material-icons {{$saldo?"text-success":"text-danger"}}">{{$saldo?"check":"close"}}</i>
                            @if(!$saldo)
                                <p class="text-danger m-0" style="font-size: 10px; line-height: 1">SALDO NEGATIVO</p>
                            @endif
                        </div>
                    </div>
                </div>
                {{-- COMPROMISSO --}}
                @component('painel.declaracoes.componentes.card')
                    COMPROMISSO
                    @slot('link') {{route('painel-compromisso')}} @endslot
                    @slot('erros')
                        @if(is_null(Auth::user()->nome))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">NOME NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cpf))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">CPF NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->cadastro))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">Nº CADASTRO NÃO CADASTRADO</p>
                        @endif
                        @if(is_null(Auth::user()->data_filiacao))
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">DATA FILIAÇÃO NÃO CADASTRADA </p>
                        @endif
                        @if(!$saldo)
                            <p class="text-danger m-0" style="font-size: 10px; line-height: 1">SALDO NEGATIVO</p>
                        @endif
                    @endslot
                @endcomponent

            </div>
            <div class="row">
                <div class="col text-right">
                    <label class="mb-3 text-muted">Toda declaração gerada será registrada para auditoria do clube!</label>
                </div>
            </div>
        </div>
    </div>
    {{-- MODAL PARA MODALIDADE E PROVA --}}
    <div class="modal fade" id="modalideModal" tabindex="-1" role="dialog" aria-labelledby="modalideModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">INFORMAÇÔES</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('painel-declaracoes-modalidade-prova')}}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="selectModalidade">MODALIDADE*</label>
                                    <select class="form-control selectpicker" data-style="btn btn-link" id="selectModalidade" name="modalidade" required>
                                        <option value="ARMA LONGA">ARMA LONGA</option>
                                        <option value="ARMA CURTA">ARMA CURTA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="selectTipo">TIPO DE ARMA*</label>
                                    <select class="form-control selectpicker" data-style="btn btn-link" id="selectTipo" name="tipo" required>
                                        <option value="ESPINGARDA">ESPINGARDA</option>
                                        <option value="CARABINA">CARABINA</option>
                                        <option value="RIFLE">RIFLE</option>
                                        <option value="FUZIL">FUZIL</option>
                                        <option value="REVÓLVER">REVÓLVER</option>
                                        <option value="PISTOLA">PISTOLA</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group bmd-form-group">
                                    <label>CALIBRE*</label>
                                    <ul class="list-group" style="text-align:left;">
                                        <li class="list-group-item"><input id="calibreProva" name="calibre[]" class="form-check-input me-1" type="checkbox" value="all">Todos os calibres</li>
                                        @foreach ($all_calibres as $calibre)
                                            <li class="list-group-item"><input id="calibreProva" name="calibre[]" class="form-check-input me-1" type="checkbox" value="{{ $calibre->nome }}">{{$calibre->nome}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
                        <button type="submit" class="btn btn-primary">GERAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        $(document).ready(() =>{
            $('#modalideModal button[type="submit"]').click(()=>{
                $('#modalideModal').modal('hide');
            });

            $('#modalideModal').on('hidden.bs.modal', function () {
                $('#modalideModal form')[0].reset();
            })
            $('#hab-166').click(function() {
                var button = $(this);
                Swal.fire({
                    title: "Filtre as habitualidades",
                    html:   '<div class="row align-items-start">'+
                                '<div class="col-4"><h4>Escolha o calibre</h4><br>' +
                                    '<ul class="list-group" style="text-align:left;">' +
                                        '<li class="list-group-item"><input id="calibre" name="calibre[]" class="form-check-input me-1" type="checkbox" value="all">Todos os calibres</li>' +
                                        '<?php foreach($calibres as $calibre) { ?> '+
                                            '<li class="list-group-item"><input id="calibre" name="calibre[]" class="form-check-input me-1" type="checkbox" value="<?= $calibre->calibre ?>"><?= $calibre->calibre ?></li>' +
                                        '<?php } ?>'+
                                    '</ul>'+
                                    '<label for="presencas">Presenças/Habitualidade por calibre</label>' +
                                    '<input type="number" name="presencas" id="presencas" class="form-control">'+
                                '</div>'+
                                '<div class="col-4">'+
                                    '<h4>Intervalo de datas</h4>'+
                                    '<small style="color:red">Deixe vazio para pegar todas</small>' +
                                    '<div><label for="inicio">Data inicial</label>' +
                                        '<input name="inicio" id="inicio" type="date" class="form-control mask-data">'+
                                    '</div>'+
                                    '<div><label for="final">Data final</label>' +
                                        '<input name="final" id="final" type="date" class="form-control mask-data">'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-4">' + 
                                    '<h4>Eventos</h4>' +
                                    '<div>'+
                                        '<ul class="list-group" style="text-align:left;">' +
                                            '<li class="list-group-item"><input id="eventos" name="eventos[]" class="form-check-input me-1" type="checkbox" value="all">Todos os eventos</li>' +
                                            '<?php foreach($eventos as $evento) { ?> '+
                                                '<li class="list-group-item"><input id="eventos" name="eventos[]" class="form-check-input me-1" type="checkbox" value="<?= $evento->id ?>"><?= $evento->titulo ?></li>' +
                                            '<?php } ?>'+
                                        '</ul>'+
                                    '</div>'+
                                '</div>'+
                            '</div>',
                    inputPlaceholder: "Gerar declaração pelo calibre",
                    customClass: "swal-wide",
                    confirmButtonText: "Gerar",
                    onOpen: ()=>{
                        jQuery('input#calibre[value="all"]').on("change", function name() {
                            if(jQuery('input#calibre[value="all"]').is(':checked')){
                                jQuery("input#calibre").attr("checked", true)
                            }else{
                                jQuery("input#calibre").removeAttr("checked")
                            }
                        });
                        jQuery('input#eventos[value="all"]').on("change", function name() {
                            if(jQuery('input#eventos[value="all"]').is(':checked')){
                                jQuery("input#eventos").attr("checked", true)
                            }else{
                                jQuery("input#eventos").removeAttr("checked")
                            }
                        });
                    },
                    showCloseButton: true,
                }).then((result) => {
                    if (result.value) {
                        let calibres = [];
                        jQuery('input[name="calibre[]"]').each(function(){
                            if(jQuery(this).is(':checked')){
                                calibres.push(jQuery(this).val());
                            }
                        });
                        let eventos = [];
                        jQuery('input[name="eventos[]"]').each(function(){
                            if(jQuery(this).is(':checked')){
                                eventos.push(jQuery(this).val());
                            }
                        });
                        const urlPrefixo = '{{ env('URL_LOCAL') }}';
                        const urlCadastro = `${urlPrefixo}/painel/gerar/habitualidade166`;
                        let inicio = jQuery("#inicio").val();
                        let fim = jQuery("#final").val();
                        let presencas = jQuery("#presencas").val();
                            axios.get(urlCadastro, {
                                params: {
                                    inicio: inicio, 
                                    fim: fim,
                                    presencas: presencas,
                                    calibres: calibres,
                                    eventos: eventos,
                                }
                            })
                            .then((response) => {
                                console.log(response.data)
                                if(response.data.tipo === 'erro'){

                                    console.error(response.data);

                                    //  Swal.fire("Erro", "Erro, atualize seus dados e tente novamente.",
                                        // "error").then(() => {
                                        // location.reload();
                                    // });
                                }else{
                                    Swal.fire("Sucesso", "Declaração emitida com sucesso!",
                                        "success").then(() => {

                                        const registro = response.data.registro;
                                        window.location.href = `${urlPrefixo}/validacao-declaracao/visualizar-documento/${registro}`;
                                    });
                                }
                            })
                            .catch((error) => {
                                console.error(error);
                                Swal.fire("Erro", "Ocorreu um erro ao emitir a declaração",
                                    "error");
                            });
                    }
                });
            });




            jQuery('input#calibreProva[value="all"]').on("change", function name() {
                if(jQuery('input#calibreProva[value="all"]').is(':checked')){
                    jQuery("input#calibreProva").attr("checked", true)
                }else{
                    jQuery("input#calibreProva").removeAttr("checked")
                }
            });
            $('.click-seguranca').click(function() {
                var button = $(this);
                Swal.fire({
                    title: "Escolha as opções",

                    html: '<input id="finalidade" name="finalidade" class="swal2-input" placeholder="Informe a finalidade">' +
                            '<label class="container-checkbox"><p>Colecionador</p>' +
                            '<input id="colecionador" type="checkbox" name="colecionador">' +
                            '<span class="checkmark"></span>' +
                            '</label>' +
                            '<label class="container-checkbox"><p>Atirador desportivo</p>' +
                            '<input id="atirador" type="checkbox" name="atirador">' +
                            '<span class="checkmark"></span>' +
                            '</label>' +
                            '<label class="container-checkbox"><p>Caçador excepcional</p>' +
                            '<input id="cacador" type="checkbox" name="cacador">' +
                            '<span class="checkmark"></span>' +
                            '</label>',

                    inputPlaceholder: "Gerar declaração pelo calibre",
                    confirmButtonText: "Gerar",
                    showCloseButton: true,
                }).then((result) => {
                    if (result.value) {
                        const urlPrefixo = '{{ env('URL_LOCAL') }}';
                        const urlCadastro = `${urlPrefixo}/painel/declaracoes/seguranca-acervo`;

                        const finalidade    = document.getElementById('finalidade').value;
                        const colecionador  = document.getElementById('colecionador').checked;
                        const atirador      = document.getElementById('atirador').checked;
                        const cacador       = document.getElementById('cacador').checked;

                        axios.post(urlCadastro, {
                                finalidade:     finalidade,
                                colecionador:   colecionador,
                                atirador:       atirador,
                                cacador:        cacador,
                            })
                            .then((response) => {
                                console.log(response.data)
                                if(response.data.tipo === 'erro'){
                                     Swal.fire("Erro", "Erro, atualize seus dados e tente novamente.",
                                        "error").then(() => {
                                        location.reload();
                                    });
                                }else{
                                    Swal.fire("Sucesso", "Declaração emitida com sucesso!",
                                        "success").then(() => {

                                        const registro = response.data.registro;
                                        window.location.href = `${urlPrefixo}/validacao-declaracao/visualizar-documento/${registro}`;
                                    });
                                }
                            })
                            .catch((error) => {
                                console.error(error);
                                Swal.fire("Erro", "Ocorreu um erro ao emitir a declaração",
                                    "error");
                            });
                    }
                });
            });
        });

    </script>
@endsection

