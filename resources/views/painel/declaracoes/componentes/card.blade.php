<div class="col-12 col-md-3">
    <a href="@if($erros == ""){{$link}} @else JavaScript:Void(0) @endif" class="text-black" {{$atributos ?? ""}}>
        <div class="card mt-0 mb-3 @if($erros != "") bg-light @endif">
            <div class="card-body text-center" style="position: relative;">
                <img width="50" class="mb-3" src="{{asset('images/pdf.svg')}}" alt="arquivo doc">
                <br/>
                <h4 class="mb-0 text-muted">{{$slot}}</h4>
                @if($erros == "")
                    <i style="position:absolute;top: 10px; right:10px; font-size: 30px;" class="material-icons text-success">check</i>
                @else
                    <i style="position:absolute;top: 10px; right:10px; font-size: 30px;" class="material-icons text-danger">close</i>
                @endif
                {{$erros ?? ""}}
            </div>
        </div>
    </a>
</div>
