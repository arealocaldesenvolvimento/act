<!DOCTYPE html>

<html lang="pt-BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Declaração</title>
        <style>
            /* cyrillic-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCkYb8td.woff2) format('woff2');
                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
            }
            /* cyrillic */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCAYb8td.woff2) format('woff2');
                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
            }
            /* greek-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCgYb8td.woff2) format('woff2');
                unicode-range: U+1F00-1FFF;
            }
            /* greek */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCcYb8td.woff2) format('woff2');
                unicode-range: U+0370-03FF;
            }
            /* vietnamese */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCsYb8td.woff2) format('woff2');
                unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
            }
            /* latin-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCoYb8td.woff2) format('woff2');
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }
            /* latin */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCQYbw.woff2) format('woff2');
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }
            /* cyrillic-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-19-7DRs5.woff2) format('woff2');
                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
            }
            /* cyrillic */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-19a7DRs5.woff2) format('woff2');
                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
            }
            /* greek-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-1967DRs5.woff2) format('woff2');
                unicode-range: U+1F00-1FFF;
            }
            /* greek */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-19G7DRs5.woff2) format('woff2');
                unicode-range: U+0370-03FF;
            }
            /* vietnamese */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-1927DRs5.woff2) format('woff2');
                unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
            }
            /* latin-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-19y7DRs5.woff2) format('woff2');
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }
            /* latin */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-19K7DQ.woff2) format('woff2');
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }
            /* cyrillic-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCkYb8td.woff2) format('woff2');
                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
            }
            /* cyrillic */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCAYb8td.woff2) format('woff2');
                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
            }
            /* greek-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCgYb8td.woff2) format('woff2');
                unicode-range: U+1F00-1FFF;
            }
            /* greek */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCcYb8td.woff2) format('woff2');
                unicode-range: U+0370-03FF;
            }
            /* vietnamese */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCsYb8td.woff2) format('woff2');
                unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
            }
            /* latin-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCoYb8td.woff2) format('woff2');
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }
            /* latin */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCQYbw.woff2) format('woff2');
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }
            *{
                font-family: 'Roboto Condensed',sans-serif !important;
            }
            p,h1,h2,h3{
                color: #3e3e3e;

            }
            h1{
                font-size: 14px;
            }
            h2{
                font-size: 10px;
            }
            p{
                font-size: 12px;
                line-height: 1.4;
            }
            h2.title{
                text-align: center;
                margin: 20px 0 10px 0;
            }
            img{
                display: inline-block;
                max-width: 100%;
            }
            .container{
                max-width: 600px;
                margin: 0 auto;
                position: relative;
            }
            .head{
                margin: 20px 0 0 0;
                text-align: center;
            }
            .texto{
                font-size: 11px;
                text-align: center;
            }
            .left {
                font-size: 11px;
                text-align: start;
            }
            .data{
                width: 100%;
                text-align: right;
                margin-top: 20px;
            }
            footer{
                /*margin-top: 30px;*/
                border-top: 2px solid #232323;
                text-align: center;
            }
            footer p{
                font-size: 12px;
                margin-top: 0px;
            }
            .left{
                display: inline-block;
                width: 69%;
                vertical-align: top;
            }
            .right{
                display: inline-block;
                width: 30%;
                text-align: right;
            }
            .presidente{
                width: 100%;
                text-align: left;
                margin-top: 50px;
            }
            .presidente img{
                display: block;
            }
            .presidente p{
                margin: 0;
                display: block;
                width: 100%;
            }
            .presidente p.cargo{
                /* padding-left: 55px; */
                font-size: 10px;
            }
            .presidente .right-presidente{
                font-size: 8px;
            }
            .registro{
                position: absolute;
                top: 0;
                right: 0;
            }
            table{
                width: 100%;
                border-collapse: collapse;
            }
            table th, table td{
                border: 1px solid #3e3e3e;
                padding: 1px 5px;
                border-collapse: collapse;
                color: #3e3e3e;
                font-size: 9px;
            }

            table.obs{
                width: 100%;
                border-collapse: collapse;
            }
            table.obs th, table.obs td{
                border: none;
                padding: 1px;
                border-collapse: collapse;
                font-size: 9px;
            }

            table.habitualidade th, table.habitualidade td{
                padding: 1px;
                font-size: 8px;
            }

            table td.center{
                text-align: center;
            }

            table.presidente{

            }
            table.presidente th, table.presidente td {
                border: none;
                padding: 0;
                border-collapse: collapse;
                color: black;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <p class="registro">Código: {{$dados['registro']}}</p>
            <div class="head">
                <div class="left">                    
                    <h1>ASSOCIAÇÃO DE CAÇA E TIRO RIO DO SUL</h1>
                </div>
                <div class="right">
                    <img src="images/logo.png">
                </div>
            </div>
            <p class="texto">COMPROVAÇÃO DE PARTICIPAÇÕES EM TREINAMENTOS E/OU COMPETIÇÕES DE TIRO (HABITUALIDADE)<br>
                (art. 35 do Decreto no 11.615/2023) – Anexo E da Portaria Colog no 166/2023</p>

            <h2 class="title">Dados da Entidade Declarante</h2>

            <table>
                <thead>
                    <tr>
                        <td style="width: 25%">Nome</td>                        
                        <td style="width: 40%">Associação de Caça e Tiro Rio do Sul</td>
                        <td style="width: 10%">CNPJ</td>
                        <td style="width: 25%">79.372.702/0001-56</td>
                    </tr>
                    <tr>
                        <td>Certificado de Registro</td>
                        <td>3121/5ª RM</td>
                        {{-- <td>{{$clube->cr}}</td>  --}}
                        <td>Data</td>
                        <td>{{\Carbon\Carbon::parse($clube->validade_cr)->format('d/m/Y')}}</td>
                    </tr>
                </thead>
            </table>

            <table>
                <thead>
                    <tr>
                        <td style="width: 25%">Endereço</td>                        
                        <td style="width: 75%">Rua Antônio Dolzani, 970 - Valada São Paulo - Rio do Sul/SC - 89162.190</td>
                    </tr>
                </thead>
                </thead>
            </table>

            <h2 class="title">Dados do Atirador Desportivo</h2>

            <table>
                <thead>
                    <tr>
                        <td style="width: 25%">Nome</td>
                        <td style="width: 40%">{{Auth::user()->nome}}</td>
                        <td style="width: 10%">CPF</td>
                        <td style="width: 25%">{{Auth::user()->cpf}}</td>
                    </tr>
                    <tr>
                        <td>Certificado de Registro</td>
                        <td>{{ Auth::user()->cr }}</td>
                        <td>Data</td>
                        <td>{{\Carbon\Carbon::parse(Auth::user()->validade_cr)->format('d/m/Y')}}</td>
                    </tr>
                </thead>
            </table>

            <table>
                <thead>
                    <tr>
                        <td style="width: 25%">Endereço</td>
                        {{-- <td style="width: 75%">desc rua, bairro nome bairro, cidade/uf</td> --}}
                        <td style="width: 75%">{{Auth::user()->endereco}} {{Auth::user()->bairro}}, {{Auth::user()->cidade}}/{{Auth::user()->estado}} </td>
                    </tr>
                </thead>
                </thead>
            </table>

            <table>
                <thead>
                    <tr>
                        <td style="width: 25%">Filiação à Entidade de Tiro</td>
                        <td style="width: 40%">Nº {{Auth::user()->cadastro}}</td>
                        <td style="width: 10%">Data</td>
                        <td style="width: 25%">{{\Carbon\Carbon::parse(Auth::user()->data_filiacao)->format('d/m/Y') }}</td>
                    </tr>
                </thead>
            </table>

            <h2 class="title">Dados da Habitualidade</h2>

            <table>
                <thead>
                    <tr>
                        <th style="width: 50%" colspan="4">CALIBRE USO PERMITIDO</th>
                        <th style="width: 15%; border-top: white" colspan="1"></th>
                        <th style="width: 35%" colspan="1">TIPO EVENTO</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Ordem</td>
                        <td>Data-Hora</td>
                        <td>Calibre</td>
                        <td>SIGMA (1)</td>
                        <td >Qtde Munição (2)</td>
                        <td >Treinamento ou Competição (Estadual, Distrital, Regional, Nacional ou Internacional)</td>
                    </tr>

                    @for ($i = 0; $i < count($dados['presencas']); $i++)

                        @if(isset($dados['presencas'][$i]) && !$dados['presencas'][$i]->calibre_restrito)
                            <tr>
                                <td>{{ $i + 1 }}</td>
                                <td>{{ \Carbon\Carbon::parse($dados['presencas'][$i]->data)->format('d/m/Y') }}</td>
                                <td class="center">{{ $dados['presencas'][$i]->calibre }}</td>
                                <td class="center">{{ $dados['presencas'][$i]->numero_serie }}</td>
                                <td class="center">{{ $dados['presencas'][$i]->quantidade_disparos }}</td>
                                <td class="center">{{ $dados['presencas'][$i]->titulo }}</td>
                            </tr>
                        @endif
                    @endfor
                </tbody>
            </table>

            <br>

            <table>
                <thead>
                    <tr>
                        <th style="width: 50%" colspan="4">CALIBRE USO RESTRITO</th>
                        <th style="width: 15%; border-top: white" colspan="1"></th>
                        <th style="width: 35%" colspan="1">TIPO EVENTO</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Ordem</td>
                        <td>Data-Hora</td>
                        <td>Calibre</td>
                        <td>SIGMA (1)</td>
                        <td >Qtde Munição (2)</td>
                        <td >Treinamento ou Competição (Estadual, Distrital,
                            Regional, Nacional ou Internacional)</td>
                    </tr>

                    @for ($i = 0; $i < count($dados['presencas']); $i++)
                        @if(isset($dados['presencas'][$i]) && $dados['presencas'][$i]->calibre_restrito)
                            <tr>
                                <td>{{ $i + 1 }}</td>
                                <td>{{ \Carbon\Carbon::parse($dados['presencas'][$i]->data)->format('d/m/Y') }}</td>
                                <td class="center">{{ $dados['presencas'][$i]->calibre }}</td>
                                <td class="center">{{ $dados['presencas'][$i]->numero_serie }}</td>
                                <td class="center">{{ $dados['presencas'][$i]->quantidade_disparos }}</td>
                                <td class="center">{{ $dados['presencas'][$i]->titulo }}</td>
                            </tr>
                        @endif
                    @endfor
                </tbody>
            </table>

            <h2 class="title">Registros da Habitualidade (3)</h2>
            <table class="habitualidade">
                <thead>
                    <tr>
                        <td style="width:15%">Livro/Sistema(4):</td>                        
                        <td style="width:15%">clubedetirorsl.com.br/validacao-declaracao/{{$dados['registro']}}</td>
                        <td style="width:15%">Folha/Nr Registro (4):</td>
                        <td style="width:30%">{{$ids}}</td>
                        <td style="width:15%">Data Lançamento</td>
                        <td style="width:15%">{{date('d/m/Y')}} </td>
                    </tr>
                </thead>
            </table>


            <p class="left" style="padding-bottom: 130px">E por ser verdade, firmo a presente declaração sob as penas da lei.
                <br>
                Rio do Sul, {{\Carbon\Carbon::parse(now())->format('d/m/Y')}}.
            </p>

            @isset($dados['presidente'])
            <div class="row-assinatura" style="width: 100%; display: flex; flex-wrap: wrap;height: 80px; align-items: center; padding-bottom: 20px">

                <div class="presidente"
                    style="width: 50%; display: flex; flex-wrap: wrap; height: 50px; align-items: center;">

                    <p style="text-align: center; font-size: 13px margin-bottom: 20px; padding-bottom: 20px" >CIENTE</p>

                   <div style="width: 150px;  padding-top:30px">
                        <div class="nome" style="border-right: 1px solid black; ">
                            <strong>
                                <p class="cargo">{{$dados['presidente']->nome ?? ""}}</p>
                                <p class="cargo">PRESIDENTE</p>
                            </strong>
                        </div>
                    </div>
                    <div class="right-presidente" style="padding-left: 160px; width: 150px; padding-top:30px ">
                        Assinado digitalmente por: JAISON MAURECI BECKER: 01995908932<br>
                        Data: {{ \Carbon\Carbon::parse(now())->format('d/m/Y H:i:s O') }}
                    </div>
                </div>

                <div class="qrcode" style="margin-left: 50%; width: 50px; display: flex; ">
                    <div class="img" style="width: 80px; margin-left: 80px;">
                        <img src="data:image/png;base64,{{ base64_encode($qrcode) }}" alt="QR Code">
                    </div>
                    <div class="confira" style="width: 100px; margin-left: 165px; ">
                        <p style="font-size: 9px; margin-top: 0">Este documento pode ser validado pelo QR Code ou pelo link: clubedetirorsl.com.br/validacao-declaracao/{{$dados['registro']}}</p>
                    </div>
                </div>
            </div>
        @endisset


            <table class="obs">
                <thead>
                    <tr>
                        <td style="width:10%"></td>
                        <td ></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Observações:</td>
                        <td>(1) Número da Arma no Sistema de Gerenciamento Militar de Armas (SIGMA). </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>(2) Quantidade de munições utilizadas na atividade.</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>(3) A temporalidade dos Registros de Habitualidade é permanente.</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>(4) Na eventualidade de a Entidade de Tiro adotar o Registro Eletrônico.</td>
                    </tr>
                </tbody>
            </table>

            <br>
            <footer>
                <p>Rua Antonio Dolzani, 970 - Bairro Valada São Paulo – Rio do Sul/SC
                    <br/>
                    CEP – 89162.190 - CNPJ nº 79.372.702/0001-56
                    <br/>
                </p>
            </footer>
        </div>

    </body>
</html>
