<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Declaração</title>
        <style>
            /* cyrillic-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCkYb8td.woff2) format('woff2');
                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
            }
            /* cyrillic */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCAYb8td.woff2) format('woff2');
                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
            }
            /* greek-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCgYb8td.woff2) format('woff2');
                unicode-range: U+1F00-1FFF;
            }
            /* greek */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCcYb8td.woff2) format('woff2');
                unicode-range: U+0370-03FF;
            }
            /* vietnamese */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCsYb8td.woff2) format('woff2');
                unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
            }
            /* latin-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCoYb8td.woff2) format('woff2');
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }
            /* latin */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCQYbw.woff2) format('woff2');
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }
            /* cyrillic-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-19-7DRs5.woff2) format('woff2');
                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
            }
            /* cyrillic */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-19a7DRs5.woff2) format('woff2');
                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
            }
            /* greek-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-1967DRs5.woff2) format('woff2');
                unicode-range: U+1F00-1FFF;
            }
            /* greek */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-19G7DRs5.woff2) format('woff2');
                unicode-range: U+0370-03FF;
            }
            /* vietnamese */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-1927DRs5.woff2) format('woff2');
                unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
            }
            /* latin-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-19y7DRs5.woff2) format('woff2');
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }
            /* latin */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-19K7DQ.woff2) format('woff2');
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }
            /* cyrillic-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCkYb8td.woff2) format('woff2');
                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
            }
            /* cyrillic */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCAYb8td.woff2) format('woff2');
                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
            }
            /* greek-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCgYb8td.woff2) format('woff2');
                unicode-range: U+1F00-1FFF;
            }
            /* greek */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCcYb8td.woff2) format('woff2');
                unicode-range: U+0370-03FF;
            }
            /* vietnamese */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCsYb8td.woff2) format('woff2');
                unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
            }
            /* latin-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCoYb8td.woff2) format('woff2');
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }
            /* latin */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCQYbw.woff2) format('woff2');
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }
            *{
                font-family: 'Roboto Condensed',sans-serif !important;
            }
            p,h1,h2,h3{
                color: #3e3e3e;

            }
            h1{
                font-size: 18px;
            }
            h2{
                font-size: 14px;
            }
            p{
                font-size: 14px;
                line-height: 1.4;
            }
            h2.title{
                text-align: center;
                margin: 50px 0 50px 0;
            }
            img{
                display: inline-block;
                max-width: 100%;
            }
            .container{
                max-width: 600px;
                margin: 0 auto;
            }
            .head{
                margin: 50px 0 0 0;
                text-align: center;
            }
            .texto{
                text-indent: 40px;
                text-align: justify;
            }
            .data{
                width: 100%;
                text-align: right;
                margin-top: 100px;
            }
            footer{
                /*margin-top: 80px;*/
                border-top: 2px solid #232323;
                text-align: center;
            }
            footer p {
                margin: 10px 0;
            }
            .left{
                display: inline-block;
                width: 69%;
                vertical-align: top;
            }
            .right{
                display: inline-block;
                width: 30%;
                text-align: right;
            }
            .presidente{
                width: 100%;
                text-align: left;
                margin-top: 50px;
            }
            .presidente img{
                display: block;
            }
            .presidente p{
                margin: 0;
                display: block;
                width: 100%;
            }
            .presidente p.cargo{
                padding-left: 55px; font-size: 12px;
            }
            .registro{
                position: absolute;
                top: 0;
                right: 0;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <p class="registro">Código: {{$registro}}</p>
            <div class="head">
                <div class="left">
                    <h1>ASSOCIAÇÃO DE CAÇA E TIRO RIO DO SUL</h1>
                </div>
                <div class="right">
                    <img src="images/logo.png">
                </div>
            </div>

            <h2 class="title">DECLARAÇÃO DE RANKING</h2>

            <p class= "texto">A Associação de Caça e Tiro Rio do Sul, inscrita no CNPJ sob o nº
                79.372.702/0001-56, com Certificado de Registro nº 3121/SFPC-5, com sede na Rua
                Antônio Dolzani, 970, bairro Valada São Paulo, município de Rio do Sul/SC, CEP
                89162-190, declara, conforme art. 79 e 92 da Portaria COLOG nº 51, de 08 de setembro
                de 2015, junto ao Exército Brasileiro, para fins de revalidação de CR, aquisição de arma
                de fogo e/ou munição ou insumos, que {{$nome}}, CR nº {{$cr}}, CPF nº {{$cpf}}, está
                regularmente inscrito nesta Entidade sob nº {{$cadastro}}, datado de {{\Carbon\Carbon::parse($data_filiacao)->format('d/m/Y')}} e que
                participou dos eventos previstos para ser considerado Atirador Desportivo de Nível {{$rank}},
                conforme previsto no art. 79 de Portaria COLOG nº 51, de 08 de setembro de 2015.</p>

            <p class="texto">Esta Associação de Caça e Tiro Rio do Sul dispõe dos registros que comprovam o nível
                do referido atirador desportivo.</p>

            <p class="texto">Esta declaração tem validade de 90 dias.</p>

            <p class="data">Rio do Sul, {{\Carbon\Carbon::parse(now())->format('d/m/Y')}}.</p>

            @isset($presidente)
                <div class="presidente">
                    <img width="200" height="100" src="data:image/png;base64, {{base64_encode(file_get_contents(storage_path(). '/app/'.$presidente->assinatura))}}" alt="assinatura presidente" />
                    <p>{{$presidente->nome ?? ""}}</p>
                    <p class="cargo">PRESIDENTE</p>
                </div>
            @endisset

            <footer>
                <p>Rua Antonio Dolzani, 970 - Bairro Valada São Paulo – Rio do Sul/SC
                    <br/>
                    CEP – 89162.190 - CNPJ nº 79.372.702/0001-56
                    <br/>
                    <strong>Para verificar a veracidade deste documento acesse <br/>clubedetirorsl.com.br/validacao-declaracao</strong>
                </p>
            </footer>
        </div>

    </body>
</html>
