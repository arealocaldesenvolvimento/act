<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Declaração</title>
        <style>
            /* cyrillic-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCkYb8td.woff2) format('woff2');
                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
            }
            /* cyrillic */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCAYb8td.woff2) format('woff2');
                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
            }
            /* greek-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCgYb8td.woff2) format('woff2');
                unicode-range: U+1F00-1FFF;
            }
            /* greek */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCcYb8td.woff2) format('woff2');
                unicode-range: U+0370-03FF;
            }
            /* vietnamese */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCsYb8td.woff2) format('woff2');
                unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
            }
            /* latin-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCoYb8td.woff2) format('woff2');
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }
            /* latin */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 300;
                src: local('Roboto Condensed Light'), local('RobotoCondensed-Light'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-33mZGCQYbw.woff2) format('woff2');
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }
            /* cyrillic-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-19-7DRs5.woff2) format('woff2');
                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
            }
            /* cyrillic */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-19a7DRs5.woff2) format('woff2');
                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
            }
            /* greek-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-1967DRs5.woff2) format('woff2');
                unicode-range: U+1F00-1FFF;
            }
            /* greek */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-19G7DRs5.woff2) format('woff2');
                unicode-range: U+0370-03FF;
            }
            /* vietnamese */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-1927DRs5.woff2) format('woff2');
                unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
            }
            /* latin-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-19y7DRs5.woff2) format('woff2');
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }
            /* latin */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 400;
                src: local('Roboto Condensed'), local('RobotoCondensed-Regular'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVl2ZhZI2eCN5jzbjEETS9weq8-19K7DQ.woff2) format('woff2');
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }
            /* cyrillic-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCkYb8td.woff2) format('woff2');
                unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
            }
            /* cyrillic */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCAYb8td.woff2) format('woff2');
                unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
            }
            /* greek-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCgYb8td.woff2) format('woff2');
                unicode-range: U+1F00-1FFF;
            }
            /* greek */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCcYb8td.woff2) format('woff2');
                unicode-range: U+0370-03FF;
            }
            /* vietnamese */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCsYb8td.woff2) format('woff2');
                unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
            }
            /* latin-ext */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCoYb8td.woff2) format('woff2');
                unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }
            /* latin */
            @font-face {
                font-family: 'Roboto Condensed';
                font-style: normal;
                font-weight: 700;
                src: local('Roboto Condensed Bold'), local('RobotoCondensed-Bold'), url(https://fonts.gstatic.com/s/robotocondensed/v17/ieVi2ZhZI2eCN5jzbjEETS9weq8-32meGCQYbw.woff2) format('woff2');
                unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }
            *{
                font-family: 'Roboto Condensed',sans-serif !important;
            }
            p,h1,h2,h3{
                color: #3e3e3e;
            }
            h1{
                font-size: 18px;
            }
            h2{
                font-size: 14px;
            }
            p{
                font-size: 14px;
                line-height: 1.4;
            }
            h2.title{
                font-size: 13px;
                text-align: center;
                margin: 50px 0 50px 0;
            }
            img{
                display: inline-block;
                max-width: 100%;
            }
            .container{
                max-width: 600px;
                margin: 0 auto;
                position: relative;
            }
            .head{
                margin: 50px 0 0 0;
                text-align: center;
            }
            .data{
                width: 100%;
                text-align: right;
                margin-top: 100px;
            }
            footer{
                /*margin-top: 190px;*/
                border-top: 2px solid #232323;
                text-align: center;
            }
            footer p {
                margin: 10px 0;
            }
            .left{
                display: inline-block;
                width: 69%;
                vertical-align: top;
            }
            .right{
                display: inline-block;
                width: 30%;
                text-align: right;
            }
            .presidente{
                width: 100%;
                text-align: left;
                margin-top: 50px;
            }
            .presidente img{
                display: block;
            }
            .presidente p{
                margin: 0;
                display: block;
                width: 100%;
            }
            .registro{
                position: absolute;
                top: 0;
                right: 0;
            }

            .presidente img{
                display: block;
            }
            .presidente p{
                margin: 0;
                display: block;
                width: 100%;
            }
            .presidente p.cargo{
                /* padding-left: 55px; */
                font-size: 10px;
            }
            .presidente .right-presidente{
                font-size: 8px;
            }


            table.tabela{
                width: 100%;
                border-collapse: collapse;
            }
            table.tabela th, table.tabela td{
                border: 1px solid #3e3e3e;
                padding: 1px 4px;
                border-collapse: collapse;
                color: #3e3e3e;
                font-size: 10px;
            }

            h3.titulo{
                font-size: 13px;
                font-weight: bold;
                margin-top: 20px;
                text-align: center;
                color: black;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <p class="registro">Código: {{$dados['registro']}}</p>
            <div class="head">
                <div class="left">
                    <h1>ASSOCIAÇÃO DE CAÇA E TIRO RIO DO SUL</h1>
                </div>
                <div class="right">
                    <img src="images/logo.png">
                </div>
            </div>

            <h2 class="title">DECLARAÇÃO DE COMPROMISSO DE PARTICIPAÇÃO EM TREINAMENTOS E COMPETIÇÕES
                <br>(art. 35 do Decreto nº 11.615/2023)</h2>



            <h3 class="titulo">Dados da Federação ou Confederação</h3>
                <table class="tabela">
                    <thead>
                        <tr>
                            <td style="width: 30%">Nome</td>
                            <td style="width: 60%">ASSOCIAÇÃO DE CAÇA E TIRO RIO DO SUL</td>
                            <td style="width: 40%">CNPJ: 79.372.702/0001-56</td>
                        </tr>
                        <tr>
                            <td>Certificado de Registro</td>
                            <td>{{$clube->cr}}</td>
                            <td>Data: {{\Carbon\Carbon::parse($clube->validade_cr)->format('d/m/Y')}}</td>
                        </tr>
                        <tr>
                            <td>Endereço</td>
                            <td colspan="2"> Rua Antonio Dolzani, 970 - Bairro Valada São Paulo – Rio do Sul/SC </td>
                        </tr>
                        <tr>
                            <td>Filiação à Entidade de tiro</td>
                            <td>Número: {{Auth::user()->cadastro}} </td>
                            <td>Data: {{\Carbon\Carbon::parse(Auth::user()->data_filiacao)->format('d/m/Y')}}</td>
                        </tr>
                    </thead>
                </table>


                @php
                $requerente = Auth::user()->nome .', portador do CPF nº ' . Auth::user()->cpf . ', residente e domiciliado na ' . Auth::user()->endereco . ' - ' . Auth::user()->cidade . '/' .  Auth::user()->estado . ', ' . Auth::user()->telefone;
                @endphp

                <h3 class="titulo">Compromisso</h3>

                <p class= "texto" style="text-align: justify; margin-top: 20px">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Eu, {{$requerente}}, filiado à
                    Entidade de Tiro acima nomeada, ME COMPROMETO a comprovar, no mínimo, <strong>por calibre
                    registrado, oito treinamentos ou competições em clube de tiro, em eventos distintos, a cada doze
                    meses, </strong> em cumprimento ao previsto no art. 35 do Decreto nº 11.615/2023, como requisitos de
                    assiduidade em prática de tiro desportivo junto ao Exército Brasileiro.
                </p>

                <p style="text-align:center;margin-bottom: 160px">Rio do Sul, {{date('d/m/Y H:m:s')}}</p>

            <div class="assinatura-requerente" style="border-top: 2px solid black; text-align:center; width: 200px; margin-bottom: 60px">
                <p style="font-size: 10px; color:black; margin-top: 0px; margin-bottom: 0px">{{Auth::user()->nome}}</p>
                <p style="margin-top: 0px; padding-top: 0px; font-size: 13px" >REQUERENTE</p>
            </div>

            @isset($dados['presidente'])
                <div class="row-assinatura" style="width: 100%; display: flex; flex-wrap: wrap;height: 80px; align-items: center; padding-bottom: 20px">

                    <div class="presidente"
                        style="width: 50%; display: flex; flex-wrap: wrap; height: 50px; align-items: center;">

                        <p style="text-align: center; font-size: 13px margin-bottom: 20px; padding-bottom: 20px" >CIENTE</p>

                    <div style="width: 150px;  padding-top:30px">
                            <div class="nome" style="border-right: 1px solid black; ">
                                <strong>
                                    <p class="cargo">{{$dados['presidente']->nome ?? ""}}</p>
                                    <p class="cargo">PRESIDENTE</p>
                                </strong>
                            </div>
                        </div>
                        <div class="right-presidente" style="padding-left: 160px; width: 150px; padding-top:30px ">
                            Assinado digitalmente por: JAISON MAURECI BECKER: 01995908932<br>
                            Data: {{ \Carbon\Carbon::parse(now())->format('d/m/Y H:i:s O') }}
                        </div>
                    </div>

                    <div class="qrcode" style="margin-left: 50%; width: 50px; display: flex; ">
                        <div class="img" style="width: 80px; margin-left: 80px;">
                            <img src="data:image/png;base64,{{ base64_encode($qrcode) }}" alt="QR Code">
                        </div>
                        <div class="confira" style="width: 100px; margin-left: 165px; ">
                            <p style="font-size: 9px; margin-top: 0">Este documento pode ser validado pelo QR Code ou pelo link: clubedetirorsl.com.br/validacao-declaracao/{{$dados['registro']}}</p>
                        </div>
                    </div>
                </div>
            @endisset


            <footer>
                <p>Rua Antonio Dolzani, 970 - Bairro Valada São Paulo – Rio do Sul/SC
                    <br/>
                    CEP – 89162.190 - CNPJ nº 79.372.702/0001-56
                </p>
            </footer>
        </div>

    </body>
</html>
