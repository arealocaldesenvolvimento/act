@extends('layouts.material')
@section('title', 'Informações dos usuários')
@section('content')
    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="{{ route('painel-usuarios-listagem') }}"><i class="material-icons">view_list</i> Listagem de usuários</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('painel-usuarios-cadastro') }}"><i class="material-icons">add</i> Cadastrar novo usuário</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('painel-usuarios-lixeira') }}"><i class="material-icons">block</i> Bloqueados</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content">
                <a href="{{route('painel-usuarios-exportacao-completa')}}" class="btn btn-primary">Exportação completa</a>
                <div class="table-responsive">
                        <table class="table datatable">
                            <thead>
                            <tr>
                                <th>NOME</th>
                                <th>CÓDIGO</th>
                                <th>CR</th>
                                <th>VALIDADE CR</th>
                                <th>FILIAÇÃO</th>
                                <th>CPF</th>
                                <th>TELEFONES</th>
                                <th>OPÇÕES</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse(App\User::all()->sortBy('nome') as $usuario)
                                <tr>
                                    <td>{{$usuario->nome}}</td>
                                    <td>{{$usuario->cadastro}}</td>
                                    <td>{{$usuario->cr}}</td>
                                    <td>{{!empty($usuario->validade_cr) ? \Carbon\Carbon::parse($usuario->validade_cr)->format('d/m/Y'): ""}}</td>
                                    <td>{{\Carbon\Carbon::parse($usuario->data_filiacao)->format('d/m/Y')}}</td>
                                    <td class="mask-cpf">{{$usuario->cpf}}</td>
                                    <td class="mask-phone">{{$usuario->telefone}}</td>
                                    <td class="td-actions text-right">
                                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#infoModal"
                                                data-nome="{{$usuario->nome}}"
                                                data-cpf="{{$usuario->cpf}}"
                                                data-tipo="{{$usuario->tipo}}"
                                                data-cr="{{$usuario->cr}}"
                                                data-telefones="{{$usuario->telefone.' '.$usuario->telefone2.' '.$usuario->telefone3}}"
                                                data-email="{{$usuario->email}}"
                                                data-profissao="{{$usuario->profissao}}"
                                                data-data_nascimento="{{$usuario->data_nascimento}}"
                                                data-estado_civil="{{$usuario->estado_civil}}"
                                                data-endereco="{{$usuario->cep. ' '.$usuario->cidade.' '.$usuario->bairro}}"
                                                data-filiacao="{{$usuario->data_filiacao}}"
                                                data-desligamento="{{$usuario->data_desligamento}}"
                                                data-tipodesligamento="{{$usuario->tipo_desligamento}}"
                                                data-primeirahabitualidade="{{$usuario->data_primeira_habitualidade}}"
                                                data-indicado="{{\App\User::find($usuario->indicado_por_id) ? \App\User::find($usuario->indicado_por_id)->nome : null}}"
                                                data-janta="{{$usuario->janta?'Sim':'Não'}}">
                                            <i class="material-icons">info</i>
                                        </button>
                                        <a href="{{ route('painel-usuarios-editar', $usuario->id) }}" class="btn btn-success">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <button type="button" class="btn btn-danger block-user">
                                            <i class="material-icons">delete</i>
                                        </button>
                                        <form class="d-none block-form" action="{{ route('painel-usuarios-block') }}" method="post">
                                            @csrf
                                            <input type="hidden" value="{{ $usuario->id }}" name="id">
                                            <input type="hidden" value="cancelamento" name="motivo">
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <td><p><strong>Nenhum usuário encontrado.</strong></p></td>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>

    {{--  Modal mais informações do usuário  --}}
    <div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">INFORMAÇÕES COMPLETAS DO USUÁRIO</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <th>NOME:</th>
                            <td class="nome"></td>
                        </tr>
                        <tr>
                            <th>TIPO USUÁRIO:</th>
                            <td class="tipo"></td>
                        </tr>
                        <tr>
                            <th>CPF:</th>
                            <td class="cpf"></td>
                        </tr>
                        <tr>
                            <th>CR:</th>
                            <td class="cr"></td>
                        </tr>
                        <tr>
                            <th>TELEFONES:</th>
                            <td class="telefones"></td>
                        </tr>
                        <tr>
                            <th>E-MAIL:</th>
                            <td class="email"></td>
                        </tr>
                        <tr>
                            <th>DATA NASCIMENTO:</th>
                            <td class="data_nascimento"></td>
                        </tr>
                        <tr>
                            <th>PROFISSÃO:</th>
                            <td class="profissao"></td>
                        </tr>
                        <tr>
                            <th>ESTADO CIVIL:</th>
                            <td class="estado_civil"></td>
                        </tr>
                        <tr>
                            <th>ENDEREÇO:</th>
                            <td class="endereco"></td>
                        </tr>
                        <tr>
                            <th>FILIAÇÃO:</th>
                            <td class="filiacao"></td>
                        </tr>
                        <tr>
                            <th>1ª HABITUALIDADE:</th>
                            <td class="primeirahabitualidade"></td>
                        </tr>
                        <tr>
                            <th>INDICADO POR:</th>
                            <td class="indicado"></td>
                        </tr>
                        <tr>
                            <th>JANTA:</th>
                            <td class="janta"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(()=>{
            $('button.block-user').click(function () {
                var button = $(this);
                 Swal.fire({
                    title: 'Você está prestes a bloquear este usuário',
                    input: 'select',
                    inputOptions: {
                        cancelamento: 'CANCELAMENTO',
                        pedido: 'PEDIDO',
                    },
                    inputPlaceholder: 'Informe o motivo:',
                    showCancelButton: true,
                    inputValidator: (value) => {
                        return new Promise((resolve) => {
                            var form = button.siblings('form.block-form');

                            if (value){
                                form.children('input[name="motivo"]').val(value);
                                form.submit();
                            }else{
                                resolve('Você precisa selecionar um motivo :)')
                            }
                        })
                    }
                });
            });

            configDatatables["buttons"] =  [
                {
                    extend: 'pdf',
                    text: 'Exportar tabela',
                    exportOptions: {
                        columns: [0,1,2,3,4,5, 6],
                    },
                }
            ];
            configDatatables['columnDefs'] = [
                { "searchable": false, "targets": 7 },
            ];
            $('.datatable').DataTable(configDatatables);

        });
        /**
         * Executa uma ação antes do modal ser exibido
         */
        $('#infoModal').on('show.bs.modal', function (e) {
            changeModalRowInfo('nome', e);
            changeModalRowInfo('tipo', e);
            changeModalRowInfo('cpf', e);
            changeModalRowInfo('cr', e);
            changeModalRowInfo('telefones', e);
            changeModalRowInfo('email', e);
            changeModalRowInfo('data_nascimento', e);
            changeModalRowInfo('profissao', e);
            changeModalRowInfo('estado_civil', e);
            changeModalRowInfo('endereco', e);
            changeModalRowInfo('filiacao', e);
            changeModalRowInfo('primeirahabitualidade', e);
            changeModalRowInfo('indicado', e);
            changeModalRowInfo('janta', e);
        });

        /**
         * Função para trocar o texto de uma classe pelo data atributte do botão que chama o evento do modal
         * @param campo
         * @param e
         */
        function changeModalRowInfo(campo, event){
            $('#infoModal tr td.'+ campo).text($.trim($(event.relatedTarget).data(campo)));
        }
    </script>
@endsection
