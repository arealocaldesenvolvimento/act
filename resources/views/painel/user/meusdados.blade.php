@extends('layouts.material')
@section('title', 'Editar usuário')
@section('content')
<?php $usuario = \Illuminate\Support\Facades\Auth::user(); ?>
    <div class="card">
        <div class="card-header card-header-icon card-header-primary">
            <div class="card-icon">
                <i class="material-icons">account_circle</i>
            </div>
            <div class="card-text">
                <h4 class="card-title">{{$usuario->nome}}</h4>
            </div>
        </div>
        <div class="card-body">
            <form method="post">
                @csrf
                <label><strong>Identificação</strong></label>
                <small class="text-muted">Para alterar essas informações contate o clube.</small>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group bmd-form-group @error('cadastro') has-danger @enderror">
                            <label>Nº CADASTRO</label>
                            <input type="text" disabled class="form-control" maxlength="20" value="{{$usuario->cadastro ?? null}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group bmd-form-group @error('data_filiacao') has-danger @enderror">
                            <label>DATA FILIAÇÃO</label>
                            <input type="date" disabled class="form-control" value="{{\Carbon\Carbon::parse($usuario->data_filiacao ?? null)->format('Y-m-d')}}">
                            @error('data_filiacao')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group bmd-form-group @error('cpf') has-danger @enderror">
                            <label>CPF</label>
                            <input type="text" class="form-control mask-cpf" disabled maxlength="14" value="{{$usuario->cpf}}">
                            @error('cpf')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                </div>
                <label class="mt-4"><strong>Informações Gerais</strong></label>
                <div class="row mt-3">
                    <div class="col-md-5">
                        <div class="form-group bmd-form-group @error('nome') has-danger @enderror">
                            <label>NOME*</label>
                            <input type="text" class="form-control text-uppercase" required name="nome" maxlength="255" value="@if(old('nome')){{old('nome')}}@else{{$usuario->nome}}@endif">
                            @error('nome')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group bmd-form-group @error('cr') has-danger @enderror">
                            <label>CR</label>
                            <input type="text" class="form-control mask-onlynumber" name="cr" readonly maxlength="11" value="@if(old('cr')){{old('cr')}}@else{{$usuario->cr}}@endif">
                            @error('cr')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group bmd-form-group @error('cr') has-danger @enderror">
                            <label>Data de Validade do CR</label>
                            <input type="date" class="form-control" name="validade_cr" readonly value="{{old('validade_cr') ? old('validade_cr') : (Auth::user()->validade_cr ? (\Carbon\Carbon::parse(Auth::user()->validade_cr)->format('Y-m-d')) : null)}}">
                            @error('validade_cr')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-6">
                        <div class="form-group bmd-form-group @error('email') has-danger @enderror">
                            <label>E-MAIL</label>
                            <input type="text" class="form-control" name="email" maxlength="200" value="@if(old('email')){{old('email')}}@else{{$usuario->email}}@endif">
                            @error('email')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group bmd-form-group @error('telefone') has-danger @enderror">
                            <label>TELEFONE*</label>
                            <input type="text" class="form-control mask-phone" required name="telefone" maxlength="12" value="@if(old('telefone')){{old('telefone')}}@else{{$usuario->telefone}}@endif">
                            @error('telefone')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group bmd-form-group @error('telefone2') has-danger @enderror">
                            <label>TELEFONE 2</label>
                            <input type="text" class="form-control mask-phone" name="telefone2" maxlength="12" value="@if(old('telefone2')){{old('telefone2')}}@else{{$usuario->telefone2}}@endif">
                            @error('telefone2')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group bmd-form-group @error('telefone3') has-danger @enderror">
                            <label>TELEFONE 3</label>
                            <input type="text" class="form-control mask-phone" name="telefone3" maxlength="12" value="@if(old('telefone3')){{old('telefone3')}}@else{{$usuario->telefone3}}@endif">
                            @error('telefone3')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group @error('profissao') has-danger @enderror">
                            <label>Profissão *</label>
                            <input type="text" class="form-control"  required  name="profissao"value="@if(old('profissao')){{old('profissao')}}@else{{$usuario->profissao}}@endif">
                            @error('profissao')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group @error('data_nascimento') has-danger @enderror">
                            <label>Data nascimento *</label>
                            <input type="date" class="form-control mask-data" required name="data_nascimento"
                                maxlength="12"
                                value="{{ old('data_nascimento') ? old('data_nascimento') : ($usuario->data_nascimento ? \Carbon\Carbon::parse($usuario->data_nascimento)->format('Y-m-d') : null) }}">
                            @error('data_nascimento')
                                <span class="material-icons form-control-feedback">clear</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group @error('estado_civil') has-danger @enderror">
                            <label>Estado civil</label>
                            <input type="text" class="form-control" name="estado_civil" maxlength="16"
                            value="@if (old('estado_civil')) {{ old('estado_civil') }}@else{{$usuario->estado_civil }} @endif">
                            @error('estado_civil')
                                <span class="material-icons form-control-feedback">clear</span>
                            @enderror
                        </div>
                    </div>
                </div>


                <label class="mt-5"><strong>Endereço onde reside</strong></label>
                <div class="row mt-3">
                    <div class="col-md-6">
                        <div class="form-group bmd-form-group @error('estado') has-danger @enderror">
                            <label>ESTADO *</label>
                            <select class="form-control selectpicker" required data-style="btn btn-link" name="estado" id="estado" title="Selecione um estado">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group bmd-form-group @error('cidade') has-danger @enderror">
                            <label>CIDADE *</label>
                            <input type="text" class="form-control" required name="cidade" maxlength="100" value="@if(old('cidade')){{old('cidade')}}@else{{$usuario->cidade}}@endif">
                            @error('cidade')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-2">
                        <div class="form-group bmd-form-group @error('cep') has-danger @enderror">
                            <label>CEP</label>
                            <input type="text" class="form-control mask-cep" name="cep" maxlength="10" value="@if(old('cep')){{old('cep')}}@else{{$usuario->cep}}@endif">
                            @error('cep')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group @error('bairro') has-danger @enderror">
                            <label>BAIRRO</label>
                            <input type="text" class="form-control" name="bairro" maxlength="100" value="@if(old('bairro')){{old('bairro')}}@else{{$usuario->bairro}}@endif">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group @error('endereco') has-danger @enderror">
                            <label>ENDEREÇO</label>
                            <input type="text" class="form-control" name="endereco" maxlength="255" value="@if(old('endereco')){{old('endereco')}}@else{{$usuario->endereco}}@endif">
                            @error('endereco')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                </div>


                <label class="mt-5"><strong>Endereço onde nasceu</strong></label>
                <div class="row mt-3">
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group @error('nacionalidade') has-danger @enderror">
                            <label>NACIONALIDADE *</label>
                            <input type="text" class="form-control" required name="nacionalidade" maxlength="100" value="{{$usuario->nacionalidade}}" placeholder="Brasileiro">
                            @error('nacionalidade')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group @error('cidade_nasceu') has-danger @enderror">
                            <label>CIDADE ONDE NASCEU *</label>
                            <input type="text" class="form-control" required name="cidade_nasceu" maxlength="100" value="@if(old('cidade_nasceu')){{old('cidade_nasceu')}}@else{{$usuario->cidade_nasceu}}@endif">
                            @error('cidade_nasceu')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group bmd-form-group @error('estado_nasceu') has-danger @enderror">
                            <label>ESTADO ONDE NASCEU *</label>
                             <select class="form-control selectpicker" required data-style="btn btn-link" name="estado_nasceu" id="estado_nasceu" title="Selecione um estado">
                            </select>
                        </div>
                    </div>
                </div>





                <div class="row mt-5">
                    <div class="col-6">
                        <button type="button" class="btn btn-primary senha">Trocar Senha</button>
                    </div>
                    <div class="col-6 text-right">
                        <button type="submit" class="btn btn-success pull-right">Salvar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <script>
        $(document).ready(()=>{
            $('button.senha').click(function() {
                Swal.fire({
                    title: 'Digite a nova senha',
                    input: 'text',
                    inputAttributes: {
                        autocapitalize: 'off'
                    },
                    showCancelButton: true,
                    confirmButtonText: 'Trocar',
                    cancelButtonText: 'Cancelar',
                    showLoaderOnConfirm: true,
                    preConfirm: (password) => {
                        return $.post('{{ route('painel-usuarios-newpassword') }}', {
                                novasenha: password,
                                id: '{{ $usuario->id }}',
                                _token: '{{ csrf_token() }}'
                            })
                            .fail(function() {
                                Swal.showValidationMessage(
                                    'Falha na requisição ao servidor'
                                );
                            });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.value) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Senha alterada com sucesso!',
                        });
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Falha ao alterar a senha! Tente novamente. ',
                        });
                    }
                });
            });
        });

        async function loadUF(campoSelect, atributo) {
            try {
                const response = await fetch("https://servicodados.ibge.gov.br/api/v1/localidades/estados");
                const estados = await response.json();

                estados.sort((a, b) => a.nome.localeCompare(b.nome))

                estados.forEach((estado) => {
                    var isSelected = estado.sigla === atributo;

                    const option = document.createElement("option");
                    option.value = estado.sigla;
                    option.text = estado.nome;
                    option.selected = isSelected;

                    campoSelect.add(option);
                });
                $(campoSelect).selectpicker('refresh');
            } catch (error) {
                console.error("Erro ao carregar estados:", error);
            }
        }

        var estadoNasceu = '{{!empty(\Illuminate\Support\Facades\Auth::user()->estado_nasceu) ? \Illuminate\Support\Facades\Auth::user()->estado_nasceu : "SC" }}';
        var estadoReside = '{{!empty(\Illuminate\Support\Facades\Auth::user()->estado) ? \Illuminate\Support\Facades\Auth::user()->estado : "SC" }}';

        loadUF(document.getElementById("estado_nasceu"), estadoNasceu);
        loadUF(document.getElementById("estado"), estadoReside);

    </script>
@endsection
