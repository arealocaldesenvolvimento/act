@extends('layouts.material')
@section('title', 'Informações dos usuários')
@section('content')
    @error('cpf')
        @if (\App\User::onlyTrashed()->where('cpf', '=', old('cpf'))->first())
            <form action="{{ route('painel-usuarios-unblock') }}" method="post" style="margin-top: -48px">
                @csrf
                <input type="hidden" value="{{ \App\User::withTrashed()->where('cpf', '=', old('cpf'))->first()->id }}"
                    name="id">
                <button type="submit" class="btn btn-danger">ESTE USUÁRIO ENCONTRA-SE BLOQUEADO. PARA DESBLOQUEAR, CLIQUE
                    AQUI</button>
            </form>
        @endif
    @enderror
    @error('nome')
        @if (\App\User::onlyTrashed()->where('nome', '=', old('nome'))->first())
            <form action="{{ route('painel-usuarios-unblock') }}" method="post" style="margin-top: -48px">
                @csrf
                <input type="hidden" value="{{ \App\User::withTrashed()->where('nome', '=', old('cpf'))->first() }}"
                    name="id">
                <button type="submit" class="btn btn-danger">ESTE USUÁRIO ENCONTRA-SE BLOQUEADO. PARA DESBLOQUEAR, CLIQUE
                    AQUI</button>
            </form>
        @endif
    @enderror
    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('painel-usuarios-listagem') }}"><i
                                    class="material-icons">view_list</i> Listagem de usuários</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="{{ route('painel-usuarios-cadastro') }}"><i
                                    class="material-icons">add</i> Cadastrar novo usuário</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('painel-usuarios-lixeira') }}"><i
                                    class="material-icons">block</i> Bloqueados</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content">
                <form method="post">
                    @csrf
                    <label><strong>Informações Gerais</strong></label>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group bmd-form-group @error('cadastro') has-danger @enderror">
                                <label>Nº CADASTRO*</label>
                                <input type="text" class="form-control mask-onlynumber" required name="cadastro"
                                    maxlength="20"
                                    value="@if (old('cadastro')) {{ old('cadastro') }}@else {{ \Illuminate\Support\Facades\DB::table('users')->selectRaw('max(cadastro) + 1 as num')->first()->num ?? 0 }} @endif">
                                @error('cadastro')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group bmd-form-group @error('data_filiacao') has-danger @enderror">
                                <label>DATA FILIAÇÃO*</label>
                                <input type="date" class="form-control mask-data" required name="data_filiacao"
                                    value="{{ old('data_filiacao') }}">
                                @error('data_filiacao')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>INDICAÇÃO*</label>
                                <select class="form-control selectpicker" data-style="btn btn-link" name="indicado">
                                    <option disabled selected>SELECIONE</option>
                                    <option value="nenhuma">NENHUMA</option>
                                    @foreach (\app\User::all()->sortBy('nome') as $user)
                                        <option
                                            @if (old('indicado')) {{ old('indicado') == $user->id ? 'selected' : null }} @endif
                                            value="{{ $user->id }}">{{ $user->nome }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-5">
                            <div class="form-group bmd-form-group @error('nome') has-danger @enderror">
                                <label>NOME*</label>
                                <input type="text" class="form-control text-uppercase" required name="nome"
                                    maxlength="60" value="{{ old('nome') }}">
                                @error('nome')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group bmd-form-group @error('cpf') has-danger @enderror">
                                <label>CPF</label>
                                <input type="text" class="form-control mask-cpf" name="cpf" maxlength="14"
                                    value="{{ old('cpf') }}">
                                @error('cpf')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-3">
                            <div class="form-group bmd-form-group @error('cr') has-danger @enderror">
                                <label>CR</label>
                                <input type="text" class="form-control mask-onlynumber" name="cr" maxlength="11"
                                    value="{{ old('cr') }}">
                                @error('cr')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group bmd-form-group @error('cr') has-danger @enderror">
                                <label>Data de Validade do CR</label>
                                <input type="date" class="form-control mask-data" name="validade_cr"
                                    value="{{ old('validade_cr') }}">
                                @error('validade_cr')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <div class="form-group bmd-form-group @error('email') has-danger @enderror">
                                <label>E-MAIL</label>
                                <input type="text" class="form-control" name="email" maxlength="200"
                                    value="{{ old('email') }}">
                                @error('email')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group bmd-form-group @error('telefone') has-danger @enderror">
                                <label>TELEFONE*</label>
                                <input type="text" class="form-control mask-phone" required name="telefone"
                                    maxlength="12" value="{{ old('telefone') }}">
                                @error('telefone')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group bmd-form-group @error('telefone2') has-danger @enderror">
                                <label>TELEFONE 2</label>
                                <input type="text" class="form-control mask-phone" name="telefone2" maxlength="12"
                                    value="{{ old('telefone2') }}">
                                @error('telefone2')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group bmd-form-group @error('telefone3') has-danger @enderror">
                                <label>TELEFONE 3</label>
                                <input type="text" class="form-control mask-phone" name="telefone3" maxlength="12"
                                    value="{{ old('telefone3') }}">
                                @error('telefone3')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-4">
                            <div class="form-group bmd-form-group @error('profissao') has-danger @enderror">
                                <label>Profissão *</label>
                                <input type="text" class="form-control" required
                                    name="profissao"value="@if (old('profissao')) {{ old('profissao') }}@else{{ \Illuminate\Support\Facades\Auth::user()->profissao }} @endif">
                                @error('profissao')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group bmd-form-group @error('data_nascimento') has-danger @enderror">
                                <label>Data nascimento *</label>
                                <input type="date" class="form-control mask-data" required name="data_nascimento"
                                    maxlength="12">
                                @error('data_nascimento')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group bmd-form-group @error('estado_civil') has-danger @enderror">
                                <label>Estado civil</label>
                                <input type="text" class="form-control" name="estado_civil" maxlength="16">
                                @error('estado_civil')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-md-3">
                            <div class="form-group bmd-form-group @error('cep') has-danger @enderror">
                                <label>CEP</label>
                                <input type="text" class="form-control mask-cep" name="cep" maxlength="10"
                                    value="{{ old('cep') }}">
                                @error('cep')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group bmd-form-group @error('estado') has-danger @enderror">
                                <label>ESTADO *</label>
                                <select class="form-control selectpicker" required data-style="btn btn-link"
                                    name="estado" id="estado" title="Selecione um estado">
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group bmd-form-group @error('cidade') has-danger @enderror">
                                <label>CIDADE*</label>
                                <input type="text" class="form-control" required name="cidade" maxlength="100"
                                    value="{{ old('cidade') }}">
                                @error('cidade')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-md-4">
                            <div class="form-group bmd-form-group @error('bairro') has-danger @enderror">
                                <label>BAIRRO</label>
                                <input type="text" class="form-control" name="bairro" maxlength="100"
                                    value="{{ old('bairro') }}">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group bmd-form-group @error('endereco') has-danger @enderror">
                                <label>ENDEREÇO</label>
                                <input type="text" class="form-control" name="endereco" maxlength="255"
                                    value="{{ old('endereco') }}">
                                @error('endereco')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-md-4">
                            <div class="form-group bmd-form-group @error('nacionalidade') has-danger @enderror">
                                <label>NACIONALIDADE *</label>
                                <input type="text" class="form-control" required name="nacionalidade" maxlength="100"
                                    value="{{ \Illuminate\Support\Facades\Auth::user()->nacionalidade }}"
                                    placeholder="Brasileiro">
                                @error('nacionalidade')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group bmd-form-group @error('cidade_nasceu') has-danger @enderror">
                                <label>CIDADE ONDE NASCEU *</label>
                                <input type="text" class="form-control" required name="cidade_nasceu" maxlength="100"
                                    value="@if (old('cidade_nasceu')) {{ old('cidade_nasceu') }}@else{{ \Illuminate\Support\Facades\Auth::user()->cidade_nasceu }} @endif">
                                @error('cidade_nasceu')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group bmd-form-group @error('estado_nasceu') has-danger @enderror">
                                <label>ESTADO ONDE NASCEU *</label>
                                <select class="form-control selectpicker" required data-style="btn btn-link"
                                    name="estado_nasceu" id="estado_nasceu" title="Selecione um estado">
                                </select>
                            </div>
                        </div>
                    </div>


                    @php
                        $senha = Str::random(15);
                    @endphp
                    <div class="row mt-3">
                        <div class="col-md-4">
                            <div
                                class="form-group bmd-form-group @error('senha') has-danger @enderror @error('senhaconfirm') has-danger @enderror">
                                <label>SENHA*</label>
                                <input type="password" class="form-control" required
                                    value="@if (old('senha')) {{ old('senha') }} @else {{ $senha }} @endif"
                                    required name="senha" maxlength="255">
                                @error('senha')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                                @error('senhaconfirm')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div
                                class="form-group bmd-form-group @error('senha') has-danger @enderror @error('senhaconfirm') has-danger @enderror">
                                <label>CONFIRAÇÃO SENHA*</label>
                                <input type="password" class="form-control" required
                                    value="@if (old('senhaconfirm')) {{ old('senhaconfirm') }} @else {{ $senha }} @endif"
                                    required name="senhaconfirm" maxlength="255">
                                @error('senha')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                                @error('senhaconfirm')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12 text-right">
                            <button type="submit" class="btn btn-success pull-right">Criar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <script>
        $(document).ready(() => {
            $('input[name="cpf"]').focusout(function() {
                if (!validaCPF($(this).val())) {
                    $(this).parent().removeClass('has-success').addClass('has-danger');
                    Swal.fire({
                        icon: 'error',
                        title: 'CPF INVÁLIDO',
                    })
                } else {
                    $(this).parent().removeClass('has-danger').addClass('has-success');
                }
            });
            if (!!$('input[name="cr"]').val()) {
                $('input[name="validade_cr"]').prop('required', true);
            } else {
                $('input[name="validade_cr"]').prop('required', false);
            }
            $('input[name="cr"]').focusout(function() {
                if (!!$(this).val()) {
                    $('input[name="validade_cr"]').prop('required', true);
                } else {
                    $('input[name="validade_cr"]').prop('required', false);
                }
            });
        });

        async function loadUF(campoSelect, atributo) {
            try {
                const response = await fetch("https://servicodados.ibge.gov.br/api/v1/localidades/estados");
                const estados = await response.json();

                estados.sort((a, b) => a.nome.localeCompare(b.nome))

                estados.forEach((estado) => {
                    var isSelected = estado.sigla === atributo;

                    const option = document.createElement("option");
                    option.value = estado.sigla;
                    option.text = estado.nome;
                    option.selected = isSelected;

                    campoSelect.add(option);
                });
                $(campoSelect).selectpicker('refresh');
            } catch (error) {
                console.error("Erro ao carregar estados:", error);
            }
        }

        var estadoNasceu =
            '{{ !empty(\Illuminate\Support\Facades\Auth::user()->estado_nasceu) ? \Illuminate\Support\Facades\Auth::user()->estado_nasceu : 'SC' }}';
        var estadoReside =
            '{{ !empty(\Illuminate\Support\Facades\Auth::user()->estado) ? \Illuminate\Support\Facades\Auth::user()->estado : 'SC' }}';

        loadUF(document.getElementById("estado_nasceu"), estadoNasceu);
        loadUF(document.getElementById("estado"), estadoReside);
    </script>
@endsection
