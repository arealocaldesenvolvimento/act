@extends('layouts.material')
@section('title', 'Atualize seus dados')
@section('content')

    <div class="card">
        <div class="card-header card-header-icon card-header-primary">
            <div class="card-icon">
                <i class="material-icons">account_circle</i>
            </div>
            <div class="card-text">
                <h4 class="card-title">{{\Illuminate\Support\Facades\Auth::user()->nome}}</h4>
            </div>
        </div>
        <div class="card-body">
            <form method="post" action="{{route('painel-usuario-meus-dados')}}">
                @csrf
                <label class="mt-4"><strong>Informações Gerais</strong></label>
                <div class="row mt-3">
                    <div class="col-md-5">
                        <div class="form-group bmd-form-group @error('nome') has-danger @enderror">
                            <label>NOME*</label>
                            <input type="text" class="form-control text-uppercase" required name="nome" maxlength="255" value="@if(old('nome')){{old('nome')}}@else{{\Illuminate\Support\Facades\Auth::user()->nome}}@endif">
                            @error('nome')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group bmd-form-group @error('cr') has-danger @enderror">
                            <label>CR</label>
                            <input type="text" class="form-control mask-onlynumber" name="cr" readonly maxlength="11" value="@if(old('cr')){{old('cr')}}@else{{\Illuminate\Support\Facades\Auth::user()->cr}}@endif">
                            @error('cr')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group bmd-form-group @error('cr') has-danger @enderror">
                            <label>Data de Validade do CR</label>
                            <input type="date" class="form-control" name="validade_cr" readonly value="{{old('validade_cr') ? old('validade_cr') : (Auth::user()->validade_cr ? (\Carbon\Carbon::parse(Auth::user()->validade_cr)->format('Y-m-d')) : null)}}">
                            @error('validade_cr')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-6">
                        <div class="form-group bmd-form-group @error('email') has-danger @enderror">
                            <label>E-MAIL</label>
                            <input type="text" class="form-control" name="email" maxlength="200" value="@if(old('email')){{old('email')}}@else{{\Illuminate\Support\Facades\Auth::user()->email}}@endif">
                            @error('email')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group bmd-form-group @error('telefone') has-danger @enderror">
                            <label>TELEFONE*</label>
                            <input type="text" class="form-control mask-phone" required name="telefone" maxlength="12" value="@if(old('telefone')){{old('telefone')}}@else{{\Illuminate\Support\Facades\Auth::user()->telefone}}@endif">
                            @error('telefone')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group bmd-form-group @error('telefone2') has-danger @enderror">
                            <label>TELEFONE 2</label>
                            <input type="text" class="form-control mask-phone" name="telefone2" maxlength="12" value="@if(old('telefone2')){{old('telefone2')}}@else{{\Illuminate\Support\Facades\Auth::user()->telefone2}}@endif">
                            @error('telefone2')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group bmd-form-group @error('telefone3') has-danger @enderror">
                            <label>TELEFONE 3</label>
                            <input type="text" class="form-control mask-phone" name="telefone3" maxlength="12" value="@if(old('telefone3')){{old('telefone3')}}@else{{\Illuminate\Support\Facades\Auth::user()->telefone3}}@endif">
                            @error('telefone3')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group @error('profissao') has-danger @enderror">
                            <label>Profissão *</label>
                            <input type="text" class="form-control"  required  name="profissao"value="@if(old('profissao')){{old('profissao')}}@else{{\Illuminate\Support\Facades\Auth::user()->profissao}}@endif">
                            @error('profissao')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group @error('data_nascimento') has-danger @enderror">
                            <label>Data nascimento *</label>
                            <input type="date" class="form-control mask-data" required name="data_nascimento"
                                maxlength="12"
                                value="{{ old('data_nascimento') ? old('data_nascimento') : (\Illuminate\Support\Facades\Auth::user()->data_nascimento ? \Carbon\Carbon::parse(\Illuminate\Support\Facades\Auth::user()->data_nascimento)->format('Y-m-d') : null) }}">
                            @error('data_nascimento')
                                <span class="material-icons form-control-feedback">clear</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group @error('estado_civil') has-danger @enderror">
                            <label>Estado civil<label>
                            <input type="text" class="form-control "  required name="estado_civil" maxlength="16">
                            @error('estado_civil')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                </div>


                <label class="mt-5"><strong>Endereço onde reside</strong></label>
                <div class="row mt-3">
                    <div class="col-md-6">
                        <div class="form-group bmd-form-group @error('estado') has-danger @enderror">
                            <label>ESTADO *</label>
                            <select class="form-control selectpicker" required data-style="btn btn-link" name="estado" id="estado" title="Selecione um estado">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group bmd-form-group @error('cidade') has-danger @enderror">
                            <label>CIDADE *</label>
                            <input type="text" class="form-control" required name="cidade" maxlength="100" value="@if(old('cidade')){{old('cidade')}}@else{{\Illuminate\Support\Facades\Auth::user()->cidade}}@endif">
                            @error('cidade')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-2">
                        <div class="form-group bmd-form-group @error('cep') has-danger @enderror">
                            <label>CEP</label>
                            <input type="text" class="form-control mask-cep" name="cep" maxlength="10" value="@if(old('cep')){{old('cep')}}@else{{\Illuminate\Support\Facades\Auth::user()->cep}}@endif">
                            @error('cep')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group @error('bairro') has-danger @enderror">
                            <label>BAIRRO</label>
                            <input type="text" class="form-control" name="bairro" maxlength="100" value="@if(old('bairro')){{old('bairro')}}@else{{\Illuminate\Support\Facades\Auth::user()->bairro}}@endif">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group @error('endereco') has-danger @enderror">
                            <label>ENDEREÇO</label>
                            <input type="text" class="form-control" name="endereco" maxlength="255" value="@if(old('endereco')){{old('endereco')}}@else{{\Illuminate\Support\Facades\Auth::user()->endereco}}@endif">
                            @error('endereco')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                </div>


                <label class="mt-5"><strong>Endereço onde nasceu</strong></label>
                <div class="row mt-3">
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group @error('nacionalidade') has-danger @enderror">
                            <label>NACIONALIDADE *</label>
                            <input type="text" class="form-control" required name="nacionalidade" maxlength="100" value="{{\Illuminate\Support\Facades\Auth::user()->nacionalidade}}" placeholder="Brasileiro">
                            @error('nacionalidade')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group @error('cidade_nasceu') has-danger @enderror">
                            <label>CIDADE ONDE NASCEU *</label>
                            <input type="text" class="form-control" required name="cidade_nasceu" maxlength="100" value="@if(old('cidade_nasceu')){{old('cidade_nasceu')}}@else{{\Illuminate\Support\Facades\Auth::user()->cidade_nasceu}}@endif">
                            @error('cidade_nasceu')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group bmd-form-group @error('estado_nasceu') has-danger @enderror">
                            <label>ESTADO ONDE NASCEU *</label>
                             <select class="form-control selectpicker" required data-style="btn btn-link" name="estado_nasceu" id="estado_nasceu" title="Selecione um estado">
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-6 text-right">
                        <button type="submit" class="btn btn-success pull-right">Salvar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
    <script>
        $(document).ready(()=>{

            var sidebar = document.querySelector('.sidebar');
            if (sidebar) {
                sidebar.remove();
            }

            var mainPanel = document.querySelector('.main-panel');
            if (mainPanel) {
                mainPanel.style.width = '100%';
                mainPanel.style.setProperty('width', '100%', 'important'); // Para garantir !important
            }

            var mainPanel = document.querySelector('.container');
            if (mainPanel) {
                mainPanel.style.width = '100%';
                mainPanel.style.setProperty('max-width', '100%', 'important'); // Para garantir !important
            }

            // $('.data-nascimento').mask('00/00/0000');
        });

        async function loadUF(campoSelect, atributo) {
            try {
                const response = await fetch("https://servicodados.ibge.gov.br/api/v1/localidades/estados");
                const estados = await response.json();

                estados.sort((a, b) => a.nome.localeCompare(b.nome))

                estados.forEach((estado) => {
                    var isSelected = estado.sigla === atributo;

                    const option = document.createElement("option");
                    option.value = estado.sigla;
                    option.text = estado.nome;
                    option.selected = isSelected;

                    campoSelect.add(option);
                });
                $(campoSelect).selectpicker('refresh');
            } catch (error) {
                console.error("Erro ao carregar estados:", error);
            }
        }

        var estadoNasceu = '{{!empty(\Illuminate\Support\Facades\Auth::user()->estado_nasceu) ? \Illuminate\Support\Facades\Auth::user()->estado_nasceu : "SC" }}';
        var estadoReside = '{{!empty(\Illuminate\Support\Facades\Auth::user()->estado) ? \Illuminate\Support\Facades\Auth::user()->estado : "SC" }}';

        loadUF(document.getElementById("estado_nasceu"), estadoNasceu);
        loadUF(document.getElementById("estado"), estadoReside);

    </script>
@endsection
