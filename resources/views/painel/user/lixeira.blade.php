@extends('layouts.material')
@section('title', 'Informações dos usuários')
@section('content')
    <style>
        .nav-tabs .nav-item.lixo .nav-link.active {
            background-color: rgb(191, 21, 21);
        }
    </style>
    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('painel-usuarios-listagem') }}"><i class="material-icons">view_list</i> Listagem de usuários</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('painel-usuarios-cadastro') }}"><i class="material-icons">add</i> Cadastrar novo usuário</a>
                        </li>
                        <li class="nav-item lixo">
                            <a class="nav-link active" href="{{ route('painel-usuarios-lixeira') }}"><i class="material-icons">block</i> Bloqueados</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content">
                <div class="table-responsive">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>NOME</th>
                                <th>DATA DESLIG.</th>
                                <th>MOTIVO DESLIG.</th>
                                <th>CPF</th>
                                <th>CR</th>
{{--                                <th>TELEFONE</th>--}}
                                <th>OPÇÕES</th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse(\app\User::onlyTrashed()->orderBy('nome')->get() as $usuario)
                            <tr>
                                <td>{{$usuario->nome}}</td>
                                <td>{{\Carbon\Carbon::parse($usuario->deleted_at)->format('d/m/Y H:i')}}</td>
                                <td class="text-uppercase">{{$usuario->tipo_desligamento}}</td>
                                <td class="mask-cpf">{{$usuario->cpf}}</td>
                                <td>{{$usuario->cr}}</td>
{{--                                <td class="mask-phone">{{$usuario->telefone}}</td>--}}
                                <td class="td-actions text-right">
                                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#infoModal"
                                            data-nome="{{$usuario->nome}}"
                                            data-cpf="{{$usuario->cpf}}"
                                            data-cr="{{$usuario->cr}}"
                                            data-telefones="{{$usuario->telefone.' '.$usuario->telefone2.' '.$usuario->telefone3}}"
                                            data-email="{{$usuario->email}}"
                                            data-endereco="{{$usuario->cep.' '.$usuario->cidade.' '.$usuario->bairro}}"
                                            data-filiacao="{{$usuario->data_filiacao}}"
                                            data-desligamento="{{$usuario->deleted_at}}"
                                            data-tipodesligamento="{{$usuario->tipo_desligamento}}"
                                            data-primeirahabitualidade="{{$usuario->data_primeira_habitualidade}}"
                                            data-indicado="{{\App\User::find($usuario->indicado_por_id) ? \App\User::find($usuario->indicado_por_id)->nome : null}}"
                                            data-janta="{{$usuario->janta?'Sim':'Não'}} ">
                                        <i class="material-icons">info</i>
                                    </button>
                                    <button type="button" class="btn btn-success unblock-user">
                                        <i class="material-icons">restore</i>
                                    </button>
                                    <form class="d-none unblock-form" action="{{ route('painel-usuarios-unblock') }}" method="post">
                                        @csrf
                                        <input type="hidden" value="{{ $usuario->id }}" name="id">
                                    </form>
                                    <button type="button" class="btn btn-danger destroy-user">
                                        <i class="material-icons">delete_forever</i>
                                    </button>
                                    <form class="d-none destroy-form" action="{{ route('painel-usuarios-destroy') }}" method="post">
                                        @csrf
                                        <input type="hidden" value="{{ $usuario->id }}" name="id">
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <td><p><strong>Nenhum usuário bloqueado.</strong></p></td>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{--  Modal mais informações do usuário  --}}
    <div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">INFORMAÇÕES COMPLETAS DO USUÁRIO</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th>NOME:</th>
                            <td class="nome"></td>
                        </tr>
                        <tr>
                            <th>TIPO:</th>
                            <td class="tipo"></td>
                        </tr>
                        <tr>
                            <th>CPF:</th>
                            <td class="cpf"></td>
                        </tr>
                        <tr>
                            <th>CR:</th>
                            <td class="cr"></td>
                        </tr>
                        <tr>
                            <th>TELEFONES:</th>
                            <td class="telefones"></td>
                        </tr>
                        <tr>
                            <th>E-MAIL:</th>
                            <td class="email"></td>
                        </tr>
                        <tr>
                            <th>ENDEREÇO:</th>
                            <td class="endereco"></td>
                        </tr>
                        <tr>
                            <th>FILIAÇÃO:</th>
                            <td class="filiacao"></td>
                        </tr>
                        <tr>
                            <th>DESLIGAMENTO:</th>
                            <td class="desligamento"></td>
                        </tr>
                        <tr>
                            <th>TIPO DESLIGAMENTO:</th>
                            <td class="tipodesligamento"></td>
                        </tr>
                        <tr>
                            <th>1ª HABITUALIDADE:</th>
                            <td class="primeirahabitualidade"></td>
                        </tr>
                        <tr>
                            <th>INDICADO POR:</th>
                            <td class="indicado"></td>
                        </tr>
                        <tr>
                            <th>JANTA:</th>
                            <td class="janta"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(()=>{
            $('button.destroy-user').click(function () {
                var button = $(this);
                Swal.fire({
                    title: 'Você tem certeza que deseja remover permanentemente esse usuário?',
                    text: 'Essa ação não poderá ser revertida!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#c0cdd6',
                    confirmButtonText: 'Sim, bloquear!',
                    cancelButtonText: 'Cancelar!'
                }).then((result) => {
                    if (result.value) {
                        button.siblings('form.destroy-form').submit();
                    }
                })
            });

            $('button.unblock-user').click(function () {
                var button = $(this);
                Swal.fire({
                    title: 'Você tem certeza que deseja restaurar esse usuário?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3ddd3c',
                    cancelButtonColor: '#c0cdd6',
                    confirmButtonText: 'Sim, restaurar!',
                    cancelButtonText: 'Cancelar!'
                }).then((result) => {
                    if (result.value) {
                        button.siblings('form.unblock-form').submit();
                    }
                })
            });

            configDatatables["buttons"] =  [
                {
                    extend: 'pdf',
                    text: 'Exportar tabela',
                    exportOptions: {
                        columns: [0,1,2,3],
                    },
                }
            ];
            configDatatables['columnDefs'] = [
                { "searchable": false, "targets": 4 },
            ];
            $('table.datatable').DataTable(configDatatables);

        });
        /**
         * Executa uma ação antes do modal ser exibido
         */
        $('#infoModal').on('show.bs.modal', function (e) {
            changeModalRowInfo('nome', e);
            changeModalRowInfo('tipo', e);
            changeModalRowInfo('cpf', e);
            changeModalRowInfo('cr', e);
            changeModalRowInfo('telefones', e);
            changeModalRowInfo('email', e);
            changeModalRowInfo('endereco', e);
            changeModalRowInfo('filiacao', e);
            changeModalRowInfo('desligamento', e);
            changeModalRowInfo('tipodesligamento', e);
            changeModalRowInfo('primeirahabilutalidade', e);
            changeModalRowInfo('indicado', e);
            changeModalRowInfo('janta', e);
        });

        /**
         * Função para trocar o texto de uma classe pelo data atributte do botão que chama o evento do modal
         * @param campo
         * @param e
         */
        function changeModalRowInfo(campo, event){
            $('#infoModal tr td.'+ campo).text($.trim($(event.relatedTarget).data(campo)));
        }
    </script>
@endsection
