@extends('layouts.material')
@section('title', 'Editar usuário')
@section('content')
    @isset($usuario)
        <div class="card">
            <div class="card-header card-header-icon card-header-primary">
                @desktop
                    <div class="card-icon">
                        <i class="material-icons">face</i>
                    </div>
                @enddesktop
                <div class="card-text">
                    <h4 class="card-title">{{ $usuario->nome }}</h4>
                </div>
            </div>
            <div class="card-body">
                <form method="post" action="{{ route('painel-usuarios-update') }}">
                    @csrf
                    <label><strong>Informações Gerais</strong></label>
                    <input type="hidden" name="id" value="{{ $usuario->id }}">
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <div class="form-group bmd-form-group @error('cadastro') has-danger @enderror">
                                <label>Nº CADASTRO*</label>
                                <input type="text" class="form-control mask-cadastro" required name="cadastro" maxlength="20"
                                    value="@if (old('cadastro')) {{ old('cadastro') }}@else{{ $usuario->cadastro }} @endif">
                                @error('cadastro')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        @if (\Illuminate\Support\Facades\Auth::user()->tipo == 'presidente')
                            <div class="col-12 col-md-4">
                                <div class="form-group">
                                    <label>TIPO DE USUÁRIO*</label>
                                    <select class="form-control selectpicker" data-style="btn btn-link" required name="tipo">
                                        <option disabled selected>TIPO*</option>
                                        <option value="associado"
                                            @if (old('tipo')) {{ old('tipo') == 'associado' ? 'selected' : null }}@else @if ($usuario->tipo == 'associado') selected @endif
                                            @endif >ASSOCIADO</option>
                                        <option value="secretario"
                                            @if (old('tipo')) {{ old('tipo') == 'secretario' ? 'selected' : null }}@else @if ($usuario->tipo == 'secretario') selected @endif
                                            @endif>SECRETÁRIO</option>
                                        <option value="tesoureiro"
                                            @if (old('tipo')) {{ old('tipo') == 'tesoureiro' ? 'selected' : null }}@else @if ($usuario->tipo == 'tesoureiro') selected @endif
                                            @endif>TESOUREIRO</option>
                                        <option value="presidente"
                                            @if (old('tipo')) {{ old('tipo') == 'presidente' ? 'selected' : null }}@else @if ($usuario->tipo == 'presidente') selected @endif
                                            @endif> PRESIDENTE</option>
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="col-12 col-md-5">
                            <div class="form-group">
                                <label>INDICAÇÃO*</label>
                                <select class="form-control selectpicker" data-style="btn btn-link" name="indicado">
                                    <option disabled selected>SELECIONE</option>
                                    @foreach (\app\User::all() as $indicar)
                                        <option
                                            @if (old('indicado')) {{ old('indicado') == $indicar->id ? 'selected' : null }}@else @if ($usuario->indicado_por_id == $indicar->id) selected @endif
                                            @endif value="{{ $indicar->id }}">{{ $indicar->nome }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12 col-md-5">
                            <div class="form-group bmd-form-group @error('nome') has-danger @enderror">
                                <label>NOME*</label>
                                <input type="text" class="form-control text-uppercase" required name="nome"
                                    maxlength="255"
                                    value="@if (old('nome')) {{ old('nome') }}@else{{ $usuario->nome }} @endif">
                                @error('nome')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group bmd-form-group @error('cpf') has-danger @enderror">
                                <label>CPF*</label>
                                <input type="text" class="form-control mask-cpf" required name="cpf" maxlength="14"
                                    value="@if (old('cpf')) {{ old('cpf') }}@else{{ $usuario->cpf }} @endif">
                                @error('cpf')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12 col-md-3">
                            <div class="form-group bmd-form-group @error('cr') has-danger @enderror">
                                <label>CR</label>
                                <input type="text" class="form-control mask-onlynumber" name="cr" maxlength="11"
                                    value="@if (old('cr')) {{ old('cr') }}@else{{ $usuario->cr }} @endif">
                                @error('cr')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group bmd-form-group @error('validade_cr') has-danger @enderror">
                                <label>Data de Validade do CR</label>
                                <input type="date" class="form-control" name="validade_cr"
                                    value="{{ old('validade_cr') ? old('validade_cr') : ($usuario->validade_cr ? \Carbon\Carbon::parse($usuario->validade_cr)->format('Y-m-d') : null) }}">
                                @error('validade_cr')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12 col-md-6">
                            <div class="form-group bmd-form-group @error('email') has-danger @enderror">
                                <label>E-MAIL</label>
                                <input type="text" class="form-control" name="email" maxlength="200"
                                    value="@if (old('email')) {{ old('email') }}@else{{ $usuario->email }} @endif">
                                @error('email')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <div class="form-group bmd-form-group @error('telefone') has-danger @enderror">
                                <label>TELEFONE*</label>
                                <input type="text" class="form-control mask-phone" required name="telefone" maxlength="12"
                                    value="@if (old('telefone')) {{ old('telefone') }}@else{{ $usuario->telefone }} @endif">
                                @error('telefone')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <div class="form-group bmd-form-group @error('telefone2') has-danger @enderror">
                                <label>TELEFONE 2</label>
                                <input type="text" class="form-control mask-phone" name="telefone2" maxlength="12"
                                    value="@if (old('telefone2')) {{ old('telefone2') }}@else{{ $usuario->telefone2 }} @endif">
                                @error('telefone2')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <div class="form-group bmd-form-group @error('telefone3') has-danger @enderror">
                                <label>TELEFONE 3</label>
                                <input type="text" class="form-control mask-phone" name="telefone3" maxlength="12"
                                    value="@if (old('telefone3')) {{ old('telefone3') }}@else{{ $usuario->telefone3 }} @endif">
                                @error('telefone3')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-4">
                            <div class="form-group bmd-form-group @error('profissao') has-danger @enderror">
                                <label>Profissão *</label>
                                <input type="text" class="form-control" required
                                    name="profissao"value="@if (old('profissao')) {{ old('profissao') }}@else{{ \Illuminate\Support\Facades\Auth::user()->profissao }} @endif">
                                @error('profissao')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group bmd-form-group @error('data_nascimento') has-danger @enderror">
                                <label>Data nascimento *</label>
                                <input type="date" class="form-control mask-data" required name="data_nascimento"
                                    maxlength="12"
                                    value="{{ old('data_nascimento') ? old('data_nascimento') : ($usuario->data_nascimento ? \Carbon\Carbon::parse($usuario->data_nascimento)->format('Y-m-d') : null) }}">
                                @error('data_nascimento')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group bmd-form-group @error('estado_civil') has-danger @enderror">
                                <label>Estado civil</label>
                                <input type="text" class="form-control" name="estado_civil" maxlength="16"
                                    value="@if (old('estado_civil')) {{ old('estado_civil') }}@else{{ $usuario->estado_civil }} @endif">
                                @error('estado_civil')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12 col-md-2">
                            <div class="form-group bmd-form-group @error('cep') has-danger @enderror">
                                <label>CEP</label>
                                <input type="text" class="form-control mask-cep" name="cep" maxlength="10"
                                    value="@if (old('cep')) {{ old('cep') }}@else{{ $usuario->cep }} @endif">
                                @error('cep')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group bmd-form-group @error('cidade') has-danger @enderror">
                                <label>CIDADE*</label>
                                <input type="text" class="form-control" required name="cidade" maxlength="100"
                                    value="@if (old('cidade')) {{ old('cidade') }}@else{{ $usuario->cidade }} @endif">
                                @error('cidade')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group bmd-form-group @error('bairro') has-danger @enderror">
                                <label>BAIRRO</label>
                                <input type="text" class="form-control" name="bairro" maxlength="100"
                                    value="@if (old('bairro')) {{ old('bairro') }}@else{{ $usuario->bairro }} @endif">
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group bmd-form-group @error('endereco') has-danger @enderror">
                                <label>ENDEREÇO</label>
                                <input type="text" class="form-control" name="endereco" maxlength="255"
                                    value="@if (old('endereco')) {{ old('endereco') }}@else{{ $usuario->endereco }} @endif">
                                @error('endereco')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-4">
                            <div class="form-group bmd-form-group @error('nacionalidade') has-danger @enderror">
                                <label>NACIONALIDADE *</label>
                                <input type="text" class="form-control" required name="nacionalidade" maxlength="100"
                                    value="{{ \Illuminate\Support\Facades\Auth::user()->nacionalidade }}"
                                    placeholder="Brasileiro">
                                @error('nacionalidade')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group bmd-form-group @error('cidade_nasceu') has-danger @enderror">
                                <label>CIDADE ONDE NASCEU *</label>
                                <input type="text" class="form-control" required name="cidade_nasceu" maxlength="100"
                                    value="@if (old('cidade_nasceu')) {{ old('cidade_nasceu') }}@else{{ \Illuminate\Support\Facades\Auth::user()->cidade_nasceu }} @endif">
                                @error('cidade_nasceu')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group bmd-form-group @error('estado_nasceu') has-danger @enderror">
                                <label>ESTADO ONDE NASCEU *</label>
                                <select class="form-control selectpicker" required data-style="btn btn-link"
                                    name="estado_nasceu" id="estado_nasceu" title="Selecione um estado">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12 col-md-4">
                            <div class="form-group bmd-form-group @error('data_filiacao') has-danger @enderror">
                                <label>DATA FILIAÇÃO*</label>
                                <input type="date" class="form-control" required name="data_filiacao"
                                    value="{{ old('data_filiacao') ? old('data_filiacao') : ($usuario->data_filiacao ? \Carbon\Carbon::parse($usuario->data_filiacao)->format('Y-m-d') : null) }}">
                                @error('data_filiacao')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group bmd-form-group @error('data_primeira_habitualidade') has-danger @enderror">
                                <label>DATA PRIMEIRA HABITUALIDADE</label>
                                <input type="date" class="form-control" name="data_primeira_habitualidade"
                                    value="{{ old('data_primeira_habitualidade') ? old('data_primeira_habitualidade') : ($usuario->data_primeira_habitualidade ? \Carbon\Carbon::parse($usuario->data_primeira_habitualidade)->format('Y-m-d') : null) }}">
                                @error('data_filiacao')
                                    <span class="material-icons form-control-feedback">clear</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-check">
                                <label class="form-check-label mt-3">
                                    <input class="form-check-input" type="checkbox" name="janta"
                                        @if (old('janta')) {{ old('endereco') ? 'checked' : null }}@else @if ($usuario->janta) checked @endif
                                        @endif>
                                    Janta
                                    <span class="form-check-sign">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12 col-md-2">
                            <a href="{{ route('painel-usuarios-listagem') }}"
                                class="btn btn-secondary @mobile
btn-block
@endmobile">Cancelar</a>
                        </div>
                        <div class="col-12 col-md-10 text-right">
                            <button type="button"
                                class="btn btn-primary senha @mobile
btn-block
@endmobile">Trocar
                                Senha</button>
                            <button type="submit"
                                class="btn btn-success pull-right @mobile
btn-block
@endmobile">Salvar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endisset
@endsection
@section('script')
    <script>
        $(document).ready(() => {
            $('button.senha').click(function() {
                Swal.fire({
                    title: 'Digite a nova senha',
                    input: 'text',
                    inputAttributes: {
                        autocapitalize: 'off'
                    },
                    showCancelButton: true,
                    confirmButtonText: 'Trocar',
                    cancelButtonText: 'Cancelar',
                    showLoaderOnConfirm: true,
                    preConfirm: (password) => {
                        return $.post('{{ route('painel-usuarios-newpassword') }}', {
                                novasenha: password,
                                id: '{{ $usuario->id }}',
                                _token: '{{ csrf_token() }}'
                            })
                            .fail(function() {
                                Swal.showValidationMessage(
                                    'Falha na requisição ao servidor'
                                );
                            });
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.value) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Senha alterada com sucesso!',
                        });
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Falha ao alterar a senha! Tente novamente. ',
                        });
                    }
                });
            });
            $('input[name="cpf"]').focusout(function() {
                if (!validaCPF($(this).val())) {
                    $(this).parent().removeClass('has-success').addClass('has-danger');
                    Swal.fire({
                        icon: 'error',
                        title: 'CPF INVÁLIDO',
                    })
                } else {
                    $(this).parent().removeClass('has-danger').addClass('has-success');
                }
            });
            if (!!$('input[name="cr"]').val()) {
                $('input[name="validade_cr"]').prop('required', true);
            } else {
                $('input[name="validade_cr"]').prop('required', false);
            }
            $('input[name="cr"]').focusout(function() {
                if (!!$(this).val()) {
                    $('input[name="validade_cr"]').prop('required', true);
                } else {
                    $('input[name="validade_cr"]').prop('required', false);
                }
            });

            async function loadUF(campoSelect, atributo) {
                try {
                    const response = await fetch("https://servicodados.ibge.gov.br/api/v1/localidades/estados");
                    const estados = await response.json();

                    estados.sort((a, b) => a.nome.localeCompare(b.nome))

                    estados.forEach((estado) => {
                        var isSelected = estado.sigla === atributo;

                        const option = document.createElement("option");
                        option.value = estado.sigla;
                        option.text = estado.nome;
                        option.selected = isSelected;

                        campoSelect.add(option);
                    });
                    $(campoSelect).selectpicker('refresh');
                } catch (error) {
                    console.error("Erro ao carregar estados:", error);
                }
            }

            var estadoNasceu =
                '{{ !empty(\Illuminate\Support\Facades\Auth::user()->estado_nasceu) ? \Illuminate\Support\Facades\Auth::user()->estado_nasceu : 'SC' }}';
            var estadoReside =
                '{{ !empty(\Illuminate\Support\Facades\Auth::user()->estado) ? \Illuminate\Support\Facades\Auth::user()->estado : 'SC' }}';

            loadUF(document.getElementById("estado_nasceu"), estadoNasceu);
            loadUF(document.getElementById("estado"), estadoReside);
        });
    </script>
@endsection
