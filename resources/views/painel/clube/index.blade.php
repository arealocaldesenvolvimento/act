@extends('layouts.material')
@section('title', 'Informações Gerais do Clube')

@section('content')
    @if(is_null($clube))
        <div class="row">
            <div class="col-12">
                <div class="alert alert-danger alert-with-icon" data-notify="container">
                    <i class="material-icons" data-notify="icon">error_outline</i>
                    <span data-notify="message"><strong>Nenhuma informação do clube cadastrada!</strong><br/>Favor cadastra-la para garantir a usuabilidade de todas as funções do sistema, bem como a emissão de declarações.</span>
                </div>
            </div>
        </div>
    @endif
    {{--SOBRE O CLUBE--}}
    <div class="card mt-5">
       <div class="card-header card-header-text card-header-primary">
           <div class="card-text">
               <h4 class="card-title">Sobre o clube</h4>
           </div>
        </div>
        <div class="card-body">
            <form method="post">
                @csrf
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group">
                            <label>Nome do clube *</label>
                            <input type="text" class="form-control" name="nome" required maxlength="255" value="@isset($clube->nome){{$clube->nome}}@endisset">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group bmd-form-group">
                            <label>CNPJ *</label>
                            <input type="text" class="form-control mask-cnpj" name="cnpj" required maxlength="17" value="@isset($clube->cnpj){{$clube->cnpj}}@endisset">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group bmd-form-group">
                            <label>CR *</label>
                            <input type="text" class="form-control mask-onlynumber" name="cr" maxlength="11" value="@isset($clube->cr){{$clube->cr}}@endisset">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group bmd-form-group">
                            <label>Validade CR *</label>
                            <input type="text" class="form-control mask-data" name="validade_cr" value="@isset($clube->validade_cr)  {{date('d/m/Y', strtotime($clube->validade_cr))}} @endisset">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group bmd-form-group">
                            <label>CEP *</label>
                            <input type="text" class="form-control mask-cep" name="cep" required maxlength="10" value="@isset($clube->cep){{$clube->cep}}@endisset">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group bmd-form-group">
                            <label>Endereço *</label>
                            <input type="text" class="form-control" name="endereco" required maxlength="255" value="@isset($clube->endereco){{$clube->endereco}}@endisset">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group">
                            <label>Bairro *</label>
                            <input type="text" class="form-control" name="bairro" required maxlength="100" value="@isset($clube->bairro){{$clube->bairro}}@endisset">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group">
                            <label>Cidade *</label>
                            <input type="text" class="form-control" name="cidade" required maxlength="100" value="@isset($clube->nome){{$clube->cidade}}@endisset">
                        </div>
                    </div>
                </div>
                <h4 class="card-title mt-3">Dados Bancários</h4>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group">
                            <label>Banco *</label>
                            <input type="text" class="form-control" name="banco" required maxlength="45" value="@isset($clube->banco){{$clube->banco}}@endisset">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group">
                            <label>Agência *</label>
                            <input type="text" class="form-control" name="agencia" required maxlength="45" value="@isset($clube->agencia){{$clube->agencia}}@endisset">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group bmd-form-group">
                            <label>Conta-corrente *</label>
                            <input type="text" class="form-control" name="conta_corrente" required maxlength="45" value="@isset($clube->conta_corrente){{$clube->conta_corrente}}@endisset">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group bmd-form-group">
                            <label>Pix 1</label>
                            <input type="text" class="form-control" name="pix1" maxlength="255" value="@isset($clube->pix1){{$clube->pix1}}@endisset">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group bmd-form-group">
                            <label>Pix 2</label>
                            <input type="text" class="form-control" name="pix2" maxlength="255" value="@isset($clube->pix2){{$clube->pix2}}@endisset">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group bmd-form-group">
                            <label>Pix 3</label>
                            <input type="text" class="form-control" name="pix3" maxlength="255" value="@isset($clube->pix3){{$clube->pix3}}@endisset">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-right">
                        <button type="submit" class="btn btn-success @mobile btn-block @endmobile">Salvar dados do clube</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--PRESIDENTE--}}
    <div class="card mt-5">
       <div class="card-header card-header-text card-header-primary">
           <div class="card-text">
               <h4 class="card-title">Presidente</h4>
           </div>
        </div>
        <div class="card-body">
            <form action="{{route('painel-sobre-clube-presidente')}}" enctype="multipart/form-data" method="post" class="form_presidente">
                @csrf
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <select class="form-control selectpicker" data-style="btn btn-link" required name="presidente_id">
                                <option disabled selected>Selecione*</option>
                                @foreach($usuarios as $usuario)
                                    <option @if(isset($presidente) && $usuario->id == $presidente->id) selected @endif value="{{$usuario->id}}">{{$usuario->nome}} ({{$usuario->cpf}})</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Data início do mandato*</label>
                            <input required type="date" value="@isset($presidente){{\Carbon\Carbon::parse($presidente->data_inicio)->format('Y-m-d')}}@endisset" class="form-control" name="presidente_data_inicio">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Data fim do mandato*</label>
                            <input required type="date" value="@isset($presidente){{\Carbon\Carbon::parse($presidente->data_fim)->format('Y-m-d')}}@endisset" class="form-control" name="presidente_data_fim">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-file-upload form-file-multiple">
                            <input type="file" class="inputFileHidden" name="assinatura_digital" required>
                            <div class="input-group">
                                <input type="text" required class="form-control inputFileVisible" value="@isset($presidente){{$presidente->nome_original}}@endisset" placeholder="Assinatura Digital*">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-fab btn-round btn-primary">
                                        <i class="material-icons">attach_file</i>
                                    </button>
                                </span>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col text-right">
                        <button type="submit" class="btn btn-success @mobile btn-block @endmobile">SALVAR PRESIDENTE</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--SECRETÁRIO--}}
    <div class="card mt-5">
       <div class="card-header card-header-text card-header-primary">
           <div class="card-text">
               <h4 class="card-title">Secretário</h4>
           </div>
        </div>
        <div class="card-body">
            <form method="post" action="{{route('painel-sobre-clube-secretario')}}" class="form_secretario">
                @csrf
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <select class="form-control selectpicker" data-style="btn btn-link" required name="secretario_id">
                                <option disabled selected>Selecione*</option>
                                @foreach($usuarios as $usuario)
                                    <option @if(isset($secretario) && $usuario->id == $secretario->id) selected @endif value="{{$usuario->id}}">{{$usuario->nome}} ({{$usuario->cpf}})</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Data inicio do mandato*</label>
                            <input required type="date" class="form-control" name="secretario_data_inicio" value="@isset($secretario->data_inicio){{\Carbon\Carbon::parse($secretario->data_inicio)->format('Y-m-d')}}@endisset">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Data fim do mandato*</label>
                            <input required type="date" class="form-control" name="secretario_data_fim" value="@isset($secretario->data_fim){{\Carbon\Carbon::parse($secretario->data_fim)->format('Y-m-d')}}@endisset">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-right">
                        <button type="submit" class="btn btn-success @mobile btn-block @endmobile">SALVAR SECRETÁRIO</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {{--TESOUREIRO--}}
    <div class="card mt-5">
       <div class="card-header card-header-text card-header-primary">
           <div class="card-text">
               <h4 class="card-title">Tesoureiro</h4>
           </div>
        </div>
        <div class="card-body">
            <form method="post" action="{{route('painel-sobre-clube-tesoureiro')}}"  class="form_tesoureiro">
                @csrf
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <select class="form-control selectpicker" data-style="btn btn-link" required name="tesoureiro_id">
                                <option disabled selected>Selecione*</option>
                                @foreach($usuarios as $usuario)
                                    <option @if(isset($tesoureiro) && $usuario->id == $tesoureiro->id) selected @endif value="{{$usuario->id}}">{{$usuario->nome}} ({{$usuario->cpf}})</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Data inicio do mandato</label>
                            <input type="date" class="form-control" name="tesoureiro_data_inicio" value="@isset($tesoureiro->data_inicio){{\Carbon\Carbon::parse($tesoureiro->data_inicio)->format('Y-m-d')}}@endisset">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Data fim do mandato</label>
                            <input type="date" class="form-control" name="tesoureiro_data_fim" value="@isset($tesoureiro->data_fim){{\Carbon\Carbon::parse($tesoureiro->data_fim)->format('Y-m-d')}}@endisset">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-right">
                        <button type="submit" class="btn btn-success @mobile btn-block @endmobile">SALVAR TESOUREIRO</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
<script>
    function validarDatas(form) {
        var dataInicio = form.querySelector('[type="date"][name$="_data_inicio"]');
        var dataFim = form.querySelector('[type="date"][name$="_data_fim"]');
        var hoje = new Date().toISOString().split('T')[0];

        if (dataInicio && dataInicio.value > hoje) {
            Swal.fire({
                title: 'Erro!',
                text: 'A Data de Início do Mandato deve ser menor ou igual à data de hoje.',
                icon: 'error',
                confirmButtonText: 'Ok'
            });
            return false;
        }

        if (dataFim && dataFim.value < hoje) {
            Swal.fire({
                title: 'Erro!',
                text: 'A Data de Fim do Mandato deve ser maior ou igual à data de hoje.',
                icon: 'error',
                confirmButtonText: 'Ok'
            });
            return false;
        }

        return true;
    }

    document.addEventListener('DOMContentLoaded', function() {
        var formPresidente = document.querySelector('.form_presidente');
        var formSecretario = document.querySelector('.form_secretario');
        var formTesoureiro = document.querySelector('.form_tesoureiro');

        formPresidente.addEventListener('submit', function(event) {
            console.log(formPresidente)
            if (!validarDatas(formPresidente)) {
                event.preventDefault();
            }
        });

        formSecretario.addEventListener('submit', function(event) {
            if (!validarDatas(formSecretario)) {
                event.preventDefault();
            }
        });

        formTesoureiro.addEventListener('submit', function(event) {
            if (!validarDatas(formTesoureiro)) {
                event.preventDefault();
            }
        });
    });
</script>
