@extends('layouts.material')
@section('title', 'Certificado digital')

<style>
    .card .card-header h5{
        font-size: 25px;
        text-align: center;
        font-weight: 400;
    }


    .a-custom {
        color: #2a6cbd;
    }

    .custom-file-upload {
        color: #2a6cbd;
        display: inline-block;
        font-weight: 700;
        line-height: 1.667;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
        background-color: transparent;
        border: 1px solid #1b569e;
        padding: 0.625rem 1.5rem;
        font-size: 0.75rem;
        border-radius: 0.5rem;
        width: 100%;

        border-bottom-left-radius: 0.5rem !important;
        border-top-left-radius: 0.5rem !important;
        margin-left: 0 !important;
    }
    .alert-warning li{
        color: black;
    }
    .alert-warning ul{
        margin-bottom: 0px;
    }
</style>

@section('content')


    <div class="row d-flex justify-content-center">
        <div class="col-6">
            <div class="card mt-5">
                <div class="card-header">
                    <h5>Certificado digital (.pfx)</h5>
                </div>

                <div class="card-body">
                    <p>Última alteração de certificado: 12/12/2023</p>
                    <p>Vencimento do certificado em: 12/12/2024</p>


                    <form method="POST" action="{{route('painel-certificado-store')}}" enctype="multipart/form-data"
                    class="multisteps-form__form">
                        @csrf

                        <div class="input-group input-group-dynamic mt-4">
                            Alterar certificado:
                            <div class="input-group">
                                <input id="fileImagem" accept=".pfx" required capture=environment type="file" name="arquivo" style="display: none;">
                                <label for="fileImagem" id="buttonFileImagem" class="custom-file-upload">
                                    Escolher Arquivo
                                </label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group bmd-form-group @error('senha') has-danger @enderror">
                                <label>Senha *</label>
                                <input type="text" class="form-control" required name="senha" >
                                @error('senhao')<span class="material-icons form-control-feedback">clear</span>@enderror
                            </div>
                        </div>

                        <div class="mt-4">
                            <button type="submit" class="mt-5 btn btn-success ml-auto d-flex ">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

<script>
    document.addEventListener("DOMContentLoaded", function(e) {
        var fileImagem = document.getElementById("fileImagem");
        var buttonFile = document.getElementById("buttonFileImagem");
        fileImagem.addEventListener("change", function() {
            if (fileImagem.files.length > 0) {
                buttonFile.textContent = fileImagem.files[0].name;
            } else {
                buttonFile.textContent = "Escolher Arquivo";
            }
        });
    });
</script>
