@extends('layouts.material')
@section('title', 'Notícias - Editar')
@section('content')
@isset($post)
  <div class="card card-nav-tabs">
      <div class="card-header card-header-primary">
          <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
          <div class="nav-tabs-navigation">
              <div class="nav-tabs-wrapper">
                  <ul class="nav nav-tabs" data-tabs="tabs">
                      <li class="nav-item">
                          <a class="nav-link" href="{{route('noticias.index')}}"><i class="material-icons">view_list</i> Listagem de notícias</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="{{route('noticias.create')}}"><i class="material-icons">add</i> Cadastrar nova notícia</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link active" href="#"><i class="material-icons">edit</i>EDITANDO</a>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
          <div class="card-body">
              <form method="post" action="{{route('noticias.update', $post->id)}}" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <input type="hidden" value="{{$post->id}}" name="id">
                  <div class="row">
                    <div class="col-12 col-md-8 mx-auto">
                      <label class="mb-2"><strong>Informações</strong></label>
                      <div class="row">

                        <div class="col-12 mt-5">
                          <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                            <div class="fileinput-new thumbnail img-raised">
                                <img src="{{ $post->thumbnail }}" alt="thumbnail">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
                            <div>
                              <span class="btn btn-raised btn-round btn-default btn-file">
                                <span class="fileinput-new">Selecionar imagem</span>
                                <span class="fileinput-exists">Mudar</span>
                                <input id="new-thumb" type="file" name="file"/>
                              </span>
                              <!-- <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a> -->
                            </div>
                          </div>
                        </div>

                        <div class="col-12 mt-3">
                          <div class="form-group bmd-form-group @error('data') has-danger @enderror">
                            <label for="title">Título*</label>
                            <input type="text" class="form-control" required name="title" value="{{old('title') ? old('title') : $post->title}}">
                            @error('title')<span class="material-icons form-control-feedback">clear</span>@enderror
                          </div>
                        </div>

                        <div class="col-12 mt-3">
                          <select class="form-control selectpicker" data-style="btn btn-link" required name="category_id" placeholder="SELECIONE UMA CATEGORIA*">
                              <option disabled selected placeholder value="none" selected>CATEGORIA*</option>
                              @foreach(\App\PostCategory::all()->sortBy('name') as $category)
                                  <option @if ($post->post_category_id == $category->id) selected @endif value="{{$post->post_category_id}}">{{$category->name}}</option>
                              @endforeach
                          </select>
                        </div>

                        <div class="col-12 mx-auto mt-3">
                          <div class="form-group">
                            <textarea name="content" id="editor" rows="20">
                              {{ old('content') ? old('content') : $post->description }}
                            </textarea>
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12 text-right">
                      <button type="submit" class="btn btn-success pull-right">Atualizar</button>
                    </div>
                  </div>
              </form>
          </div>
      </div>
    @else
        <p>Nenhuma notícia encontrada neste endereço. Tente novamente ou volte a listagem de notícias.</p>
    @endisset

<style>
  .fileinput-preview img {
    width: 100%;
    max-height: 500px;
    object-fit: cover;
    object-position :center;
  }
  .fileinput-new img{
    width: 100%;
    max-height: 500px;
    object-fit: cover;
    object-position :center;
  }
  .fileinput-exists {
    display : none;
  }
  .btn-file input{
    position: absolute;
    top: 0;
    right: 0;
    width: 100%;
    height: 100%;
    margin: 0;
    font-size: 23px;
    cursor: pointer;
    filter: alpha(opacity=0);
    opacity: 0;
    direction: ltr;
  }
</style>
@endsection

@section('script')
  <script>
    tinymce.init({
      selector: '#editor'
    });

    $(document).ready( function(){
      $('#new-thumb').change( function(){
        setTimeout(() => {
          $('.fileinput-exists').css('display','block')
          $('.fileinput-new').hide('fast')
        },100);
      })
    })
  </script>
@endsection