@extends('layouts.material')
@section('title', 'Notícias - cadastro')
@section('content')
<div class="card card-nav-tabs">
  <div class="card-header card-header-primary">
    <div class="nav-tabs-navigation">
      <div class="nav-tabs-wrapper">
        <ul class="nav nav-tabs" data-tabs="tabs">
          <li class="nav-item">
            <a class="nav-link" href="{{route('noticias.index')}}"><i class="material-icons">view_list</i> Listagem de notícias</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="{{route('noticias.create')}}"><i class="material-icons">add</i> Cadastrar novo post</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="card-body ">
    <form method="post" id="new-post" action="{{ route('noticias.store') }}" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="fileinput fileinput-new text-center w-100 mt-3" data-provides="fileinput">
          <div class="col-md-6 col-12 mx-auto">
            <div class="fileinput-preview fileinput-exists thumbnail img-raised"></div>
          </div>
          <div class="col-12 mx-auto mt-3">
            <div class="form-group form-file-upload form-file-multiple">
              <input name="file" type="file" multiple="" class="inputFileHidden">
              <div class="input-group">
                <input type="text" class="form-control inputFileVisible" placeholder="Imagem destacada">
                <span class="input-group-btn">
                  <button type="button" class="btn btn-fab btn-round btn-primary">
                    <i class="material-icons">attach_file</i>
                  </button>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 mx-auto">
          <div class="form-group">
            <input value="{{ old('title') }}" name="title" type="text" class="form-control" placeholder="Título notícia">
          </div>
        </div>
        <div class="col-12 mt-3">
          <select class="form-control selectpicker" data-style="btn btn-link" required name="category_id" placeholder="SELECIONE A CATEGORIA*">
              <option disabled selected placeholder value="none" selected>SELECIONAR CATEGORIA</option>
              @foreach(\App\PostCategory::all()->sortBy('nome') as $category)
                <option value="{{$category->id}}" data-cr="{{$category->name}}">{{$category->name}}</option>
              @endforeach
          </select>
        </div>
        <div class="col-12 mx-auto mt-3">
          <div class="form-group">
            <textarea name="content" id="editor" rows="20">
              {{ old('content') }}
            </textarea>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 text-right">
          <button type="submit" form="new-post" class="btn btn-success pull-right">Criar</button>
        </div>
      </div>
    </form>
  </div>
</div>

<style>
  .fileinput-preview img {
    width: 100%
  }
</style>
@endsection

@section('script')
  <script>
    tinymce.init({
      selector: '#editor'
    });
  </script>
@endsection