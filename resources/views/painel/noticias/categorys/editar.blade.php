@extends('layouts.material')
@section('title', 'Categorias - Editar')
@section('content')

@isset($category)
  <div class="card card-nav-tabs">
      <div class="card-header card-header-primary">
          <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
          <div class="nav-tabs-navigation">
              <div class="nav-tabs-wrapper">
                  <ul class="nav nav-tabs" data-tabs="tabs">
                      <li class="nav-item">
                          <a class="nav-link" href="{{route('categorias.index')}}"><i class="material-icons">view_list</i> Listagem de categorias</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="{{route('categorias.create')}}"><i class="material-icons">add</i> Cadastrar nova categoria</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link active" href="#"><i class="material-icons">edit</i>EDITANDO</a>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
          <div class="card-body">
              <form method="post" action="{{route('categorias.update', $category->id)}}" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                  <input type="hidden" value="{{$category->id}}" name="id">
                  <div class="row">
                    <div class="col-12 col-md-8 mx-auto">
                      <label class="mb-2"><strong>Informações</strong></label>
                      <div class="row">

                        <div class="col-12 mt-3">
                          <div class="form-group bmd-form-group @error('data') has-danger @enderror">
                            <label for="title">Nome categoria*</label>
                            <input type="text" class="form-control" required name="name" value="{{old('name') ? old('name') : $category->name}}">
                            @error('name')<span class="material-icons form-control-feedback">clear</span>@enderror
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12 text-right">
                      <button type="submit" class="btn btn-success pull-right">Atualizar</button>
                    </div>
                  </div>
              </form>
          </div>
      </div>
    @else
        <p>Nenhuma categoria encontrada neste endereço. Tente novamente ou volte a listagem de categorias.</p>
    @endisset

@endsection