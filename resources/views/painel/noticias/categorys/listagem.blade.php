@extends('layouts.material')
@section('title', 'Categorias - Listagem')
@section('content')

<div class="card card-nav-tabs">
  <div class="card-header card-header-primary">
      <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
      <div class="nav-tabs-navigation">
          <div class="nav-tabs-wrapper">
              <ul class="nav nav-tabs" data-tabs="tabs">
                  @if(\Illuminate\Support\Facades\Auth::user()->tipo == "presidente" || \Illuminate\Support\Facades\Auth::user()->tipo == "secretario")
                      <li class="nav-item">
                          <a class="nav-link active" href="{{route('noticias.index')}}"><i class="material-icons">view_list</i> Listagem de notícias</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('noticias.create') }}"><i class="material-icons">add</i> Cadastrar novo post</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link active" href="{{route('categorias.index')}}"><i class="material-icons">view_list</i> Listagem de categorias</a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('categorias.create') }}"><i class="material-icons">add</i> Cadastrar nova categoria</a>
                      </li>
                  @endif
              </ul>
          </div>
      </div>
  </div>
  <div class="card-body ">
      <div class="tab-content">
          <div class="table-responsive">
            <table id="tabela-cat" class="w-100 table datatable d-none">
              <thead>
                <tr>
                  <td>NOME</td>
                  <td style="width:100px">OPÇÕES</td>
                </tr>
              </thead>
              <tbody>
                @forelse($categorys as $category)
                  <tr>
                      <td>{{$category->name}}</td>
                      <td class="td-actions text-right">
                          <input id="category_id" type="hidden" value="{{$category->id}}">
                          <a href="{{ route('categorias.edit', $category->id) }}" class="btn btn-success">
                              <i class="material-icons">edit</i>
                          </a>
                          <button type="button" class="btn btn-danger delete">
                              <i class="material-icons">delete</i>
                          </button>
                      </td>
                  </tr>
                @empty
                @endforelse
              </tbody>
            </table>
          </div>
          <div class="row spinner">
              <div class="col-12 text-center">
                  <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
              </div>
          </div>
      </div>
  </div>
</div>
@endsection
@section('script')
  <script>
      $(document).ready(()=>{
          $('button.delete').click(function () {
              var button = $(this);
              Swal.fire({
                  title: 'Você tem certeza que deseja remover essa categoria?',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#d33',
                  cancelButtonColor: '#c0cdd6',
                  confirmButtonText: 'Sim, remover!',
                  cancelButtonText: 'Cancelar!'
              }).then((result) => {
                  const id = $('#category_id').val()
                  if (result.value) {
                    $.post(`categorias/${id}`, 
                        { 
                            _token: '{{csrf_token()}}', 
                            _method : 'DELETE'
                        }
                    )
                    .done( response =>  {
                        console.log(response)
                        button.parent().parent().find('*').css('text-decoration', 'line-through').hide('slow')
                    })
                    .fail( () => {
                        Swal.fire({
                            title: 'Categoria não pode ser deletada, existe notícias dependentes',
                            type: 'warning',
                            showCancelButton: false,
                            confirmButtonColor: '#c0cdd6',
                            confirmButtonText: 'Ok!',
                        })
                    })
                  }
              })
          });

          configDatatables['initComplete'] = function() {
              $('div.dt-buttons').css('display','none')
              $('#tabela-cat').removeClass('d-none');
              $('div.spinner').remove();
          };

          $('.datatable').DataTable(configDatatables);
      });
  </script>

@endsection