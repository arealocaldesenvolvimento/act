@extends('layouts.material')
@section('title', 'Categorias - Cadastro')
@section('content')

<div class="card card-nav-tabs">
  <div class="card-header card-header-primary">
      <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
      <div class="nav-tabs-navigation">
        <div class="nav-tabs-wrapper">
          <ul class="nav nav-tabs" data-tabs="tabs">
            <li class="nav-item">
              <a class="nav-link" href="{{route('categorias.index')}}"><i class="material-icons">view_list</i> Listagem de categorias</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="{{route('categorias.create')}}"><i class="material-icons">add</i> Cadastrar nova categoria</a>
            </li>
          </ul>
        </div>
      </div>
  </div>
  <div class="card-body ">

    <form method="post" id="new-post" action="{{ route('categorias.store') }}">
      @csrf
      <div class="row">
        <div class="col-12 mx-auto mt-5">
          <div class="form-group">
            <input name="name" type="text" class="form-control" placeholder="Nome categoria">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 text-right">
          <button type="submit" form="new-post" class="btn btn-success pull-right">Criar</button>
        </div>
      </div>
    </form>

  </div>
</div>

@endsection