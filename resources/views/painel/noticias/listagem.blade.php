@extends('layouts.material')
@section('title', 'Notícias - Listagem')
@section('content')
    <style>
        .dataTable>thead>tr>th,
        .dataTable>tbody>tr>th,
        .dataTable>tfoot>tr>th,
        .dataTable>thead>tr>td,
        .dataTable>tbody>tr>td,
        .dataTable>tfoot>tr>td {
            min-height: 55px;
        }

        .td-actions.text-right {
            display: flex;
            justify-content: flex-end;
            align-items: center;
            gap: 5px;
        }
    </style>
    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        @if (
                            \Illuminate\Support\Facades\Auth::user()->tipo == 'presidente' ||
                                \Illuminate\Support\Facades\Auth::user()->tipo == 'secretario')
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ route('noticias.index') }}"><i
                                        class="material-icons">view_list</i> Listagem de notícias</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('noticias.create') }}"><i class="material-icons">add</i>
                                    Cadastrar novo post</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="{{ route('categorias.index') }}"><i
                                        class="material-icons">view_list</i> Listagem de categorias</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('categorias.create') }}"><i
                                        class="material-icons">add</i> Cadastrar nova categoria</a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content">
                <div class="table-responsive">
                    <table id="tabela-posts" class="table datatable d-none">
                        <thead>
                            <tr>
                                <th>TÍTULO DA NOTÍCIA</th>
                                <th style="width: 150px;">DATA</th>
                                <th style="width: 130px;">OPÇÕES</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($posts as $post)
                                <tr>
                                    <td>{{ $post->title }}</td>
                                    <td>{{ $post->created_at }}</td>
                                    <td class="td-actions text-right">
                                        <input id="id_post" type="hidden" value="{{ $post->id }}">
                                        <a href="{{ route('noticias.edit', $post->id) }}" class="btn btn-success">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <form action="{{ url('/noticias', $post->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger delete">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="row spinner">
                    <div class="col-12 text-center">
                        <div class="lds-spinner">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(() => {
            $('button.delete').click(function(event) {
                event.preventDefault();
                var button = $(this);
                Swal.fire({
                    title: 'Você tem certeza que deseja remover esse post?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#c0cdd6',
                    confirmButtonText: 'Sim, remover!',
                    cancelButtonText: 'Cancelar!'
                }).then((result) => {
                    if (result.value == true) {
                        console.log(true)
                        button.closest('form').submit();
                    }
                })
            });

            configDatatables['initComplete'] = function() {
                $('div.dt-buttons').hide();
                $('#tabela-posts').removeClass('d-none');
                $('div.spinner').remove();
            };

            $('.datatable').DataTable(configDatatables);
        });
    </script>
@endsection
