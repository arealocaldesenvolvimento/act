@extends('layouts.material')
@section('title', 'Contas')
@section('content')
    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('painel-conta-listagem')}}"><i class="material-icons">view_list</i> Contas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('painel-conta-debitar-creditar')}}"><i class="material-icons">unfold_more</i> DEBITAR/CREDITAR</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="{{route('painel-conta-anuidade')}}"><i class="material-icons">event_available</i> GERAR ANUIDADES</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content">
                <div class="row">
                    <div class="col">
                        <div class="jumbotron">
                            <h1 class="display-4">Gerador de anuidade automático!</h1>
                            <p class="lead">Lembre-se: ao utilizar o gerador todas as operações financeiras executadas <strong class="text-danger">não poderam ser desfeitas</strong>!</p>
                            <hr class="my-4">
                            @if(is_null($anuidade))
                                <p>anuidade cadastrada para esse ano! <a href="{{route('painel-anuidade-listagem')}}">Clique aqui</a> para ir à página de cadastro</p>
                            @else
                                <p>Anuidade atual de <strong>R$ <span class="valoranuidade">{{number_format($anuidade->valor, 2, '.', '')}}</span></strong> para o ano de <strong>{{$anuidade->ano}}</strong>.</p>
                                <button class="btn btn-primary btn-lg gerar">GERAR ANUIDADES</button>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>NOME</th>
                                    <th>SALDO FINAL</th>
                                    <th>STATUS</th>
                                </tr>
                                </thead>
                                <tbody id="results">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(()=>{
            $('button.gerar').click(function () {
                Swal.fire({
                    title: 'Você tem certeza que deseja gerar todas anuidades?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#c0cdd6',
                    confirmButtonText: 'Sim, gerar!',
                    cancelButtonText: 'Cancelar!'
                }).then((result) => {
                    if (result.value) {
                        $.get('{{route('painel-conta-anuidade-gerar')}}')
                            .done(function (data) {
                                data = JSON.parse(data);
                                $.each(data, function (key, value) {
                                    $('tbody#results').append('<tr><td>'+value.nome+'</td><td class="'+ (value.saldo <= 0 ? "text-danger" : "text-success")+'">R$ '+parseFloat(value.saldo).toFixed(2)+'</td><td>'+(value.status ? "SUCESSO": "FALHA")+'</td></tr>');
                                })
                            })
                    }
                })
            });
        });
    </script>
@endsection
