@extends('layouts.material')
@section('title', 'Contas - Debitar/Creditar')
@section('content')
    <div class="card card-nav-tabs" id="card"
        style="transition: all linear .3s; @if (isset($last) && $last->debito != 0) box-shadow: rgba(202, 63, 63, 0.35) 0px 1px 20px 2px @endif @if (isset($last) && $last->credito != 0) box-shadow: 0 1px 20px 2px rgba(46, 128, 60, 0.35) @endif">
        <div class="card-header card-header-primary">
            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('painel-conta-listagem') }}"><i
                                    class="material-icons">view_list</i> Contas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="{{ route('painel-conta-debitar-creditar') }}"><i
                                    class="material-icons">unfold_more</i> DEBITAR/CREDITAR</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('painel-conta-anuidade') }}"><i
                                    class="material-icons">event_available</i> GERAR ANUIDADES</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="tab-content">
                <div class="row">
                    <div class="col">
                        <form method="post" id="form-debitar-creditar">
                            @csrf
                            <div class="row">
                                <div class="col-12 col-md-4 border-right">
                                    <div class="row">
                                        <div class="col">
                                            <label class="mb-2"><strong>Evento</strong></label>
                                            @error('evento_id')
                                                <div class="alert alert-danger" role="alert">Selecione um tipo de
                                                    movimentação:</div>
                                            @enderror
                                            <div class="custom-radio @error('tipo') border border-danger @enderror">
                                                <div class="inputGroup">
                                                    <input id="radio-debito" value="debito" name="tipo" type="radio"
                                                        @if (isset($last) && $last->debito != 0) checked @endif />
                                                    <label for="radio-debito"><i style="vertical-align: middle"
                                                            class="material-icons">remove</i> DEBITAR</label>
                                                </div>
                                                <div class="inputGroup">
                                                    <input id="radio-credito" value="credito" name="tipo" type="radio"
                                                        @if (isset($last) && $last->credito != 0) checked @endif />
                                                    <label for="radio-credito"><i style="vertical-align: middle"
                                                            class="material-icons">add</i> CREDITAR</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-8">
                                    <div class="row">
                                        <div class="col">
                                            <div class="row">
                                                <div class="col-12 col-md-3">
                                                    <div
                                                        class="form-group bmd-form-group @error('data') has-danger @enderror">
                                                        <label>DATA*</label>
                                                        <input type="date" class="form-control" required name="data"
                                                            value="{{ old('data') ? old('data') : (isset($last) ? \Carbon\Carbon::parse($last->data)->format('Y-m-d') : null) }}">
                                                        @error('data')
                                                            <span class="material-icons form-control-feedback">clear</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-md-8">
                                            <select class="select-user" required name="associado_id" id="associado_id"
                                                placeholder="SELECIONE UM ASSOCIADO*">
                                                <option disabled selected placeholder value="none" selected>ASSOCIADO*
                                                </option>
                                                @foreach ($usuarios as $usuario)
                                                    <option data-saldo="{{ number_format($usuario->saldo, 2, ',', '') }}"
                                                        @if (old('associado_id') == $usuario->id) selected @endif
                                                        @if ($usuario->deleted_at) data-bloqueado="true" @endif
                                                        value="{{ $usuario->id }}">
                                                        {{ $usuario->nome }} ({{ $usuario->cadastro }}) @if ($usuario->deleted_at)
                                                            &#x1F6AB;
                                                        @endif
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col">
                                            <div class="alert alert-secondary" id="statussaldo" role="alert"
                                                style="padding: 12px 15px;">
                                                Saldo: R$ <span style="display: unset" class="saldo">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group bmd-form-group @error('valor') has-danger @enderror">
                                                <label>VALOR</label>
                                                <input type="number" step="any" pattern="[0-9]+([,\.][0-9]+)?"
                                                    class="form-control" name="valor" min="0"
                                                    value="{{ old('valor') }}">
                                                @error('valor')
                                                    <span class="material-icons form-control-feedback">clear</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="alert alert-secondary" id="saldoatualizado" role="alert"
                                                style="padding: 12px 15px;">
                                                Saldo final: R$ <span style="display: unset" class="valorfinal">0</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group bmd-form-group @error('descricao') has-danger @enderror">
                                                <label>DESCRIÇÃO</label>
                                                <input type="text" class="form-control" name="descricao"
                                                    maxlength="255"
                                                    value="{{ old('descricao') ? old('descricao') : (isset($last) ? $last->descricao : null) }}">
                                                @error('descricao')
                                                    <span class="material-icons form-control-feedback">clear</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-right">
                                    @if (isset($last) && $last->debito != 0)
                                        <button type="submit" id="submit"
                                            class="btn btn-danger pull-right">DEBITAR</button>
                                    @elseif(isset($last) && $last->credito != 0)
                                        <button type="submit" id="submit"
                                            class="btn btn-success pull-right">CREDITAR</button>
                                    @else
                                        <button type="submit" id="submit"
                                            class="btn btn-secondary pull-right">DEBITAR/CREDITAR</button>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col">
                        <label class="mb-2"><strong>Ultimas transações</strong></label>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>ASSOCIADO</th>
                                        <th>DESCRIÇÃO</th>
                                        <th>TIPO</th>
                                        <th>VALOR</th>
                                        <th>DATA</th>
                                        <th>GERADOR</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse(\App\Conta::take(10)->orderByDesc('created_at')->get() as $row)
                                        <tr>
                                            <td><a title="Ver Histórico" style="color: black"
                                                    href="{{ route('painel-conta-historico', \App\User::withTrashed()->find($row->users_id)->id) }}">{{ \App\User::withTrashed()->find($row->users_id)->nome }}</a>
                                            </td>
                                            <td>{{ $row->descricao ?? 'Sem descrição' }}</td>
                                            <td class="text-center">
                                                @if ($row->debito != 0)
                                                    <span class="text-danger">D</span>
                                                @endif
                                                @if ($row->credito != 0)
                                                    <span class="text-success">C</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($row->debito != 0)
                                                    <span class="text-danger">-
                                                        R${{ number_format($row->debito, 2, ',', '') }}</span>
                                                @endif
                                                @if ($row->credito != 0)
                                                    <span class="text-success">+ R$
                                                        {{ number_format($row->credito, 2, ',', '') }}</span>
                                                @endif
                                            </td>
                                            <td>{{ \Carbon\Carbon::parse($row->data)->format('d/m/Y') }}</td>
                                            <td>{{ \App\User::withTrashed()->find($row->gerador_id)->nome }}</td>
                                        </tr>
                                    @empty
                                        <td colspan="3">
                                            <p><strong>Nenhuma movimentação.</strong></p>
                                        </td>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(() => {
            $('select.select-user').sm_select();

            var isConfirmed = false;

            $('#form-debitar-creditar').on('submit', function(e) {
                if (isConfirmed) {
                    return true;
                }

                var selecionado = $('#associado_id option:selected');
                if (selecionado.data('bloqueado') == true) {
                    e.preventDefault();

                    Swal.fire({
                        title: 'Atenção!',
                        text: 'O associado está bloqueado. Deseja lançar a movimentação mesmo assim?',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Sim',
                        cancelButtonText: 'Não',
                        reverseButtons: true
                    }).then((result) => {
                        if (result.value) {
                            $('#form-debitar-creditar').append($('<input>').attr({
                                type: 'hidden',
                                name: 'confirmado',
                                value: 'sim'
                            }));
                            isConfirmed = true;
                            $('#submit').click();
                        }
                    });
                }
            });

            $('input#radio-debito').click(function() {
                var button = $('#submit');
                $('div#card').css('box-shadow', 'rgba(202, 63, 63, 0.35) 0px 1px 20px 2px');

                if (!button.hasClass('btn-danger'))
                    button.addClass('btn-danger');

                if (button.hasClass('btn-secondary'))
                    button.removeClass('btn-secondary');

                if (button.hasClass('btn-success'))
                    button.removeClass('btn-success');

                button.text('DEBITAR');
                $('input[name="valor"]').val('');
            });

            $('input#radio-credito').click(function() {
                var button = $('#submit');
                button.val(0);
                $('div#card').css('box-shadow', '0 1px 20px 2px rgba(46, 128, 60, 0.35)');
                if (!button.hasClass('btn-success'))
                    button.addClass('btn-success');

                if (button.hasClass('btn-secondary'))
                    button.removeClass('btn-secondary');

                if (button.hasClass('btn-danger'))
                    button.removeClass('btn-danger');

                button.text('CREDITAR');
                $('input[name="valor"]').val('');
            });

            $('select[name="associado_id"]').change(function() {
                var alert = $('#statussaldo');
                var saldo = parseFloat($(this).children('option:selected').data('saldo')).toFixed(2);
                changeColor(saldo, alert);
                $('span.saldo').text(saldo);
            });

            $('input[name="valor"]').keyup(function() {
                let valor = parseFloat($(this).val());
                if (isNaN(valor))
                    valor = 0;
                const alert = $('#saldoatualizado');
                let saldo = parseFloat($('span.saldo').text());
                let adiciona = parseFloat(saldo) + parseFloat(valor);
                let remove = saldo - valor;
                if ($('input#radio-debito:checked').length > 0) {
                    changeColor(remove, alert);
                    $('span.valorfinal').text(remove.toFixed(2));
                }
                if ($('input#radio-credito:checked').length > 0) {
                    changeColor(adiciona, alert);
                    $('span.valorfinal').text(adiciona.toFixed(2));
                }
            })
        });

        /**
         *  Troca cor do alert
         * @param saldo
         * @param alert
         */
        function changeColor(saldo, alert) {
            if (saldo == 0) {
                if (!alert.hasClass('alert-secondary'))
                    alert.addClass('alert-secondary');

                if (alert.hasClass('alert-danger'))
                    alert.removeClass('alert-danger');

                if (alert.hasClass('alert-success'))
                    alert.removeClass('alert-success');
            } else if (saldo > 0) {
                if (!alert.hasClass('alert-success'))
                    alert.addClass('alert-success');

                if (alert.hasClass('alert-danger'))
                    alert.removeClass('alert-danger');

                if (alert.hasClass('alert-secondary'))
                    alert.removeClass('alert-secondary');
            } else if (saldo < 0) {
                if (!alert.hasClass('alert-danger'))
                    alert.addClass('alert-danger');

                if (alert.hasClass('alert-success'))
                    alert.removeClass('alert-success');

                if (alert.hasClass('alert-secondary'))
                    alert.removeClass('alert-secondary');
            }
        }
    </script>
@endsection
