@extends('layouts.material')
@section('title', 'Contas - Editar')
@section('content')
    <div class="card card-nav-tabs" id="card" style="transition: all linear .3s; @if(isset($conta) && $conta->debito != 0) box-shadow: rgba(202, 63, 63, 0.35) 0px 1px 20px 2px @endif @if(isset($conta) && $conta->credito != 0) box-shadow: 0 1px 20px 2px rgba(46, 128, 60, 0.35) @endif">
        <div class="card-header card-header-primary">
            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('painel-conta-listagem')}}"><i class="material-icons">view_list</i> Contas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('painel-conta-debitar-creditar')}}"><i class="material-icons">unfold_more</i> DEBITAR/CREDITAR</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('painel-conta-anuidade')}}"><i class="material-icons">event_available</i> GERAR ANUIDADES</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="#"><i class="material-icons">edit</i> EDITANDO</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="tab-content">
                <div class="row">
                    <div class="col">
                        <form method="post" action="{{route('painel-conta-update', $conta->id)}}">
                            @csrf
                            <div class="row">
                                <div class="col-12 col-md-4 border-right">
                                    <div class="row">
                                        <div class="col">
                                            <label class="mb-2"><strong>Evento</strong></label>
                                            @error('evento_id')
                                            <div class="alert alert-danger" role="alert">Selecione um tipo de movimentação:</div>
                                            @enderror
                                            <div class="custom-radio @error('tipo') border border-danger @enderror">
                                                <div class="inputGroup">
                                                    <input id="radio-debito" value="debito" name="tipo" type="radio" @if(isset($conta) && $conta->debito != 0) checked @endif/>
                                                    <label for="radio-debito"><i  style="vertical-align: middle" class="material-icons">remove</i> DEBITAR</label>
                                                </div>
                                                <div class="inputGroup">
                                                    <input id="radio-credito" value="credito" name="tipo" type="radio" @if(isset($conta) && $conta->credito != 0) checked @endif/>
                                                    <label for="radio-credito"><i style="vertical-align: middle" class="material-icons">add</i> CREDITAR</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-8">
                                    <div class="row">
                                        <div class="col">
                                            <div class="row">
                                                <div class="col-12 col-md-3">
                                                    <div class="form-group bmd-form-group @error('data') has-danger @enderror">
                                                        <label>DATA*</label>
                                                        <input type="date" class="form-control" required name="data" value="{{old('data') ? old('data') : (isset($conta) ? \Carbon\Carbon::parse($conta->data)->format('Y-m-d') : null)}}">
                                                        @error('data')<span class="material-icons form-control-feedback">clear</span>@enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-3 mt-3">
                                            <div class="form-group bmd-form-group @error('valor') has-danger @enderror">
                                                <label>VALOR</label>
                                                <input type="number" step="any" pattern="[0-9]+([,\.][0-9]+)?" class="form-control" name="valor" min="0" value="{{old('valor') ? old('valor') : ($conta->debito ? $conta->debito : $conta->credito)}}">
                                                @error('valor')<span class="material-icons form-control-feedback">clear</span>@enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group bmd-form-group @error('descricao') has-danger @enderror">
                                                <label>DESCRIÇÃO</label>
                                                <input type="text" class="form-control" name="descricao" maxlength="255" value="{{old('descricao') ? old('descricao') : (isset($conta) ? $conta->descricao : null)}}">
                                                @error('descricao')<span class="material-icons form-control-feedback">clear</span>@enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <a class="btn btn-secondary" href="{{route('painel-conta-historico', $conta->users_id)}}">VOLTAR</a>
                                </div>
                                <div class="col text-right">
                                    <button type="submit" id="submit" class="btn btn-primary pull-right">SALVAR</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col">
                        <label class="mb-2"><strong>Outras transações</strong></label>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ASSOCIADO</th>
                                    <th>DESCRIÇÃO</th>
                                    <th>TIPO</th>
                                    <th>VALOR</th>
                                    <th>SALDO FINAL</th>
                                    <th>DATA</th>
                                    <th>GERADOR</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse(\App\Conta::all()->where('users_id', '=', $conta->users_id)->where('id', '!=', $conta->id)->sortByDesc('created_at') as $row)
                                    <tr>
                                        <td><a title="Ver Histórico" style="color: black" href="{{route('painel-conta-historico', \App\User::withTrashed()->find($row->users_id)->id)}}">{{\App\User::withTrashed()->find($row->users_id)->nome}}</a> </td>
                                        <td>{{$row->descricao ?? "Sem descrição"}}</td>
                                        <td class="text-center">
                                            @if($row->debito != 0) <span class="text-danger">D</span>@endif
                                            @if($row->credito != 0) <span class="text-success">C</span> @endif
                                        </td>
                                        <td>
                                            @if($row->debito != 0) <span class="text-danger">- R${{number_format($row->debito, 2, ',', '')}}</span> @endif
                                            @if($row->credito != 0) <span class="text-success">+ R$ {{number_format($row->credito, 2, ',', '')}}</span> @endif
                                        </td>
                                        <td>
                                            @if($row->saldo < 0)
                                                <span class="text-danger"> R$ {{number_format($row->saldo, 2, ',', '')}} <i style="vertical-align: middle" class="material-icons">keyboard_arrow_down</i> </span>
                                            @elseif($row->saldo > 0)
                                                <span class="text-success"> R$ {{number_format($row->saldo, 2, ',', '')}} <i style="vertical-align: middle" class="material-icons">keyboard_arrow_up</i> </span>
                                            @endif
                                        </td>
                                        <td>{{\Carbon\Carbon::parse($row->data)->format('d/m/Y')}}</td>
                                        <td>{{ \App\User::withTrashed()->find($row->gerador_id)->nome }}</td>
                                    </tr>
                                @empty
                                    <td colspan="3"><p><strong>Nenhuma movimentação.</strong></p></td>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
