@extends('layouts.material')
@section('title', 'Contas')
@section('content')
    <div class="card card-nav-tabs">
        <div class="card-header card-header-primary">
            <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
            <div class="nav-tabs-navigation">
                <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="{{route('painel-conta-listagem')}}"><i class="material-icons">view_list</i> Contas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('painel-conta-debitar-creditar')}}"><i class="material-icons">unfold_more</i> DEBITAR/CREDITAR</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('painel-conta-anuidade')}}"><i class="material-icons">event_available</i> GERAR ANUIDADES</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="card-body ">
            <div class="tab-content">
                <div class="row">
                    <div class="col">
                        <a href="{{route('painel-conta-exportacao-completa')}}" class="btn btn-primary">Exportação completa</a>
                        <div class="table-responsive">
                            <table class="table datatable">
                                <thead>
                                <tr>
                                    <th>STATUS</th>
                                    <th>NOME</th>
                                    <th>DATA LANÇAMENTO</th>
                                    <th>SALDO(R$)</th>
                                    <th>OPÇÕES</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($contas as $conta)
                                    <tr @if($conta->saldo < 0) class="text-danger" @elseif($conta->saldo > 0) class="text-success" @endif >
                                        <td>@if($conta->status) ATIVO @else INATIVO @endif</td>
                                        <td>{{$conta->nome}}</td>
                                        <td>{{\Carbon\Carbon::parse($conta->ultima_data)->format('d/m/Y')}}</td>
                                        <td>
                                            {{number_format($conta->saldo, 2, ',', '')}}
{{--                                            @if($conta->saldo < 0)  <i style="vertical-align: middle" class="material-icons">keyboard_arrow_down</i>--}}
{{--                                            @elseif($conta->saldo > 0)  <i style="vertical-align: middle" class="material-icons">keyboard_arrow_up</i>@endif--}}
                                        </td>
                                        <td class="td-actions text-right">
{{--                                            <a href="{{ route('painel-evento-editar', $evento ->id) }}" class="btn btn-success">--}}
                                            <a href="{{route('painel-conta-historico', $conta->id)}}" class="bnt btn-sm btn-primary">Histórico</a>
{{--                                            </a>--}}
                                        </td>
                                    </tr>
                                @empty
                                    <td colspan="3"><p><strong>Nenhuma conta localizada.</strong></p></td>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(()=>{
            configDatatables["buttons"] =  [
                {
                    extend: 'pdf',
                    text: 'Exportar tabela',
                    exportOptions: {
                        columns: [0,1,2,3],
                        format: {
                                body: function (data, row, column, node) {
                                if(column === 3)
                                    data = data.replace('<i style="vertical-align: middle" class="material-icons">keyboard_arrow_down</i>', '');

                                return data;
                            },
                        }
                    },

                }
            ];
            $('.datatable').DataTable(configDatatables);
        });
    </script>
@endsection


