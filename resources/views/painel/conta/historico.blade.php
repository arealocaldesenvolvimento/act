@extends('layouts.material')
@section('title', 'Histórico')
@section('content')
    @if(\App\Helpers\AppHelper::isAdmin())
        <a href="{{ route('painel-conta-listagem') }}" class="btn btn-primary mb-3 mt-0"><i class="material-icons">arrow_back</i> Voltar a listagem</a>
    @endif
    <div class="card mb-3">
        <div class="card-header card-header-icon card-header-primary">
            <div class="card-icon">
                <i class="material-icons">list</i>
            </div>
        </div>
        <div class="card-body">
            <h3>Histórico de: <strong>{{$usuario->nome ?? ''}}</strong></h3>
            <label><strong>Filtrar por ano:</strong></label>
            <form>
                <div class="row my-3">
                    <div class="col-2">
                        <div class="form-group bmd-form-group @error('ano') has-danger @enderror">
                            <input type="text" class="form-control" required name="ano" placeholder="Digite um ano..." value="{{$ano ?? null}}">
                            @error('ano')<span class="material-icons form-control-feedback">clear</span>@enderror
                        </div>
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-success pull-right">Filtrar</button>
                            <a href="{{route('painel-conta-historico', $usuario->id)}}?ano={{date('Y')}}" class="btn btn-secondary">ANO ATUAL ({{date('Y')}})</a>
                            @if($ano)
                                <a href="{{route('painel-conta-historico', $usuario->id)}}" class="btn btn-danger">Limpar filtro</a>
                            @endif
                        </div>
                    </div>
            </form>
            <br/>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>DESCRIÇÃO</th>
                            <th>VALOR</th>
                            <th>DATA</th>
                            <th>SALDO</th>
                            @if(\App\Helpers\AppHelper::isAdmin())
                                <th class="text-right">OPÇÕES</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                    @php $saldoSomadorRow = 0; @endphp
                    @forelse($historico as $row)
                        @php $saldoSomadorRow = $saldoSomadorRow + ($row->credito - $row->debito) @endphp
                        <tr>
                            <td>{{$row->descricao ?? "Sem Descrição"}}</td>
                            <td>
                                @if($row->debito != 0) <span class="text-danger">R$ -{{number_format($row->debito, 2, ',', '')}}</span> @endif
                                @if($row->credito != 0) <span class="text-success">R$ +{{number_format($row->credito, 2, ',', '')}}</span> @endif
                            </td>
                            <td>{{\Carbon\Carbon::parse($row->dataAtual)->format('d/m/Y')}}</td>
                            <td>
                                @if($saldoSomadorRow > 0)
                                    <span class="text-success">R$ {{number_format($saldoSomadorRow, 2, ',', '')}}</span>
                                @elseif($saldoSomadorRow < 0)
                                    <span class="text-danger">R$ {{number_format($saldoSomadorRow, 2, ',', '')}}</span>
                                @else
                                    <span class="text-muted">R$ {{number_format($saldoSomadorRow, 2, ',', '')}}</span>
                                @endif
                            </td>
                            @if(\App\Helpers\AppHelper::isAdmin())
                                <td class="td-actions text-right">
                                    <button type="button" class="btn btn-light" data-toggle="popover" title="Lançamento efetuado por:" data-content="{{ \App\User::find($row->gerador_id)->nome }}"><span class="material-icons" title="">history_edu</span></button>
                                    <form class="delete-form d-none" action="{{route('painel-conta-destroy')}}" method="post">
                                        @csrf
                                        <input type="hidden" value="{{$row->idAtual}}" name="id">
                                    </form>
                                    <a href="{{ route('painel-conta-editar', $row->idAtual) }}" class="btn btn-success">
                                        <i class="material-icons">edit</i>
                                    </a>
                                    <button type="button" class="btn btn-danger delete">
                                        <i class="material-icons">delete</i>
                                    </button>
                                </td>
                            @endif
                        </tr>
                    @empty
                        <td colspan="3"><p><strong>Nenhuma movimentação.</strong></p></td>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="card mt-3 @if($saldo < 0) bg-danger text-white @elseif($saldo > 0) bg-success text-white @endif">
        <div class="card-body">
            <div class="row" style="line-height: 1">
                <div style="font-size: 25px; line-height: 1" class="col-4 col-md-6 ">
                    Saldo atual:
                </div>
                <div class="col-8 col-md-6 text-right">
                    <strong style="font-size: 25px;line-height: 1">
                        @if($saldo < 0)
                            R$ {{number_format($saldo, 2, ',', '')}} <i style="vertical-align: middle" class="material-icons">keyboard_arrow_down</i>
                        @elseif($saldo > 0)
                            R$ {{number_format($saldo, 2, ',', '')}} <i style="vertical-align: middle" class="material-icons">keyboard_arrow_up</i>
                        @else
                            R$ {{number_format($saldo, 2, ',', '')}}
                        @endif
                    </strong>
                </div>
            </div>
        </div>
    </div>
    @if(\App\Helpers\AppHelper::isAdmin())
        <a href="{{ route('painel-conta-listagem') }}" class="btn btn-primary"><i class="material-icons">arrow_back</i> Voltar a listagem</a>
    @endif
    <div class="row">
        <div class="col text-center">
            <p style="line-height: 1.2; margin:0"><strong>INFORMAÇÕES PARA DEPÓSITO</strong></p>
            <p style="line-height: 1.2; margin:0">Agência: <strong>{{$banco->agencia}}</strong></p>
            <p style="line-height: 1.2; margin:0">Conta Corrente: <strong>{{$banco->conta_corrente}}</strong></p>
            <p style="line-height: 1.2; margin:0">Banco: <strong>{{$banco->banco}}</strong></p>
            <p style="line-height: 1.2; margin:0">CNPJ do clube: <strong>{{$banco->cnpj}}</strong></p>
            <p style="line-height: 1.2; margin:0">Lembre-se de enviar o comprovante de depósito para o e-mail <strong>contferreira@uol.com.br</strong></p>
            <br>
            <p style="line-height: 1.2; margin:0"><strong>PIX</strong></p>
            <p style="line-height: 1.2; margin:0">Chave 1: <strong>{{$banco->pix1 ?? ' -'}}</strong></p>
            <p style="line-height: 1.2; margin:0">Chave 2: <strong>{{$banco->pix2 ?? ' -'}}</strong></p>
            <p style="line-height: 1.2; margin:0">Chave 3: <strong>{{$banco->pix3 ?? ' -'}}</strong></p>

        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(()=>{

            $(function () {
                 $('[data-toggle="popover"]').popover()
            })
            $('button.delete').click(function () {
                var button = $(this);
                Swal.fire({
                    title: 'Você tem certeza que deseja remover esse lançamento?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#c0cdd6',
                    confirmButtonText: 'Sim, remover!',
                    cancelButtonText: 'Cancelar!'
                }).then((result) => {
                    if (result.value) {
                        button.siblings('form.delete-form').submit();
                    }
                })
            });
        });
    </script>
@endsection
