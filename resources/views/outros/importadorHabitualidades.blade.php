<html>
<head><title>a</title></head>
<body>
    <form method="post" enctype="multipart/form-data">
        @csrf
        <input type="file" name="arquivo">
        <button type="submit">Enviar</button>
    </form>
    <hr>
    @if(session()->get('results'))
        <p>Sucessos: {{session()->get('results')->sucessos}}</p>
        <p>Falhas: {{session()->get('results')->falhas}}</p>
        <p>Linhas: {{session()->get('results')->linhas}}</p>
    @endif
</body>
</html>
