<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>ACT - IMPORTADOR</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!--     Fonts and icons     -->
    <!-- Material Kit CSS -->
    <style>
        @import url('https://fonts.googleapis.com/css?family=Abel');

        body {
            background: #4f6072;
            font-family: Abel, Arial, Verdana, sans-serif;
        }

        .center {
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
        }

        .container {
            width: 1000px;
            max-width: 100%;
            margin: 0 auto;
            text-align: center;
        }

        .card {
            width: 950px;
            height: 170px;
            background-color: #fff;
            background: linear-gradient(#f8f8f8, #fff);
            box-shadow: 0 8px 16px -8px rgba(0,0,0,0.4);
            border-radius: 6px;
            overflow: hidden;
            position: relative;
            margin: 1.5rem;
        }

        .card h1 {
            text-align: center;
            white-space: nowrap;
            overflow: hidden;
        }

        .card .additional {
            position: absolute;
            width: 150px;
            height: 100%;
            background: linear-gradient(#dE685E, #EE786E);
            transition: width 0.4s;
            overflow: hidden;
            z-index: 2;
        }

        .card.green .additional {
            background: linear-gradient(#92bCa6, #A2CCB6);
        }


        .card:hover .additional {
            width: 100%;
            border-radius: 0 5px 5px 0;
        }

        .card .additional .user-card {
            width: 150px;
            height: 100%;
            position: relative;
            float: left;
        }

        .card .additional .user-card::after {
            content: "";
            display: block;
            position: absolute;
            top: 10%;
            right: -2px;
            height: 80%;
            border-left: 2px solid rgba(0,0,0,0.025);
        }

        .card .additional .user-card .level,
        .card .additional .user-card .points {
            top: 15%;
            color: #fff;
            text-transform: uppercase;
            font-size: 0.75em;
            font-weight: bold;
            background: rgba(0,0,0,0.15);
            padding: 0.125rem 0.75rem;
            border-radius: 100px;
            white-space: nowrap;
        }

        .card .additional .user-card .points {
            top: 85%;
        }

        .card .additional .user-card svg {
            top: 50%;
        }

        .card .additional .more-info {
            width: 800px;
            float: left;
            position: absolute;
            left: 150px;
            height: 100%;
            overflow-y: auto;
        }

        .card .additional .more-info h1 {
            color: #fff;
            margin-bottom: 0;
        }
        .card .additional .more-info p {
            margin: 0;
            line-height: 1.2;
        }

        .card.green .additional .more-info h1 {
            color: #224C36;
        }

        .card .additional .coords {
            margin: 0 1rem;
            color: #fff;
            font-size: 1rem;
        }

        .card.green .additional .coords {
            color: #325C46;
        }

        .card .additional .coords span + span {
            float: right;
        }

        .card .additional .stats {
            font-size: 2rem;
            display: flex;
            position: absolute;
            bottom: 1rem;
            left: 1rem;
            right: 1rem;
            top: auto;
            color: #fff;
        }

        .card.green .additional .stats {
            color: #325C46;
        }

        .card .additional .stats > div {
            flex: 1;
            text-align: center;
        }

        .card .additional .stats i {
            display: block;
        }

        .card .additional .stats div.title {
            font-size: 0.75rem;
            font-weight: bold;
            text-transform: uppercase;
        }

        .card .additional .stats div.value {
            font-size: 1.5rem;
            font-weight: bold;
            line-height: 1.5rem;
        }

        .card .additional .stats div.value.infinity {
            font-size: 2.5rem;
        }

        .card .general {
            width: 800px;
            height: 100%;
            position: absolute;
            top: 0;
            right: 0;
            z-index: 1;
            box-sizing: border-box;
            padding: 1rem;
            padding-top: 0;
            text-align: center;
        }

        .card .general .more {
            position: absolute;
            bottom: 1rem;
            right: 1rem;
            font-size: 0.9em;
        }




    </style>
</head>
    <body>
        <div class="container">
            @isset($resultados)
                @foreach($resultados as $status)
                <div class="card @if($status->status != false) green @endif">
                    <div class="additional">
                        <div class="user-card">
                            <div class="level center">
                                @if($status->status != false)
                                    Usuário Localizado
                                @else
                                    Usuário Não Localizado
                                @endif
                            </div>
                            @if($status->status != false)
                                <img width="80" height="80" class="center" src="{{asset('images/imported.svg')}}"/>
                            @else
                                <img width="80" height="80" class="center" src="{{asset('images/notimported.svg')}}"/>
                            @endif
                        </div>
                        <div class="more-info">
                            <h1>{{$status->nome}}</h1>
                            @isset($status->campos)<p> @foreach($status->campos as $key => $campos){{$campos}} <br/>@endforeach </p> @endisset
                        </div>
                    </div>
                    <div class="general">
                        <h1>{{$status->nome}}</h1>
                        @isset($status->sucessos, $status->falhas)<p><strong>{{$status->sucessos}} linhas importadas, {{$status->falhas}} linhas com falha.</strong></p>@endisset
                        @isset($status->saldo_final)<p>Saldo final: <strong>{{$status->saldo_final}} </strong>@endisset
                    </div>
                </div>

                @endforeach
            @endisset
        </div>
{{--        @isset($resultados)--}}
{{--            @foreach($resultados as $status)--}}
{{--                <div style="@isset($status->status)@if($status->status == false) background: red @endif @endisset">--}}
{{--                    <p>--}}
{{--                        @isset($status->nome)<strong>{{$status->nome}}</strong> <br/>@endisset--}}
{{--                        @isset($status->id)Associado id: <strong>{{$status->id}} </strong>@endisset--}}
{{--                        @isset($status->saldo_final)| Saldo final: <strong>{{$status->saldo_final}} </strong>@endisset--}}
{{--                        @isset($status->status)| Usuário: <strong>{{$status->status == false ? "Associado não localizado!" : $status->status}}</strong>@endisset--}}
{{--                        @isset($status->sucessos, $status->falhas)| Importação: <strong>{{$status->sucessos}} linhas importadas, {{$status->falhas}} linhas com falta de informação.</strong>@endisset--}}
{{--                        @isset($status->campos) @if($status->campos != "")<span style="background-color: #ffd96f"><strong>{!!$status->campos!!}</strong></span> @endif @endisset--}}
{{--                    </p>--}}
{{--                </div>--}}
{{--                <hr/>--}}
{{--            @endforeach--}}
{{--        @endisset--}}
    </body>
</html>
