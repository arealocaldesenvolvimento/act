<html>
<head><title>Importador Jantas</title></head>
<body>
    <form method="post" enctype="multipart/form-data">
        @csrf
        <input type="file" name="arquivo">
        <button type="submit">Enviar</button>
    </form>
    <hr>
    @if(session()->get('results'))
        <p>Sucessos: {{session()->get('results')->sucessos}}</p>
        <p>Falhas: {{session()->get('results')->falhas}}</p>
        <p>User 404: {{session()->get('results')->associados}}</p>
        <p>Nomes users 404: {!! session()->get('results')->nomes_falhas !!}</p>
    @endif
</body>
</html>
